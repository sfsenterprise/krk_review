<?php
/**
 * Created by PhpStorm.
 * User: dustin
 * Date: 1/2/17
 * Time: 12:33 PM
 */
?>

<div id="event-calendar"></div>
<script type="application/javascript">
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var events_array = [];
    <?php
    foreach ($calendar_events as $calendar_event) {
        if($calendar_event['enddate'] != null) {
                                        echo 'events_array.push({
                                            title: "' . $calendar_event['title'] . '",
                                            start: "' . $calendar_event['startdate'] . '",
                                            end: "' . $calendar_event['enddate'] . '",
                                            url: "'. $calendar_event['url'] .'",
                                            allDay: true
                                       });
                                        ';
                                    }
        else {
            echo 'events_array.push({
                    title: "' . $calendar_event['title'] . '",
                    start: "' . $calendar_event['startdate'] . '",
                    url: "'. $calendar_event['url'] .'",
                    allDay: true
                  });';
        }

    }
    ?>
    jQuery('#event-calendar').fullCalendar({
        header: {
            left: 'title',
            right: 'prev, next'
        },
        events: events_array,
        buttonIcons: {
            prev: '',
            next: ''
        },
        buttonText: {
            prev: '<',
            next: '>'
        }
    });
</script>