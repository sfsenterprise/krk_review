<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 */

get_header();

$customizer = new KRK_Home_Customizer();
$sections = $customizer->get_setting('krk_home_sections');
$sections = explode(",", $sections);

$enabled_main_video_section = strlen($customizer->get_setting('krk_home_enabled_main_video_section')) > 0;
$main_img_cust = $customizer->get_setting('krk_home_main_image_custom');
$main_img = $customizer->get_setting('krk_home_main_image');

?>

<?php if(strlen($main_img) > 0 || strlen($main_img_cust) > 0) : ?>
<style>
    .section-first {
        background: url(<?php if(strlen($main_img_cust) > 0): echo $main_img_cust; else: echo get_template_directory_uri() . '/images/home/' . $main_img; endif;?>);
        background-size: cover;
    }
    <?php if($enabled_main_video_section): ?>
    .section-first {
        background: initial;
    }
    .section-first .container-fluid {
        padding: 0;
        margin: 0;
        max-width: initial;
    }
    .section-first .slogan {
        text-shadow: initial;
    }
    .section-first {
        padding: initial;
    }
    <?php endif;?>
</style>
<?php endif; ?>

<script>
    $(document).ready(function(){
        //render sections in order
        var currentSection = 0;
        var maxSection = <?php echo count($sections); ?>;
        var sectionBeforeMap = $('#section-before-map');
        var sectionAfterMap = $('#section-after-map');
        var mapIndex = $('.section-map').data('index');

        //default, render all templates before the map section
        if(maxSection == 1) {
            $('script[type="text/template"]').each(function(i, temp){
                sectionBeforeMap.append($(temp).html());
            });
        } else {
            while(currentSection < maxSection) {

                if(currentSection < mapIndex) {
                    sectionBeforeMap.append($('script[type="text/template"][data-index="'+ currentSection +'"]').html());
                }
                else if(currentSection > mapIndex) {
                    sectionAfterMap.append($('script[type="text/template"][data-index="'+ currentSection +'"]').html());
                }

                currentSection++;
            }
        }
        
    });
</script>

<div class="main-holder">
    <div class="first-mobile visible-xs">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="text">
                        <span class="title">Welcome!</span>
                        <p>We believe that <b>happy, loved and connected children</b> are destined for success in every facet of their lives.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-t visible-xs">
        <h2>Contact Us</h2>
        <?php echo do_shortcode("[phone_number]"); ?>
        <br> </br>
        <?php echo do_shortcode("[location_full_address]"); ?>
        <ul class="social-networks">
            <?php if(!empty(do_shortcode("[url_instagram]"))){ ?>
                <li><a href="<?php echo do_shortcode("[url_instagram]"); ?>" target="_blank"><span class="ico icon-instagram"></span></a></li>
            <?php } ?>
            <?php if(!empty(do_shortcode("[url_google_plus]"))){ ?>
                <li><a href="<?php echo do_shortcode("[url_google_plus]"); ?>" target="_blank"><span class="ico icon-google"></span></a></li>
            <?php } ?>
            <?php if(!empty(do_shortcode("[url_pinterest]"))){ ?>
                <li><a href="<?php echo do_shortcode("[url_pinterest]"); ?>" target="_blank"><span class="ico icon-pinterest"></span></a></li>
            <?php } ?>
            <?php if(!empty(do_shortcode("[url_twitter]"))){ ?>
                <li><a href="<?php echo do_shortcode("[url_twitter]"); ?>" target="_blank"><span class="ico icon-twitter"></span></a></li>
            <?php } ?>
            <?php if(!empty(do_shortcode("[url_facebook]"))){ ?>
                <li><a href="<?php echo do_shortcode("[url_facebook]"); ?>" target="_blank"><span class="ico icon-facebook"></span></a></li>
            <?php } ?>
            <?php if(!empty(do_shortcode("[url_youtube]"))){ ?>
                <li><a href="<?php echo do_shortcode("[url_youtube]"); ?>" target="_blank"><span class="ico icon-youtube"></span></a></li>
            <?php } ?>
            <?php if(!empty(do_shortcode("[url_yelp]"))){ ?>
                <li><a href="<?php echo do_shortcode("[url_yelp]"); ?>" target="_blank"><span class="ico icon-yelp"></span></a></li>
            <?php } ?>
            <?php if(!empty(do_shortcode("[url_angies_list]"))){ ?>
                <li><a href="<?php echo do_shortcode("[url_angies_list]"); ?>" target="_blank"><span class="ico icon-angies-list"></span></a></li>
            <?php } ?>
        </ul>
    </div>

    <div class="section-first <?php if(!$enabled_main_video_section) echo 'hidden-xs'; ?>">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $main_button_disabled = (strlen($customizer->get_setting('krk_home_disable_main_button')) > 0);
                    $main_button_text = $customizer->get_setting('krk_home_main_button_text');
                    $main_button_url = $customizer->get_setting('krk_home_main_button_url'); ?>

                    <?php if($enabled_main_video_section) :
                        $section_content = $customizer->get_setting('krk_home_main_video_section_content'); ?>

                        <div class="video-container">
                            <div class="video-text-wrap">
                                <div class="info">
                                    <?php echo $section_content ?>
                                </div>
                                <?php if(!$main_button_disabled) : ?>
                                    <a href="<?php echo $main_button_url ?>" class="btn btn-primary"><?php echo $main_button_text; ?><span class="icon icon-arrow-right"></span></a>
                                <?php endif; ?>
                            </div>
                            <div id="main-carousel" class="video-img-wrap carousel slide" data-ride="carousel">
                                <?php wp_reset_query();
                                $type = 'krk_homepage_slide';
                                $args=array(
                                    'post_type' => $type,
                                    'post_status' => 'publish',
                                    'order' => 'ASC',
                                    'orderby' => 'menu_order',
                                    'posts_per_page' => -1);

                                $query = null;
                                $query = new WP_Query($args);
                                $first_post = true;
                                if( $query->have_posts() ) :
                                    $post_count = $query->post_count;
                                    if($post_count > 1) :?>
                                    <ol class="carousel-indicators">
                                        <?php for($i = 0; $i < $post_count; $i++): ?>
                                        <li data-target="#main-carousel" data-slide-to="<?php echo $i; ?>"
                                            class="<?php if($i == 0) echo 'active'; ?>">
                                        </li>
                                        <?php endfor; ?>
                                    </ol>
                                    <?php endif; ?>
                                    <div class="carousel-inner" role="listbox">

                                    <?php while( $query->have_posts() ) :
                                        $query->the_post();
                                        $video_source = get_post_meta(get_the_ID(), 'krk_homepage_slide_video_source', true);
                                        $thumbnail = get_the_post_thumbnail_url(get_the_ID()); ?>

                                        <div class="item <?php if($first_post): echo 'active'; $first_post = false; endif;?>">
                                            <?php if (!empty($thumbnail)): ?>
                                                <img src="<?php echo $thumbnail; ?>" class="homepage-video-thumb" alt="image description">
                                            <?php else: ?>
                                                <?php if(strpos($video_source, 'youtube.com') != false) : ?>
                                                    <iframe width="680" height="429" src="<?php echo $video_source; ?>" frameborder="0" allowfullscreen></iframe>
                                                <?php else : ?>
                                                    <video width="680" height="429">
                                                      <source src="<?php echo $video_source ?>">
                                                    </video>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <?php if(strlen($video_source) > 0 ): ?>
                                                <a href="#"
                                                   data-featherlight='
                                                   <?php if(strpos($video_source, 'www.youtube.com') != false) : ?>
                                                        <iframe width="800" height="450" src="<?php echo $video_source; ?>" frameborder="0" allowfullscreen></iframe>
                                                    <?php else : ?>
                                                        <video width="100%" height="auto" controls autoplay>
                                                          <source src="<?php echo $video_source ?>">
                                                        </video>
                                                    <?php endif; ?>
                                                    '
                                                   class="btn-play"><span class="icon icon-play"></span></a>
                                            <?php endif; ?>
                                        </div>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php else : ?>
                        <?php
                        $title = $customizer->get_setting('krk_home_main_title');
                        $vt_button_disabled = (strlen($customizer->get_setting('krk_home_disable_virtual_tour_button')) > 0);

                        ?>
                        <strong class="slogan"><?php echo $title; ?></strong>
                        <div class="btn-wrap">
                            <?php if(!$main_button_disabled) : ?>
                                <a href="<?php echo $main_button_url ?>" class="btn btn-primary"><?php echo $main_button_text; ?><span class="icon icon-arrow-right"></span></a>
                            <?php endif; ?>
                        </div>
                        <?php if( !empty(Redwood_KRK_External_Link_Settings::get_virtual_tour_url()) && !$vt_button_disabled ):?>
                            <div class="btn-wrap">
                                <a href="<?php multisite_path('/our-location/#virtual-tour'); ?>" class="btn btn-info btn-tour">Take a Virtual Tour  <span class="icon icon-arrow-right"></span></a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div id="section-before-map">
    </div>

    <?php //We need to render the map inline to avoid google maps render error ?>
    <?php
    $disabled = $customizer->get_setting('krk_home_disable_Address');
    if(strlen($disabled) == 0) : ?>
        <div class="section-map battlement" data-index="<?php echo array_search("Address", $sections); ?>">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10">
                    <div class="contact-wrap">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="map">
                                    <?php echo do_shortcode("[location-map]"); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="contact">
                                    <div class="box">
                                        <h2>ADDRESS</h2>
                                        <address>
                                            <?php echo do_shortcode("[location_full_address]"); ?>
                                        </address>
                                    </div>
                                    <div class="box">
                                        <h2>OUR HOURS</h2>
                                        <p><?php echo do_shortcode("[redwood_hours id='hours_operation' should_display_title=0 combine_matching_days=1]"); ?></p>
                                    </div>
                                    <div class="box">
                                        <h2>CONTACT</h2>
                                        <address>
                                            <?php if (!empty(do_shortcode("[phone_number]"))): ?>
                                                <?php echo '<b>P </b>' . do_shortcode("[phone_number]"); ?>
                                            <?php endif ?>

                                            <br/>

                                            <?php if (!empty(do_shortcode("[fax_number]"))): ?>
                                                <?php echo '<b>F </b>' . do_shortcode("[fax_number]"); ?>
                                            <?php endif ?>
                                        </address>
                                    </div>
                                    <?php  $location = get_option('redwood_location_settings', array()); ?>
                                    <?php  $street = $location['street_address'];
                                    $city = $location['city'];
                                    $state = $location['state'];
                                    $zip = $location['zip'];  ?>
                                    <div class="btn-wrap hidden-xs">
                                        <a href="http://www.maps.google.com/maps?saddr=<?php echo "$street, $city, $state, $zip"; ?>" class="btn btn-primary">Directions</a>
                                    </div>
                                    <a href="<?php echo network_site_url(); ?>locations" class="link"><i>Find a different location?</i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php else : ?>
        <div class="section-map" style="padding: 0;" data-index="<?php echo array_search("Address", $sections); ?>"></div>
    <?php endif; ?>
    <div id="section-after-map">
    </div>
</div>

<?php get_footer(); ?>
<?php
$disabled = $customizer->get_setting('krk_home_disable_Video');
if(strlen($disabled) == 0) : ?>
    <script id="video-section" type="text/template" data-index="<?php echo array_search("Video", $sections); ?>">
    <section class="section-owners">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="text-block">
                        <h2>From Our Owners to You</h2>
                        <div class="img-hold">
                            <picture>
                                <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/pc-1.png, <?php bloginfo('stylesheet_directory'); ?>/images/pc-1-2x.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/pc-1-3x.png 3x">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/pc-1.png" height="412" width="616" alt="image description">
                            </picture>
                            <a href="#"
                               data-featherlight=
                               '
                            <?php
                               $custom_video = $customizer->get_setting( 'krk_home_video_upload');
                               if(strlen($custom_video) > 0) : ?>
                                    <video width="100%" height="auto" controls autoplay>
                                        <source src="<?php echo wp_get_attachment_url($custom_video); ?>" type="<?php echo get_post_mime_type($custom_video); ?>">
                                    </video>
                            <?php
                               else : ?>
                                <iframe width="800" height="450" src="https://www.youtube.com/embed/W8StuwRUo-A?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>
                            <?php endif; ?>              
                            '
                               class="btn btn-info play">Play Video <span class="icon icon-arrow-right"></span></a>
                        </div>

                        <div class="text-wrap">
                            <?php if( empty(Redwood_KRK_Settings::get_from_owners_to_you_text()) ):?>
                            <p>From our family to your family, we extend a sincere thank you for considering Kids <span class="krk-ticks">R</span> Kids, the premier preschool for your child. Each and every Kids <span class="krk-ticks">R</span> Kids Learning Academy is independently owned and operated. <?php echo do_shortcode("[operator_name]"); ?> is on site and takes a very active role in the management of our school. Our founders, Pat and Janice Vinson, have the philosophy that children should be hugged first and then taught. It was this very philosophy of Kids <span class="krk-ticks">R</span> Kids that inspired <?php echo do_shortcode("[operator_name]"); ?> to open our very own Kids <span class="krk-ticks">R</span> Kids in <?php echo do_shortcode("[location_city]"); ?>, <?php echo do_shortcode("[location_state]"); ?>. </p>
                        </div>
                        <?php else:
                            echo str_replace("\n", "<br>", Redwood_KRK_Settings::get_from_owners_to_you_text()) ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>
<?php endif; ?>

<?php
$disabled = $customizer->get_setting('krk_home_disable_Philosophy');
if(strlen($disabled) == 0) : ?>
    <script id="curriculum-section" type="text/template" data-index="<?php echo array_search("Philosophy", $sections); ?>">
    <section class="section-curriculum">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="text-block">
                        <h2>Our Philosophy & Curriculum</h2>
                        <span class="desk">Our exclusive curriculum propels Kids <span class="krk-ticks">R</span> Kids Learning Academies beyond daycare centers and childcare providers. Take a peek into our programs.</span>
                        <ul class="kids-curriculum">
                            <li>
                                <a href="<?php multisite_path('/our-philosophy'); ?>">
                                    <picture>
                                        <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-04.png, <?php bloginfo('stylesheet_directory'); ?>/images/img-04-2x.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-04-3x.png 3x">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-04.png" height="179" width="278" alt="image description">
                                    </picture>
                                </a>
                            </li>
                            <li>
                                <a href="<?php multisite_path('/our-curriculum'); ?>">
                                    <picture>
                                        <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-05.png, <?php bloginfo('stylesheet_directory'); ?>/images/img-05-2x.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-05-3x.png 3x">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-05.png" height="179" width="278" alt="image description">
                                    </picture>
                                </a>
                            </li>
                            <li>
                                <a href="<?php multisite_path('/our-curriculum/#steam'); ?>">
                                    <picture>
                                        <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-07.png, <?php bloginfo('stylesheet_directory'); ?>/images/img-07-2x.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-07-3x.png 3x">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-07.png" height="179" width="278" alt="image description">
                                    </picture>
                                </a>
                            </li>
                            <li>
                                <a href="<?php multisite_path('/our-curriculum/#brainwaves'); ?>">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/brain-waves.jpg" height="179" width="278" alt="image description">
                                </a>
                            </li>
                        </ul>
                        <a href="<?php multisite_path('/our-curriculum'); ?>" class="btn btn-primary hidden-xs">Learn More <span class="icon icon-arrow-right"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>
<?php endif; ?>

<?php
$disabled = $customizer->get_setting('krk_home_disable_Accreditations');
if(strlen($disabled) == 0) : ?>
    <script id="accreditations-section" type="text/template" data-index="<?php echo array_search("Accreditations", $sections); ?>">
    <?php if(Redwood_KRK_Accreditation_Settings::get_show_accreditation_any_logo() ): ?>
        <section class="section-second">
            <div class="container-fluid">
                <div class="row text-center"> <h2>Our Accreditations</h2>
                </div>
                <div class="row accredidations">
                    <?php Redwood_KRK_Accreditation_Settings::render_accreditations() ?>
                </div>
            </div>
        </section>
    <?php endif; ?>
</script>
<?php endif; ?>

<?php
$disabled = $customizer->get_setting('krk_home_disable_Our-Stories');
if(strlen($disabled) == 0) : ?>
    <script id="our-stories-section" type="text/template" data-index="<?php echo array_search("Our-Stories", $sections); ?>">
    <?php
    wp_reset_query();
    $type = 'krk_testimonial';
    $args=array(
        'post_type' => $type,
        'post_status' => 'publish',
        'order' => 'DESC',
        'posts_per_page' => 3);

    $query = null;
    $query = new WP_Query($args);
    if( $query->have_posts() ): ?>
        <section class="section-gallery our-stories">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <h2>Our Stories</h2>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php
                    $first_post = true;
                    $max_content_length = 0; //used to create dynamic section height
                    $MAX_EXCERT_LENGTH = 150;

                    while ($query->have_posts()):
                        $query->the_post();
                        $name = get_post_meta(get_the_ID(), 'krk_testimonial_name', true);
                        $title = get_post_meta(get_the_ID(), 'krk_testimonial_title', true);
                        $source= get_post_meta(get_the_ID(), 'krk_testimonial_source', true);
                        //skip empty testimonials
                        if(empty(strip_tags(get_the_content()))){
                            continue;
                        }

                        ?>
                        <div class="item 
                	<?php if($first_post) {
                            echo 'active';
                            $first_post = false;
                        } ?>">
                            <div class="text-block">
                        <span class="desk">
                            <?php
                            $content = get_the_content();
                            if(strlen($content) > $MAX_EXCERT_LENGTH) {
                                echo "<p>" . strip_tags(substr($content, 0, strpos($content, ' ', $MAX_EXCERT_LENGTH))) . "...</p>";
                                $max_content_length = $MAX_EXCERT_LENGTH;
                            }

                            else {
                                the_content();
                                $max_content_length = $max_content_length < strlen($content) ? strlen($content) : $max_content_length;
                            }
                            ?>
                        </span>
                                <div class="link-to">
                                    <?php include(locate_template('testimonial-social.php')); ?>
                                    <span class="name"><?php echo $name?> <br> <?php echo $title ?></span>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <style>
                        .our-stories .carousel-inner {
                            height: <?php echo ($max_content_length + 100) ?>px;
                        }
                    </style>
                </div>
                <?php if($query->post_count > 1) { ?>
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                <?php } ?>
                <a href="<?php multisite_path('/our-stories'); ?>" class="btn btn-primary">Read More <span class="icon icon-arrow-right"></span></a>
            </div>
        </section>
        <?php
    endif;
    ?>
</script>
<?php endif; ?>

<?php
$disabled = $customizer->get_setting('krk_home_disable_Menu');
if(strlen($disabled) == 0) : ?>
    <script id="menu-section" type="text/template" data-index="<?php echo array_search("Menu",$sections); ?>">
    <section class="section-food-menu">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
                    <div class="text-block">
                        <h2>Our Menu</h2>
                        <?php $menu_content = $customizer->get_setting( 'krk_home_menu_content'); ?>
                        <div class="text-wrap"">
                            <?php echo $menu_content; ?>
                        </div>
                        <?php
                        wp_reset_query();
                        $menus = query_posts('post_type=krk_menu');
                        foreach ($menus as $menu) {
                            if ($menu->krk_menu_active === 'on' && strlen($menu->krk_menu_attachment) > 0): ?>
                                <div class="btn-wrap">
                                    <a href="<?php multisite_path('/our-school/#pdf-menu'); ?>"
                                       class="btn btn-primary">
                                        Download Menu
                                        <span class="icon icon-arrow-right"></span>
                                    </a>
                                </div>
                            <?php endif;
                        } ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
</script>
<?php endif; ?>

<?php
$disabled = $customizer->get_setting('krk_home_disable_Technology');
if(strlen($disabled) == 0) : ?>
    <script id="technology-section" type="text/template" data-index="<?php echo array_search("Technology", $sections); ?>">
    <?php if(! Redwood_KRK_Settings::get_hide_interactive_technology() ):?>
        <section class="section-learning">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
                        <div class="text-block">
                            <h2>Latest Technology & Learning Programs</h2>
                            <a href="<?php multisite_path('/our-curriculum/#interactive'); ?>">
                                <picture>
                                    <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-06.png, <?php bloginfo('stylesheet_directory'); ?>/images/img-06-2x.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-06-3x.png 3x">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-06.png" height="179" width="278" alt="image description">
                                </picture>
                            </a>
                            <span class="desk">Our newest generation of learners are growing up in the digital age where everything is touch activated.</span>
                            <a href="<?php multisite_path('/our-curriculum#interactive'); ?>" class="btn btn-primary">Learn More <span class="icon icon-arrow-right"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
</script>
<?php endif; ?>

<?php
$disabled = $customizer->get_setting('krk_home_disable_Our-School');
if(strlen($disabled) == 0) : ?>
    <script id="our-school-section" type="text/template" data-index="<?php echo array_search("Our-School", $sections); ?>">
    <section class="section-school">
        <div class="boxed">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10">
                        <div class="text-block">
                            <h2>Our School</h2>
                            <?php $school_content = $customizer->get_setting( 'krk_home_school_content'); ?>
                            <div class="text-wrap"">
                                <?php echo $school_content; ?>
                            </div>

                            <div class="btn-wrap">
                                <a href="<?php multisite_path('/our-school'); ?>" class="btn btn-primary">Learn More <span class="icon icon-arrow-right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>
<?php endif; ?>

<?php
$disabled = $customizer->get_setting('krk_home_disable_Events');
if(strlen($disabled) == 0) : ?>
    <script id="events-section" type="text/template" data-index="<?php echo array_search("Events", $sections); ?>">
    <?php
    wp_reset_query();
    $type = 'krk_event';
    $args=array(
        'post_type' => $type,
        'post_status' => 'publish',
        'order' => 'ASC',
        'orderby' => 'meta_value',
        'meta_key' => 'krk_event_date',
        'posts_per_page' => 1,
        'meta_query' => array(
            array(
                'key' => 'krk_event_date',
                'value' => time() - (24 * 60 * 60),
                'compare' => '>=',
                'type' => 'NUMERIC',
            )
        ),
    );

    $query = null;
    $query = new WP_Query($args);

    if( $query->have_posts() ) : ?>

        <section class="section-events">
            <div class="container-fluid">
                <div class="row sameheight-block">
                    <div class="col-sm-12">
                        <div class="title-event">
                            <h2>Events &amp; Calendar</h2>
                        </div>
                    </div>

                    <?php while($query->have_posts()) :
                        $query->the_post();
                        $start_unixtime = get_post_meta(get_the_ID(), 'krk_event_date', true);
                        $end_unixtime = get_post_meta(get_the_ID(), 'krk_event_end_date', true); ?>

                        <div class="col-sm-6 hidden-xs">
                            <div class="event sameheight">
                                <div class="visual">
                                    <?php
                                    if (has_post_thumbnail()) {
                                        the_post_thumbnail(array(328, 530));
                                    }
                                    ?>
                                </div>
                                <div class="body-event">
                                    <h3>
                                        <a href="<?php the_permalink(); ?>">
                                            <?php echo get_the_title(); ?>
                                        </a>
                                    </h3>
                                    <time datetime="<?php echo date('Y-m-d', $start_unixtime); ?>">
                                        <i><b><?php echo date('F d, Y', $start_unixtime)?></b></i>
                                        <?php if($end_unixtime != false) : ?>
                                            <i><b> - <?php echo date('F d, Y', $end_unixtime)?></b></i>
                                        <?php endif; ?>
                                    </time>
                                    <?php
                                    $content = get_the_content();
                                    if(strlen($content) > 85)
                                        echo "<p>" . strip_tags(substr($content, 0, strpos($content, ' ', 85))) . "...</p>";
                                    else
                                        echo $content;
                                    ?>
                                    <a href="<?php echo get_post_type_archive_link('krk_event') ?>" class="more"><b>All Events ></b></a>
                                </div>
                            </div>
                        </div>
                        <?php
                    endwhile;

                    $type = 'krk_event';
                    $args=array(
                        'post_type' => $type,
                        'post_status' => 'publish',
                        'order' => 'ASC',
                        'orderby' => 'meta_value',
                        'meta_key' => 'krk_event_date',
                        'posts_per_page' => 5,
                        'meta_query' => array(
                            array(
                                'key' => 'krk_event_date',
                                'value' => time() - (24 * 60 * 60),
                                'compare' => '>=',
                                'type' => 'NUMERIC',
                            )
                        ),
                    );

                    $query = null;
                    $query = new WP_Query($args);
                    $post_count = $query->post_count;
                    if( $query->have_posts() && $post_count > 0 ): ?>
                        <div class="col-sm-6">
                            <div class="event-wrap sameheight <?php if($post_count == 1) echo 'visible-xs'; ?>">
                                <ul class="list-event">
                                    <?php
                                    $i = 0;
                                    while ($query->have_posts()):
                                        $query->the_post();
                                        $unixtime = get_post_meta(get_the_ID(), 'krk_event_date', true);
                                        $i++; ?>
                                        <li class="<?php if($i==1) echo 'visible-xs'; ?>">
                                            <span class="data"><?php echo date('M', $unixtime); ?><span class="num"><?php echo date('d', $unixtime) ?></span></span>
                                            <div class="text">
                                                <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                                                <?php
                                                $content = get_the_content();
                                                if(strlen($content) > 85)
                                                    echo "<p>" . strip_tags(substr($content, 0, strpos($content, ' ', 85))) . "...</p>";
                                                else
                                                    the_content();
                                                ?>
                                                <a href="<?php the_permalink(); ?>" class="more"><b>Read More ></b></a>
                                            </div>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    <?php endif; ?>
</script>
<?php endif; ?>

<?php
$disabled = $customizer->get_setting('krk_home_disable_News');
if(strlen($disabled) == 0) : ?>
    <script id="news-section" type="text/template" data-index="<?php echo array_search("News", $sections); ?>">
    <?php
    wp_reset_query();
    $type = 'krk_news';
    $args=array(
        'post_type' => $type,
        'post_status' => 'publish',
        'order' => 'DESC',
        'posts_per_page' => 2);
    $query = null;
    $query = new WP_Query($args);
    if( $query->have_posts() ): ?>
        <section class="section-news">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <div class="text-block">
                            <h2>Latest News</h2>
                            <?php
                            while ($query->have_posts()):
                                $query->the_post();?>
                                <div class="post-news container">
                                    <h3><a href="<?php the_permalink() ?>"> <?php the_title(); ?> </a></h3>
                                    <div class="row">
                                        <?php
                                        if (has_post_thumbnail()):
                                            ?>
                                            <div class="col-md-2">
                                                <?php the_post_thumbnail('thumbnail', array( 'class' => 'img-thumbnail' ));  ?>
                                            </div>
                                            <?php
                                        endif
                                        ?>
                                        <div class="<?php echo has_post_thumbnail() ? 'col-md-6' : 'col-md-8' ?>">
                                            <?php $content = get_the_content();
                                            if (strlen($content) > 250) {
                                                echo strip_tags(substr($content, 0, strpos($content, " ", 250)))."...";
                                            } else {
                                                the_content();
                                            }
                                            ?>
                                            <br />
                                            <a href="<?php the_permalink() ?>" class="read-more">Read More <span class="icon icon-arrow-right"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            endwhile;?>
                            <div class="btn-wrap">
                                <a href="news" class="load-more">See All News</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
    endif;
    wp_reset_query();  // Restore global post data stomped by the_post().
    ?>
</script>
<?php endif; ?>
<?php Redwood_KRK_Accreditation_Settings::render_accreditation_modals(); ?>
