<?php
    /**
     * Created by PhpStorm.
     * User: dustin
     * Date: 1/27/16
     * Time: 11:09 AM
     */

    get_header(); ?>

    <div class="main-holder">
        <div class="breadcrumbs-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php multisite_path('/'); ?>">Home</a></li>
                            <li class="active">About</li>
                        </ol>
                        <div class="title-page">
                            <h1>Forms & Documents</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="twocolumns" class="battlement">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                        $active_nav = 'stories';
                        include(locate_template('nav-about.php'));
                        ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="visual-img">
                            <picture>
                                <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-20.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-20-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-20-3x.jpg 3x">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-20.jpg" height="299" width="823" alt="image description">
                            </picture>
                            <div class="text">
                                <strong>Forms & Documents</strong>
                            </div>
                        </div>
                        <div class="block-posts">
                            <ul>
                                <?php
                                $type = 'krk_document';
                                $args=array(
                                    'post_type' => $type,
                                    'post_status' => 'publish',
                                    'order' => 'DESC',
                                    'posts_per_page' => -1);

                                $query = null;
                                $query = new WP_Query($args);
                                if( $query->have_posts() ):
                                    while ($query->have_posts()):
                                        $query->the_post();
                                        $description = get_post_meta(get_the_ID(), 'krk_document_description', true);
                                        $attachment = get_post_meta(get_the_ID(), 'krk_document_attachment', true);
                                        $link_text = get_post_meta(get_the_ID(), 'krk_document_link_text', true);
                                        ?>
                                        <li>
                                            <a href="<?php echo $attachment ?>"><?php if (empty($link_text)) the_title(); else echo $link_text; ?></a>
                                            <p><?php echo $description; ?></p>
                                        </li>
                                    <?php
                                    endwhile;
                                endif;
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
    </div>

    <?php
    get_footer();
    ?>