<?php
    $images = get_children( array (
        'post_parent' => $post->ID,
        'post_type' => 'attachment',
        'post_mime_type' => 'image'
    ));

    if ( empty($images) ) {
        // no attachments here
    } else {
        foreach ( $images as $attachment_id => $attachment ) {
            echo wp_get_attachment_image( $attachment_id, 'thumbnail' );
        }
    }
?>