<?php
    get_header();
    $location = get_option('redwood_location_settings', array());
    $state = $location['state'];
?>

    <main id="main">
        <div class="main-holder">
            <div class="breadcrumbs-wrap">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="title-page title">
                                <h1>CAREER APPLICATION</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="twocolumns" class="battlement">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <nav class="menu-wrap">
                                <ul class="sub-menu">
                                    <li class="active"><a href="#">Career Application</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-sm-9">
                            <div class="info-block">
                                <div class="visual-img">
                                    <picture>
                                        <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-22.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-22-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-22-3x.jpg 3x">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-22.jpg" height="299" width="823" alt="image description">
                                    </picture>
                                </div>

                                <h2>APPLICANT'S STATEMENT</h2>
                                <div class="disclaimer">
                                    <p>I understand that the Center is committed to providing equal opportunity in all employment practices, including but not limited to selection, hiring promotion, transfer, and compensation to all qualified applicants and employees without regard to age, race, color, national origin, sex or gender, pregnancy or pregnancy-relation conditions, religion, handicap or disability, citizenship or service member status or any other category protected by federal, state, or local law.</p>

                                    <p>I authorize former and present employers, and professional, work, and personal references listed in the application and any other individuals I may name, to give the Center or its designee any and all information concerning my previous employment and any pertinent information they may have, personal or otherwise, and release such parties from all liability for any damages that may result from furnishing same to the Center.  I also authorize the Center to provide truthful information concerning my employment with it to future employers and I agree to hold it harmless for providing such information.</p>

                                    <p>I certify that I do not use illegal drugs. I understand that the Center reserves the right, to the extent permitted by law, to require drug and alcohol screening tests of an applicant or an employee either prior to employment or any time during employment and I hereby give my consent to any such tests.  I consent to the release of the results of any such tests to the Center or its designee.  I release the Center and its designee from any and all liability and damages that may result or arise from any drug test or the provision of information in connection with such a test.</p>

                                    <p>I understand that specific laws, regulations and rules apply to the Center’s operation and I agree to comply with all such applicable laws, regulations and rules. I also agree to a comply with all applicable laws, regulations and rules that may apply to my own initial certification and continued certification to work for the Center.</p>

                                    <p>I understand that this employment application and any other Center documents are not promises of employment.  <b><u>SHOULD I BE EMPLOYED, I UNDERSTAND THAT MY EMPLOYMENT WILL BE ON AN AT-WILL BASIS.  I FURTHER UNDERSTAND THAT, IF I AM EMPLOYED, I CAN TERMINATE MY EMPLOYMENT AT ANY TIME WITH OR WITHOUT CAUSE AND WITH OR WITHOUT ADVANCE NOTICE AND THAT THE CENTER HAS A SIMILAR RIGHT.</u></b>  I understand that no manager, representative, or agent of the Center has any authority to enter into any agreement for employment for any specified period of time or to make any agreement contrary to the foregoing, except that the owner may do so in writing.</p>

                                    <p>I certify that the information given by me on this application and during the interview process is true and complete in all respects, and I agree that if the information is found to be false, misleading, or unsatisfactory in any respect (in the Center’s judgment) that I will be disqualified from consideration for employment or subject to immediate dismissal if discovered after I am hired.</p>

                                    <p>I certify that I have received a separate written notification that the Center may obtain a consumer report on me for use in connection with my application and, if I am hired, my employment with the Center.  I authorize the Center to obtain this report.</p>

                                    <p>This application will be considered “active” for a maximum of thirty (30) days.  If you wish to be considered for employment after that time, you must reapply.</p>
                                </div>

                                <hr class="line grey" />
                    
                                <h2>Career Application Form</h2>
                                <p>Each inquiry on this application must be fully answered or completed.  Otherwise, you will not be considered for employment.
                                </p>
                            </div>

                            <div class="alert alert-success hidden" id="career-form-success" style="color: #000000;">Thank you for your interest in our school! We will be in contact with you shortly!</div>
                            <div class="alert alert-danger hidden" id="career-form-error">Uh oh! Something went wrong trying to send your application. Please try again later.</div>

                            <form id='career-form'
                                  class="info-form"
                                  method="post"
                                  data-type="ajax"
                                  data-fade="true">

                                <fieldset>
                                <input type="hidden" name="action" value="career_application" />
                                <h2>Personal Information</h2>
                                    <div class="form-box">
                                        <h3>YOUR NAME</h3>
                                        <div class="form-group">
                                            <input type="text"
                                                   name="FirstName"
                                                   class="form-control"
                                                   placeholder="First Name"
                                                   data-valiation-type="name"
                                                   data-display-name="First Name"
                                                   required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="MiddleName" class="form-control" placeholder="Middle Name">
                                        </div>
                                        <div class="form-group">
                                            <input type="text"
                                                   name="LastName"
                                                   class="form-control"
                                                   placeholder="Last Name"
                                                   data-valiation-type="name"
                                                   data-display-name="Last Name"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-box">
                                        <h3>YOUR CURRENT ADDRESS</h3>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="CurrAddress" placeholder="Street Address">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="CurrCity" placeholder="City">
                                        </div>
                                        <div class="form-group">
                                            <select name="CurrState">
                                                <option class="hidden">State</option>
                                                <option>AL</option>
                                                <option>AK</option>
                                                <option>AZ</option>
                                                <option>AR</option>
                                                <option>CA</option>
                                                <option>CO</option>
                                                <option>CT</option>
                                                <option>DE</option>
                                                <option>FL</option>
                                                <option>GA</option>
                                                <option>HI</option>
                                                <option>ID</option>
                                                <option>IL</option>
                                                <option>IN</option>
                                                <option>IA</option>
                                                <option>KS</option>
                                                <option>KY</option>
                                                <option>LA</option>
                                                <option>ME</option>
                                                <option>MD</option>
                                                <option>MA</option>
                                                <option>MI</option>
                                                <option>MN</option>
                                                <option>MS</option>
                                                <option>MO</option>
                                                <option>MT</option>
                                                <option>NE</option>
                                                <option>NV</option>
                                                <option>NH</option>
                                                <option>NJ</option>
                                                <option>NM</option>
                                                <option>NY</option>
                                                <option>NC</option>
                                                <option>ND</option>
                                                <option>OH</option>
                                                <option>OK</option>
                                                <option>OR</option>
                                                <option>PA</option>
                                                <option>RI</option>
                                                <option>SC</option>
                                                <option>SD</option>
                                                <option>TN</option>
                                                <option>TX</option>
                                                <option>UT</option>
                                                <option>VT</option>
                                                <option>VA</option>
                                                <option>WA</option>
                                                <option>WV</option>
                                                <option>WI</option>
                                                <option>WY</option>
                                                <option>DC</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" name="CurrZip" class="form-control" placeholder="Zip Code">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="CurrAddressTime" placeholder="How long have you lived there?">
                                        </div>
                                    </div>

                                    <div class="form-box">
                                        <h3>YOUR PREVIOUS ADDRESS</h3>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="PrevAddress" placeholder="Street Address">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="PrevCity" placeholder="City">
                                        </div>
                                        <div class="form-group">
                                            <select name="PrevState">
                                                <option class="hidden">State</option>
                                                <option>AL</option>
                                                <option>AK</option>
                                                <option>AZ</option>
                                                <option>AR</option>
                                                <option>CA</option>
                                                <option>CO</option>
                                                <option>CT</option>
                                                <option>DE</option>
                                                <option>FL</option>
                                                <option>GA</option>
                                                <option>HI</option>
                                                <option>ID</option>
                                                <option>IL</option>
                                                <option>IN</option>
                                                <option>IA</option>
                                                <option>KS</option>
                                                <option>KY</option>
                                                <option>LA</option>
                                                <option>ME</option>
                                                <option>MD</option>
                                                <option>MA</option>
                                                <option>MI</option>
                                                <option>MN</option>
                                                <option>MS</option>
                                                <option>MO</option>
                                                <option>MT</option>
                                                <option>NE</option>
                                                <option>NV</option>
                                                <option>NH</option>
                                                <option>NJ</option>
                                                <option>NM</option>
                                                <option>NY</option>
                                                <option>NC</option>
                                                <option>ND</option>
                                                <option>OH</option>
                                                <option>OK</option>
                                                <option>OR</option>
                                                <option>PA</option>
                                                <option>RI</option>
                                                <option>SC</option>
                                                <option>SD</option>
                                                <option>TN</option>
                                                <option>TX</option>
                                                <option>UT</option>
                                                <option>VT</option>
                                                <option>VA</option>
                                                <option>WA</option>
                                                <option>WV</option>
                                                <option>WI</option>
                                                <option>WY</option>
                                                <option>DC</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" name="PrevZip" class="form-control" placeholder="Zip Code">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="PrevAddressTime" placeholder="How long did you lived there?">
                                        </div>
                                    </div>
                                    <div class="form-box">
                                        <h3>CONTACT INFORMATION</h3>
                                        <div class="form-group">
                                        	<input type="tel"
                                                   name="PhoneNumber"
                                                   class="form-control"
                                                   placeholder="Phone Number (XXX) XXX-XXXX"
                                                   data-validation-type="phone"
                                                   data-display-name="Contact Phone Number"
                                                   required>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="Email" class="form-control" placeholder="Email Address" required>
                                        </div>
                                    </div>

                                    <div class="form-box">
                                        <h3>OTHER INFORMATION</h3>
                                        <div class="form-group">
                                        	<input type="text" name="SSN" class="form-control" placeholder="Social Security Number (XXX-XX-XXXX)" required>
                                        </div>
                                        <div class="form-group">
                                        	<p class="checkbox-description">
                                        		Are you 18 years of age or older?
                                        	</p>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="Above18" id="radio-above18" value="yes" class="form-control" required>
                                            	<label for="radio-above18">Yes</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="Above18" id="radio-under18" value="no" checked class="form-control">
                                            	<label for="radio-under18">No</label>
                                            </div>
                                        </div>
                                    </div>

                                    <hr class="line grey">
                                    <div class="info-block">
                                		<h2>PREVIOUS EMPLOYMENT</h2>
                                    	<p>Please go back at least 10 years. List the names of your present or previous employers in chronological order with present or last employer listed first.  Include part-time and seasonal employment.  If self-employed, give firm name and supply business references.</p>
                                    	<button type="button" class="btn btn-primary add-employer"> + ADD EMPLOYER</button>
                            		</div>
                            		<div id="employer-wrapper"></div>

                                    <hr class="line grey">
                                    <div class="info-block">
                                        <h2>BACKGROUND INFORMATION</h2>
                                    </div>
                                    <div class="form-box">
                                        <div class="form-group">
                                            <p class="checkbox-description">
                                                Desired Position:
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="desiredPosition" id="radio-director" value="director" class="form-control" required>
                                                <label for="radio-director">Director</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="desiredPosition" id="radio-assistant-director" value="assistant director" class="form-control">
                                                <label for="radio-assistant-director">Assistant Director</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="desiredPosition" id="radio-lead-teacher" value="lead teacher" class="form-control">
                                                <label for="radio-lead-teacher">Lead Teacher</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="desiredPosition" id="radio-teacher" value="teacher" class="form-control">
                                                <label for="radio-teacher">Teacher</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="desiredPosition" id="radio-sub-teacher" value="substitute teacher" class="form-control">
                                                <label for="radio-sub-teacher">Substitute Teacher</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="desiredPosition" id="radio-cook" value="cook" class="form-control">
                                                <label for="radio-cook">Cook</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="desiredPosition" id="radio-bus-driver" value="bus driver" class="form-control">
                                                <label for="radio-bus-driver">Bus Driver</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="desiredPosition" id="radio-other" value="" class="form-control">
                                                <input type="text" id="desiredPosition-other" value="" placeholder="Other" />
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="form-group">
                                            <p class="checkbox-description">
                                                Employment Desired:
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="desiredEmployment" id="radio-fulltime" value="full time" class="form-control" required>
                                                <label for="radio-fulltime">Full Time</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="desiredEmployment" id="radio-parttime" value="part time" class="form-control" required>
                                                <label for="radio-parttime">Part Time</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input type="radio" name="desiredEmployment" id="radio-temporary" value="temporary" class="form-control" required>
                                                <label for="radio-temporary">Temporary</label>
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="form-group">
                                            <input type="text" name="preferedAge" class="form-control" placeholder="What age group do you prefer?" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="availableForWork" class="form-control" placeholder="When are you available for work?" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="expectedWage" class="form-control" placeholder="Expected Salary/Wage" required>
                                        </div>
                                        <br/>
                                        <div class="form-group">
                                            <p class="checkbox-description">
                                                Do you have any commitments to any other employer which may affect your employment?
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="otherCommitments"
                                                        id="radio-yes-commitments"
                                                        value="yes"
                                                        class="form-control show-explanation"
                                                        required>
                                                <label for="radio-yes-commitments">Yes</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="otherCommitments"
                                                        id="radio-no-commitments"
                                                        value="no"
                                                        class="form-control hide-explanation">
                                                <label for="radio-no-commitments">No</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <textarea   name="commitmentsExplanation"
                                                        class="form-control explanation"
                                                        placeholder="If yes, explain..."
                                                        style="display:none;"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <p><b>Please explain fully any gaps in your employment history.  Be sure to account for all periods of time including military service and any period of unemployment.</b></p>
                                            <textarea name="employmentGaps" class="form-control"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <p class="checkbox-description">
                                                If hired, can you provide proof that you are legally entitled to work in the U.S.?
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="legallyUS"
                                                        id="radio-yes-legallyUS"
                                                        class="form-control hide-explanation"
                                                        value="yes"
                                                        required>
                                                <label for="radio-yes-legallyUS">Yes</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="legallyUS"
                                                        id="radio-no-legallyUS"
                                                        value="no"
                                                        class="form-control show-explanation">
                                                <label for="radio-no-legallyUS">No</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <textarea   name="legallyUSExplanation"
                                                        class="form-control explanation"
                                                        placeholder="If no, what steps must be taken for you to begin employment lawfully?"
                                                        style="display:none;"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <p class="checkbox-description">
                                                Have you ever been terminated or asked to resign from any job?
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="beenTerminated"
                                                        id="radio-yes-terminated"
                                                        value="yes"
                                                        class="form-control show-explanation"
                                                        required>
                                                <label for="radio-yes-terminated">Yes</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="beenTerminated"
                                                        id="radio-no-terminated"
                                                        value="no"
                                                        class="form-control hide-explanation">
                                                <label for="radio-no-terminated">No</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <textarea   name="terminationExplanation"
                                                        class="form-control explanation"
                                                        placeholder="If yes, please explain the circumstances"
                                                        style="display:none;"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <p class="checkbox-description">
                                                May we contact your current employer?
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="contactPrevEmployer"
                                                        id="radio-yes-contactPrevEmployer"
                                                        value="yes"
                                                        class="form-control hide-explanation"
                                                        required>
                                                <label for="radio-yes-contactPrevEmployer">Yes</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="contactPrevEmployer"
                                                        id="radio-no-contactPrevEmployer"
                                                        value="no"
                                                        class="form-control show-explanation">
                                                <label for="radio-no-contactPrevEmployer">No</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <textarea   name="contactPrevEmployerExplanation"
                                                        class="form-control explanation"
                                                        placeholder="If no, please explain..."
                                                        style="display:none;"></textarea>
                                        </div>

                                         <div class="form-group">
                                            <p class="checkbox-description">
                                                Do you have any friends or relatives working at this center?
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="friendsWorkingHere"
                                                        id="radio-yes-friendsWorkingHere"
                                                        value="yes"
                                                        class="form-control show-explanation"
                                                        required>
                                                <label for="radio-yes-friendsWorkingHere">Yes</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="friendsWorkingHere"
                                                        id="radio-no-friendsWorkingHere"
                                                        value="no"
                                                        class="form-control hide-explanation">
                                                <label for="radio-no-friendsWorkingHere">No</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <textarea   name="friendsWorkingHereList"
                                                        class="form-control explanation"
                                                        placeholder="If yes, Name(s) and Relationship"
                                                        style="display:none;"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <p class="checkbox-description">
                                                Have you ever worked for this Center?
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="workedAtThisCenter"
                                                        id="radio-yes-workedAtThisCenter"
                                                        value="yes"
                                                        class="form-control hide-followup show-related-explanation"
                                                        data-related-explanation="1"
                                                        required>
                                                <label for="radio-yes-workedAtThisCenter">Yes</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="workedAtThisCenter"
                                                        id="radio-no-workedAtThisCenter"
                                                        value="no"
                                                        class="form-control show-followup hide-related-explanation"
                                                        data-related-explanation="1">
                                                <label for="radio-no-workedAtThisCenter">No</label>
                                            </div>
                                        </div>

                                        <div class="form-group followup" style="display: none;">
                                            <p class="checkbox-description">
                                                Have you ever worked for any Kid <span class="krk-ticks">R</span> Kids Center?
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="workedAtAnyCenter"
                                                        id="radio-yes-workedAtAnyCenter"
                                                        value="yes"
                                                        class="form-control show-related-explanation"
                                                        data-related-explanation="1">
                                                <label for="radio-yes-workedAtAnyCenter">Yes</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="workedAtAnyCenter"
                                                        id="radio-no-workedAtAnyCenter"
                                                        value="no"
                                                        class="form-control hide-related-explanation"
                                                        data-related-explanation="1">
                                                <label for="radio-no-workedAtAnyCenter">No</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <p class="checkbox-description">
                                                Have you ever applied to work for this Center or any other Kids <span class="krk-ticks">R</span> Kids Center?
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="appliedToKRK"
                                                        id="radio-yes-appliedToKRK"
                                                        value="yes"
                                                        class="form-control show-related-explanation"
                                                        data-related-explanation="1"
                                                        required>
                                                <label for="radio-yes-appliedToKRK">Yes</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="appliedToKRK"
                                                        id="radio-no-appliedToKRK"
                                                        value="no"
                                                        class="form-control hide-related-explanation"
                                                        data-related-explanation="1">
                                                <label for="radio-no-appliedToKRK">No</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <textarea   name="appliedToKRKExplanation"
                                                        class="form-control related-explanation"
                                                        placeholder="If you have worked at this center, any other Kids R Kids Center, or have ever applied to work at a Kids R Kids Center, please give the dates and positions"
                                                        style="display:none;"
                                                        data-related-explanation="1"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <p class="checkbox-description">
                                                Have you ever used another name?
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="anotherName"
                                                        id="radio-yes-another-name"
                                                        value="yes"
                                                        class="form-control show-explanation"
                                                        required>
                                                <label for="radio-yes-another-name">Yes</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="anotherName"
                                                        id="radio-no-another-name"
                                                        value="no"
                                                        class="form-control hide-explanation">
                                                <label for="radio-no-another-name">No</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <textarea   name="anotherNameExplanation"
                                                        class="form-control explanation"
                                                        placeholder="Is any additional information relative to change of name, use of an assumed name, or nickname necessary to enable a check on your work and educational record?"
                                                        style="display:none;"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <p class="checkbox-description">
                                                Do you have adequate transportation to get to and from work on a reliable and consistent basis?
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="haveTransportation"
                                                        id="radio-yes-haveTransportation"
                                                        value="yes"
                                                        class="form-control"
                                                        required>
                                                <label for="radio-yes-haveTransportation">Yes</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="haveTransportation"
                                                        id="radio-no-haveTransportation"
                                                        value="no"
                                                        class="form-control">
                                                <label for="radio-no-haveTransportation">No</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <p class="checkbox-description">
                                                How were you referred to us?
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="referredType"
                                                        id="radio-referred-friend"
                                                        value="friend"
                                                        class="form-control"
                                                        required>
                                                <label for="radio-referred-friend">Friend</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="referredType"
                                                        id="radio-referred-relative"
                                                        value="relative"
                                                        class="form-control">
                                                <label for="radio-referred-relative">Relative</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="referredType"
                                                        id="radio-referred-advertisement"
                                                        value="advertisement"
                                                        class="form-control">
                                                <label for="radio-referred-advertisement">Adversitement</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="referredType"
                                                        id="radio-referred-internet"
                                                        value="internet"
                                                        class="form-control">
                                                <label for="radio-referred-internet">Internet</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="referredType"
                                                        id="radio-referred-sea"
                                                        value="state employment agency"
                                                        class="form-control" >
                                                <label for="radio-referred-sea">State Employment Agency</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="referredType"
                                                        id="radio-referred-pea"
                                                        value="private employment agency"
                                                        class="form-control" >
                                                <label for="radio-referred-pea">Private Employment Agency</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="referredType"
                                                        id="radio-referred-other"
                                                        value="other"
                                                        class="form-control" >
                                                <label for="radio-referred-other">Other</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <p class="checkbox-description">
                                                Do you have any children who will attend this Center?
                                            </p>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="childrenAttendingCenter"
                                                        id="radio-yes-childrenAttendingCenter"
                                                        value="yes"
                                                        class="form-control show-explanation"
                                                        required>
                                                <label for="radio-yes-childrenAttendingCenter">Yes</label>
                                            </div>
                                            <div class="checkbox-wrapper">
                                                <input  type="radio"
                                                        name="childrenAttendingCenter"
                                                        id="radio-no-childrenAttendingCenter"
                                                        value="no"
                                                        class="form-control hide-explanation">
                                                <label for="radio-no-childrenAttendingCenter">No</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <textarea   name="childrenAttendingCenterList"
                                                        class="form-control explanation"
                                                        placeholder="If yes, list age and grade of each child"
                                                        style="display:none;"></textarea>
                                        </div>
                                    </div>

                                    <hr class="line grey">
                                    <div class="info-block">
                                        <h2>EDUCATION</h2>
                                    </div>

                                    <div class="form-box">
                                        <h3>HIGH SCHOOL</h3>
                                        <div class="form-group">
                                            <select name="highschoolYearsCompleted">
                                                <option value="0" class="hidden">Years Completed</option>
                                                <option value="9">9 Years</option>
                                                <option value="10">10 Years</option>
                                                <option value="11">11 Years</option>
                                                <option value="12">12 Years</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="highschoolName" class="form-control" placeholder="School Name">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="highschoolCity" placeholder="City">
                                        </div>
                                        <div class="form-group">
                                            <select name="highschoolState">
                                                <option class="hidden">State</option>
                                                <option>AL</option>
                                                <option>AK</option>
                                                <option>AZ</option>
                                                <option>AR</option>
                                                <option>CA</option>
                                                <option>CO</option>
                                                <option>CT</option>
                                                <option>DE</option>
                                                <option>FL</option>
                                                <option>GA</option>
                                                <option>HI</option>
                                                <option>ID</option>
                                                <option>IL</option>
                                                <option>IN</option>
                                                <option>IA</option>
                                                <option>KS</option>
                                                <option>KY</option>
                                                <option>LA</option>
                                                <option>ME</option>
                                                <option>MD</option>
                                                <option>MA</option>
                                                <option>MI</option>
                                                <option>MN</option>
                                                <option>MS</option>
                                                <option>MO</option>
                                                <option>MT</option>
                                                <option>NE</option>
                                                <option>NV</option>
                                                <option>NH</option>
                                                <option>NJ</option>
                                                <option>NM</option>
                                                <option>NY</option>
                                                <option>NC</option>
                                                <option>ND</option>
                                                <option>OH</option>
                                                <option>OK</option>
                                                <option>OR</option>
                                                <option>PA</option>
                                                <option>RI</option>
                                                <option>SC</option>
                                                <option>SD</option>
                                                <option>TN</option>
                                                <option>TX</option>
                                                <option>UT</option>
                                                <option>VT</option>
                                                <option>VA</option>
                                                <option>WA</option>
                                                <option>WV</option>
                                                <option>WI</option>
                                                <option>WY</option>
                                                <option>DC</option>
                                            </select>
                                        </div>
                                        <textarea   class="form-control"
                                                    name="highschoolExtraCurr"
                                                    placeholder="Describe any Extra-Curricular Activities"></textarea>
                                    </div>

                                    <div class="form-box">
                                        <h3>COLLEGE / UNIVERSITY</h3>
                                        <div class="form-group">
                                            <select name="collegeYearsCompleted">
                                                <option value="0" class="hidden">Years Completed</option>
                                                <option value="0">0 Years</option>
                                                <option value="1">1 Years</option>
                                                <option value="2">2 Years</option>
                                                <option value="3">3 Years</option>
                                                <option value="4">4 Years</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="collegelName" class="form-control" placeholder="School Name">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="collegeCity" placeholder="City">
                                        </div>
                                        <div class="form-group">
                                            <select name="collegeState">
                                                <option class="hidden">State</option>
                                                <option>AL</option>
                                                <option>AK</option>
                                                <option>AZ</option>
                                                <option>AR</option>
                                                <option>CA</option>
                                                <option>CO</option>
                                                <option>CT</option>
                                                <option>DE</option>
                                                <option>FL</option>
                                                <option>GA</option>
                                                <option>HI</option>
                                                <option>ID</option>
                                                <option>IL</option>
                                                <option>IN</option>
                                                <option>IA</option>
                                                <option>KS</option>
                                                <option>KY</option>
                                                <option>LA</option>
                                                <option>ME</option>
                                                <option>MD</option>
                                                <option>MA</option>
                                                <option>MI</option>
                                                <option>MN</option>
                                                <option>MS</option>
                                                <option>MO</option>
                                                <option>MT</option>
                                                <option>NE</option>
                                                <option>NV</option>
                                                <option>NH</option>
                                                <option>NJ</option>
                                                <option>NM</option>
                                                <option>NY</option>
                                                <option>NC</option>
                                                <option>ND</option>
                                                <option>OH</option>
                                                <option>OK</option>
                                                <option>OR</option>
                                                <option>PA</option>
                                                <option>RI</option>
                                                <option>SC</option>
                                                <option>SD</option>
                                                <option>TN</option>
                                                <option>TX</option>
                                                <option>UT</option>
                                                <option>VT</option>
                                                <option>VA</option>
                                                <option>WA</option>
                                                <option>WV</option>
                                                <option>WI</option>
                                                <option>WY</option>
                                                <option>DC</option>
                                            </select>
                                        </div>
                                        <textarea   class="form-control"
                                                    name="collegeExtraCurr"
                                                    placeholder="Describe Specialized Experience, Training, Skills, and Extra-Curricular Activities"></textarea>
                                    </div>

                                    <div class="form-box">
                                        <h3>GRADUATE / PROFESSIONAL</h3>
                                        <div class="form-group">
                                            <select name="graduateYearsCompleted">
                                                <option value="0" class="hidden">Years Completed</option>
                                                <option value="0">0 Years</option>
                                                <option value="1">1 Years</option>
                                                <option value="2">2 Years</option>
                                                <option value="3">3 Years</option>
                                                <option value="4">4 Years</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="graduateName" class="form-control" placeholder="School Name">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="graduateCity" placeholder="City">
                                        </div>
                                        <div class="form-group">
                                            <select name="graduateState">
                                                <option class="hidden">State</option>
                                                <option>AL</option>
                                                <option>AK</option>
                                                <option>AZ</option>
                                                <option>AR</option>
                                                <option>CA</option>
                                                <option>CO</option>
                                                <option>CT</option>
                                                <option>DE</option>
                                                <option>FL</option>
                                                <option>GA</option>
                                                <option>HI</option>
                                                <option>ID</option>
                                                <option>IL</option>
                                                <option>IN</option>
                                                <option>IA</option>
                                                <option>KS</option>
                                                <option>KY</option>
                                                <option>LA</option>
                                                <option>ME</option>
                                                <option>MD</option>
                                                <option>MA</option>
                                                <option>MI</option>
                                                <option>MN</option>
                                                <option>MS</option>
                                                <option>MO</option>
                                                <option>MT</option>
                                                <option>NE</option>
                                                <option>NV</option>
                                                <option>NH</option>
                                                <option>NJ</option>
                                                <option>NM</option>
                                                <option>NY</option>
                                                <option>NC</option>
                                                <option>ND</option>
                                                <option>OH</option>
                                                <option>OK</option>
                                                <option>OR</option>
                                                <option>PA</option>
                                                <option>RI</option>
                                                <option>SC</option>
                                                <option>SD</option>
                                                <option>TN</option>
                                                <option>TX</option>
                                                <option>UT</option>
                                                <option>VT</option>
                                                <option>VA</option>
                                                <option>WA</option>
                                                <option>WV</option>
                                                <option>WI</option>
                                                <option>WY</option>
                                                <option>DC</option>
                                            </select>
                                        </div>
                                        <textarea   class="form-control"
                                                    name="graduateExtraCurr"
                                                    placeholder="Describe Specialized Experience, Training, Skills, and Extra-Curricular Activities"></textarea>
                                    </div>

                                    <div class="form-box">
                                        <h3>TRADE OR CORRESPONDENCE</h3>

                                        <div class="form-group">
                                            <input type="text" name="tradeName" class="form-control" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="tradeCity" placeholder="City">
                                        </div>
                                        <div class="form-group">
                                            <select name="tradeState">
                                                <option class="hidden">State</option>
                                                <option>AL</option>
                                                <option>AK</option>
                                                <option>AZ</option>
                                                <option>AR</option>
                                                <option>CA</option>
                                                <option>CO</option>
                                                <option>CT</option>
                                                <option>DE</option>
                                                <option>FL</option>
                                                <option>GA</option>
                                                <option>HI</option>
                                                <option>ID</option>
                                                <option>IL</option>
                                                <option>IN</option>
                                                <option>IA</option>
                                                <option>KS</option>
                                                <option>KY</option>
                                                <option>LA</option>
                                                <option>ME</option>
                                                <option>MD</option>
                                                <option>MA</option>
                                                <option>MI</option>
                                                <option>MN</option>
                                                <option>MS</option>
                                                <option>MO</option>
                                                <option>MT</option>
                                                <option>NE</option>
                                                <option>NV</option>
                                                <option>NH</option>
                                                <option>NJ</option>
                                                <option>NM</option>
                                                <option>NY</option>
                                                <option>NC</option>
                                                <option>ND</option>
                                                <option>OH</option>
                                                <option>OK</option>
                                                <option>OR</option>
                                                <option>PA</option>
                                                <option>RI</option>
                                                <option>SC</option>
                                                <option>SD</option>
                                                <option>TN</option>
                                                <option>TX</option>
                                                <option>UT</option>
                                                <option>VT</option>
                                                <option>VA</option>
                                                <option>WA</option>
                                                <option>WV</option>
                                                <option>WI</option>
                                                <option>WY</option>
                                                <option>DC</option>
                                            </select>
                                        </div>
                                        <textarea   class="form-control"
                                                    name="tradeExtraCurr"
                                                    placeholder="Describe Specialized Experience, Training, Skills, and Extra-Curricular Activities"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <p class="checkbox-description">
                                            Is your CPR certificate current?
                                        </p>
                                        <div class="checkbox-wrapper">
                                            <input  type="radio"
                                                    name="cprCurrent"
                                                    id="radio-yes-cprCurrent"
                                                    value="yes"
                                                    class="form-control"
                                                    required>
                                            <label for="radio-yes-cprCurrent">Yes</label>
                                        </div>
                                        <div class="checkbox-wrapper">
                                            <input  type="radio"
                                                    name="cprCurrent"
                                                    id="radio-no-cprCurrent"
                                                    value="no"
                                                    class="form-control">
                                            <label for="radio-no-cprCurrent">No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="checkbox-description">
                                            Do you hold any certificates for childcare training?
                                        </p>
                                        <div class="checkbox-wrapper">
                                            <input  type="radio"
                                                    name="anyCertOrTraining"
                                                    id="radio-yes-anyCertOrTraining"
                                                    value="yes"
                                                    class="form-control show-explanation"
                                                    required>
                                            <label for="radio-yes-anyCertOrTraining">Yes</label>
                                        </div>
                                        <div class="checkbox-wrapper">
                                            <input  type="radio"
                                                    name="anyCertOrTraining"
                                                    id="radio-no-anyCertOrTraining"
                                                    value="no"
                                                    class="form-control hide-explanation">
                                            <label for="radio-no-anyCertOrTraining">No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <textarea   name="anyCertOrTrainingList"
                                                    class="form-control explanation"
                                                    placeholder="If yes, list the certificates or training"
                                                    style="display:none;"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <p><b>List any other professional designations, certifications, licenses, or courses that may be applicable to the position for which you are applying:</b></p>
                                        <textarea name="professionalCerts" class="form-control"></textarea>
                                    </div>

                                    <hr class="line grey">
                                    <div class="info-block">
                                        <h2>CRIMINAL BACKGROUND</h2>
                                    </div>

                                    <div class="form-group">
                                        <p class="checkbox-description">
                                            Have you ever plead no contest,  nolo, or guilty to a crime, or been convicted of a crime (other than minor traffic offenses)?
                                        </p>
                                        <div class="checkbox-wrapper">
                                            <input  type="radio"
                                                    name="hasBeenConvicted"
                                                    id="radio-yes-hasBeenConvicted"
                                                    value="yes"
                                                    class="form-control show-related-explanation"
                                                    data-related-explanation="2"
                                                    required>
                                            <label for="radio-yes-hasBeenConvicted">Yes</label>
                                        </div>
                                        <div class="checkbox-wrapper">
                                            <input  type="radio"
                                                    name="hasBeenConvicted"
                                                    id="radio-no-hasBeenConvicted"
                                                    value="no"
                                                    class="form-control hide-related-explanation"
                                                    data-related-explanation="2">
                                            <label for="radio-no-hasBeenConvicted">No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="checkbox-description">
                                            Have you been arrested for any matters for which you are currently out on bail or on your own recognizance pending trial?
                                        </p>
                                        <div class="checkbox-wrapper">
                                            <input  type="radio"
                                                    name="hasPendingTrial"
                                                    id="radio-yes-hasPendingTrial"
                                                    value="yes"
                                                    class="form-control show-related-explanation"
                                                    data-related-explanation="2"
                                                    required>
                                            <label for="radio-yes-hasPendingTrial">Yes</label>
                                        </div>
                                        <div class="checkbox-wrapper">
                                            <input  type="radio"
                                                    name="hasPendingTrial"
                                                    id="radio-no-hasPendingTrial"
                                                    value="no"
                                                    class="form-control hide-related-explanation"
                                                    data-related-explanation="2">
                                            <label for="radio-no-hasPendingTrial">No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="checkbox-description">
                                            Are any charges currently pending against you?
                                        </p>
                                        <div class="checkbox-wrapper">
                                            <input  type="radio"
                                                    name="hasPendingCharges"
                                                    id="radio-yes-hasPendingCharges"
                                                    value="yes"
                                                    class="form-control show-related-explanation"
                                                    data-related-explanation="2"
                                                    required>
                                            <label for="radio-yes-hasPendingCharges">Yes</label>
                                        </div>
                                        <div class="checkbox-wrapper">
                                            <input  type="radio"
                                                    name="hasPendingCharges"
                                                    id="radio-no-hasPendingCharges"
                                                    value="no"
                                                    class="form-control hide-related-explanation"
                                                    data-related-explanation="2">
                                            <label for="radio-no-hasPendingCharges">No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="checkbox-description">
                                            Has any adjudication ever been withheld?
                                        </p>
                                        <div class="checkbox-wrapper">
                                            <input  type="radio"
                                                    name="hasAdjudication"
                                                    id="radio-yes-hasAdjudication"
                                                    value="yes"
                                                    class="form-control show-related-explanation"
                                                    data-related-explanation="2"
                                                    required>
                                            <label for="radio-yes-hasAdjudication">Yes</label>
                                        </div>
                                        <div class="checkbox-wrapper">
                                            <input  type="radio"
                                                    name="hasAdjudication"
                                                    id="radio-no-hasAdjudication"
                                                    value="no"
                                                    class="form-control hide-related-explanation"
                                                    data-related-explanation="2">
                                            <label for="radio-no-hasAdjudication">No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <textarea   name="criminalList"
                                                    class="form-control related-explanation"
                                                    placeholder="If you answered yes to any of the preceding questions, please give dates and details"
                                                    style="display:none;"
                                                    data-related-explanation="2"></textarea>
                                    </div>

                                    <p class="disclaimer"><b>NOTE:</b> Answering “yes” to the preceding questions does not constitute an automatic bar to employment.  Factors such as age and time of the offense, seriousness and nature of the violation, and rehabilitation will be taken into account.  Do not include minor traffic infractions, and convictions for which the record has been sealed or expunged, any conviction for which probation has been successfully completed or otherwise discharged and the case has been judicially dismissed, referrals to and participation in any pretrial or post trial diversion programs, and misdemeanor marijuana-related offenses that occurred over two years ago in answering these questions.</p>

                                    <hr class="line grey">
                                    <div class="info-block">
                                        <h2>OTHER INFORMATION</h2>
                                    </div>

                                    <div class="form-group">
                                        <p><b>What is your philosophy in educating preschool children?</b></p>
                                        <textarea name="teachingPhilosophy" class="form-control"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <p><b>What is you experience with children? Indicate ages of children, duties, dates worked, reason for leaving. ( Include volunteer experience with children, if applicable)</b></p>
                                        <textarea name="experienceWithChildren" class="form-control"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <p><b>Please describe any other experience or skills that you have that you believe would be relevant to the job for which you are applying.</b></p>
                                        <textarea name="otherExperience" class="form-control"></textarea>
                                    </div>

                                    <?php if(strcmp($state, "GA") == 0) :?>
                                        <hr class="line grey">
                                        <div class="info-block">
                                            <h2>SPECIAL APPLICANT'S CERTIFICATION FOR GEORGIA CENTERS</h2>
                                            <p class="disclaimer">In addition to those statements contained on the Applicant’s Statement portion of our Application for Employment, applicants are required by state laws and regulations to certify to certain facts before being considered for employment in a child care facility. Therefore, pursuant to the Georgia Department of Early Care and Learning, Rules for Child Care Learning, Chapter 591-1-1 et. al., and Rules and Regulations for Group Day Care Homes, Chapter 290-2-1, I, certify, under penalty of perjury, to the following statements</p>
                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="georgiaAgreement1"
                                                    id="checkbox-ga-agree1"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ga-agree1">I am not suffering from any physical handicap or mental health disorder that would interfere with the my ability to provide child care and perform my job duties, with or without reasonable accommodation.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="georgiaAgreement2"
                                                    id="checkbox-ga-agree2"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ga-agree2">I have never abused, neglected, or deprived a child, adult, or subjected any person to a serious injury as a result of intentional or grossly negligent misconduct. </label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="georgiaAgreement3"
                                                    id="checkbox-ga-agree3"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ga-agree3">I do not have a criminal record other than minor traffic violations</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="georgiaAgreement4"
                                                    id="checkbox-ga-agree4"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ga-agree4">I consent to a criminal records check.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="georgiaAgreement5"
                                                    id="checkbox-ga-agree5"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ga-agree5">I have not made any material false statements either to the Georgia Department of Early Care and Learning or the licensees of a child care facility.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="georgiaAgreement6"
                                                    id="checkbox-ga-agree6"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ga-agree6">I do not currently use illegal drugs.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="georgiaAgreement7"
                                                    id="checkbox-ga-agree7"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ga-agree7">I am not listed on any sex offender registry as a sex offender.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="georgiaAgreement8"
                                                    id="checkbox-ga-agree8"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ga-agree8">I will comply with all state requirements for initial certification and continuing certification.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="georgiaAgreement9"
                                                    id="checkbox-ga-agree9"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ga-agree9">I will comply with the Center’s standards of regular attendance.</label>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php if(strcmp($state, "FL") == 0) :?>
                                        <hr class="line grey">
                                        <div class="info-block">
                                        <h2>SPECIAL APPLICANT'S CERTIFICATION FOR FLORIDA CENTERS</h2>
                                        <p class="disclaimer">In addition to those statements contained on the Applicant’s Statement portion of our Application for Employment, applicants are required by state laws and regulations to certify to certain facts before being considered for employment in a child care facility. Therefore, pursuant to the Florida Department of Children and Families, Chapter 65C-22 of the Florida Administrative Code Child Care Statutes, and Florida Statutes Sections 402.26 - 402.319, I, certify, under penalty of perjury, to the following statements</p>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="floridaAgreement1"
                                                    id="checkbox-fl-agree1"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-fl-agree1">I have never worked in a child care facility that had its license denied, revoked, or suspended in any state or jurisdiction.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="floridaAgreement2"
                                                    id="checkbox-fl-agree2"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-fl-agree2">I have never been the subject of a disciplinary action or been fined while employed in a child care facility.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="floridaAgreement3"
                                                    id="checkbox-fl-agree3"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-fl-agree3">I am in good physical and emotional health with no physical or mental conditions which would interfere with child care responsibilities, with or without reasonable accommodation.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="floridaAgreement4"
                                                    id="checkbox-fl-agree4"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-fl-agree4">I do not have a criminal record other than minor traffic violations.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="floridaAgreement5"
                                                    id="checkbox-fl-agree5"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-fl-agree5">I have never abused, neglected, or deprived a child, adult, or subjected any person to a serious injury as a result of intentional or grossly negligent misconduct.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="floridaAgreement6"
                                                    id="checkbox-fl-agree6"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-fl-agree6">I consent to a criminal background check and screening for child abuse and neglect.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="floridaAgreement7"
                                                    id="checkbox-fl-agree7"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-fl-agree7">I am not listed on any sex offender registry as a sex offender.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="floridaAgreement8"
                                                    id="checkbox-fl-agree8"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-fl-agree8">I will comply with all state requirements for initial certification and continuing certification, including the successful completion of the Department of Children and Family Services’ 40 hour Introductory Child Care Training, as evidenced by successful completion of a competency examination.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="floridaAgreement9"
                                                    id="checkbox-fl-agree9"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-fl-agree9">I will comply with the Center’s standards of regular attendance.</label>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php if(strcmp($state, "MS") == 0) :?>
                                        <hr class="line grey">
                                        <div class="info-block">
                                        <h2>SPECIAL APPLICANT'S CERTIFICATION FOR MISSISSIPPI CENTERS</h2>
                                        <p class="disclaimer">In addition to those statements contained on the Applicant’s Statement portion of our Application for Employment, applicants are required by state laws and regulations to certify to certain facts before being considered for employment in a child care facility. Therefore, pursuant  to the Mississippi State Department of Health, Division of Child Care, Regulations Governing Licensure of Child Care Facilities, I certify to the following:</p>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="mississippiAgreement1"
                                                    id="checkbox-ms-agree1"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ms-agree1">I am in good physical and emotional health with no physical or mental conditions which would interfere with child care responsibilities.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="mississippiAgreement2"
                                                    id="checkbox-ms-agree2"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ms-agree2">I do not have a criminal record other than a minor traffic violation.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="mississippiAgreement3"
                                                    id="checkbox-ms-agree3"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ms-agree3">I have never abused, neglected, or deprived a child, adult, or subjected any person to a serious injury as a result of intentional or grossly negligent misconduct. </label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="mississippiAgreement4"
                                                    id="checkbox-ms-agree4"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ms-agree4">I consent to a criminal background check (including submitting a fingerprint sample), and a review of the  child abuse central registry and sex offender record.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="mississippiAgreement5"
                                                    id="checkbox-ms-agree5"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ms-agree5">I am not listed on any sex offender registry as a sex offender.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="mississippiAgreement6"
                                                    id="checkbox-ms-agree6"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ms-agree6">I will comply with all state requirements for initial certification and continuing certification.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="mississippiAgreement7"
                                                    id="checkbox-ms-agree7"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ms-agree7">I am not listed on any sex offender registry as a sex offender.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="mississippiAgreement8"
                                                    id="checkbox-ms-agree8"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-ms-agree8">I will comply with the Center’s standards of regular attendance.</label>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php if(strcmp($state, "TX") == 0) :?>
                                        <hr class="line grey">
                                        <div class="info-block">
                                        <h2>SPECIAL APPLICANT'S CERTIFICATION FOR TEXAS CENTERS</h2>
                                        <p class="disclaimer">In addition to those statements contained on the Applicant’s Statement portion of our Application for Employment, applicants are required by state laws and regulations to certify to certain facts before being considered for employment in a child care facility. Therefore, pursuant  to the Mississippi State Department of Health, Division of Child Care, Regulations Governing Licensure of Child Care Facilities, I certify to the following:</p>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="texasAgreement1"
                                                    id="checkbox-tx-agree1"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-tx-agree1">I am in good physical and emotional health with no physical or mental conditions which would interfere with child care responsibilities.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="texasAgreement2"
                                                    id="checkbox-tx-agree2"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-tx-agree2">I do not have a criminal record other than a minor traffic violation.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="texasAgreement3"
                                                    id="checkbox-tx-agree3"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-tx-agree3">I have never abused, neglected, or deprived a child, adult, or subjected any person to a serious injury as a result of intentional or grossly negligent misconduct. </label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="texasAgreement4"
                                                    id="checkbox-tx-agree4"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-tx-agree4">I consent to a criminal background check (including submitting a fingerprint sample), and a review of the  child abuse central registry and sex offender record.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="texasAgreement5"
                                                    id="checkbox-tx-agree5"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-tx-agree5">I am not listed on any sex offender registry as a sex offender.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="texasAgreement6"
                                                    id="checkbox-tx-agree6"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-tx-agree6">I will comply with all state requirements for initial certification and continuing certification.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="texasAgreement7"
                                                    id="checkbox-tx-agree7"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-tx-agree7">I am not listed on any sex offender registry as a sex offender.</label>
                                        </div>

                                        <div class="checkbox-wrapper tall">
                                            <input  type="checkbox"
                                                    name="texasAgreement8"
                                                    id="checkbox-tx-agree8"
                                                    value="agree"
                                                    class="form-control"
                                                    required>
                                            <label for="checkbox-tx-agree8">I will comply with the Center’s standards of regular attendance.</label>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php
                                    wp_reset_query();
                                    $type = 'krk_app_document';
                                    $args = array(
                                    'post_type' => $type,
                                    'post_status' => 'publish',
                                    'order' => 'DESC',
                                    'posts_per_page' => -1);

                                    $query = null;
                                    $query = new WP_Query($args);
                                    if ($query->have_posts()) : ?>
                                        <hr class="line grey">
                                        <div class="info-block">
                                            <h2>ADDITIONAL DOCUMENTS</h2>
                                            <p class="disclaimer">Please fill out these additional form(s) and submit them along with this form.</p>
                                        </div>
                                        <div class="text-container">
                                            <ul>
                                                <?php while ($query->have_posts()):
                                                    $query->the_post();
                                                    $description = get_post_meta(get_the_ID(), 'krk_app_document_description', true);
                                                    $attachment = get_post_meta(get_the_ID(), 'krk_app_document_attachment', true);
                                                    $link_text = get_post_meta(get_the_ID(), 'krk_app_document_link_text', true);
                                                    ?>
                                                    <li>
                                                        <a href="<?php echo $attachment ?>"><?php if (empty($link_text)) the_title(); else echo $link_text; ?></a>
                                                        <p><?php echo $description; ?></p>
                                                    </li>
                                                    <?php
                                                endwhile;
                                                ?>
                                            </ul>
                                            <h3>Upload Completed Files</h3>
                                            <input id="uploadFile" type="file" name="file">
                                        </div>
                                    <?php endif; ?>
                                    <button id="submit-button" type="submit" class="btn btn-primary">SUBMIT<span class="icon-arrow-1"></span></button>
                                </fieldset>
                            </form>
                            <br />
                            <br />
                        </div>
                    </div>
                </div>
            </div>
            <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
        </div>

        <script type="text/template" id="employer-template" style="display: none;">                              
            <div class="form-box">
                <h3>EMPLOYER <span class="employer-num">#</span></h3>
                <div class="form-group">
                    <input type="text" class="form-control" name="EmployerName" placeholder="Company Name" required>
                </div>
                <div class="form-group">
                	<input type="tel"
                           class="form-control"
                           name="EmployerPhone"
                           placeholder="Phone Number (XXX) XXX-XXXX"
                           data-valiation-type="phone"
                           data-display-name="Employer Phone Number"
                           required>
                </div>
            </div>
            <div class="form-box">
               <div class="form-group">
                  	<input type="text" class="form-control" name="EmployerAddress" placeholder="Street Address">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="EmployerCity" placeholder="City">
                </div>
                <div class="form-group">
                        <select name="EmployerState">
                            <option class="hidden">State</option>
                            <option>AL</option>
                            <option>AK</option>
                            <option>AZ</option>
                            <option>AR</option>
                            <option>CA</option>
                            <option>CO</option>
                            <option>CT</option>
                            <option>DE</option>
                            <option>FL</option>
                            <option>GA</option>
                            <option>HI</option>
                            <option>ID</option>
                            <option>IL</option>
                            <option>IN</option>
                            <option>IA</option>
                            <option>KS</option>
                            <option>KY</option>
                            <option>LA</option>
                            <option>ME</option>
                            <option>MD</option>
                            <option>MA</option>
                            <option>MI</option>
                            <option>MN</option>
                            <option>MS</option>
                            <option>MO</option>
                            <option>MT</option>
                            <option>NE</option>
                            <option>NV</option>
                            <option>NH</option>
                            <option>NJ</option>
                            <option>NM</option>
                            <option>NY</option>
                            <option>NC</option>
                            <option>ND</option>
                            <option>OH</option>
                            <option>OK</option>
                            <option>OR</option>
                            <option>PA</option>
                            <option>RI</option>
                            <option>SC</option>
                            <option>SD</option>
                            <option>TN</option>
                            <option>TX</option>
                            <option>UT</option>
                            <option>VT</option>
                            <option>VA</option>
                            <option>WA</option>
                            <option>WV</option>
                            <option>WI</option>
                            <option>WY</option>
                            <option>DC</option>
                        </select>
                </div>
               	<div class="form-group">
                    <input type="number" name="EmployerZip" class="form-control" placeholder="Zip Code">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="EmployerSupervisor" placeholder="Supervisor Name And Title">
                </div>
            </div>
            <div class="form-box">
                <h4>Dates Employed</h4>
                <div class="form-group small">
                    <input type="date" name="EmployedFrom" class="form-control" placeholder="From Date mm/dd/yyyy" title="To Date mm/dd/yyyy" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" required>
                </div>
                <div class="form-group small">
                    <input type="date" name="EmployedTo" class="form-control" placeholder="To Date mm/dd/yyyy" title="To Date mm/dd/yyyy" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" required>
                </div>
            </div>
            <div class="form-box">
                <h4>Hourly Rate / Salary</h4>
                <div class="form-group small">
                    <input type="text" name="SalaryStarting" class="form-control" placeholder="Starting" required>
                </div>
                <div class="form-group small">
                    <input type="text" name="SalaryEnding" class="form-control" placeholder="Final" required>
                </div>
            </div>                     
            <div class="form-box">
                <div class="form-group">
                    <textarea name="WorkPerformed" placeholder="Work Performed" class="form-control"></textarea>
                </div>
                 <div class="form-group">
                    <input type="text" class="form-control" name="EmployerReasonForLeaving" placeholder="Reason For Leaving" required>
                </div>
            </div>
            <hr>
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
            	var employerTemplate = $('#employer-template');
                var stateOptionsTemplate = $('#state-options');

                $('#submit-button').click(function(){
                    var uploadFile = $('#uploadFile').val();
                    if(uploadFile != undefined  && uploadFile == '') {
                        $('#uploadFile').focus();
                        alert('Please upload completed application documents');
                        return false;
                    }
                });

            	var prevEmployers =  prevEmployers || 0;
            	$('.btn.add-employer').click(function(){

            		prevEmployers++;
            		var template = $(employerTemplate.clone().html());
                    template.find('.employer-num').text(prevEmployers);

            		template.find('input, select, textarea').each(function(i, input){
            			var name = $(input).attr('name');
            			$(input).attr('name', name+prevEmployers);
            		});

            		$('#employer-wrapper').append(template);
                    jcf.replaceAll();
            	});

                $('input[type="radio"]#radio-other').click(function(){
                    $('input#desiredPosition-other').focus();
                });

                $('input#desiredPosition-other').bind('keyup change', function(e){
                    $('input[type="radio"]#radio-other').val($(this).val());
                });

                //toggle explanations
                $('input.show-explanation').click(function(){
                    var nextExplanation = $(this).parent().parent().next().find('.explanation');
                    nextExplanation.show();
                    nextExplanation.prop('required', true);
                });

                 $('input.hide-explanation').click(function(){
                    var nextExplanation = $(this).parent().parent().next().find('.explanation');
                    nextExplanation.hide();
                    nextExplanation.val('');
                    nextExplanation.removeAttr('required');
                });

                //toggle related explanation. Show if any are true, hide if all are false
                $('input.show-related-explanation').click(function(){
                    var explanationId = $(this).data('related-explanation');
                    var relatedExplanation = $('textarea.related-explanation[data-related-explanation="'+explanationId+'"]');
                    relatedExplanation.show();
                    relatedExplanation.prop('required', true);
                });

                $('input.hide-related-explanation').click(function(){
                    //get all hide related explanation
                    var explanationId = $(this).data('related-explanation');
                    
                    var inputs = $('input.hide-related-explanation[data-related-explanation="'+explanationId+'"]');
                    for(var i=0; i<inputs.length; i++) {
                        if(!$(inputs[i]).is(':checked')) {
                            return;
                        }
                    }
                    
                    var relatedExplanation = $('textarea.related-explanation[data-related-explanation="'+explanationId+'"]');
                    relatedExplanation.hide();
                    relatedExplanation.val('');
                    relatedExplanation.removeAttr('required');
                });

                $('input.show-followup').click(function(){
                    var followup = $(this).parent().parent().next();
                    followup.show();
                    followup.find('input').prop('required', true);
                });

                $('input.hide-followup').click(function(){
                    var followup = $(this).parent().parent().next();
                    followup.hide();
                    followup.find('input').val('');
                    followup.find('input').removeAttr('required');
                });
            });
        </script>

<?php
    get_footer();
?>
