<?php

//load other functional scripts
require_once(get_template_directory() . '/functions/init-functions.php');
require_once(get_template_directory() . '/functions/meta-fields-functions.php');
require_once(get_template_directory() . '/functions/customizer-functions.php');
require_once(get_template_directory() . '/functions/event-functions.php');
require_once(get_template_directory() . '/functions/additional-page-functions.php');
require_once(get_template_directory() . '/functions/menu-functions.php');
require_once(get_template_directory() . '/functions/monthly-calendar-functions.php');
require_once(get_template_directory() . '/functions/custom-nav-walker.class.php');
require_once(get_template_directory() . '/functions/nav-menu-functions.php');

/*function meals_post_links() {
	//$blogs = wp_get_sites ();
	//foreach ($blogs as $blog) {
    	//switch_to_blog($blog["blog_id"]);

    	$type = 'krk_meal';
    	$args = array(
    	    'post_type' => $type,
    	    'post_status' => 'publish',
    	    'orderby' => 'parent meal_order',
    	    'order' => 'ASC',
    	    'posts_per_page' => -1);

    	 $query_meal = null;
    	 $query_meal = new WP_Query($args);

    	if ($query_meal->have_posts()){

    	    while ($query_meal->have_posts()){
    	        $query_meal->the_post();
                if(get_the_title() == 'Complete, balanced meals'){
                    wp_delete_post(get_the_ID());
                    continue;
                }
    	        $content_meal = get_the_content();
    	        $updated_content = $content_meal;

                $breakfast = 'Check out our menu <a href="#pdf-menu">HERE</a> to see what\'s for breakfast today.';
                $break_alt = 'Check out our menu <a href="#">HERE</a> to see what\'s for breakfast today.';

                $m_snack = 'Find our menu <a href="#pdf-menu">HERE</a> to see what\'s for morning snack today.';
                $m_snack_alt = 'Find our menu <a href="#">HERE</a> to see what\'s for morning snack today.';

                $lunch = 'Find our menu <a href="#pdf-menu">HERE</a> to see what\'s for morning snack today.';
                $lunch_alt = 'Find our menu <a href="#">HERE</a> to see what\'s for morning snack today.';

                $a_snack = 'See our menu <a href="#pdf-menu">HERE</a> for today\'s afternoon snack.';
                $a_snack_alt = 'See our menu <a href="#">HERE</a> for today\'s afternoon snack.';

                $has_breakfast_link = strpos($content_meal, $breakfast);
                $has_break_alt_link = strpos($content_meal, $break_alt);

                $has_m_snack_link = strpos($content_meal, $m_snack);
                $has_m_snack_alt_link = strpos($content_meal, $m_snack_alt);

                $has_lunch_link = strpos($content_meal, $lunch);
                $has_lunch_alt_link = strpos($content_meal, $lunch_alt);

                $has_a_snack_link = strpos($content_meal, $a_snack);
                $has_a_snack_alt_link = strpos($content_meal, $a_snack_alt);

                if($has_breakfast_link != false) {
                    $updated_content = substr($content_meal, 0, $has_breakfast_link);
                    update_post_meta(get_the_ID(), 'krk_meal_has_menu_link', 'on');
                }
                if($has_break_alt_link != false) {
                    $updated_content = substr($content_meal, 0, $has_break_alt_link);
                    update_post_meta(get_the_ID(), 'krk_meal_has_menu_link', 'on');
                }
                if($has_lunch_link != false) {
                    $updated_content = substr($content_meal, 0, $has_lunch_link);
                    update_post_meta(get_the_ID(), 'krk_meal_has_menu_link', 'on');
                }
                if($has_lunch_alt_link != false) {
                    $updated_content = substr($content_meal, 0, $has_lunch_alt_link);
                    update_post_meta(get_the_ID(), 'krk_meal_has_menu_link', 'on');
                }
                if($has_m_snack_link != false) {
                    $updated_content = substr($content_meal, 0, $has_m_snack_link);
                    update_post_meta(get_the_ID(), 'krk_meal_has_menu_link', 'on');
                }
                if($has_m_snack_alt_link != false) {
                    $updated_content = substr($content_meal, 0, $has_m_snack_alt_link);
                    update_post_meta(get_the_ID(), 'krk_meal_has_menu_link', 'on');
                }
                if($has_a_snack_link != false) {
                    $updated_content = substr($content_meal, 0, $has_a_snack_link);
                    update_post_meta(get_the_ID(), 'krk_meal_has_menu_link', 'on');
                }
                if($has_m_snack_alt_link != false) {
                    $updated_content = substr($content_meal, 0, $has_m_snack_alt_link);
                    update_post_meta(get_the_ID(), 'krk_meal_has_menu_link', 'on');
                }


    	        $my_post = array(
    	  			'ID'           => get_the_ID(),
    	  			'post_title'   => get_the_title(),
    	  			'post_content' => $updated_content,
  				);


				wp_update_post($my_post);
                update_post_meta(get_the_ID(), 'krk_meal_has_menu_link', 'on');
    		}
    	}
    	wp_reset_query();
	//}
	//restore_current_blog();
}
add_action('admin_init', 'meals_post_links');*/

add_filter( 'nav_menu_link_attributes', 'cfw_add_data_atts_to_nav', 10, 4 );
function cfw_add_data_atts_to_nav( $atts, $item, $args ) {
    if($item->url == '#') {
        $atts['data-toggle'] = 'dropdown';
    }
    return $atts;
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) && $item->menu_item_parent == 0 ){
            $classes[] = 'active ';
    }
    if($item->url == '#') {
        $classes[]='dropdown';
    }
    return $classes;
}

if (! function_exists('has_published_posts_with_type')) {
    function has_published_posts_with_type($type) {
        wp_reset_query();
        $type = $type;
        $args=array(
            'post_type' => $type,
            'post_status' => 'publish',
            'order' => 'DESC',
            'posts_per_page' => -1);

        $query = null;
        $query = new WP_Query($args);
        return ($query->have_posts());
    }
}

if ( ! function_exists( 'krkfranchise_setup' ) ) :
    function krkfranchise_setup() {

        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on krkfranchise, use a find and replace
         * to change 'krkfranchise' to the name of your theme in all the template files
         */
        load_theme_textdomain( 'krkfranchise', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 825, 510, true );

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus( array(
            'primary' => __( 'Primary Menu',      'krkfranchise' ),
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
        ) );

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support( 'post-formats', array(
            'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
        ) );

        /*
         * This theme styles the visual editor to resemble the theme style,
         * specifically font, colors, icons, and column width.
         */
        add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css' ) );
    }
endif; // krkfranchise_setup
add_action( 'after_setup_theme', 'krkfranchise_setup' );

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Fifteen 1.1
 */
function krkfranchise_javascript_detection() {
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'krkfranchise_javascript_detection', 0 );

function media_to_editor($html, $send_id, $attachment ){
    if(wp_attachment_is_image($send_id)){
        return $html;
    }
    $attachment_url = wp_get_attachment_url($send_id);
    return $attachment_url;
}

add_filter('media_send_to_editor', 'media_to_editor', 10, 10);

function krkfranchise_scripts() {

    // Add Genericons, used in the main stylesheet.
    wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

    // Load our main stylesheet.
    wp_enqueue_style( 'bootstrap-extended-style', get_template_directory_uri() . '/css/bootstrap-extended.css', array());
    wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.css', array());
    wp_enqueue_style( 'fullcalendar-style', get_template_directory_uri() . '/css/fullcalendar.css', array());
    wp_enqueue_style( 'fullcalendar-extended-style', get_template_directory_uri() . '/css/fullcalendar-extended.css', array());
    wp_enqueue_style( 'featherlight-style', get_template_directory_uri() . '/css/featherlight.css', array());
    wp_enqueue_style( 'krkfranchise-style', get_stylesheet_uri() );

    //Load our js
    wp_enqueue_script( 'krkfranchise-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array());
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array());
    wp_enqueue_script( 'jquery-main', get_template_directory_uri() . '/js/jquery.main.js', array());
    wp_enqueue_script( 'jquerydotdotdot', get_template_directory_uri() . '/js/jquery.dotdotdot.js', array());
    wp_enqueue_script( 'moment', get_template_directory_uri() . '/js/moment.min.js', array());
    wp_enqueue_script( 'fullcalendar', get_template_directory_uri() . '/js/fullcalendar.min.js', array());
    wp_enqueue_script( 'featherlight', get_template_directory_uri() . '/js/featherlight.js', array());
    wp_enqueue_script( 'form-validator', get_template_directory_uri() . '/js/form-validator.js', array());
    wp_enqueue_script( 'form-submitter', get_template_directory_uri() . '/js/form-submitter.js', array());
    wp_enqueue_script( 'functions', get_template_directory_uri() . '/js/functions.js', array());
}
add_action( 'wp_enqueue_scripts', 'krkfranchise_scripts' );

function krkfranchise_admin_scripts() {
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');
    wp_enqueue_script( 'jquery-ui-datepicker' );
    wp_enqueue_style( 'datepicker-style', get_template_directory_uri() . '/css/datepicker.css', array());
    wp_enqueue_script( 'myDatepicker', get_template_directory_uri() . '/js/myDatepicker.js', array());
}
add_action('admin_init', 'krkfranchise_admin_scripts');

function krkfranchise_customizer_scripts() {
    // Add Genericons, used in the main stylesheet.
    wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

    // Load our stylesheets.
    wp_enqueue_style( 'bootstrap-extended-style', get_template_directory_uri() . '/css/bootstrap-extended.css', array());
    wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.css', array());
    wp_enqueue_style( 'bootstrap-wyishtml5-style', get_template_directory_uri() . '/css/bootstrap3-wysihtml5.min.css', array());
    wp_enqueue_style( 'theme-customizer-style', get_template_directory_uri() . '/css/theme-customizer.css', array());

    //Load our js
    wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.css', array());
    wp_enqueue_script( 'jquery-main', get_template_directory_uri() . '/js/jquery.main.js', array());
    wp_enqueue_script( 'jquerydotdotdot', get_template_directory_uri() . '/js/jquery.dotdotdot.js', array());
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array());
    wp_enqueue_script( 'bootstrap-wysihtml5', get_template_directory_uri() . '/js/bootstrap3-wysihtml5.all.min.js', array('jquery'));
    wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js', array('jquery'));
    wp_enqueue_script( 'theme-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array('jquery', 'bootstrap-wysihtml5' ,'customize-preview', 'jquery-ui'));

}
add_action( 'customize_controls_enqueue_scripts', 'krkfranchise_customizer_scripts' );