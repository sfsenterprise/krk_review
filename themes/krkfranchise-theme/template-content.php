<?php
/**
 * Template Name: KRK Content Page
 */


get_header();
    while(have_posts()): the_post();
        $content = get_the_content();
        $DOM = new DOMDocument();
        $DOM->LoadHTML($content);
        $sections = $DOM->getElementsByTagName('h4');
        error_log(print_r($sections->item(0)->textContent, true));
?>

<main id="main">
			<div class="main-holder">
				<div class="breadcrumbs-wrap">
					<div class="container-fluid">
						<div class="row">
							<div class="col-sm-12">
								<ol class="breadcrumb">
                                    <br/>
								</ol>
								<div class="title-page">
									<h1><?php the_title(); ?></h1>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="twocolumns" class="battlement">
					<div class="container-fluid">
						<div class="row">
							<div class="col-sm-3">
								<nav class="menu-wrap">
									<ul class="sub-menu">
										<?php
                                            foreach($sections as $section) {
                                                echo "<li><a href=' #". $section->getAttribute('id') . "'>". $section->textContent."</a></li>";
                                            }
                                        ?>
									</ul>
								</nav>
							</div>
							<div class="col-sm-9">
								<div class="visual-img">
                                    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
									<img src="<?php echo $image[0]?>" height="299" width="823" alt="image description">
									<div class="text">
										<strong><?php the_title(); ?></strong>
									</div>
								</div>
								<?php
                                    the_content();
                                ?>
							</div>
						</div>
					</div>
				</div>
				<a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
			</div>
			<div class="box-tour visible-xs">
				<strong class="title">Come and See How We Hold the Future™!</strong>
				<p>Children of all ages are welcome!</p>
				<a href="#" class="button-tour">SCHEDULE A TOUR <span class="ico"></span></a>
			</div>
		</main>


<?php
    endwhile;
get_footer();

?>