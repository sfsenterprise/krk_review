<?php

$krk_document_meta_fields = array(
    array(
        'label' => 'File Attachment',
        'desc'  => 'Upload or link to file. Select <b>File Url</b> for local media',
        'id'    => 'krk_document_attachment',
        'type'  => 'upload'
    ),

    array(
        'label' => 'Link Text',
        'desc'  => 'text of file link',
        'id'    => 'krk_document_link_text',
        'type'  => 'text'
    ),
    array(
        'label' => 'Description',
        'desc'  => 'document description',
        'id'    => 'krk_document_description',
        'type'  => 'textarea'
    )
);

function register_krk_document_meta_box(){
    add_meta_box(
        'krk_document_options', // $id
        'Document Options', // $title
        'show_krk_document_meta_box', // $callback
        'krk_document', // $page
        'normal', // $context
        'high'); // $priority
}
add_action('add_meta_boxes', 'register_krk_document_meta_box');



function show_krk_document_meta_box(){

    global $krk_document_meta_fields, $post;
    // Use nonce for verification
    echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';

    echo '<table class="form-table">';
    foreach ($krk_document_meta_fields as $field) {
        $meta = get_post_meta($post->ID, $field['id'], true);
        echo '<tr>
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
                <td>';
        switch($field['type']) {
            case 'text':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
                    <br /><span class="description">'.$field['desc'].'</span>';
                break;
            case 'textarea':
                echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" style="width: 350px;" rows="4" >'.$meta.'</textarea>
                    <br /><span class="description">'.$field['desc'].'</span>';
                break;
            case 'upload':
                echo  '<input type="text" id="'. $field['id'] .'" size="60" name="'. $field['id'].'" value="'. $meta .'" />
                    <input type="button" id="upload-file-button"  class="button" value="Upload file" />
                    <br /><span class="description">'.$field['desc'].'</span>';
                break;
        }
        echo '</td></tr>';
    }
    echo '</table>';
}

function save_krk_document_meta($post_id) {
    global $krk_document_meta_fields;
    // verify nonce
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
        return $post_id;
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }
    foreach ($krk_document_meta_fields as $field) {

        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}
add_action('save_post', 'save_krk_document_meta');

function krk_document_metabox_script() {

    $screen = get_current_screen();

    // Make sure we are on the Download screen
    if ( isset( $screen->post_type ) && $screen->post_type == 'krk_document' ) : ?>

        <script type="text/javascript">
            jQuery(document).ready(function($){

                var formfield;

                function extractUri(text) {
                    var uri_pattern = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/ig;

                    return text.match(uri_pattern)[0];
                }

                // Open upload window
                $('#upload-file-button').click(function() {
                    formfield = $('#krk_document_attachment').attr('name');
                    tb_show( '','media-upload.php?type=file&TB_iframe=true' );

                    return false;
                });

                window.original_send_to_editor = window.send_to_editor;

                window.send_to_editor = function(html) {
                     var uri = extractUri(html);
                    if (formfield) {
                        $('#krk_document_attachment').val(uri);
                        tb_remove();
                    } else {
                        window.original_send_to_editor(uri);
                        tb_remove();
                    }
                };
            });
        </script>

    <?php
    endif;
}

add_action( 'admin_footer', 'krk_document_metabox_script' );
?>