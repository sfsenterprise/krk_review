<?php
class KRK_Testimonial_Meta_Fields extends KRK_Meta_Fields {

    public function __construct() {
        parent::__construct();
    }

    public function get_meta_box_data(){
        return array(
            'id' => 'krk_testimonial_options',
            'title' => 'Testimonial Options',
            'page' => 'krk_testimonial',
            'context' => 'normal',
            'priority' => 'high',
        );
    }

    public function get_meta_fields(){
        return array(
            array(
                'label' => 'Name',
                'desc'  => 'The name of the quoted individual',
                'id'    => 'krk_testimonial_name',
                'type'  => 'text',
                'max-length' => 15
            ),
            array(
                'label' => 'Title',
                'desc'  => 'The title of the quoted individual',
                'id'    => 'krk_testimonial_title',
                'type'  => 'text',
                'max-length' => 15
            ),
            array(
                'label' => 'Source',
                'desc'  => 'The source of the quote.',
                'id'    => 'krk_testimonial_source',
                'type'  => 'select',
                'options' => array (
                    'one' => array (
                        'label' => 'Facebook',
                        'value' => 'facebook'
                    ),
                    'two' => array (
                        'label' => 'Twitter',
                        'value' => 'twitter'
                    ),
                    'three' => array (
                        'label' => 'Google Plus',
                        'value' => 'google'
                    ),
                    'four' => array (
                        'label' => 'Other',
                        'value' => 'other'
                    )
                )
            )
        );
    }
}
new KRK_Testimonial_Meta_Fields();
?>