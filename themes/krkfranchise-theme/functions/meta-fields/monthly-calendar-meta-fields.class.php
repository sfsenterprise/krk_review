<?php
class KRK_Monthly_Calendar_Meta_Fields extends KRK_Meta_Fields {

    public function __construct() {
        parent::__construct();
    }

    public function get_meta_box_data(){
        return array(
            'id' => 'krk_monthly_calendar_options',
            'title' => 'Monthly Calendar Options',
            'page' => 'krk_monthly_calendar',
            'context' => 'normal',
            'priority' => 'high',
        );
    }

    public function get_meta_fields(){
        return array(
            array(
                'label' => 'File Attachment',
                'desc'  => 'Upload or link to file. Select <b>File Url</b> for local media',
                'id'    => 'krk_monthly_calendar_attachment',
                'type'  => 'upload'
            ),

            array(
                'label' => 'Link Text',
                'desc'  => 'text of file link',
                'id'    => 'krk_monthly_calendar_link_text',
                'type'  => 'text'
            ),
            array(
                'label' => 'Active',
                'desc'  => 'Check this box to display the calendar on the website.',
                'id'    => 'krk_monthly_calendar_active',
                'type'  => 'checkbox'
            )
        );
    }

    public function save_meta($post_id) {
        $fields = $this->get_meta_fields();

        // check autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post_id;
        // check permissions
        if ('page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id))
                return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }
        foreach ($fields as $field) {
            $old = get_post_meta($post_id, $field['id'], true);
            $new = $_POST[$field['id']];

            if ($new && $new != $old) {
                update_post_meta($post_id, $field['id'], $new);
            } elseif ('' == $new && $old) {
                delete_post_meta($post_id, $field['id'], $old);
            }
        }

        $status = get_post_meta($post_id, 'krk_monthly_calendar_active', 'false');
        if ($status === 'on') {
            // disable all other calendars
            $posts = query_posts('post_type=krk_monthly_calendar');
            foreach( $posts as $post ) {
                if ($post->ID != $post_id) {
                    update_post_meta($post->ID, 'krk_monthly_calendar_active', 'false');
                }
            }
        }
    }
}
new KRK_Monthly_Calendar_Meta_Fields();
?>