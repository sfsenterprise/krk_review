<?php
class KRK_Meal_Meta_Fields extends KRK_Meta_Fields {

    public function __construct() {
        parent::__construct();
    }

    public function get_meta_box_data(){
        return array(
            'id' => 'krk_meal_options',
            'title' => 'Meal Options',
            'page' => 'krk_meal',
            'context' => 'normal',
            'priority' => 'high',
        );
    }

    public function get_meta_fields(){
        return array(
            array(
                'label' => 'Link to Menu',
                'desc'  => 'Check this box to add a link to the active food meal. An active meal is required.',
                'id'    => 'krk_meal_has_menu_link',
                'type'  => 'checkbox'
            )
        );
    }
}
new KRK_Meal_Meta_Fields();
?>