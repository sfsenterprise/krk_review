<?php
class KRK_Event_Meta_Fields extends KRK_Meta_Fields {

    public function __construct() {
        parent::__construct();
    }

    public function get_meta_box_data(){
        return array(
            'id' => 'krk_event_options',
            'title' => 'Event Options',
            'page' => 'krk_event',
            'context' => 'normal',
            'priority' => 'high',
        );
    }

    public function get_meta_fields(){
        return array(
            array(
                'label' => 'Start Date',
                'desc'  => 'The starting date of the event.',
                'id'    => 'krk_event_date',
                'type'  => 'date',
                'required' => 'required'
            ),
            array(
                'label' => 'End Date',
                'desc'  => 'The ending date of the event. Leave blank for same day',
                'id'    => 'krk_event_end_date',
                'type'  => 'date'
            )
        );
    }
}
new KRK_Event_Meta_Fields();
?>