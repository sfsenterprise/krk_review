<?php
    class KRK_Application_Document_Meta_Fields extends KRK_Meta_Fields {

        public function __construct() {
            parent::__construct();
        }

        public function get_meta_box_data(){
            return array(
                'id' => 'krk_app_document_options',
                'title' => 'Applcation Document Options',
                'page' => 'krk_app_document',
                'context' => 'normal',
                'priority' => 'high',
            );
        }

        public function get_meta_fields(){
            return array(
                array(
                    'label' => 'File Attachment',
                    'desc'  => 'Upload or link to file.',
                    'id'    => 'krk_app_document_attachment',
                    'type'  => 'upload'
                ),

                array(
                    'label' => 'Link Text',
                    'desc'  => 'text of file link',
                    'id'    => 'krk_app_document_link_text',
                    'type'  => 'text'
                ),
                array(
                    'label' => 'Description',
                    'desc'  => 'document description',
                    'id'    => 'krk_app_document_description',
                    'type'  => 'textarea'
                )
            );
        }
    }
    new KRK_Application_Document_Meta_Fields();
?>