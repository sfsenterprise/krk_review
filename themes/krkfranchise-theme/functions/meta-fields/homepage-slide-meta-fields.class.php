<?php
class KRK_Homepage_Slide_Meta_Fields extends KRK_Meta_Fields {

    public function __construct() {
        parent::__construct();
       // add_action('save_post', array($this, 'require_option_thumbnail'));
        add_action('admin_notices', array($this, 'showAdminMessages'));
    }

    public function get_meta_box_data(){
        return array(
            'id' => 'krk_homepage_slide_options',
            'title' => 'Homepage Slide Options',
            'page' => 'krk_homepage_slide',
            'context' => 'normal',
            'priority' => 'high',
        );
    }

    public function get_meta_fields(){
        return array(
            array(
                'label' => 'Video Source (MP4 or Youtube Link)',
                'desc'  => 'Upload or link to youtube video. Leave blank for picture only',
                'id'    => 'krk_homepage_slide_video_source',
                'type'  => 'upload'
            )
        );
    }

    public function require_option_thumbnail($post_id) {
        $type = get_post_type($post_id);
        if ( !has_post_thumbnail( $post_id ) && $type == 'krk_homepage_slide') {
            // set a transient to show the users an admin message
            set_transient( "post_not_ok", "not_ok" );
            // unhook this function so it doesn't loop infinitely
            remove_action('save_post', array($this, 'require_option_thumbnail'));
            // update the post set it to draft
            wp_update_post(array('ID' => $post_id, 'post_status' => 'draft'));
            // re-hook this function
            add_action('save_post', 'f711_option_thumbnail');
        } else {
            delete_transient( "post_not_ok" );
        }
    }

    public function showAdminMessages()
    {
        // check if the transient is set, and display the error message
        if ( get_transient( "post_not_ok" ) == "not_ok" ) {
            // Shows as an error message. You could add a link to the right page if you wanted.
            $this->showMessage("Warning: You need to select a Featured Image. Your Post was not published.", true);
            // delete the transient to make sure it gets shown only once.
            delete_transient( "post_not_ok" );
        }

    }

    public function showMessage($message, $errormsg = false)
    {
        if ($errormsg) {
            echo '<div id="message" class="error">';
        }
        else {
            echo '<div id="message" class="updated fade">';
        }

        echo "<p><strong>$message</strong></p></div>";

    }
}
new KRK_Homepage_Slide_Meta_Fields();
?>