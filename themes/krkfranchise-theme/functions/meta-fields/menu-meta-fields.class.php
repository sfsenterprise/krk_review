<?php
class KRK_Menu_Meta_Fields extends KRK_Meta_Fields {

    public function __construct() {
        parent::__construct();
    }

    public function get_meta_box_data(){
        return array(
            'id' => 'krk_menu_options',
            'title' => 'Menu Options',
            'page' => 'krk_menu',
            'context' => 'normal',
            'priority' => 'high',
        );
    }

    public function get_meta_fields(){
        return array(
            array(
                'label' => 'File Attachment',
                'desc'  => 'Upload or link to file. Select <b>File Url</b> for local media',
                'id'    => 'krk_menu_attachment',
                'type'  => 'upload'
            ),
            array(
                'label' => 'Active',
                'desc'  => 'Check this box to display the menu on the website.',
                'id'    => 'krk_menu_active',
                'type'  => 'checkbox'
            )
        );
    }

    //Override
    function save_meta($post_id) {
        $fields = $this->get_meta_fields();

        // check autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post_id;
        // check permissions
        if (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }
        foreach ($fields as $field) {
            $old = get_post_meta($post_id, $field['id'], true);
            $new = $_POST[$field['id']];

            if ($new && $new != $old) {
                update_post_meta($post_id, $field['id'], $new);
            } elseif ('' == $new && $old) {
                delete_post_meta($post_id, $field['id'], $old);
            }
        }

        $status = get_post_meta($post_id, 'krk_menu_active', 'false');
        if ($status === 'on') {
            // disable all other ones
            $posts = query_posts('post_type=krk_menu');
            foreach( $posts as $post ) {
                if ($post->ID != $post_id) {
                    update_post_meta($post->ID, 'krk_menu_active', 'false');
                }
            }
        }
    }
}
new KRK_Menu_Meta_Fields();
?>