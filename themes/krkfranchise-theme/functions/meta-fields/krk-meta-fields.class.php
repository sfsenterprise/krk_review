<?php
    abstract class KRK_Meta_Fields {
        public abstract function get_meta_fields();
        public abstract function get_meta_box_data();

        function __construct()
        {
            add_action('add_meta_boxes', array($this, 'register_meta_box'));
            add_action('save_post', array($this, 'save_meta'));
            add_action('admin_footer', array($this, 'metabox_script'));
        }

        function register_meta_box(){
            $data = $this->get_meta_box_data();
            add_meta_box(
                $data['id'], // $id
                $data['title'], // $title
                array($this, 'show_meta_box'), // $callback
                $data['page'], // $page
                $data['context'], // $context
                $data['priority']); // $priority
        }

        function show_meta_box(){
            global $post;
            $fields = $this->get_meta_fields();

            // Use nonce for verification
            echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';

            echo '<table class="form-table">';
            foreach ($fields as $field) {
                $meta = get_post_meta($post->ID, $field['id'], true);
                echo '<tr>
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
                <td>';
                switch($field['type']) {
                    case 'text':
                        echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
                    <br /><span class="description">'.$field['desc'].'</span>';
                        break;
                    
                    case 'textarea':
                        echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" style="width: 350px;" rows="4" >'.$meta.'</textarea>
                    <br /><span class="description">'.$field['desc'].'</span>';
                        break;
                    
                    case 'upload':
                        echo  '<input type="text" id="'. $field['id'] .'" size="60" name="'. $field['id'].'" value="'. $meta .'" />
                    <input type="button" id="upload-file-button"  class="button" value="Upload file" />
                    <br /><span class="description">'.$field['desc'].'</span>';
                        break;

                    case 'checkbox':
                        echo '<input type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" ' .
                            ($meta === 'on' ? 'checked' : '') . '/>
                            <br /><span class="description">'.$field['desc'].'</span>';
                        break;

                    case 'date':
                        $date = $meta ? gmdate("m/d/Y", $meta) : '';
                        echo '<input type="text" class="datepicker" name="'.$field['id'].'" id="'.$field['id'].'" value="'.
                            $date .'" size="30" />
                        <br /><span class="description">'.$field['desc'].'</span>';
                        break;
                    
                    case 'select':
                        echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';
                        foreach ($field['options'] as $option) {
                            echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">' .
                                $option['label'] .
                                '</option>';
                        }
                        echo '</select><br /><span class="description">'.$field['desc'].'</span>';
                        break;
                }
                echo '</td></tr>';
            }
            echo '</table>';
        }

        function save_meta($post_id) {
            $fields = $this->get_meta_fields();

            // verify nonce
            if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
                return $post_id;
            // check autosave
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
                return $post_id;
            // check permissions
            if ('page' == $_POST['post_type']) {
                if (!current_user_can('edit_page', $post_id))
                    return $post_id;
            } elseif (!current_user_can('edit_post', $post_id)) {
                return $post_id;
            }
            foreach ($fields as $field) {

                $old = get_post_meta($post_id, $field['id'], true);
                $new = $_POST[$field['id']];

                //convert to unix timestamp
                if($field['type'] == 'date'){
                    $new = strtotime($new);
                }

                if ($new && $new != $old) {
                    update_post_meta($post_id, $field['id'], $new);
                } elseif ('' == $new && $old) {
                    delete_post_meta($post_id, $field['id'], $old);
                }
            }
        }

        function metabox_script() {
            $fields = $this->get_meta_fields();
            $meta_data = $this->get_meta_box_data();
            $screen = get_current_screen();

            $upload_field_id = nil;
            foreach ($fields as $field) {
                if ($field['type'] == 'upload') {
                    $upload_field_id = $field['id'];
                }
            }

            // Make sure we are on the Download screen
            if ( isset( $screen->post_type ) && $screen->post_type == $meta_data['page'] && $upload_field_id != nil ) : ?>

                <script type="text/javascript">
                    jQuery(document).ready(function($){

                        var formfield;

                        function extractUri(text) {
                            var uri_pattern = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/ig;

                            return text.match(uri_pattern)[0];
                        }

                        // Open upload window
                        $('#upload-file-button').click(function() {
                            formfield = $('#<?php echo $upload_field_id; ?>').attr('name');
                            tb_show( '','media-upload.php?type=file&TB_iframe=true' );

                            return false;
                        });

                        window.original_send_to_editor = window.send_to_editor;

                        window.send_to_editor = function(html) {
                            var uri = extractUri(html);
                            if (formfield) {
                                $('#<?php echo $upload_field_id; ?>').val(uri);
                                tb_remove();
                            } else {
                                window.original_send_to_editor(uri);
                                tb_remove();
                            }
                        };
                    });
                </script>

                <?php
            endif;
        }
    }
?>