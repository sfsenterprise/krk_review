<?php
class KRK_Career_Meta_Fields extends KRK_Meta_Fields {

    public function __construct() {
        parent::__construct();
    }

    public function get_meta_box_data(){
        return array(
            'id' => 'krk_career_options',
            'title' => 'Career Options',
            'page' => 'krk_career',
            'context' => 'normal',
            'priority' => 'high',
        );
    }

    public function get_meta_fields(){
        return array();
    }
}
new KRK_Career_Meta_Fields();
?>