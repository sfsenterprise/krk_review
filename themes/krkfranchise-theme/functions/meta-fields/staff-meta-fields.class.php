<?php
class KRK_Staff_Meta_Fields extends KRK_Meta_Fields {

    public function __construct() {
        parent::__construct();
    }

    public function get_meta_box_data(){
        return array(
            'id' => 'krk_staff_options',
            'title' => 'Staff Options',
            'page' => 'krk_staff',
            'context' => 'normal',
            'priority' => 'high',
        );
    }

    public function get_meta_fields(){
        return array(
            array(
                'label' => 'Name',
                'desc'  => 'The name of the staff member',
                'id'    => 'krk_staff_name',
                'type'  => 'text'
            ),
            array(
                'label' => 'Title',
                'desc'  => 'The title of the staff member',
                'id'    => 'krk_staff_title',
                'type'  => 'text'
            )
        );
    }
}
new KRK_Staff_Meta_Fields();
?>