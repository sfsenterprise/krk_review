<?php
    class KRK_Document_Meta_Fields extends KRK_Meta_Fields {

        public function __construct() {
            parent::__construct();
        }

        public function get_meta_box_data(){
            return array(
                'id' => 'krk_document_options',
                'title' => 'Document Options',
                'page' => 'krk_document',
                'context' => 'normal',
                'priority' => 'high',
            );
        }

        public function get_meta_fields(){
            return array(
                array(
                    'label' => 'File Attachment',
                    'desc'  => 'Upload or link to file. Select <b>File Url</b> for local media',
                    'id'    => 'krk_document_attachment',
                    'type'  => 'upload'
                ),

                array(
                    'label' => 'Link Text',
                    'desc'  => 'text of file link',
                    'id'    => 'krk_document_link_text',
                    'type'  => 'text'
                ),
                array(
                    'label' => 'Description',
                    'desc'  => 'document description',
                    'id'    => 'krk_document_description',
                    'type'  => 'textarea'
                )
            );
        }
    }
    new KRK_Document_Meta_Fields();
?>