<?php
function get_active_krk_menu() {
    $menu = nil;
    $type = 'krk_menu';
    $args = array(
        'post_type' => $type,
        'post_status' => 'publish',
        'orderby' => 'meta_value',
        'order' => 'ASC',
        'posts_per_page' => -1);

    $query_menu = null;
    $query_menu = new WP_Query($args);

    if ($query_menu->have_posts()):
        while ($query_menu->have_posts()):
            $query_menu->the_post();
            $is_active = get_post_meta(get_the_ID(), 'krk_menu_active', true);
            $attachment = get_post_meta(get_the_ID(), 'krk_menu_attachment', true);

            if ($is_active === 'on' && strlen($attachment) > 0)
                $menu = $attachment;
        endwhile;
    endif;
    wp_reset_query();
    return $menu;
}
?>