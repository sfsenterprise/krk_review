<?php
function get_active_krk_monthly_calendar(){
    $active_calendar = nil;
    $calendars = query_posts('post_type=krk_monthly_calendar');
    foreach ($calendars as $calendar) :
        $attachment = $calendar->krk_monthly_calendar_attachment;
        $link_text = $calendar->krk_monthly_calendar_link_text;
        if ($calendar->krk_monthly_calendar_active === 'on' && strlen($attachment) > 0):
            $active_calendar = array(
                'attachment' => $attachment,
                'link-text' => empty($link_text) ? $calendar->post_title : $link_text
            );
        endif;
    endforeach;
    wp_reset_query();
    return $active_calendar;
}
?>
