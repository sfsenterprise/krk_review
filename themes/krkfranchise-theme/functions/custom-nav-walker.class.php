<?php
    class Custom_Nav_Walker extends Walker_Nav_Menu
    {
        function start_lvl( &$output, $depth = 0, $args = array() ) {
            $indent = str_repeat("\t", $depth);
            $output .= "\n$indent<div class='dropdown-menu single-column'><div class='box'><ul>\n";
        }
        function end_lvl( &$output, $depth = 0, $args = array() ) {
            $indent = str_repeat("\t", $depth);
            $output .= "$indent</ul></div></div>\n";
        }
    }
?>