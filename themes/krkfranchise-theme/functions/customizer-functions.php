<?php 
	
	/**
	* Controls
	**/
	require_once(get_template_directory() . '/functions/customizers/controls/richtext-controls.php');
	require_once(get_template_directory() . '/functions/customizers/controls/sortable-controls.php');

	/**
	* Customizers
	**/
	require_once(get_template_directory() . '/functions/customizers/krk-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/infants-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/toddlers-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/preschool-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/prekindergarten-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/kindergarten-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/schoolage-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/curriculum-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/location-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/philosophy-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/school-customizer.class.php');
    require_once(get_template_directory() . '/functions/customizers/staff-customizer.class.php');
    require_once(get_template_directory() . '/functions/customizers/home-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/faq-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/news-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/blog-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/schedule-tour-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/events-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/camp-customizer.class.php');
	require_once(get_template_directory() . '/functions/customizers/extra-curricular-activities-customizer.class.php');
?>