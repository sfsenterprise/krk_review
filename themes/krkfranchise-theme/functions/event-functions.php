<?php
/**
 * Returns an array of active events
 */

function get_active_krk_events(){
    wp_reset_query();
    $type = 'krk_event';
    $args = array(
        'post_type' => $type,
        'post_status' => 'publish',
        'order' => 'ASC',
        'orderby' => 'meta_value',
        'meta_key' => 'krk_event_date',
        'posts_per_page' => -1);

    $query = null;
    $query = new WP_Query($args);
    $events = array();
    if ($query->have_posts()) {
        while($query->have_posts()){
            //check if event has_expired
            $query->the_post();
            $start_date = get_post_meta(get_the_ID(), 'krk_event_date', true);
            $end_date = get_post_meta(get_the_ID(), 'krk_event_end_date', true);

            $unixtime_start = $start_date;
            $start_date = gmdate("m/d/Y", $start_date);
            $expire_date = 0;
            $unixtime_end = 0;

            if(!empty($end_date)) {
                $unixtime_end = $end_date;
                $expire_date = $end_date + (24 * 60 * 60);
                $expire_gmdate = gmdate("m/d/Y", $expire_date);
                $end_date = gmdate("m/d/Y", $end_date);

                //check if event expired
                if(time() >= $expire_date) {
                    continue;
                }
            }
            else {
                $expire_date = time() - ( 24 * 60 * 60);

                if( $unixtime_start < $expire_date ) {
                    continue;
                }
            }
            //if not expired, add to events array
            $events[] = array(
                'id' => get_the_ID(),
                'title' => get_the_title(),
                'startdate' => $start_date,
                'enddate' => $end_date,
                'unixtime_start' => $unixtime_start,
                'unixtime_end' => $unixtime_start,
                'url' => get_the_permalink(),
                'excerpt' => get_the_excerpt(),
                'thumbnail' => has_post_thumbnail() ? get_the_post_thumbnail(get_the_ID(), array(306, 343)) : false,
            );
        }
    }
    wp_reset_query();
    return $events;
}
?>