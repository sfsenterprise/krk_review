<?php
require_once(get_template_directory() . '/functions/meta-fields/krk-meta-fields.class.php');
require_once(get_template_directory() . '/functions/meta-fields/document-meta-fields.class.php');
require_once(get_template_directory() . '/functions/meta-fields/application-document-meta-fields.class.php');
require_once(get_template_directory() . '/functions/meta-fields/announcement-meta-fields.class.php');
require_once(get_template_directory() . '/functions/meta-fields/event-meta-fields.class.php');
require_once(get_template_directory() . '/functions/meta-fields/career-meta-fields.class.php');
require_once(get_template_directory() . '/functions/meta-fields/meal-meta-fields.class.php');
require_once(get_template_directory() . '/functions/meta-fields/menu-meta-fields.class.php');
require_once(get_template_directory() . '/functions/meta-fields/homepage-slide-meta-fields.class.php');
require_once(get_template_directory() . '/functions/meta-fields/monthly-calendar-meta-fields.class.php');
require_once(get_template_directory() . '/functions/meta-fields/staff-meta-fields.class.php');
require_once(get_template_directory() . '/functions/meta-fields/testimonial-meta-fields.class.php');
?>