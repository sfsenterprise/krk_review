<?php
    function get_nav_menu_dynamic_style() {
        $CHARS_THRESHOLD = 85; //amount of chars required to start decrementing font_size and horrizontal_padding
        $CHARS_STEP = 10; //amount of chars to calculate the next decrementing step
        $FONT_SIZE_STEP = 1; //amount of px's to decrement the font size per step
        $HORIZONTAL_PADDING_STEP = 1.25; //amount of px's to decrement the horizontal padding per step
        $horizontal_padding = 15;
        $font_size = 16;
        $total_chars = 0;
        $menu_items = wp_get_nav_menu_items("nav");
        foreach( $menu_items as $menu_item) {
            $is_submenu = $menu_item->menu_item_parent != 0;
            if(!$is_submenu) {
                $total_chars += strlen($menu_item->title);
            }
        }
        if($total_chars > $CHARS_THRESHOLD) {
            $chars_diff = $total_chars - $CHARS_THRESHOLD;
            $steps = $chars_diff / $CHARS_STEP;
            $font_size -= $steps * $FONT_SIZE_STEP;
            $horizontal_padding -= $steps * $HORIZONTAL_PADDING_STEP;
        }

        return array(
            "font-size" => $font_size,
            "horizontal-padding" => $horizontal_padding
        );
    }
?>