<?php
class KRK_Camp_Customizer extends KRK_Customizer {

	public $page_name = 'our-camps';

	public function __construct() {
		add_action( 'customize_register', array($this, 'register_customizer'));
	}

	function register_customizer($wp_customize) {
		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_camp_page_content',
			array(
				'title' => 'Camp Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_page($this->page_name); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_camp_header_title', array(
			'default' => $this->defaults('krk_camp_header_title')
		));
		$wp_customize->add_setting( 'krk_camp_header_content', array(
			'default' => $this->defaults('krk_camp_header_content')
		));
		$wp_customize->add_setting( 'krk_camp_left_content', array(
			'default' => $this->defaults('krk_camp_left_content')
		));
		$wp_customize->add_setting( 'krk_camp_right_content_section1', array(
			'default' => $this->defaults('krk_camp_right_content_section1')
		));
		$wp_customize->add_setting( 'krk_camp_right_content_section2', array(
			'default' => $this->defaults('krk_camp_right_content_section2')
		));
		$wp_customize->add_setting( 'krk_camp_right_content_section3', array(
			'default' => $this->defaults('krk_camp_right_content_section3')
		));
		$wp_customize->add_setting( 'krk_camp_right_content_section4', array(
			'default' => $this->defaults('krk_camp_right_content_section4')
		));
		$wp_customize->add_setting( 'krk_camp_right_content_section5', array(
			'default' => $this->defaults('krk_camp_right_content_section5')
		));
		$wp_customize->add_setting( 'krk_camp_right_content_section6', array(
			'default' => $this->defaults('krk_camp_right_content_section6')
		));
		$wp_customize->add_setting( 'krk_camp_right_content_section7', array(
			'default' => $this->defaults('krk_camp_right_content_section7')
		));
		$wp_customize->add_setting( 'krk_camp_right_content_section8', array(
			'default' => $this->defaults('krk_camp_right_content_section8')
		));
		$wp_customize->add_setting( 'krk_camp_right_content_section9', array(
			'default' => $this->defaults('krk_camp_right_content_section9')
		));
		$wp_customize->add_setting( 'krk_camp_right_content_section10', array(
			'default' => $this->defaults('krk_camp_right_content_section10')
		));
		/**
		 * Controls
		 */
		$wp_customize->add_control( 'krk_camp_header_title',
			array(
				'label' => __( 'Header Title' ),
				'type' => 'text',
				'section' => 'krk_camp_page_content',
				'settings' => 'krk_camp_header_title',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_camp_header_content',
				array(
					'label' => __( 'Header Content' ),
					'section' => 'krk_camp_page_content',
					'settings' => 'krk_camp_header_content',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_camp_left_content',
				array(
					'label' => __( 'Left Content' ),
					'section' => 'krk_camp_page_content',
					'settings' => 'krk_camp_left_content',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_camp_right_content_section1',
				array(
					'label' => __( 'Engineer It Content' ),
					'section' => 'krk_camp_page_content',
					'settings' => 'krk_camp_right_content_section1',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_camp_right_content_section2',
				array(
					'label' => __( 'Beyond It Content' ),
					'section' => 'krk_camp_page_content',
					'settings' => 'krk_camp_right_content_section2',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_camp_right_content_section3',
				array(
					'label' => __( 'Produce It Content' ),
					'section' => 'krk_camp_page_content',
					'settings' => 'krk_camp_right_content_section3',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_camp_right_content_section4',
				array(
					'label' => __( 'Move It Content' ),
					'section' => 'krk_camp_page_content',
					'settings' => 'krk_camp_right_content_section4',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_camp_right_content_section5',
				array(
					'label' => __( 'Live It Content' ),
					'section' => 'krk_camp_page_content',
					'settings' => 'krk_camp_right_content_section5',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_camp_right_content_section6',
				array(
					'label' => __( 'Love It Content' ),
					'section' => 'krk_camp_page_content',
					'settings' => 'krk_camp_right_content_section6',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_camp_right_content_section7',
				array(
					'label' => __( 'Imagine It Content' ),
					'section' => 'krk_camp_page_content',
					'settings' => 'krk_camp_right_content_section7',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_camp_right_content_section8',
				array(
					'label' => __( 'Picasso It Content' ),
					'section' => 'krk_camp_page_content',
					'settings' => 'krk_camp_right_content_section8',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_camp_right_content_section9',
				array(
					'label' => __( 'Cook It Content' ),
					'section' => 'krk_camp_page_content',
					'settings' => 'krk_camp_right_content_section9',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_camp_right_content_section10',
				array(
					'label' => __( 'Give It Content' ),
					'section' => 'krk_camp_page_content',
					'settings' => 'krk_camp_right_content_section10',
				)
			)
		);
	}

	protected function defaults($setting) {
		if($setting == 'krk_camp_header_title') {
			return 'A Unique Camp <br> Each Week!';
		}
		elseif ($setting == 'krk_camp_header_content') {
			return 'That’s IT! During our 10-week summer camp series, campers explore their hidden talents, potential passions and intriguing interests.';
		}
		elseif($setting == 'krk_camp_left_content') {
			$activity_book_src = get_stylesheet_directory_uri() . '/images/Infant_Toddler_Activity_Book.pdf';
			return <<<EOT
			<p>That’s IT! During our 10-week summer camp series, campers explore their hidden talents, potential passions and intriguing interests.
			Each boy and girl will experience art and cooking, engineering and community service, and everything in-between!
			This summer camp is all about the journey to discover what suits each camper’s personality - the quest to discover what they like best. </p>
			<br>
			<h3>You’re Really Going to “LIKE” this!</h3>
			<p>While campers explore various camps and form meaningful connections with fellow campers, we invite families to join in the fun by following our weekly adventures across social media!
			Search and Share <b>#KRKCAMP17</b></p>
EOT;
		}
		elseif($setting == 'krk_camp_right_content') {
			return <<<EOT
            <h2>Big Steps Curriculum&trade; for camp</h2>
            <p>The Big Steps Curriculum&trade; for camp continues to build on the big steps taken during infancy. camp experience an environment that invites interaction with play materials and organized play spaces. </p>
            <p>The children practice literacy through sign language, books, puppet play, music, singing, games, and much conversation with peers and adults. Their cognitive skills blossom with a multitude of activities designed to unleash their already-inquisitive and creative nature. </p>
EOT;
		}
		elseif ($setting == 'krk_camp_right_content_section1') {
			return <<<EOT
			<h3>Building Towards the Future</h3>
			<p>Ever wanted to be an inventor, or just figuring out how things work? Gadgets and things are what this minicamp is all about.
			Campers will have opportunities to explore robotics, experiment with simple machines, and develop original engineering projects.</p>
EOT;
		}
		elseif ($setting == 'krk_camp_right_content_section2') {
			return <<<EOT
			<h3>The Sky’s the Limit</h3>
			<p>Campers will set up their own mission control to plan a space adventure to the great beyond,
			while communicating with command central to plot a course to a distant imaginary planet.</p>
EOT;
		}
		elseif ($setting == 'krk_camp_right_content_section3') {
			return <<<EOT
			<h3>All the World’s a Stage</h3>
			<p>Campers will have the opportunity to create their own dramatic production. Whether it’s a play, a dance, or a talent; the sky is the limit!</p>
EOT;
		}
		elseif ($setting == 'krk_camp_right_content_section4') {
			return <<<EOT
			<h3>Put on Those Dancing Shoes</h3>
			<p>Campers are invited to bring their best dance steps for a week of rhythm and original movement.
			They will learn cultural dances from around the world as well as some close to home.</p>
EOT;
		}
		elseif ($setting == 'krk_camp_right_content_section5') {
			return <<<EOT
			<h3>In Pursuit of ‘The Good Life’</h3>
			<p>Thrive and be well! Campers will discover ways to stay fit, healthy, and happy as they delve into healthy living,
			good nutrition, and opportunities for physical activity.</p>
EOT;
		}
		elseif ($setting == 'krk_camp_right_content_section6') {
			return <<<EOT
			<h3>Community Connection</h3>
			<p>Bring pets, plants, and people together as campers merge their minds in developing and implementing ways to coexist with dignity, respect, and love.</p>
EOT;
		}
		elseif ($setting == 'krk_camp_right_content_section7') {
			return <<<EOT
			<h3> Making Magic Happen </h3>
			<p> Hocus Pocus! Campers will enter a world of imagination to create, explore, and mesmerize onlookers with remarkable creations! </p>
EOT;
		}
		elseif ($setting == 'krk_camp_right_content_section8') {
			return <<<EOT
			<h3> ’Artful’ Displays of Talent </h3>
			<p> Campers will become mini Picassos by recreating original masterpieces by famous artists.
			They will learn about different types of media, various artistic techniques, and how art is infused in our everyday lives.</p>
EOT;
		}
		elseif ($setting == 'krk_camp_right_content_section9') {
			return <<<EOT
			<h3> Food-Focused FUNdamentals </h3>
			<p> Campers will learn about cooking techniques, reading recipes, measurements and conversions, and how to prepare quick, easy meals at home. </p>
EOT;
		}
		elseif ($setting == 'krk_camp_right_content_section10') {
			return <<<EOT
			<h3> Making a Difference </h3>
			<p> Campers will learn the importance of giving back to the community while participating in community service activities,
			organizing local drives for those in need, and kicking off a giving campaign for a charity of their choice. </p>
EOT;
		}
		else {
			return '';
		}
	}
}

new KRK_Camp_Customizer();
?>