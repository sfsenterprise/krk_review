<?php


add_action( 'customize_register', 'krk_blog_customizer' );
function krk_blog_customizer( $wp_customize ) {

	/**
	 * Sections
	 */
	$wp_customize->add_section(
		'krk_blog_page_content',
		array(
			'title' => 'Blog Page Content',
			'priority' => 35,
			'active_callback' => function(){ return is_post_type_archive('krk_blog');  }
		)
	);

	/**
	 * Settings
	 */
	$wp_customize->add_setting( 'krk_blog_header_image');

	/**
	 * Controls
	 */
	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'krk_blog_header_image',
			array(
				'label' => __( 'Header Image' ),
				'section' => 'krk_blog_page_content',
				'settings' => 'krk_blog_header_image',
			)
		)
	);
}
?>