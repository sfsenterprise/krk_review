<?php

function krk_schoolage_customizer_defaults($setting) {
  if($setting == 'krk_schoolage_header_title') {
    return 'Our Big Kids';
  }
  elseif($setting == 'krk_schoolage_video_thumbnail'){
    return 'schoolage1.jpg';
  }
  elseif ($setting == 'krk_schoolage_header_content') {
    return '<b>Continuing academic growth through activities and games.</b>';
  }
  elseif($setting == 'krk_schoolage_left_content') {
    $camp_url = get_multisite_path('/our-camps');
    $activity_book_src = get_stylesheet_directory_uri() . '/images/School_Age_Activity_Book.pdf';
    return <<<EOT
              <div class="section-xtx">
                  <h2>Our Before & After School Program</h2>
                  <p>Welcome to the Kids 'R' Kids before and after school age program which offers the <b> G.Y.M Curriculum&#8482!</b> G.Y.M. stands for Growing Young Minds and offers school age children, ages 5 through 12, a place to relax with friends and opportunities to grow, explore, and learning in a safe and secure environment. The G.Y.M. curriculum offers a wide range of clubs with the following entertaining activities: </p>
                  <ul>
                    <li> <b> Action Club </b> - This club includes fun and exciting activities that foster building relationships, sharing, good sportsmanship, taking responsibility, trust, kindness, and compassion. </li>
                    <li><b> Artistry Club </b> - Children get a chance to develop their own artistic styles through exploration. </li>
                    <li> <b> Be A Star Club </b> - Children explore and develop their acting and musical skills through improvisation, skits, drama games, jam sessions, and more!</li>
                    <li><b>Computer Whiz Club</b> - Club members explore amazing things to do on the computer with creative programs, funny research, and investigative searches of what the web has to offer!</li>
                    <li><b>Crossfit Challenge Club</b> - This club offers a variety of fun, exciting physical activities that strengthen core muscles and promote overall body health.</li>
                    <li><b> Ooey, Gooey, & Gross Club</b> - This club offers members activities and projects that are ooey, gooey, and grossly fascinating!</li>
                    <li><b> Page Turners Club</b> - Club members hone their skills as they try their hands at writing all kinds - creative, informative, silly, and fun! </li>
                    <li><b>Snack Attack Club</b> - Members prepare and enjoy yummy foods and beverages while using math, science, health, and literacy!</li>
                    <li><b> Weird Science Club</b> - Club members enjoy participating in a range of crazy, cool experiments, learning strange facts and working on funky projects!</li>
                  </ul>
                </div>
                <div class="section-xtx">
                  <h2>Year Round Programs</h2>
                  <p>Kids 'R' Kids Learning Academies provide year-round programs for school age children. Fun field trips for teacher work days, week long camps for Winter and Spring Breaks. Summer Day Camp throughout the Summer with a new theme every year. 
                  Click <a href="{$camp_url}">HERE</a> to see what our Academy's are doing this Summer.</p>
                  <p> Click <a href="{$activity_book_src}"> HERE </a> to download our Brain Waves Activity Book!</p>
                </div>
EOT;
  }
  elseif($setting == 'krk_schoolage_right_content') {
    return <<<EOT
            <h2>G.Y.M. School Age Curriculum</h2>
            <p>Growing Young Minds or G.Y.M. is the premise behind our curriculum for this age group. We have nine different clubs each child can choose from that allows for independent play or group play.</p>
            <p>Kids 'R' Kids believes after school time should be spent encouraging social and emotional growth as well as continuing academic growth through activities and games. At any age, it's best to learn through play and Kids 'R' Kids makes learning fun.</p>
EOT;
  }
}
?>