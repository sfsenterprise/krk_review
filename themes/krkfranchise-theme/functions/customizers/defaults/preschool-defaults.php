<?php

function krk_preschool_customizer_defaults($setting) {
  if($setting == 'krk_preschool_header_title') {
    return 'Our Eager Learners!';
  }
  elseif($setting == 'krk_preschool_video_thumbnail'){
    return 'preschool1.jpg';
  }
  elseif ($setting == 'krk_preschool_header_content') {
    return '<b>Our Advanced Accredited Program</b> ensures that every child receives an accelerated foundation for elementary school.';
  }
  elseif($setting == 'krk_preschool_left_content') {
    $activity_book_src = get_stylesheet_directory_uri() . '/images/Brainwave_Activity_Book_preschool.pdf';
    return <<<EOT
    <div class="section-xtx">
      <h2>Our Preschool Program</h2>
      <p>Our activities are standards-driven and address the whole child by promoting:  </p>
      <ul>
        <li> Developmentally-appropriate practices </li>
        <li> Hands-on involvement </li>
        <li> Decision-making and problem solving skills </li>
        <li>Character development </li>
        <li> Vocabulary </li>
        <li> Social-emotional activities </li>
        <li> Literacy skills </li>
        <li> Small group instruction </li>
      </ul>
      <p>Life is an adventure when you are a preschooler. Our Learning Academy provides plenty of choice for your preschooler to explore and grow. The sounds of children laughing and playing are the sounds of children learning at Kids 'R' Kids Learning Academies</p>
    </div>
    <div class="section-xtx">
      <h2>Our Brain Waves Activity Book</h2>
      <p>It's crucial to connect a child's neural pathways as quickly as possible during the first 5 years. This activity book will enlighten you and your child of how the brain works and how Brain Waves&trade; can prepare your child for future educational success!</p>
      <p> Click <a href="{$activity_book_src}"> HERE </a> to download our Brain Waves Activity Book!</p>
    </div>          
EOT;
  }
  elseif($setting == 'krk_preschool_right_content') {
    return <<<EOT
    <div class="section-xtx">
      <h2>Our Exclusive Kids 'R' Kids Preschool Fast Track Curriculum&#8482</h2>
      
      <p></p>
      <p>Our preschoolers, ages 3 through 5, are on an accelerated track to acquire literacy and cognitive skills through a standards-driven, play-based curriculum. They are immersed in an abundance of hands-on, multi-sensory activities designed to advance their learning. Preschoolers thrive in well-organized classrooms with schedules that allow for a balance of active and quiet activities. All learning domains are covered in the <b>Fast Track Curriculum&#8482</b> through large and small group instruction. Students further practice their skills through independent play. Get ready for the abundant adventures that await your preschooler! </p>
    </div>
EOT;
  }
}
?>