<?php

function krk_home_customizer_defaults($setting) {
  if($setting == 'krk_home_school_content') {
    $location_name = do_shortcode('[location_name]');
    return <<<EOT
            <p>Our families have something in common with our school, immense peace of mind. First and foremost is the safety and security of the children in our care.</p>
            <p>Each classroom opens up in to a fenced playground area designed specifically for the developmental age of your child. We have cameras throughout the building and on the playground to insure each classroom has measured security while in our care. No one is allowed access in our center without being buzzed in and we check IDs of all guests and visitors. Families can also log on to our secure site to view their child throughout the day. We fingerprint and run background checks on every staff member hired and insure they all receive first aid and infant, child and adult CPR within the first few months of employment. Children receive more than an engaging educational experience in a state-of-the-art facility at Kids 'R' Kids of {$location_name} they are safe while in our care.</p>
EOT;
  }
  elseif($setting == 'krk_home_menu_content') {
    return <<<EOT
        <p>Complete, balanced meals! Our chef prepares and serves two delicious meals and
         two nutritious snacks to provide the balanced nutrition each child needs to learn and grow.</p>
EOT;

  }
  elseif($setting == 'krk_home_main_image') {
    return 'Franchise_Main1.jpg';
  }

  elseif($setting == 'krk_home_main_title') {
    return 'Welcome to Our School!';
  }

  elseif($setting == 'krk_home_main_button_text') {
    return 'Schedule a Tour';
  }

  elseif($setting == 'krk_home_main_button_url') {
    return get_multisite_path('schedule-tour');
  }
}
?>