<?php

function krk_infants_customizer_defaults($setting) {
  if($setting == 'krk_infants_header_title') {
    return 'Our Youngest Learners!';
  }
  elseif($setting == 'krk_infants_video_thumbnail'){
    return 'infant1.jpg';
  }
  elseif ($setting == 'krk_infants_header_content') {
    return '<b>At no other stage of life</b> does learning occur so rapidly.';
  }
  elseif($setting == 'krk_infants_left_content') {
    $activity_book_src = get_stylesheet_directory_uri() . '/images/Infant_Toddler_Activity_Book.pdf';
    return <<<EOT
              <h2>Our Infant Program</h2>
                <ul>
                 <li>Sign language</li>
                 <li>Pre-literacy skills</li>
                 <li>Infant curriculum activities and lessons</li>
                 <li>Tummy time with friends</li>
                 <li>Developmentally appropriate practices</li>
                 <li>Indoor and outdoor activities</li>
                 <li>Sensory play</li>
                </ul>
            </div>
            <div class="section-xtx">
              <p>At birth, your infant's brain has 100 billion brain cells, but only 17% are activated. Just two weeks after conception, an infant’s brain expands at the rate of 500,000 cells per minute. The first year offers a tremendous opportunity to shape your child’s future. The Kids ‘R’ Kids Infant Program is designed to engage infants and to provide each one with the stimulation that is so crucial for early brain development.</p>
            </div>
            <div class="section-xtx">
              <p>We understand that choosing the best care for your infant can be tough. Rest assured that Kids ‘R’ Kids helps with your decision making process, and you’ll find that your baby is in terrific hands with us. Our Infant Program has been carefully designed to deliver a safe, nurturing, and stimulating environment.</p>
              <p> Click <a href="{$activity_book_src}"> HERE </a> to download our Brain Waves Activity Book!</p>
EOT;
  }
  elseif($setting == 'krk_infants_right_content') {
    return <<<EOT
            <h2>Our Exclusive Kids ‘R’ Kids Toddler Big Steps Curriculum&trade;</h2>
            <p>The Infant Big Steps Curriculum&trade;, ages 6 weeks through 36 months, takes into account that development and learning begin prenatally and continue throughout life. The young infants are immersed in curriculum that invites and promotes multiple opportunities for motor and sensory activities. Their cognitive skills are developed through planned activities that invite them to interact with the environment to demonstrate eagerness and curiosity, persistence, creativity, and problem solving. Literacy and language development are nurtured.</p>
            
EOT;
  }
}
?>