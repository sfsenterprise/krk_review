<?php

function krk_faq_customizer_defaults($setting) {
  if($setting == 'krk_faq_content') {
    return <<<EOT
            <p><b> What ages of children do you accept? </b></p>
            <p> <i>We accept children ages six weeks to twelve years old.</i></p>
            <p><b> What are your hours of operation? </b></p>
            <p> <i>Our hours of operation are 6:30 a.m. to 6:30 p.m.</i></p>
            <p><b> Is Internet viewing available? </b></p>
            <p> <i>Internet viewing is always available for parents to become involved in their children's daily activities.</i></p>
            <p><b> Are you open during school holidays? </b></p>
            <p><b> Is transportation provided to local elementary schools? </b></p>
            <p><b> Does your school provide a drop-in or part-time service? </b></p>
            <p><b> Is there are discount if I have more than one child enrolled? </b></p>
            <p><b> What are the benefits of the Kids 'R' Kids program? </b></p>
            <p> <i>Our exclusive curriculum propels Kids 'R' Kids Learning Academies far beyond the childcare industry. The stanards driven learning goals provide teachers with the tools to engage children in fun and purposeful activities.</p>
            <p> Benefits of our Kids 'R' Kids curriculum that your child will be engaged in:</p>
            <ul>
              <p><strong> Big Steps Curriculum&#8482 - </strong> Continues to build on the big steps taken during infancy. Toddlers bound into an environment that invites interaction with play materials and organized play spaces. </p>

              <p><strong>Fast Track Curriculum&#8482 - </strong> In preparation for elementary school, our Pre-Kindergarten children work with original themed modules, designed to be appealing and educational. Our standards-driven curriculum has been approved by the state of (state name) so transitioning to Kindergarten is a breeze. A great percentage of our Pre-K graduates test high above their peers within the first year of Kindergarten.</p>

              <p><strong> G.Y.M. Curriculum&#8482 - </strong> Growing Young Minds offers school age children, ages 5 through 12, a place to relax with friends and opportunities to grow, explore, and learn in a safe and secure environment.</p>

              <p><strong> Brain Waves Curriculum&#8482 - </strong> We use strategies from our Brain Waves&#8482 curriculum to support neural pathways for language, social-emotional, cognitive, and physical development during these critical years. </p>

              <p><strong> STEAM Ahead&#8482 - </strong>Our project-based curriculum incorporates more science, technology, engineering, art, and math into student's everyday learning through play.</i></p>
          </ul>
          <p><b> Does your program include field trips? </b></p>
          <p> <i>Yes. We provide an array of fun and educational field trips throughout the year.</i></p>
          <p><b> Is playtime/outdoor time available? </b></p>
          <p> <i>Yes. Every classroom has a designated time to play and explore outside.</i> </p>
          <p><b> Are meals and snacks provided? </b></p>
          <p> <i>Yes. Breakfast, lunch, and snacks are provided daily.</i> </p>
          <p><b> What type of training is your staff required to have? </b></p>
          <p> <i>Our highly qualified staff is required to attend all state mandated training and be CPR and First Aid certified. Additionally, Kids 'R' Kids corporate staff provides mandatory and requested on- and off- site training.</i> </p>
          <p><b> What times are the best if I want to visit your school? </b></p>
          <p><b> If I have questions about your tuition, how is the best way to receive that information? </b></p>
          <p><b> Do you accept subsidy programs? </b></p>
          <p><b> How can I get more information about registering my child? </b></p>
EOT;
  }
}
?>