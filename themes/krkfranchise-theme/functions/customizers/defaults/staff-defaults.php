<?php

function krk_staff_customizer_defaults($setting) {
  if($setting == 'krk_staff_header_title') {
    return 'The Children Come<br>First';
  }
  elseif($setting == 'krk_staff_content') {
    return <<<EOT
               <div class="section-xtx">
                  <h4>Our Teachers Teach "First Class Kids" </h4>
                  <p>Our teachers are dedicated to the Kids 'R' Kids Learning Academies' philosphy, "Hug First, and Then Teach." Our staff members are highly trained, thoughtful, and nurturing individuals with a passion for work in early childcare and education. They have a passon for what they do and believe the lives they touch every day makes a big impact for families in our community.</p>
                  <p>Our teachers:</p>
                  <ul>
                      <li>"Hug First, and Then Teach." Kids 'R' Kids understand the learning process is optimized when children trust their caregivers.</li>
                      <li> Invest in ther students and families</li>
                      <li> Participate in on-going and professional childcare development training</li>
                      <li> Are CPR and first-aid certified for infant, child, and adult</li>
                      <li> Maintain a safe, clean, and loving environment</li>
                  </ul>
                  <p> Our staff are carefully selected and extensively trained to provide the best childcare experience available. The Kids 'R' Kids staff are central to our success. We encourage you to schedule a tour today so you and your child can visit and speak with our teachers first hand!</p>
              </div>
EOT;
  }
}
?>