<?php

function krk_kindergarten_customizer_defaults($setting) {
  if($setting == 'krk_kindergarten_header_title') {
    return 'Our Active Learners!';
  }
  elseif($setting == 'krk_kindergarten_video_thumbnail'){
    return get_stylesheet_directory_uri() . '/images/preschool1.jpg';
  }
  elseif ($setting == 'krk_kindergarten_header_content') {
    return '<b>Preparing for Elementary School</b>';
  }
  elseif($setting == 'krk_kindergarten_left_content') {
    $location_state = do_shortcode("[location_state]");
    $kindergarten_program_name = do_shortcode("[kindergarten_program_name]");
    return <<<EOT
              <div class="section-xtx">
                <h2>Our {$kindergarten_program_name} Program</h2>
                <p>The {$kindergarten_program_name} programs are both state-approved and privately-funded programs that promote:</p>
                <ul>
                  <li> Decision-making and problem solving skills </li>
                  <li> Character development</li>
                  <li> Increased vocabulary </li>
                  <li> Social-emotional development </li>
                  <li> Literacy Skills </li>
                  <li> Small group instruction </li>
                </ul>
                    
                <p>The Kids ‘R’ Kids <b>First Class Curriculum&#8482</b> proudly offers activities that are academically challenging while still fun! The students learn through theme-based modules which are designed to be entertaining as well as educational. Each child receives an unparalleled level of individual attention to ensure academic success. The program is tailored to increase knowledge along with a child’s self-confidence. Kids ‘R’ Kids Learning Academies offer the best foundation in preparing your child for their elementary school years.</p>
              </div>
              <div class="section-xtx">
                <h2>{$kindergarten_program_name} <b> Fast Track Curriculum&#8482</b></h2>
                <p>In preparation for Elementary school, our {$kindergarten_program_name} children work with original themed modules, designed to be appealing and educational. Our standards-driven curriculum as been approved by the state of {$location_state} so transitioning to Kindergarten is a breeze. A great percentage of our Pre-K graduates test high above their peers within the first year of Kindergarten.</p>
              </div>
EOT;
  }
  elseif($setting == 'krk_kindergarten_right_content') {
    return <<<EOT
      <div class="section-xtx">
        <h2> The Teacher's Role as Facilitator </h2>
        <p>Teachers create high quality learning environments that provide students with opportunities to observe, question, investigate, predict, experiment, build, and share what they learn.</p>
        <p>Students and teachers explore STEAM AHEAD&reg; projects together. To tap into each student’s natural interests while facilitating learning, project materials are placed in learning stations for independent and small group exploration.</p>
        <p> Teachers serve as guides while students participate in STEAM AHEAD&reg; projects, making observations of each student’s growth, development, and interests along the way. </p>
      </div>
EOT;
  }
}
?>