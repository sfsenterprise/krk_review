<?php

function krk_location_customizer_defaults($setting) {
  if($setting == 'krk_location_directions_header_title') {
    return 'Directions';
  }
  elseif ($setting == 'krk_location_directions_content') {
    $schedule_tour_url = get_site_url() . '/schedule-tour';
    return <<<EOT
      <h4>Hello!</h4>
      <p>We're so happy you found us. Kids 'R' Kids of <?php echo do_shortcode("[location_name]"); ?> continues to be the leader for early care and education in the <?php echo do_shortcode("[location_name]"); ?> community. We are proud to represent <?php echo "$city, $state"; ?> and understand the importance of a first class preschool and what it means to the families with young children. We want to meet you and show you around our Learning Academy. Click <a href="{$schedule_tour_url}">HERE</a> to plan your visit today.</p>
EOT;
  }
  elseif($setting == 'krk_location_section2_content') {
    $schedule_tour_url = get_site_url() . '/schedule-tour';

    return <<<EOT
      <div class="row-text">
          <h4>We look forward to your visit!</h4>
          <p>Now that you have seen our Learning Academy online, our staff are anxious to meet you and your family in person. Our address and hours of operation are above and we welcome visitors anytime. However, for you and your family to see us outside of drop off and pick up times or nap time, call us and we can schedule your visit. You'll then be able to see the classrooms in action and stay awhile to speak with our teaching staff about our accredited program. Click <a href="{$schedule_tour_url}">HERE</a> to make an appointment today.</p>
      </div>
      <h4>Facilities Overview</h4>
      <ul class="list">
        <li>Our Academies are equipped with multiple cameras in every classroom, cafeteria and outdoor play areas. Our interior is designed with safety glass walls and doors to ensure each child is supervised by staff at all times. </li>
        <li>With over 17,000 square feet, we design a one-of-a-kind building that features age appropriate classrooms equipped with the right tools to properly educate your child along with large fenced-in playgrounds matching each developmental age range.</li>
        <li>School staff members and volunteers greet each child under our Kiss 'N Go Lane stationed right at the front door.</li>
        <li>Our cafeteria is a unique room designed for each classroom to join one another for lunch and snacks. We believe it's important to teach manners and help create conversations meant for dining. </li>
        <li>The equipment used in our buildings is specifically made for children. It's durable and safe and has been researched among early childhood educators as what works best. Each room is arranged to meet the needs of child led and teacher led lessons. </li>
        <li>Our gym area, mainly used for our before and after school age children is also used for indoor play with our preschoolers during inclement weather. </li>
        <li>Outdoor time is always a treat and our play yards are packed full of fun. We have shade units with UV protection built above our play structures and most of our Academies are now installing Splash Pads for those hot summer days.</li>
      </ul>
EOT;
  }
}
?>