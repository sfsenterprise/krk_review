<?php

require_once(get_template_directory() . '/functions/customizers/defaults/kindergarten-defaults.php');

add_action( 'customize_register', 'krk_kindergarten_customizer' ); 
function krk_kindergarten_customizer( $wp_customize ) {
	/**
	* Sections
	*/
	$wp_customize->add_section(
        'krk_kindergarten_page_content',
        array(
            'title' => 'Kindergarten Page Content',
            'priority' => 35,
            'active_callback' => function(){ return is_page("our-kindergarten"); }
        )
    );
   
	/**
	* Settings
	*/
    $wp_customize->add_setting( 'krk_kindergarten_video_upload' );
    $wp_customize->add_setting( 'krk_kindergarten_video_thumbnail', array(
        'default' => krk_kindergarten_customizer_defaults('krk_kindergarten_video_thumbnail')
      ));
    $wp_customize->add_setting( 'krk_kindergarten_header_image', array(
        'default' => krk_kindergarten_customizer_defaults('krk_kindergarten_header_image')
      ));
    $wp_customize->add_setting( 'krk_kindergarten_header_title', array(
        'default' => krk_kindergarten_customizer_defaults('krk_kindergarten_header_title')
      ));
    $wp_customize->add_setting( 'krk_kindergarten_header_content', array(
        'default' => krk_kindergarten_customizer_defaults('krk_kindergarten_header_content')
      ));
    $wp_customize->add_setting( 'krk_kindergarten_left_content', array(
        'default' => krk_kindergarten_customizer_defaults('krk_kindergarten_left_content')
      ));
    $wp_customize->add_setting( 'krk_kindergarten_right_content', array(
        'default' => krk_kindergarten_customizer_defaults('krk_kindergarten_right_content')
      ));
	/**
	* Controls
	*/
    $wp_customize->add_control( 
    	new WP_Customize_Media_Control( $wp_customize, 'krk_kindergarten_video_upload', 
    		array(
    			'label' => __( 'Kindergarten Page Video (MP4 Video)' ),
    			'section' => 'krk_kindergarten_page_content',
    			'settings' => 'krk_kindergarten_video_upload',
    			'mime_type' => 'video/mp4',
			) 
		) 
	);
	$wp_customize->add_control( 
    	new WP_Customize_Image_Control( $wp_customize, 'krk_kindergarten_video_thumbnail', 
    		array(
    			'label' => __( 'Video Thumbnail' ),
    			'section' => 'krk_kindergarten_page_content',
    			'settings' => 'krk_kindergarten_video_thumbnail',
			) 
		) 
	);
	$wp_customize->add_control( 
    	new WP_Customize_Image_Control( $wp_customize, 'krk_kindergarten_header_image', 
    		array(
    			'label' => __( 'Header Image' ),
    			'section' => 'krk_kindergarten_page_content',
    			'settings' => 'krk_kindergarten_header_image',
			) 
		) 
	);
	$wp_customize->add_control( 'krk_kindergarten_header_title', 
		array(
  			'label' => __( 'Header Title' ),
  			'type' => 'text',
  			'section' => 'krk_kindergarten_page_content',
  			'settings' => 'krk_kindergarten_header_title',
		) 
	);
	$wp_customize->add_control( 
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_kindergarten_header_content', 
			array(
  				'label' => __( 'Header Content' ),
  				'section' => 'krk_kindergarten_page_content',
  				'settings' => 'krk_kindergarten_header_content',
			)
		) 
	);

	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_kindergarten_left_content', 
			array(
  				'label' => __( 'Left Content' ),
  				'section' => 'krk_kindergarten_page_content',
  				'settings' => 'krk_kindergarten_left_content',
			) 
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_kindergarten_right_content',
			array(
  				'label' => __( 'Right Content' ),
  				'section' => 'krk_kindergarten_page_content',
  				'settings' => 'krk_kindergarten_right_content',
			) 
		)
	);
}
?>