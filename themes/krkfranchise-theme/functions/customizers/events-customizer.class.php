<?php
class KRK_Events_Customizer extends KRK_Customizer {

	public $page_name = 'krk_event';

	public function __construct() {
		add_action( 'customize_register', array($this, 'register_customizer'));
	}

	function register_customizer($wp_customize) {
		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_events_page_content',
			array(
				'title' => 'Events Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_post_type_archive($this->page_name); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_events_header_image');

		/**
		 * Controls
		 */
		$wp_customize->add_control(
			new WP_Customize_Image_Control( $wp_customize, 'krk_events_header_image',
				array(
					'label' => __( 'Header Image' ),
					'section' => 'krk_events_page_content',
					'settings' => 'krk_events_header_image',
				)
			)
		);
	}

	protected function defaults($setting) {
		if($setting == 'krk_events_header_image') {
			return  get_stylesheet_directory_uri() . '/images/img-31.jpg';
		}
	}
}

new KRK_Events_Customizer();
?>