<?php
class KRK_Infants_Customizer extends KRK_Customizer {

	public $page_name = 'Our-Infants';

	public function __construct() {
		add_action( 'customize_register', array($this, 'register_customizer'));
	}

	function register_customizer( $wp_customize ) {

		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_infants_page_content',
			array(
				'title' => 'Infants Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_page("Our-Infants"); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_infants_video_upload' );
		$wp_customize->add_setting( 'krk_infants_video_thumbnail', array(
			'default' => $this->defaults('krk_infants_video_thumbnail')
		));
		$wp_customize->add_setting( 'krk_infants_header_image');
		$wp_customize->add_setting( 'krk_infants_header_title', array(
			'default' => $this->defaults('krk_infants_header_title')
		));
		$wp_customize->add_setting( 'krk_infants_header_content', array(
			'default' => $this->defaults('krk_infants_header_content')
		));
		$wp_customize->add_setting( 'krk_infants_left_content', array(
			'default' => $this->defaults('krk_infants_left_content')
		));
		$wp_customize->add_setting( 'krk_infants_right_content', array(
			'default' => $this->defaults('krk_infants_right_content')
		));
		/**
		 * Controls
		 */
		$wp_customize->add_control(
			new WP_Customize_Media_Control( $wp_customize, 'krk_infants_video_upload',
				array(
					'label' => __( 'Infants Page Video (MP4 Video)' ),
					'section' => 'krk_infants_page_content',
					'settings' => 'krk_infants_video_upload',
					'mime_type' => 'video/mp4',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control( $wp_customize, 'krk_infants_video_thumbnail',
				array(
					'label' => __( 'Video Thumbnail' ),
					'section' => 'krk_infants_page_content',
					'settings' => 'krk_infants_video_thumbnail',
					'type'           => 'select',
					'choices'        => array(
						'infant1.jpg' => __('Image 1'),
						'infant2.jpg' => __('Image 2'),
						'infant3.jpg' => __('Image 3'),
						'infant4.jpg' => __('Image 4'),
						'infant5.jpg' => __('Image 5'),
						'infant6.jpg' => __('Image 6'),
					)
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Image_Control( $wp_customize, 'krk_infants_header_image',
				array(
					'label' => __( 'Header Image' ),
					'section' => 'krk_infants_page_content',
					'settings' => 'krk_infants_header_image',
				)
			)
		);
		$wp_customize->add_control(
			'krk_infants_header_title',
			array(
				'label' => __( 'Header Title' ),
				'type' => 'text',
				'section' => 'krk_infants_page_content',
				'settings' => 'krk_infants_header_title',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_infants_header_content',
				array(
					'label' => __( 'Header Content' ),
					'section' => 'krk_infants_page_content',
					'settings' => 'krk_infants_header_content',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_infants_left_content',
				array(
					'label' => __( 'Left Content' ),
					'section' => 'krk_infants_page_content',
					'settings' => 'krk_infants_left_content',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_infants_right_content',
				array(
					'label' => __( 'Right Content' ),
					'section' => 'krk_infants_page_content',
					'settings' => 'krk_infants_right_content',
				)
			)
		);
	}

	protected function defaults($setting) {
		if($setting == 'krk_infants_video_thumbnail') {
			return 'infant1.jpg';
		}
		elseif($setting == 'krk_infants_header_title') {
			return 'Our Youngest Learners!';
		}
		elseif($setting == 'krk_infants_video_thumbnail'){
			return 'infant1.jpg';
		}
		elseif ($setting == 'krk_infants_header_content') {
			return '<b>At no other stage of life</b> does learning occur so rapidly.';
		}
		elseif($setting == 'krk_infants_left_content') {
			$activity_book_src = get_stylesheet_directory_uri() . '/assets/Infant_Toddler_Activity_Book.pdf';
			return <<<EOT
              <h2>Our Infant Program</h2>
                <ul>
                 <li>Sign language</li>
                 <li>Pre-literacy skills</li>
                 <li>Infant curriculum activities and lessons</li>
                 <li>Tummy time with friends</li>
                 <li>Developmentally appropriate practices</li>
                 <li>Indoor and outdoor activities</li>
                 <li>Sensory play</li>
                </ul>
            </div>
            <div class="section-xtx">
              <p>At birth, your infant's brain has 100 billion brain cells, but only 17% are activated. Just two weeks after conception, an infant’s brain expands at the rate of 500,000 cells per minute. The first year offers a tremendous opportunity to shape your child’s future. The Kids <span class="krk-ticks">R</span> Kids Infant Program is designed to engage infants and to provide each one with the stimulation that is so crucial for early brain development.</p>
            </div>
            <div class="section-xtx">
              <p>We understand that choosing the best care for your infant can be tough. Rest assured that Kids <span class="krk-ticks">R</span> Kids helps with your decision-making process, and you’ll find that your baby is in terrific hands with us. Our Infant Program has been carefully designed to deliver a safe, nurturing, and stimulating environment.</p>
              <p> Click <a href="{$activity_book_src}"> HERE </a> to download our Brain Waves&#8480; Activity Book!</p>
EOT;
		}
		elseif($setting == 'krk_infants_right_content') {
			return <<<EOT
            <h2>Our Exclusive Big Steps Curriculum&trade; for Infants</h2>
            <p>The Infant Big Steps Curriculum&trade;, for ages 6 weeks through 36 months, takes into account that development and learning begin prenatally and continue throughout life. The young infants are immersed in curriculum that invites and promotes multiple opportunities for motor and sensory activities. Their cognitive skills are developed through planned activities that invite them to interact with the environment to demonstrate eagerness and curiosity, along with persistence, creativity, and problem solving. Literacy and language development are also nurtured.</p>
            
EOT;
		}
		else {
			return '';
		}
	}
}

new KRK_Infants_Customizer();
?>