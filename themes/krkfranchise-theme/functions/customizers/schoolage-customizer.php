<?php

require_once(get_template_directory() . '/functions/customizers/defaults/schoolage-defaults.php');

add_action( 'customize_register', 'krk_schoolage_customizer' ); 
function krk_schoolage_customizer( $wp_customize ) {
	/**
	* Sections
	*/
	$wp_customize->add_section(
        'krk_schoolage_page_content',
        array(
            'title' => 'School Age Page Content',
            'priority' => 35,
            'active_callback' => function(){ return is_page("our-schoolage"); }
        )
    );
   
	/**
	* Settings
	*/
    $wp_customize->add_setting( 'krk_schoolage_video_upload' );
    $wp_customize->add_setting( 'krk_schoolage_video_thumbnail', array(
        'default' => krk_schoolage_customizer_defaults('krk_schoolage_video_thumbnail')
      ));
    $wp_customize->add_setting( 'krk_schoolage_header_image', array(
        'default' => krk_schoolage_customizer_defaults('krk_schoolage_header_image')
      ));
    $wp_customize->add_setting( 'krk_schoolage_header_title', array(
        'default' => krk_schoolage_customizer_defaults('krk_schoolage_header_title')
      ));
    $wp_customize->add_setting( 'krk_schoolage_header_content', array(
        'default' => krk_schoolage_customizer_defaults('krk_schoolage_header_content')
      ));
    $wp_customize->add_setting( 'krk_schoolage_left_content', array(
        'default' => krk_schoolage_customizer_defaults('krk_schoolage_left_content')
      ));
    $wp_customize->add_setting( 'krk_schoolage_right_content', array(
        'default' => krk_schoolage_customizer_defaults('krk_schoolage_right_content')
      ));
	/**
	* Controls
	*/
    $wp_customize->add_control( 
    	new WP_Customize_Media_Control( $wp_customize, 'krk_schoolage_video_upload', 
    		array(
    			'label' => __( 'School Age Page Video (MP4 Video)' ),
    			'section' => 'krk_schoolage_page_content',
    			'settings' => 'krk_schoolage_video_upload',
    			'mime_type' => 'video/mp4',
			) 
		) 
	);
	$wp_customize->add_control( 
    	new WP_Customize_Control( $wp_customize, 'krk_schoolage_video_thumbnail',
    		array(
    			'label' => __( 'Video Thumbnail' ),
    			'section' => 'krk_schoolage_page_content',
    			'settings' => 'krk_schoolage_video_thumbnail',
				'type'           => 'select',
				'choices'        => array(
					'schoolage1.jpg' => __('Image 1'),
					'schoolage2.jpg' => __('Image 2'),
					'schoolage3.jpg' => __('Image 3'),
					'schoolage4.jpg' => __('Image 4'),
					'schoolage5.jpg' => __('Image 5'),
					'schoolage6.jpg' => __('Image 6'),
				)
			) 
		) 
	);
	$wp_customize->add_control( 
    	new WP_Customize_Image_Control( $wp_customize, 'krk_schoolage_header_image', 
    		array(
    			'label' => __( 'Header Image' ),
    			'section' => 'krk_schoolage_page_content',
    			'settings' => 'krk_schoolage_header_image',
			) 
		) 
	);
	$wp_customize->add_control( 'krk_schoolage_header_title', 
		array(
  			'label' => __( 'Header Title' ),
  			'type' => 'text',
  			'section' => 'krk_schoolage_page_content',
  			'settings' => 'krk_schoolage_header_title',
		) 
	);
	$wp_customize->add_control( 
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_schoolage_header_content', 
			array(
  				'label' => __( 'Header Content' ),
  				'section' => 'krk_schoolage_page_content',
  				'settings' => 'krk_schoolage_header_content',
			)
		) 
	);

	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_schoolage_left_content', 
			array(
  				'label' => __( 'Left Content' ),
  				'section' => 'krk_schoolage_page_content',
  				'settings' => 'krk_schoolage_left_content',
			) 
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_schoolage_right_content',
			array(
  				'label' => __( 'Right Content' ),
  				'section' => 'krk_schoolage_page_content',
  				'settings' => 'krk_schoolage_right_content',
			) 
		)
	);
}
?>