<?php
class KRK_Staff_Customizer extends KRK_Customizer {

	public $page_name = 'our-staff';

	public function __construct() {
		add_action( 'customize_register', array($this, 'register_customizer'));
	}

	function register_customizer($wp_customize) {
		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_staff_page_content',
			array(
				'title' => 'Staff Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_page($this->page_name); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_staff_header_image1');
		$wp_customize->add_setting( 'krk_staff_header_image2');
		$wp_customize->add_setting( 'krk_staff_header_title', array(
			'default' => $this->defaults('krk_staff_header_title')
		));
		$wp_customize->add_setting( 'krk_staff_content', array(
			'default' => $this->defaults('krk_staff_content')
		));

		/**
		 * Controls
		 */
		$wp_customize->add_control(
			new WP_Customize_Image_Control( $wp_customize, 'krk_staff_header_image1',
				array(
					'label' => __( 'Header Image 1' ),
					'section' => 'krk_staff_page_content',
					'settings' => 'krk_staff_header_image1',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control( $wp_customize, 'krk_staff_header_image2',
				array(
					'label' => __( 'Our Team Image' ),
					'section' => 'krk_staff_page_content',
					'settings' => 'krk_staff_header_image2',
					'type'           => 'select',
					'choices'        => array(
						'OurTeam_1.jpg' => __('Image 1'),
						'OurTeam_2.jpg' => __('Image 2')
					)
				)
			)
		);
		$wp_customize->add_control( 'krk_staff_header_title',
			array(
				'label' => __( 'Header Title' ),
				'type' => 'text',
				'section' => 'krk_staff_page_content',
				'settings' => 'krk_staff_header_title',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_staff_content',
				array(
					'label' => __( 'Page Content' ),
					'section' => 'krk_staff_page_content',
					'settings' => 'krk_staff_content',
				)
			)
		);
	}

	protected function defaults($setting) {
		if($setting == 'krk_staff_header_image1') {
			return get_template_directory_uri() . '/images/ChildrenComeFirst_1.jpg';
		}
		elseif($setting == 'krk_staff_header_image2') {
			return 'OurTeam_1.jpg';
		}
		elseif($setting == 'krk_staff_header_title') {
			return 'The Children Come<br>First';
		}
		elseif($setting == 'krk_staff_content') {
			return <<<EOT
               <div class="section-xtx">
                  <h4>Our Teachers Teach "First Class Kids" </h4>
                  <p>Our teachers are dedicated to the Kids <span class="krk-ticks">R</span> Kids Learning Academies' philosphy, "Hug First, and Then Teach." Our staff members are highly trained, thoughtful, and nurturing individuals with a passion for work in early childcare and education. They have a passon for what they do and believe the lives they touch every day makes a big impact for families in our community.</p>
                  <p>Our teachers:</p>
                  <ul>
                      <li>"Hug First, and Then Teach." Kids <span class="krk-ticks">R</span> Kids understand the learning process is optimized when children trust their caregivers.</li>
                      <li> Invest in their students and families</li>
                      <li> Participate in on-going and professional childcare development training</li>
                      <li> Are CPR and first-aid certified for infant, child, and adult</li>
                      <li> Maintain a safe, clean, and loving environment</li>
                  </ul>
                  <p> Our staff are carefully selected and extensively trained to provide the best childcare experience available. The Kids <span class="krk-ticks">R</span> Kids staff are central to our success. We encourage you to schedule a tour today so you and your child can visit and speak with our teachers first hand!</p>
              </div>
EOT;
		}
	}
}

new KRK_Staff_Customizer();
?>
