<?php

require_once(get_template_directory() . '/functions/customizers/defaults/school-defaults.php');

add_action( 'customize_register', 'krk_school_customizer' );
function krk_school_customizer( $wp_customize ) {

	/**
	 * Sections
	 */
	$wp_customize->add_section(
		'krk_school_page_content',
		array(
			'title' => 'School Page Content',
			'priority' => 35,
			'active_callback' => function(){ return is_page("our-school"); }
		)
	);

	/**
	 * Settings
	 */
	$wp_customize->add_setting( 'krk_school_header_image');
	$wp_customize->add_setting( 'krk_school_header_title', array(
        'default' => krk_school_customizer_defaults('krk_school_header_title')
      ));
	$wp_customize->add_setting( 'krk_school_meal_content', array(
		'default' => krk_school_customizer_defaults('krk_school_meal_content')
	));
	$wp_customize->add_setting( 'krk_school_classroom_content', array(
        'default' => krk_school_customizer_defaults('krk_school_classroom_content')
      ));

	/**
	 * Controls
	 */
	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'krk_school_header_image',
			array(
				'label' => __( 'School Information Header Image' ),
				'section' => 'krk_school_page_content',
				'settings' => 'krk_school_header_image',
			)
		)
	);
	$wp_customize->add_control( 'krk_school_header_title',
		array(
			'label' => __( 'School Information Header Title' ),
			'type' => 'text',
			'section' => 'krk_school_page_content',
			'settings' => 'krk_school_header_title',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_school_classroom_content',
			array(
				'label' => __( 'Classroom Content' ),
				'section' => 'krk_school_page_content',
				'settings' => 'krk_school_classroom_content',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_school_meal_content',
			array(
				'label' => __( 'Meal Content' ),
				'section' => 'krk_school_page_content',
				'settings' => 'krk_school_meal_content',
			)
		)
	);
}
?>