<?php

require_once(get_template_directory() . '/functions/customizers/defaults/home-defaults.php');

add_action( 'customize_register', 'krk_home_customizer' ); 
function krk_home_customizer( $wp_customize ) {
	/**
	* Sections
	*/
	$wp_customize->add_section(
        'krk_home_page_content',
        array(
            'title' => 'Home Page Content',
            'priority' => 35,
            'active_callback' => function(){ return is_home(); }
        )
    );
   
	/**
	* Settings
	*/
	$wp_customize->add_setting( 'krk_home_main_image_custom',
		array(
		));
	$wp_customize->add_setting( 'krk_home_main_image',
		array(
			'default' => krk_home_customizer_defaults('krk_home_main_image'),
		));
	$wp_customize->add_setting( 'krk_home_main_title', array(
		'default' => krk_home_customizer_defaults('krk_home_main_title')
	));
	$wp_customize->add_setting( 'krk_home_disable_main_button', array(
	));
	$wp_customize->add_setting( 'krk_home_disable_virtual_tour_button', array(
	));
	$wp_customize->add_setting( 'krk_home_main_button_text', array(
		'default' => krk_home_customizer_defaults('krk_home_main_button_text')
	));
	$wp_customize->add_setting( 'krk_home_main_button_url', array(
		'default' => krk_home_customizer_defaults('krk_home_main_button_url')
	));
    $wp_customize->add_setting( 'krk_home_video_upload' );
    $wp_customize->add_setting( 'krk_home_sections',
        array(
            'default' => __('Video,Philosophy,Accreditations,Our-Stories,Menu,Technology,Our-School,Events,News,Address'),
        ));
    $wp_customize->add_setting( 'krk_home_school_content', array(
		'default' => krk_home_customizer_defaults('krk_home_school_content')
	));

	$wp_customize->add_setting( 'krk_home_menu_content', array(
		'default' => krk_home_customizer_defaults('krk_home_menu_content')
	));

	$wp_customize->add_setting( 'krk_home_disable_Video', array(
	));
	$wp_customize->add_setting( 'krk_home_disable_Philosophy', array(
	));
	$wp_customize->add_setting( 'krk_home_disable_Accreditations', array(
	));
	$wp_customize->add_setting( 'krk_home_disable_Our-Stories', array(
	));
	$wp_customize->add_setting( 'krk_home_disable_Menu', array(
	));
	$wp_customize->add_setting( 'krk_home_disable_Technology', array(
	));
	$wp_customize->add_setting( 'krk_home_disable_Our-School', array(
	));
	$wp_customize->add_setting( 'krk_home_disable_Events', array(
	));
	$wp_customize->add_setting( 'krk_home_disable_News', array(
	));
	$wp_customize->add_setting( 'krk_home_disable_Address', array(
	));

	/**
	* Controls
	*/
	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'krk_home_main_image_custom',
			array(
				'label' => __( 'Jumbo Custom Image' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_main_image_custom',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_main_image',
			array(
				'label' => __( 'Jumbo Image' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_main_image',
				'type'           => 'select',
				'choices'        => array(
					'Franchise_Main1.jpg' => __('Image 1'),
					'Franchise_Main2.jpg' => __('Image 2'),
					'Franchise_Main3.jpg'=> __('Image 3'),
				)
			)
		)
	);
	$wp_customize->add_control( 'krk_home_main_title',
		array(
			'label' => __( 'Main Section Title' ),
			'type' => 'text',
			'section' => 'krk_home_page_content',
			'settings' => 'krk_home_main_title',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_disable_main_button',
			array(
				'label' => __( 'Disable Main Button' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_disable_main_button',
				'type'           => 'checkbox',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_disable_virtual_tour_button',
			array(
				'label' => __( 'Disable Virtual Tour Button' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_disable_virtual_tour_button',
				'type'           => 'checkbox',
			)
		)
	);
	$wp_customize->add_control( 'krk_home_main_button_text',
		array(
			'label' => __( 'Main Button Text' ),
			'type' => 'text',
			'section' => 'krk_home_page_content',
			'settings' => 'krk_home_main_button_text',
		)
	);
	$wp_customize->add_control( 'krk_home_main_button_url',
		array(
			'label' => __( 'Main Button URL' ),
			'type' => 'text',
			'section' => 'krk_home_page_content',
			'settings' => 'krk_home_main_button_url',
		)
	);
    $wp_customize->add_control( 
    	new WP_Customize_Media_Control( $wp_customize, 'krk_home_video_upload', 
    		array(
    			'label' => __( 'From Our Owners to You Video (MP4 Video)' ),
    			'section' => 'krk_home_page_content',
    			'settings' => 'krk_home_video_upload',
    			'mime_type' => 'video/mp4',
			) 
		) 
	);
	$wp_customize->add_control( 
		new WP_Customize_Sortable_Control($wp_customize, 'krk_home_order', 
			array(
  				'label' => __( 'Home Sections Order' ),
  				'section' => 'krk_home_page_content',
  				'settings' => 'krk_home_sections'
			)
		) 
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_disable_Video',
			array(
				'label' => __( 'Disable Video Section' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_disable_Video',
				'type'           => 'checkbox',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_disable_Philosophy',
			array(
				'label' => __( 'Disable Philosophy Section' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_disable_Philosophy',
				'type'           => 'checkbox',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_disable_Accreditations',
			array(
				'label' => __( 'Disable Accreditations Section' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_disable_Accreditations',
				'type'           => 'checkbox',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_disable_Our-Stories',
			array(
				'label' => __( 'Disable Our-Stories Section' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_disable_Our-Stories',
				'type'           => 'checkbox',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_disable_Menu',
			array(
				'label' => __( 'Disable Menu Section' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_disable_Menu',
				'type'           => 'checkbox',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_disable_Technology',
			array(
				'label' => __( 'Disable Technology Section' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_disable_Technology',
				'type'           => 'checkbox',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_disable_Our-School',
			array(
				'label' => __( 'Disable Our-School Section' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_disable_Our-School',
				'type'           => 'checkbox',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_disable_Events',
			array(
				'label' => __( 'Disable Events Section' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_disable_Events',
				'type'           => 'checkbox',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_disable_News',
			array(
				'label' => __( 'Disable News Section' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_disable_News',
				'type'           => 'checkbox',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_home_disable_Address',
			array(
				'label' => __( 'Disable Address Section' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_disable_Address',
				'type'           => 'checkbox',
			)
		)
	);
    $wp_customize->add_control(
        new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_home_school_content',
            array(
                'label' => __( 'Our School Content' ),
                'section' => 'krk_home_page_content',
                'settings' => 'krk_home_school_content',
            )
        )
    );
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_home_menu_content',
			array(
				'label' => __( 'Our Menu Content' ),
				'section' => 'krk_home_page_content',
				'settings' => 'krk_home_menu_content',
			)
		)
	);
}
?>