<?php

require_once(get_template_directory() . '/functions/customizers/defaults/staff-defaults.php');

add_action( 'customize_register', 'krk_staff_customizer' );
function krk_staff_customizer( $wp_customize ) {

	/**
	 * Sections
	 */
	$wp_customize->add_section(
		'krk_staff_page_content',
		array(
			'title' => 'Staff Page Content',
			'priority' => 35,
			'active_callback' => function(){ return is_page("our-staff"); }
		)
	);

	/**
	 * Settings
	 */
	$wp_customize->add_setting( 'krk_staff_header_image1');
	$wp_customize->add_setting( 'krk_staff_header_image2');
	$wp_customize->add_setting( 'krk_staff_header_title', array(
        'default' => krk_staff_customizer_defaults('krk_staff_header_title')
      ));
	$wp_customize->add_setting( 'krk_staff_content', array(
        'default' => krk_staff_customizer_defaults('krk_staff_content')
      ));

	/**
	 * Controls
	 */
	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'krk_staff_header_image1',
			array(
				'label' => __( 'Header Image 1' ),
				'section' => 'krk_staff_page_content',
				'settings' => 'krk_staff_header_image1',
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'krk_staff_header_image2',
			array(
				'label' => __( 'Header Image 2' ),
				'section' => 'krk_staff_page_content',
				'settings' => 'krk_staff_header_image2',
			)
		)
	);
	$wp_customize->add_control( 'krk_staff_header_title',
		array(
			'label' => __( 'Header Title' ),
			'type' => 'text',
			'section' => 'krk_staff_page_content',
			'settings' => 'krk_staff_header_title',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_staff_content',
			array(
				'label' => __( 'Page Content' ),
				'section' => 'krk_staff_page_content',
				'settings' => 'krk_staff_content',
			)
		)
	);
}
?>