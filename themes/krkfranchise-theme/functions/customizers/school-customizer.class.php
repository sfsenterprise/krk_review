<?php
class KRK_School_Customizer extends KRK_Customizer {

	public $page_name = 'our-school';

	public function __construct() {
		add_action( 'customize_register', array($this, 'register_customizer'));
	}

	function register_customizer($wp_customize) {
		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_school_page_content',
			array(
				'title' => 'School Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_page($this->page_name); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_school_header_image');
		$wp_customize->add_setting( 'krk_school_header_title', array(
			'default' => $this->defaults('krk_school_header_title')
		));
		$wp_customize->add_setting( 'krk_school_meal_content', array(
			'default' => $this->defaults('krk_school_meal_content')
		));
		$wp_customize->add_setting( 'krk_school_classroom_content', array(
			'default' => $this->defaults('krk_school_classroom_content')
		));

		/**
		 * Controls
		 */
		$wp_customize->add_control(
			new WP_Customize_Image_Control( $wp_customize, 'krk_school_header_image',
				array(
					'label' => __( 'School Information Header Image' ),
					'section' => 'krk_school_page_content',
					'settings' => 'krk_school_header_image',
				)
			)
		);
		$wp_customize->add_control( 'krk_school_header_title',
			array(
				'label' => __( 'School Information Header Title' ),
				'type' => 'text',
				'section' => 'krk_school_page_content',
				'settings' => 'krk_school_header_title',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_school_classroom_content',
				array(
					'label' => __( 'Classroom Content' ),
					'section' => 'krk_school_page_content',
					'settings' => 'krk_school_classroom_content',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_school_meal_content',
				array(
					'label' => __( 'Meal Content' ),
					'section' => 'krk_school_page_content',
					'settings' => 'krk_school_meal_content',
				)
			)
		);
	}

	protected function defaults($setting)
	{
		if ($setting == 'krk_school_header_image') {
			return get_template_directory_uri() . '/images/OurClassroom.jpg';
		} elseif ($setting == 'krk_school_header_title') {
			return 'Our Classrooms';
		} elseif ($setting == 'krk_school_meal_content') {
			return <<<EOT
    <h2> Complete, balanced meals </h2>
    <p></p>
    <p>Our chef prepares and serves two delicious meals and two nutritious snacks per day to provide
     the balanced nutrition each child needs to learn and grow.</p>
EOT;
		} elseif ($setting == 'krk_school_classroom_content') {
			$location_city = do_shortcode('[location_city]');
			$location_full_state = do_shortcode('[location_full_state]');
			$location_state = do_shortcode('[location_state]');
			$contact_us_url = get_multisite_path('schedule-tour');

			return <<<EOT
      <h2>School Information</h2>
      <p>Our Learning Academy is privately owned and operated as part of a franchising system. With the support we receive, we are able to offer the very best in early childhood education and care. If you haven't had a chance to visit us and see our school, <a href="{$contact_us_url}">CONTACT US</a> today and we'll be glad to show you around.
      </p>

      <h2>Highest Quality Childcare</h2>
      <p>Our Learning Academy is licensed through the state, and we understand the importance of staying on top of our quality of service and programming. If you ever notice something that doesn't fall under our top-quality service and programming, please let us know and we'll be sure to bring it up to code.
      </p>

      <h2>Policies &amp; Procedures</h2>
      <p>Kids <span class="krk-ticks">R</span> Kids Learning Academy of {$location_city}, {$location_state} is open to you, the parent, any time your child is present in the school. For the safety of all children, we do request that you make your presence known to the person in charge and cooperate in not disrupting the school´s program.
      </p>

      <p>Families are encouraged to eat breakfast or lunch with their children at school. (See Director for details.)</p>

      <h3>Children Served</h3>
      <p>Kids <span class="krk-ticks">R</span> Kids of {$location_city}, {$location_state} is open to children ages 6 weeks to 12 years old without discrimination based on political affiliation, religion, race, color, sex, mental or physical disabilities.
      </p>

      <h3>Hours of Operation</h3>
      <p>Monday through Friday from 6:00 AM- 6:30 PM, 12 months per year.</p>

      <h3>Standard Holidays Observed</h3>
      <p>New Year´s Day, Memorial Day, Independence Day, Labor Day, Thanksgiving Day, Christmas Day</p>

      <h3>Nutrition</h3>
      <p>Breakfast, lunch, morning and afternoon snacks are available at Kids <span class="krk-ticks">R</span> Kids. Information is provided by the school on any charges that may apply.
      </p>

      <p>For bottle-fed children, families must provide prepared formula or mother's milk placed in bottles. We will inform you of the policy regarding baby foods. All infant items should be marked with the child´s first and last name.
      </p>

      <p>Weekly menus for children on table food are available for review.</p>

      <p>Please see the Director for information regarding special diets and allergies.</p>

      <h3>Fees</h3>
      <p>A tuition and fee schedule will be provided by Kids <span class="krk-ticks">R</span> Kids. You will be informed of all rates, discounts, late fees, breakfast charges, activity fees, registration fees, check return fees, and any school-specific fees.</p>

      <p>The tuition and fee schedule will also provide you with information regarding when payment is due and any late charges to be applied. Also included on the tuition and fee schedule is the School Policy on attendance, explaining the number of days that require full tuition, half tuition, and vacation days allowed. </p>

      <h3>Transportation (To and From Public School)</h3>
      <p>Transportation is provided to and from public or private elementary or grade schools designated by our school.</p>

      <p>Transportation Agreements are signed once for each school year. Vehicle Emergency Forms must be completed and information must be kept up-to-date.</p>

      <p>We will inform you of the time your child must be present at our center for transportation to his or her elementary or grade school.</p>

      <p>If at any time your child will not be transported to school by Kids <span class="krk-ticks">R</span> Kids or will not be picked up by Kids <span class="krk-ticks">R</span> Kids from school at the end of the school day, you must notify Kids <span class="krk-ticks">R</span> Kids in advance. Kids <span class="krk-ticks">R</span> Kids will inform you of the time by which you must make this notification.</p>

      <h3>Transportation (Field Trips)</h3>
      <p>Field Trip Permission Forms must be signed and dated for each field trip. Vehicle Emergency Forms must be completed and information must be kept up-to-date.</p>

      <p>Children going on field trips must wear a Kids <span class="krk-ticks">R</span> Kids t-shirt. The school will also provide an identification bracelet will be provided by the school for each child.</p>

      <h3>Internet Access</h3>
      <p>Internet access to your child´s classroom is by password only. By enrolling your child at Kids <span class="krk-ticks">R</span> Kids you consent that your child(ren) may also be seen on the Internet by other persons viewing with passwords.</p>

      <h3>Severe Weather, Fire or Emergency Situations</h3>
      <p>If an emergency situation develops such as severe weather, fire, physical damage to the building, or any other situation that poses a threat, the safety of the children is our first concern.</p>

      <p>Our school is equipped with a weather band radio, a fire alarm sprinkler system and fire extinguishers. Fire and severe weather drills are conducted every 30 days.</p>

      <p>If there is an emergency situation and it becomes necessary to close the school, parents will be notified to make arrangements for early pick-up.</p>

      <p>If there is inclement weather and it is determined that the school will not open, parents will be informed by the school Facebook page with closing details.</p>

      <p>If an emergency situation develops and it is determined that the building or premises are unsafe, your child(ren) will be transported to a safe location.</p>

      <p>Parents will be notified of the situation and you will be required to pick up your child(ren) as soon as possible.</p>

      <h3>Discipline</h3>
      <p>At Kids <span class="krk-ticks">R</span> Kids of {$location_city}, {$location_state}, we use a method of "redirection" to guide your child(ren) toward appropriate behavior. If a child is engaged in behavior not conducive to a safe and happy learning environment, the teacher will "redirect" the child toward appropriate behavior. (The use of physical punishment and harsh language is prohibited.)</p>

      <h3>The Family's Role</h3>
      <p>The relationship between families and school staff is vital to the success of a child´s experience. A partnership must be formed the first day with open communication and understanding that the development and growth of the child is our top priority.</p>

      <p>Families can assist and help ensure a smooth transition by doing the following:</p>
      <ul>
          <li>Sign children in and out at the front desk and then escort them to their designated class.</li>
          <li>Have all forms completed promptly.</li>
          <li>Update forms as needed when changes occur (i.e. new phone number, address,  etc.) </li> 
          <li>Keep staff informed of special needs or changes that might affect your child's behavior.</li>
          <li>Notify the school if your child is ill.</li>
          <li>Do not bring an ill child to the school.</li>
          <li>Notify the school if your child will be absent.</li>
          <li>Notify the school if you will be later than usual picking up your child.</li>
          <li>Provide a change of clothes marked with your child´s first and last name. (School is not responsible for lost clothing.)</li>
          <li>Children should be dressed properly for the weather and play.</li>
          <li>Do not allow children to bring toys.</li>
          <li>Participate in the school´s special activities.</li>
          <li>Attend scheduled parent meetings and conferences.</li>
          <li>Ask questions and address concerns as they arise.</li>
      </ul>
EOT;
		}
		else {
			return  '';
		}
	}
}

new KRK_School_Customizer();
?>