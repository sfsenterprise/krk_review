<?php

add_action( 'customize_register', 'krk_curriculum_customizer' ); 
function krk_curriculum_customizer( $wp_customize ) {

	/**
	* Sections
	*/
	$wp_customize->add_section(
        'krk_curriculum_page_content',
        array(
            'title' => 'Curriculum Page Content',
            'priority' => 35,
            'active_callback' => function(){ return is_page("our-curriculum"); }
        )
    );
   
	/**
	* Settings
	*/
    $wp_customize->add_setting( 'krk_curriculum_section1_header_image');
    $wp_customize->add_setting( 'krk_curriculum_section1_header_title');
    $wp_customize->add_setting( 'krk_curriculum_section1_content');
	$wp_customize->add_setting( 'krk_curriculum_big_steps_snip_content');
	$wp_customize->add_setting( 'krk_curriculum_fast_track_snip_content');
	$wp_customize->add_setting( 'krk_curriculum_gym_snip_content');
	$wp_customize->add_setting( 'krk_curriculum_steam_ahead_snip_content');
	$wp_customize->add_setting( 'krk_curriculum_brain_waves_snip_content');
	$wp_customize->add_setting( 'krk_curriculum_camp_snip_content');
	$wp_customize->add_setting( 'krk_curriculum_steam_ahead_header_image');
	$wp_customize->add_setting( 'krk_curriculum_steam_ahead_header_title');
	$wp_customize->add_setting( 'krk_curriculum_steam_ahead_content');
	$wp_customize->add_setting( 'krk_curriculum_interactive_tech_content');


	/**
	* Controls
	*/
	$wp_customize->add_control( 
    	new WP_Customize_Image_Control( $wp_customize, 'krk_curriculum_section1_header_image',
    		array(
    			'label' => __( 'Section 1 Header Image' ),
    			'section' => 'krk_curriculum_page_content',
    			'settings' => 'krk_curriculum_section1_header_image',
			) 
		) 
	);
	$wp_customize->add_control( 'krk_curriculum_section1_header_title',
		array(
  			'label' => __( 'Section 1 Header Title' ),
  			'type' => 'text',
  			'section' => 'krk_curriculum_page_content',
  			'settings' => 'krk_curriculum_section1_header_title',
		) 
	);
	$wp_customize->add_control( 
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_section1_content',
			array(
  				'label' => __( 'Section 1 Content' ),
  				'section' => 'krk_curriculum_page_content',
  				'settings' => 'krk_curriculum_section1_content',
			)
		) 
	);

	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'krk_curriculum_steam_ahead_header_image',
			array(
				'label' => __( 'Steam Ahead Header Image' ),
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_steam_ahead_header_image',
			)
		)
	);
	$wp_customize->add_control( 'krk_curriculum_steam_ahead_header_title',
		array(
			'label' => __( 'Steam Ahead Header Title' ),
			'type' => 'text',
			'section' => 'krk_curriculum_page_content',
			'settings' => 'krk_curriculum_steam_ahead_header_title',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_steam_ahead_content',
			array(
				'label' => __( 'Steam Ahead Content' ),
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_steam_ahead_content',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_big_steps_snip_content',
			array(
				'label' => __( 'Big Steps Snippet' ),
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_big_steps_snip_content',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_fast_track_snip_content',
			array(
				'label' => __( 'Fast Tracks Snippet' ),
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_fast_track_snip_content',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_gym_snip_content',
			array(
				'label' => __( 'G.Y.M. Snippet' ),
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_gym_snip_content',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_steam_ahead_snip_content',
			array(
				'label' => __( 'Steam Ahead Snippet' ),
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_steam_ahead_snip_content',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_brain_waves_snip_content',
			array(
				'label' => __( 'Brain Waves Snippet' ),
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_brain_waves_snip_content',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_camp_snip_content',
			array(
				'label' => __( 'Camp Snippet' ),
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_camp_snip_content',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_interactive_tech_content',
			array(
				'label' => __( 'Interactive Technology Content' ),
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_interactive_tech_content',
			)
		)
	);
}
?>