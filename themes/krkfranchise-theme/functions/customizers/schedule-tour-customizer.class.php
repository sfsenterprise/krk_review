<?php
class KRK_Schedule_Tour_Customizer extends KRK_Customizer{

    public $page_name = 'schedule-tour';

    public function __construct() {
        add_action( 'customize_register', array($this, 'register_customizer'));
    }

    function register_customizer($wp_customize) {
        /**
         * Sections
         */
        $wp_customize->add_section(
            'krk_schedule_tour_page_content',
            array(
                'title' => 'Schedule Tour Page Content',
                'priority' => 35,
                'active_callback' => function(){ return is_page($this->page_name); }
            )
        );

        /**
         * Settings
         */
        $wp_customize->add_setting( 'krk_schedule_tour_header_image', array(
        ));

        /**
         * Controls
         */
        $wp_customize->add_control(
            new WP_Customize_Image_Control( $wp_customize, 'krk_schedule_tour_header_image',
                array(
                    'label' => __( 'Header Image' ),
                    'section' => 'krk_schedule_tour_page_content',
                    'settings' => 'krk_schedule_tour_header_image',
                )
            )
        );
    }

    protected function defaults($setting) {
        if($setting == 'krk_schedule_tour_header_image') {
            return  get_stylesheet_directory_uri() . '/images/img-22.jpg';
        }
}
}

new KRK_Schedule_Tour_Customizer();
?>