<?php
class KRK_Curriculum_Customizer extends KRK_Customizer {

	public $page_name = 'our-curriculum';

	public function __construct() {
		add_action( 'customize_register', array($this, 'register_customizer'));
	}

	function register_customizer( $wp_customize ) {

		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_curriculum_page_content',
			array(
				'title' => 'Curriculum Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_page("our-curriculum"); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_curriculum_section1_header_image');
		$wp_customize->add_setting( 'krk_curriculum_section1_header_title');
		$wp_customize->add_setting( 'krk_curriculum_section1_content');
		$wp_customize->add_setting( 'krk_curriculum_big_steps_snip_content');
		$wp_customize->add_setting( 'krk_curriculum_fast_track_snip_content');
		$wp_customize->add_setting( 'krk_curriculum_gym_snip_content');
		$wp_customize->add_setting( 'krk_curriculum_steam_ahead_snip_content');
		$wp_customize->add_setting( 'krk_curriculum_brain_waves_snip_content');
		$wp_customize->add_setting( 'krk_curriculum_camp_snip_content');
		$wp_customize->add_setting( 'krk_curriculum_steam_ahead_header_image');
		$wp_customize->add_setting( 'krk_curriculum_steam_ahead_header_title');
		$wp_customize->add_setting( 'krk_curriculum_steam_ahead_content');
		$wp_customize->add_setting( 'krk_curriculum_interactive_tech_image');
		$wp_customize->add_setting( 'krk_curriculum_interactive_tech_content');


		/**
		 * Controls
		 */

		$wp_customize->add_control(
			new WP_Customize_Image_Control( $wp_customize, 'krk_curriculum_section1_header_image',
				array(
					'label' => __( 'Section 1 Header Image' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_section1_header_image',
				)
			)
		);

		$wp_customize->add_control( 'krk_curriculum_section1_header_title',
			array(
				'label' => __( 'Section 1 Header Title' ),
				'type' => 'text',
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_section1_header_title',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_section1_content',
				array(
					'label' => __( 'Section 1 Content' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_section1_content',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control( $wp_customize, 'krk_curriculum_steam_ahead_header_image',
				array(
					'label' => __( 'STEAM AHEAD Header Image' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_steam_ahead_header_image',
					'type'           => 'select',
					'choices'        => array(
						'STEAM1.jpg' => __('Image 1'),
						'STEAM2.jpg' => __('Image 2'),
					)
				)
			)
		);
		$wp_customize->add_control( 'krk_curriculum_steam_ahead_header_title',
			array(
				'label' => __( 'Steam Ahead Header Title' ),
				'type' => 'text',
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_steam_ahead_header_title',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_steam_ahead_content',
				array(
					'label' => __( 'Steam Ahead Content' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_steam_ahead_content',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_big_steps_snip_content',
				array(
					'label' => __( 'Big Steps Snippet' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_big_steps_snip_content',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_fast_track_snip_content',
				array(
					'label' => __( 'Fast Tracks Snippet' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_fast_track_snip_content',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_gym_snip_content',
				array(
					'label' => __( 'G.Y.M. Snippet' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_gym_snip_content',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_steam_ahead_snip_content',
				array(
					'label' => __( 'Steam Ahead Snippet' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_steam_ahead_snip_content',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_brain_waves_snip_content',
				array(
					'label' => __( 'Brain Waves Snippet' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_brain_waves_snip_content',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_camp_snip_content',
				array(
					'label' => __( 'Camp Snippet' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_camp_snip_content',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control( $wp_customize, 'krk_curriculum_interactive_tech_image',
				array(
					'label' => __( 'Interactive Technology Header Image' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_interactive_tech_image',
					'type'           => 'select',
					'choices'        => array(
						'Technology1.jpg' => __('Image 1'),
						'Technology2.jpg' => __('Image 2'),
					)
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_curriculum_interactive_tech_content',
				array(
					'label' => __( 'Interactive Technology Content' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_interactive_tech_content',
				)
			)
		);
	}

	protected function defaults($setting) {
		if($setting == 'krk_curriculum_steam_ahead_header_image') {
			return "STEAM1.jpg";
		}elseif($setting == 'krk_curriculum_interactive_tech_image') {
			return "Technology1.jpg";
		}
		elseif($setting == 'krk_curriculum_section1_header_image') {
			return get_template_directory_uri() . "/images/OurCurriculum.jpg";
		}

		elseif($setting == 'krk_curriculum_section1_header_title') {
			return <<<EOT
                Our <br> Curriculum 
EOT;
		}
		elseif($setting == 'krk_curriculum_section1_content') {
			return <<<EOT
        <p>The Kids <span class="krk-ticks">R</span> Kids Learning Academies’ experience offers a unique learning package of loving guidance and creative exploration for your child. </p>
        <p> Our professional staff cares about all aspects of your child's academic and social growth. Our <b> First Class Curriculum&reg;</b> has been developed by a team of writers
         with years of experience in early-childhood education and formative-years. The <b> First Class Curriculum&reg;</b> is specifically designed for every stage of your child's
         growth. Our teaching philosophy addresses the addresses the “whole” child within a loving environment, nurturing inquisitive minds through our learning avenues.
         Our teachers have early childhood backgrounds which provide our schools with a qualified staff to care for children from early infancy thru pre-adolescence.</p>
        <p>Kids <span class="krk-ticks">R</span> Kids is truly a learning academy - a creative destination for your child to thrive!</p>
EOT;

		}
		elseif($setting == 'krk_curriculum_steam_ahead_header_title') {
			return <<<EOT
                STEAM AHEAD Curriculum
EOT;
		}
		elseif($setting == 'krk_curriculum_steam_ahead_content') {
			return <<<EOT
                <p> The classroom explore STEAM AHEAD&#174; projects together. In an effort to develop a child's natural 
                curiosity, teachers encorage them to ask, "why?" Children work and play in an environment where they 
                have opportunities to observe, question, investigate, predict, experiment, build and share what they learn. 
                Projects are hands-on and bring together many facets of what children find interesting in a way that packs an 
                array of educational "AH HA" moments.</p>
                <br>

                <h2> What is STEAM AHEAD? </h2>
                <p> Kids <span class="krk-ticks">R</span> Kids STEAM AHEAD&#174; is an integrated project-based curriculum incorporating more science, 
                technology, engineering, art, and math into a child's everyday learning through exploration and play. </p>
                <br>

                <h2> Why STEAM AHEAD? </h2>
                <p> Unlike academic learning of memorization with no logical application, Kids <span class="krk-ticks">R</span> Kids STEAM AHEAD&#174; 
                projects encourage early learners to be natural scientists using reasoning, hypothesising, predicting, and 
                theorising during the natural trial and error of play. </p>
EOT;
		}
		elseif($setting == 'krk_curriculum_big_steps_snip_content') {
			return <<<EOT
                <p> 6 weeks through 36 months: Infant and toddlers are immersed in a stimulating hands-on environment.
                Each activity is designed to help your child achieve important milestones while having fun at the same time.</p>
 
EOT;
		}
		elseif($setting == 'krk_curriculum_fast_track_snip_content') {
			return <<<EOT
                <p>3 through 5 years: Our goal is to help prepare your preschool child for the challenges ahead 
                 while instilling a sense of confidence and accomplishment.</p>

EOT;
		}
		elseif($setting == 'krk_curriculum_gym_snip_content') {
			return <<<EOT
                <p>5 through 12 years: Your child's afternoon is filled with age-appropriate activities 
                 geared to challenge young minds while building confidence and security.</p>

EOT;
		}
		elseif($setting == 'krk_curriculum_steam_ahead_snip_content') {
			return <<<EOT
               <p>3 through 5 years: We provide students with opportunities to be innovative, 
               creative and resourceful, as they build strong foundations in science,
               technology, engineering, art, and math. </p>
 
EOT;
		}
		elseif($setting == 'krk_curriculum_brain_waves_snip_content') {
			return <<<EOT
               <p>All ages: We use strategies to support neural pathways for language, 
               social-emotional, cognitive, and physical development during these crucial years. </p>

EOT;
		}
		elseif($setting == 'krk_curriculum_camp_snip_content') {
			return <<<EOT
                <p>5 through 12 years: Ten engaging weeks designed to instill confidence, encourage 
                teamwork, and foster fun. Campers will also experience fabulous weekly field trips.</p>

EOT;
		}
		elseif($setting == 'krk_curriculum_interactive_tech_content') {
			return <<<EOT
               <h4>The 21st Century Learner</h4>
               <p>Our newest generation of learners are growing up in the digital age where everything
                  is touch activated. Our schools have Smart Board technology with Internet capabilities
                  to log on and visit the world from the classroom. Our curriculum also correlates with
                  the award-winning ABCmouse.com for online activities designed to reinforce lessons
                  from the classroom to home. We encourage family involvement in every child's education
                  at home to prepare for kindergarten and beyond. </p>
EOT;
		}
	}
}

new KRK_Curriculum_Customizer();

?>