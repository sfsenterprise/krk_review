<?php

add_action( 'customize_register', 'krk_news_customizer' );
function krk_news_customizer( $wp_customize ) {

	/**
	 * Sections
	 */
	$wp_customize->add_section(
		'krk_news_page_content',
		array(
			'title' => 'News Page Content',
			'priority' => 35,
			'active_callback' => function(){ return is_post_type_archive('krk_news'); }
		)
	);

	/**
	 * Settings
	 */
	$wp_customize->add_setting( 'krk_news_header_image');

	/**
	 * Controls
	 */
	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'krk_news_header_image',
			array(
				'label' => __( 'Header Image' ),
				'section' => 'krk_news_page_content',
				'settings' => 'krk_news_header_image',
			)
		)
	);
}
?>