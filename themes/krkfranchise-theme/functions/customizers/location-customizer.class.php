<?php
class KRK_Location_Customizer extends KRK_Customizer {

	public $page_name = 'our-location';

	public function __construct() {
		add_action( 'customize_register', array($this, 'register_customizer'));
	}

	function register_customizer($wp_customize) {
		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_location_page_content',
			array(
				'title' => 'Location Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_page($this->page_name); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_location_directions_header_image');
		$wp_customize->add_setting( 'krk_location_directions_header_title', array(
			'default' => $this->defaults('krk_location_directions_header_title')
		));
		$wp_customize->add_setting( 'krk_location_directions_content', array(
			'default' => $this->defaults('krk_location_directions_content')
		));
		$wp_customize->add_setting( 'krk_location_section2_content', array(
			'default' => $this->defaults('krk_location_section2_content')
		));

		/**
		 * Controls
		 */
		$wp_customize->add_control(
			new WP_Customize_Image_Control( $wp_customize, 'krk_location_directions_header_image',
				array(
					'label' => __( 'Directions Header Image' ),
					'section' => 'krk_location_page_content',
					'settings' => 'krk_location_directions_header_image',
				)
			)
		);
		$wp_customize->add_control( 'krk_location_directions_header_title',
			array(
				'label' => __( 'Directions Header Title' ),
				'type' => 'text',
				'section' => 'krk_location_page_content',
				'settings' => 'krk_location_directions_header_title',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_location_directions_content',
				array(
					'label' => __( 'Directions Content' ),
					'section' => 'krk_location_page_content',
					'settings' => 'krk_location_directions_content',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_location_section2_content',
				array(
					'label' => __( 'Section 2 Content' ),
					'section' => 'krk_location_page_content',
					'settings' => 'krk_location_section2_content',
				)
			)
		);
	}

	protected function defaults($setting) {
		if($setting == 'krk_location_directions_header_image') {
			return get_template_directory_uri() . '/images/img-31.jpg';
		}
		elseif($setting == 'krk_location_directions_header_title') {
			return 'Directions';
		}
		elseif ($setting == 'krk_location_directions_content') {
			$schedule_tour_url = get_site_url() . '/schedule-tour';
			return <<<EOT
      <h4>Hello!</h4>
      <p>We're so happy you found us. Kids <span class="krk-ticks">R</span> Kids of <?php echo do_shortcode("[location_name]"); ?> continues to be the leader for early care and education in the <?php echo do_shortcode("[location_name]"); ?> community. We are proud to represent <?php echo "$city, $state"; ?> and understand the importance of a first-class preschool and what it means to families with young children. We want to meet you and show you around our Learning Academy. Click <a href="{$schedule_tour_url}">HERE</a> to plan your visit today.</p>
EOT;
		}
		elseif($setting == 'krk_location_section2_content') {
			$schedule_tour_url = get_site_url() . '/schedule-tour';

			return <<<EOT
      <div class="row-text">
          <h4>We look forward to your visit!</h4>
          <p>Now that you have seen our Learning Academy online, our staff are anxious to meet you and your family in person. Our address and hours of operation are above, and we welcome visitors anytime. However, for you and your family to see us outside of drop off and pick up times or nap time, call us and we can schedule your visit. You'll then be able to see the classrooms in action and stay a while to speak with our teaching staff about our accredited program. Click <a href="{$schedule_tour_url}">HERE</a> to make an appointment today.</p>
      </div>
      <h4>Overview of Our Facility</h4>
      <ul class="list">
        <li>Our Academies are equipped with multiple cameras in every classroom, cafeteria and outdoor play areas. Our interior is designed with safety glass walls and doors to ensure each child is supervised by staff at all times. </li>
        <li>With over 17,000 square feet, we design a one-of-a-kind building that features age appropriate classrooms equipped with the right tools to properly educate your child along with large fenced-in playgrounds matching each developmental age range.</li>
        <li>School staff members and volunteers greet each child under our Kiss 'N Go Lane stationed right at the front door.</li>
        <li>Our cafeteria is a unique room designed for each classroom to join one another for lunch and snacks. We believe it's important to teach manners and help create conversations meant for dining. </li>
        <li>The equipment used in our building is specifically made for children. It's durable and safe and has been researched by early childhood educators to ensure what works best. Each room is arranged to meet the needs of child-led and teacher-led lessons. </li>
        <li>Our gym area, mainly used for our before and after school age children is also used for indoor play with our preschoolers during inclement weather. </li>
        <li>Outdoor time is always a treat, and our play yards are packed full of fun. We have shade units with UV protection built above our play structures, and our Academy also has a Splash Pad for those hot summer days.</li>
      </ul>
EOT;
		}
	}
}

new KRK_Location_Customizer();
?>