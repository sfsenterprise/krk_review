<?php

require_once(get_template_directory() . '/functions/customizers/defaults/faq-defaults.php');

add_action( 'customize_register', 'krk_faq_customizer' ); 
function krk_faq_customizer( $wp_customize ) {
	/**
	* Sections
	*/
	$wp_customize->add_section(
        'krk_faq_page_content',
        array(
            'title' => 'FAQ Page Content',
            'priority' => 35,
            'active_callback' => function(){ return is_page("faq"); }
        )
    );
   
	/**
	* Settings
	*/
    $wp_customize->add_setting( 'krk_faq_content', array(
    		'default' => krk_faq_customizer_defaults('krk_faq_content')
    	) );
	/**
	* Controls
	*/
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_faq_content', 
			array(
  				'label' => __( 'FAQ' ),
  				'section' => 'krk_faq_page_content',
  				'settings' => 'krk_faq_content'
			) 
		)
	);
}
?>