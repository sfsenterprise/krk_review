<?php

add_action('init', 'check_announcement_expiration');
function check_announcement_expiration(){
    if(is_home() || is_admin()) {
        $type = 'krk_announcement';
        $args = array(
            'post_type' => $type,
            'post_status' => 'publish',
            'order' => 'ASC',
            'posts_per_page' => -1);

        $query = null;
        $query = new WP_Query($args);
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $is_active = get_post_meta(get_the_ID(), 'krk_announcement_active', true);
                $expiration_time = get_post_meta(get_the_ID(), 'krk_announcement_expiration_date', true);

                if ($is_active === 'on') {
                    if($expiration_time > 0) {
                        $now = time();
                        if($now > $expiration_time) {
                            update_post_meta(get_the_ID(), 'krk_announcement_active', '');
                        }
                    }
                }
            }
        }
        wp_reset_query();
    }
}


$krk_announcement_meta_fields = array(
    array(
        'label' => 'Url',
        'desc'  => 'The Url that will be linked from the announcement.',
        'id'    => 'krk_announcement_url',
        'type'  => 'text'
    ),
    array(
        'label' => 'Active',
        'desc'  => 'Check this box to display the announcement on the website.',
        'id'    => 'krk_announcement_active',
        'type'  => 'checkbox'
    ),
    array(
        'label' => 'Expiration Date',
        'desc'  => 'The announcement will automatically deactivate on the give date.',
        'id'    => 'krk_announcement_expiration_date',
        'type'  => 'date'
    ),
    array(
        'label' => 'Background Color',
        'desc'  => 'Background color of the announcement',
        'id'    => 'krk_announcement_background_color',
        'type'  => 'select',
        'options' => array (
            'one' => array (
                'label' => 'Navy Blue',
                'value' => '#073b66'
            ),
            'two' => array (
                'label' => 'Red',
                'value' => '#FF0000'
            ),
            'three' => array (
                'label' => 'Orange',
                'value' => '#FFA500'
            ),
            'four' => array (
                'label' => 'Gold',
                'value' => '#FFD700'
            )
        )
    )
);

function register_krk_announcement_meta_box(){
    add_meta_box(
        'krk_announcement_options', // $id
        'Announcement Options', // $title
        'show_krk_announcement_meta_box', // $callback
        'krk_announcement', // $page
        'normal', // $context
        'high'); // $priority
}
add_action('add_meta_boxes', 'register_krk_announcement_meta_box');

function show_krk_announcement_meta_box(){
    global $krk_announcement_meta_fields, $post;
    // Use nonce for verification
    echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';

    echo '<table class="form-table">';
    foreach ($krk_announcement_meta_fields as $field) {
        $meta = get_post_meta($post->ID, $field['id'], true);
        echo '<tr>
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
                <td>';
        switch($field['type']) {
            case 'text':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" size="30" value="'.
                    $meta . '"  />
                <br /><span class="description">'.$field['desc'].'</span>';
                break;

            case 'checkbox':
                echo '<input type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" ' .
                    ($meta === 'on' ? 'checked' : '') . '/>
                <br /><span class="description">'.$field['desc'].'</span>';
                break;

            case 'date':
                $date = $meta ? gmdate("m/d/Y", $meta) : '';
                echo '<input type="text" class="datepicker" name="'.$field['id'].'" id="'.$field['id'].'" value="'.
                    $date .'" size="30" />
                <br /><span class="description">'.$field['desc'].'</span>';
                break;
            case 'select':
                echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';
                foreach ($field['options'] as $option) {
                    echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">' . 
                            $option['label'] .
                        '</option>';
                }
                echo '</select><br /><span class="description">'.$field['desc'].'</span>';
                break;
        }
        echo '</td></tr>';
    }
    echo '</table>';
}

function save_krk_active_announcement_meta($post_id) {
    global $krk_announcement_meta_fields;
    // verify nonce
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
        return $post_id;
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    // check permissions
    if (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }
    foreach ($krk_announcement_meta_fields as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        
        //convert to unix timestamp
        if($field['type'] == 'date'){
            $new = strtotime($new);
            error_log($new);
        }
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }

    $status = get_post_meta($post_id, 'krk_announcement_active', 'false');
    if ($status === 'on') {
        // disable all other ones
        $posts = query_posts('post_type=krk_announcement');
        foreach( $posts as $post ) {
            if ($post->ID != $post_id) {
                update_post_meta($post->ID, 'krk_announcement_active', 'false');
            }
        }
    }
}
add_action('save_post', 'save_krk_active_announcement_meta');
?>