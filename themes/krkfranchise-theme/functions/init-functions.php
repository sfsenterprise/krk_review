<?php

if ( ! function_exists( 'multisite_path' ) ) {
    /**
     * Returns the path within the current multisite that will provide access
     * to the given non-multisite link.
     *
     * Example: /infants would return /currentmultisite/infants, where
     * currentmultisite is the path of the currently active multisite.
     */
    function multisite_path($page) {
        if ($page[0] == '/') {
            $page = substr($page, 1);
        }

        if(is_main_site()) {
            echo site_url() . '/' . $page;
        }

        else {
            global $current_blog;
            echo $current_blog->path . $page;
        }
    }
}

if ( ! function_exists( 'get_multisite_path' ) ) {
    /**
     * Returns the path within the current multisite that will provide access
     * to the given non-multisite link.
     *
     * Example: /infants would return /currentmultisite/infants, where
     * currentmultisite is the path of the currently active multisite.
     */
    function get_multisite_path($page) {
        if ($page[0] == '/') {
            $page = substr($page, 1);
        }

        if(is_main_site()) {
            return site_url() . '/' . $page;
        }

        else {
            global $current_blog;
            return $current_blog->path . $page;
        }
    }
}

/**
 * Adds the krk franchise pages
 */
function krkfranchise_add_default_pages()
{
    $default_pages = ['Camps','FAQ', 'Schedule-Tour', 'Our-Infants', 'Our-Preschool', 'Our-Toddlers', 'Our-PreKindergarten', 'Our-SchoolAge', 
        'Our-Camps', 'Our-Philosophy', 'Our-School', 'Our-Stories', 'Our-Staff', 'Our-Location', 'Our-Curriculum', 'Documents', 'Our-Kindergarten', 'Career-Application'];
    foreach ($default_pages as $page) {
        if (get_page_by_title($page) == NULL) {
            $post = array(
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_date' => date('Y-m-d H:i:s'),
                'post_name' => $page,
                'post_status' => 'publish',
                'post_title' => $page,
                'post_type' => 'page',
            );
            //insert page and save the id
            $newvalue = wp_insert_post($post, false);
        }
    }
}
add_action('init', 'krkfranchise_add_default_pages');

function krkfranchise_hide_post_page(){
    remove_menu_page( 'edit.php' );
}
add_action( 'admin_menu', 'krkfranchise_hide_post_page' ); 

function register_camera_login_field() {
        register_setting( 'general', 'krkfranchise_camera_login_url');
        add_settings_field('krkfranchise_camera_login_url', '<label for="krkfranchise_camera_login_url">'.__('Camera Login Url' , 'krkfranchise_camera_login_url' ).'</label>' , 'camera_login_html', 'general' );
    }
function camera_login_html() {
        $value = get_option( 'krkfranchise_camera_login_url', '' );
        echo '<input type="text" id="krkfranchise_camera_login_url" name="krkfranchise_camera_login_url" value="'. $value .'" />';
}
add_action( 'admin_init', 'register_camera_login_field' );

/**
 * Registers our custom post types
 */
function krkfranchise_register_post_types(){
    register_post_type( 'krk_additional_pages',
        array(
            'labels' => array(
                'name' => 'Additional Pages',
                'singular_name' => 'Add New Page',
                'add_new_item' => 'Add Page',
                'edit_item' => 'Edit Page',
                'new_item' => 'New Page',
                'view_item' => 'View Page'
            ),
            'menu_icon' => 'dashicons-welcome-add-page',
            'public' => true,
            'has_archive' => false,
            'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes')
        )
    );
    register_post_type( 'krk_news',
        array(
            'labels' => array(
                'name' => 'News',
                'singular_name' => 'Add New News',
                'add_new_item' => 'Add News',
                'edit_item' => 'Edit News',
                'new_item' => 'New News',
                'view_item' => 'View News'
            ),
            'menu_icon' => 'dashicons-welcome-widgets-menus',
            'public' => true,
            'has_archive' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'news'),
            'supports' => array( 'title', 'editor', 'thumbnail')
        )
    );
    register_post_type( 'krk_blog',
        array(
            'labels' => array(
                'name' => 'Blog',
                'singular_name' => 'Add New Blog',
                'add_new_item' => 'Add Blog',
                'edit_item' => 'Edit Blog',
                'new_item' => 'New Blog',
                'view_item' => 'View Blog'
            ),
            'menu_icon' => 'dashicons-welcome-write-blog',
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'blog'),
            'supports' => array( 'title', 'editor', 'thumbnail')
        )
    );
    register_post_type( 'krk_announcement',
        array(
            'labels' => array(
                'name' => 'Announcements',
                'singular_name' => 'Announcement',
                'add_new_item' => 'Add New Announcement',
                'edit_item' => 'Edit Announcement',
                'new_item' => 'New Announcement',
                'view_item' => 'View Announcement'
            ),
            'menu_icon' => 'dashicons-megaphone',
            'public' => false,
            'show_ui' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'announcements'),
            'supports' => array( 'title')
        )
    );
    register_post_type( 'krk_event',
        array(
            'labels' => array(
                'name' => 'Events',
                'singular_name' => 'Event',
                'add_new_item' => 'Add Event',
                'edit_item' => 'Edit Event',
                'new_item' => 'New Event',
                'view_item' => 'View Event'
            ),
            'menu_icon' => 'dashicons-calendar-alt',
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'events'),
            'supports' => array( 'title', 'editor', 'thumbnail')
        )
    );
    register_post_type('krk_monthly_calendar',
        array(
            'labels' => array(
                'name' => 'Monthly Calendars',
                'singular_name' => 'Monthly Calendar',
                'add_new_item' => 'Add Monthly Calendar',
                'edit_item' => 'Edit Monthly Calendar',
                'new_item' => 'New Monthly Calendar',
                'view_item' => 'View Monthly Calendar'
            ),
            'menu_icon' => 'dashicons-calendar',
            'public' => false,
            'show_ui' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'monthly-calendar'),
            'supports' => array('title')
        )
    );
    register_post_type( 'krk_extracurricular',
        array(
            'labels' => array(
                'name' => 'Extra-Curricular Activities',
                'singular_name' => 'Add New Extra-Curricular Activity',
                'add_new_item' => 'Add Extra-Curricular Activity',
                'edit_item' => 'Edit Extra-Curricular Activities',
                'new_item' => 'New Extra-Curricular Activity',
                'view_item' => 'View Extra-Curricular Activities'
            ),
            'menu_icon' => 'dashicons-palmtree',
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'extracurricular'),
            'supports' => array( 'title', 'editor', 'thumbnail')
        )
    );
    register_post_type( 'krk_staff',
        array(
            'labels' => array(
                'name' => 'Staff',
                'singular_name' => 'Staff',
                'add_new_item' => 'Add New Staff',
                'edit_item' => 'Edit Staff',
                'new_item' => 'New Staff',
                'view_item' => 'View Staff'
            ),
            'menu_icon' => 'dashicons-businessman',
            'public' => false,
            'show_ui' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'staff'),
            'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
            'hierarchical' => true
        )
    );
    register_post_type('krk_career',
        array(
            'labels' => array(
                'name' => 'Careers',
                'singular_name' => 'Career',
                'add_new_item' => 'Career',
                'edit_item' => 'Edit Career',
                'new_item' => 'New Career',
                'view_item' => 'View Career'
            ),
            'menu_icon' => 'dashicons-id',
            'public' => false,
            'show_ui' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'careers'),
            'supports' => array('title', 'editor', 'thumbnail')
        )
    );
    register_post_type('krk_app_document',
        array(
            'labels' => array(
                'name' => 'Application Documents',
                'singular_name' => 'Application Document',
                'add_new_item' => 'Add New Application Document',
                'edit_item' => 'Edit Application Document',
                'new_item' => 'New Application Document',
                'view_item' => 'View Application Document'
            ),
            'menu_icon' => 'dashicons-media-text',
            'public' => false,
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'application-documents'),
            'supports' => array('title')
        )
    );
    register_post_type( 'krk_testimonial',
        array(
            'labels' => array(
                'name' => 'Testimonials',
                'singular_name' => 'Testimonial',
                'add_new_item' => 'Add New Testimonial',
                'edit_item' => 'Edit Testimonial',
                'new_item' => 'New Testimonial',
                'view_item' => 'View Testimonial'

            ),
            'menu_icon' => 'dashicons-share',
            'public' => false,
            'show_ui' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'testimonials'),
            'supports' => array('title','editor')
        )
    );
    register_post_type( 'krk_meal',
        array(
            'labels' => array(
                'name' => 'Food Meals',
                'singular_name' => 'Meal',
                'add_new_item' => 'Add New Meal',
                'edit_item' => 'Edit Meal',
                'new_item' => 'New Meal',
                'view_item' => 'View Meal'
            ),
            'menu_icon' => 'dashicons-carrot',
            'public' => false,
            'show_ui' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'meals'),
            'supports' => array( 'title', 'editor', 'page-attributes'),
            'hierarchical' => true
        )
    );
    register_post_type( 'krk_menu',
        array(
            'labels' => array(
                'name' => 'Food Menus',
                'singular_name' => 'Menu',
                'add_new_item' => 'Add New Menu',
                'edit_item' => 'Edit Menu',
                'new_item' => 'New Menu',
                'view_item' => 'View Menu'
            ),
            'menu_icon' => 'dashicons-media-spreadsheet',
            'public' => false,
            'show_ui' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'menus'),
            'supports' => array( 'title'),
            'hierarchical' => true
        )
    );
    register_post_type('krk_document',
        array(
            'labels' => array(
                'name' => 'Documents',
                'singular_name' => 'Document',
                'add_new_item' => 'Add New Document',
                'edit_item' => 'Edit Document',
                'new_item' => 'New Document',
                'view_item' => 'View Document'
            ),
            'menu_icon' => 'dashicons-media-default',
            'public' => false,
            'show_ui' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'documents'),
            'supports' => array('title')
        )
    );
    register_post_type( 'krk_homepage_slide',
        array(
            'labels' => array(
                'name' => 'Homepage Slides',
                'singular_name' => 'Add New Homepage Slide',
                'add_new_item' => 'Add Homepage Slide',
                'edit_item' => 'Edit Homepage Slides',
                'new_item' => 'New Homepage Slide',
                'view_item' => 'View Homepage Slides'
            ),
            'menu_icon' => 'dashicons-format-gallery',
            'public' => false,
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'homepage-slide'),
            'supports' => array( 'title', 'thumbnail', 'page-attributes'),
            'hierarchical' => true
        )
    );
}
add_action('init', 'krkfranchise_register_post_types');


/**
 * Set max number of posts to paginated custom post types
 */

function krkfranchise_custom_type_archive_display($query) {
    $max_num = 4;
    if (!is_admin() && (is_post_type_archive('krk_news') ||
                        is_post_type_archive('krk_blog') ||
                        is_post_type_archive('krk_extracurricular')
        )) {

        $query->set('posts_per_page', $max_num);
        return;
    }
}
add_action('pre_get_posts', 'krkfranchise_custom_type_archive_display');


/**
 * create default nav menu if none exists
 */
function krkfranchise_create_default_nav_menu(){
    $menu_name = 'Nav';
    $menu_exists = wp_get_nav_menu_object( $menu_name );

    //If it doesn't exist, let's create it.
    if( !$menu_exists){
        $settings = get_option('redwood_krk_school_programs', array());
        $menu_id = wp_create_nav_menu($menu_name);

        // Set up default menu items
        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' =>  __('Infants'),
            'menu-item-classes' => '',
            'menu-item-position' => 0,
            'menu-item-url' => get_multisite_path('/our-infants'),
            'menu-item-status' => 'publish'));

        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' =>  __('Toddlers'),
            'menu-item-classes' => '',
            'menu-item-position' => 1,
            'menu-item-url' => get_multisite_path('/our-toddlers'),
            'menu-item-status' => 'publish'));

        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' =>  __('Preschool'),
            'menu-item-classes' => '',
            'menu-item-position' => 2,
            'menu-item-url' => get_multisite_path('/our-preschool'),
            'menu-item-status' => 'publish'));

        if(empty($settings['disable_pre_k'])) {
            wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-title' =>  do_shortcode('[pre_k_program_name]'),
                'menu-item-classes' => 'pre_k',
                'menu-item-position' => 3,
                'menu-item-url' => get_multisite_path('/our-prekindergarten'),
                'menu-item-status' => 'publish'));
        }

        if(! empty($settings['toggle_kindergarten'])) {
            wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-title' =>  do_shortcode('[kindergarten_program_name]'),
                'menu-item-classes' => 'kindergarten',
                'menu-item-position' => 4,
                'menu-item-url' => get_multisite_path('/our-kindergarten'),
                'menu-item-status' => 'publish'));
        }

        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' =>  __('School-Age'),
            'menu-item-classes' => '',
            'menu-item-position' => 5,
            'menu-item-url' => get_multisite_path('/our-schoolage'),
            'menu-item-status' => 'publish'));

        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' =>  __('Camps'),
            'menu-item-classes' => '',
            'menu-item-position' => 6,
            'menu-item-url' => get_multisite_path('/our-camps'),
            'menu-item-status' => 'publish'));

        $type = 'krk_extracurricular';
        $args=array(
            'post_type' => $type,
            'post_status' => 'publish',
            'order' => 'DESC',
            'posts_per_page' );
        $query = null;
        $query = new WP_Query($args);
        if( $query->have_posts() ) {
            wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-title' => __('Extra-Curricular'),
                'menu-item-classes' => '',
                'menu-item-position' => 7,
                'menu-item-url' => get_multisite_path('/extracurricular'),
                'menu-item-status' => 'publish'));
        }
        wp_reset_query();
        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' =>  __('About'),
            'menu-item-classes' => '',
            'menu-item-position' => 8,
            'menu-item-url' => get_multisite_path('/our-location'),
            'menu-item-status' => 'publish'));

        $type = 'krk_additional_pages';
        $args=array(
            'post_type' => $type,
            'post_status' => 'publish',
            'order' => 'DESC',
            'posts_per_page' );
        $query = null;
        $query = new WP_Query($args);
        if (empty($settings['disable_additional_pages']) && $query->have_posts()) {
            wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-title' =>  __('Additional Pages'),
                'menu-item-classes' => 'additional_pages right-drop',
                'menu-item-position' => 10,
                'menu-item-url' => '#',
                'menu-item-status' => 'publish'));

            Redwood_KRK_School_Programs_Settings::create_additional_pages_dropdown_items($menu_name);
        }
        wp_reset_query();

        //add menu as primary menu
        $locations = get_theme_mod('nav_menu_locations');
        $locations['primary'] = $menu_id;
        set_theme_mod( 'nav_menu_locations', $locations );
    }
}
add_action('init', 'krkfranchise_create_default_nav_menu');

remove_meta_box( 'slugdiv', 'krk_event', 'normal' );
remove_meta_box( 'slugdiv', 'krk_news', 'normal' );
remove_meta_box( 'slugdiv', 'krk_testimonial', 'normal' );
remove_meta_box( 'slugdiv', 'krk_menu', 'normal' );
remove_meta_box( 'slugdiv', 'krk_announcement', 'normal' );
remove_meta_box( 'slugdiv', 'krk_homepage_slide', 'normal' );

?>