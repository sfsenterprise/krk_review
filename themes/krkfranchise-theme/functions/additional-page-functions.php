<?php

//remove the post type name from the slug
function krk_remove_additional_pages_slug( $post_link, $post, $leavename ) {

    if ( 'krk_additional_pages' != $post->post_type || 'publish' != $post->post_status ) {
        return $post_link;
    }

    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
    return $post_link;
}
add_filter( 'post_type_link', 'krk_remove_additional_pages_slug', 10, 3 );

//add additional page to nameless requests
function krk_parse_additional_pages_request( $query ) {

    // Only noop the main query
    if ( ! $query->is_main_query() )
        return;

    // Only noop our very specific rewrite rule match
    if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }

    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
    if ( ! empty( $query->query['name'] ) ) {
        $query->set( 'post_type', array( 'post', 'page', 'krk_additional_pages' ) );
    }
}
add_action( 'pre_get_posts', 'krk_parse_additional_pages_request' );

function krk_flush_rewrite_rules($post_id, $post_after=nil, $post_before=nil) {
    global $wp_rewrite;
    $wp_rewrite->init();
    $wp_rewrite->flush_rules();
}
add_action( 'post_updated', 'krk_flush_rewrite_rules' );

function krk_add_additional_page_to_dropdown($post_id, $post_after=nil, $post_before=nil) {
    $post_type = get_post_type($post_id);
    if($post_type != 'krk_additional_pages') return;
    $status = get_post_status( $post_id );

    if($status == 'trash') krk_delete_additional_page_to_dropdown($post_id);
    if($status != 'publish') return;

    $menu_name = "Nav";
    $menu = wp_get_nav_menu_object( $menu_name );
    $settings = get_option('redwood_krk_school_programs', array());

    if($menu) {
        $additional_page_dropdown_id = nil;
        $item_id = 0;

        $menu_items = wp_get_nav_menu_items($menu_name);
        foreach ($menu_items as $key => $item) {
            if (in_array('additional_pages', $item->classes)) {
                $additional_page_dropdown_id = $item->ID;
            }
            //check if nav item already exists
            elseif ($item->url == get_permalink($post_id)){
                $item_id = $item->db_id;
            }
        }
        if($additional_page_dropdown_id != nil) {
            wp_update_nav_menu_item($menu->term_id, $item_id, array(
                'menu-item-title' => get_the_title($post_id),
                'menu-item-classes' => '',
                'menu-item-url' => get_permalink($post_id),
                'menu-item-parent-id' => $additional_page_dropdown_id,
                'menu-item-status' => 'publish'));
        } elseif(empty($settings['disable_additional_pages'])) {
            wp_update_nav_menu_item($menu->term_id, 0, array(
                'menu-item-title' => "Additional Pages",
                'menu-item-classes' => 'additional_pages right-drop',
                'menu-item-position' => 10,
                'menu-item-url' => '#',
                'menu-item-status' => 'publish'));

            krk_add_additional_page_to_dropdown($post_id);
        }
    }
}
add_action( 'post_updated', 'krk_add_additional_page_to_dropdown' );

function krk_delete_additional_page_to_dropdown($post_id) {
    $post_type = get_post_type($post_id);
    if($post_type != 'krk_additional_pages') return;

    $type = 'krk_additional_pages';
    $args = array(
        'post_type' => $type,
        'post_status' => 'publish',
        'order' => 'ASC',
        'posts_per_page');
    $query = null;
    $query = new WP_Query($args);
    $count = $query->post_count;

    $menu_name = "Nav";
    $menu = wp_get_nav_menu_object( $menu_name );

    if($menu) {
        $menu_items = wp_get_nav_menu_items($menu_name);
        foreach ($menu_items as $key => $item) {
            if ($item->title == get_the_title($post_id)){
                wp_delete_post($item->ID);
                return;
            }
            elseif (in_array('additional_pages', $item->classes) && $count <= 0) {
                wp_delete_post($item->ID);
            }
        }
    }
}
add_action( 'delete_post', 'krk_add_additional_page_to_dropdown' );
?>