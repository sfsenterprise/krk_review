<?php
/**
 * Created by PhpStorm.
 * User: dustin
 * Date: 1/27/16
 * Time: 11:11 AM
 */
get_header();
$customizer = new KRK_Location_Customizer();
?>
    <main id="main">
        <div class="main-holder">
            <div class="breadcrumbs-wrap">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <ol class="breadcrumb">
                                <li><a href="<?php multisite_path('/'); ?>">Home</a></li>
                                <li class="active">About</li>
                            </ol>
                            <div class="title-page">
                                <h1>OUR Location</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="twocolumns" class="battlement">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <?php
                                $active_nav = 'location';
                                include( locate_template( 'nav-about.php' ));
                            ?>
                        </div>
                        <div class="col-sm-9">
                            <div class="visual-img">
                               <?php
                                $directions_image = $customizer->get_setting('krk_location_directions_header_image'); ?>
                                <img src="<?php echo $directions_image; ?>" height="299" width="823" alt="image description">
                                <?php
                                $directions_title = $customizer->get_setting('krk_location_directions_header_title'); ?>
                                <div class="text">
                                    <strong><?php echo $directions_title; ?></strong>
                                </div>    
                            </div>
                            <?php  $location = get_option('redwood_location_settings', array()); ?>  
                            <?php  $street = $location['street_address'];   
                                   $city = $location['city']; 
                                   $state = $location['state'];
                                   $zip = $location['zip']; 
                                   $email = $location['email']; ?>

                            <?php $directions_content = $customizer->get_setting('krk_location_directions_content'); ?>
                            <div class="txt">
                                <?php echo $directions_content; ?>
                            </div>
                            <div class="contact-block">
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="contact" id="location">
                                            <div class="box">
                                                <h3>ADDRESS</h3>
                                                <?php echo do_shortcode("[location_full_address]"); ?>
                                            </div>
                                            <div class="box">
                                                <h3>OUR HOURS</h3>
                                                <p><?php echo do_shortcode("[redwood_hours id='hours_operation' should_display_title=0 combine_matching_days=1]"); ?></p>
                                            </div>
                                            <div class="box">
                                                <h3>CONTACT</h3>
                                                <address>
                                                    <?php if (!empty(do_shortcode("[phone_number]"))): ?>
                                                    <b>P </b><?php echo do_shortcode("[phone_number]"); ?><br/>
                                                    <?php endif ?>
                                              
                                                    <?php if (!empty(do_shortcode("[fax_number]"))): ?>
                                                    <b>F </b><?php echo do_shortcode("[fax_number]"); ?><br/>
                                                    <?php endif ?>

                                                    <?php if (!empty($email)): ?>
                                                    <b><a style="text-decoration: underline" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></b>
                                                    <?php endif ?>
                                                </address>
                                            </div>
                                            
                                            <div class="btn-wrap hidden-xs">
                                                <a target="_blank" href="http://www.maps.google.com/maps?daddr=<?php echo "$street, $city, $state, $zip"; ?>" class="btn btn-primary">Directions</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="map">
                                            <?php echo do_shortcode("[location-map]"); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="line grey" id="virtual-tour">

                            <?php if ( !empty( do_shortcode('[redwood_virtual_tour_iframe]') ) ): ?>
                            <div class="visual-img">
                                <picture>
                                    <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-32.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-32-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-32-3x.jpg 3x">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-32.jpg" height="299" width="823" alt="image description">
                                </picture>
                                <div class="text">
                                    <strong>Virtual <br>Tour</strong>
                                </div>
                            </div>
                            <div class="box-view">
                                <div class="visual" id="tour-visual">
                                    <a class="cursor-pointer">
                                        <?php echo do_shortcode('[redwood_virtual_tour_iframe]'); ?>
										<span class="text">
											Take a Virtual Tour!
											<span class="ico icon-arrow-click"></span>
										</span>
                                    </a>
                                </div>
                                <div class="body-view">
                                    <h2>How to navigate:</h2>
                                    <p><i>Click to</i> “Walk”</p>
                                    <p><i>Scroll to</i> “Zoom”</p>
                                    <p><i>Click &amp; Hold to</i> “Look Around”</p>
                                </div>
                            </div>
                            <?php endif ?>

                            <?php
                            //Render the image gallery from our-location
                                while ( have_posts() ) : the_post();
                                    get_template_part('gallery', 'our-location');
                                endwhile; ?>
                            <?php $section2 = $customizer->get_setting('krk_location_section2_content'); ?>
                            <div class="txt">
                                <?php echo $section2; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
            </div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#tour-visual').click(function () {
            var elm = jQuery(this);
            if (!elm.data('tour-active')) {
                elm.data('tour-active', true);
                elm.removeClass('visual');

                jQuery('.text', elm).hide();
            }
        });
    });
</script>
<?php get_footer();?>
