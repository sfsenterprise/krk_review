<?php
get_header();
$customizer = new KRK_FAQ_Customizer();
?>

<main id="main">
    <div class="main-holder">
        <div class="breadcrumbs-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php multisite_path('/'); ?>">Home</a></li>
                            <li class="active">About</li>
                        </ol>
                        <div class="title-page">
                            <h1>FAQ</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="twocolumns" class="battlement">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <?php $active_nav = 'faq';
                         include( locate_template( 'nav-about.php' )); ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="text">
                            <h2>Frequently Asked Questions</h2>
                        </div>

                        <div class="wrap-sections">
                            <?php $section_content = $customizer->get_setting('krk_faq_content'); ?>
                            <div class="section-xtx"><?php echo $section_content ?></div>
                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
    </div>

<?php
get_footer();
?>