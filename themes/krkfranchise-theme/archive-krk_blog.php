<?php
/**
 * Created by PhpStorm.
 * User: dustin
 * Date: 1/27/16
 * Time: 11:09 AM
 */

get_header();
$customizer = new KRK_Blog_Customizer();
?>

    <div class="main-holder">
        <div class="breadcrumbs-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php multisite_path('/'); ?>">Home</a></li>
                            <li class="active">About</li>
                        </ol>
                        <div class="title-page">
                            <h1>OUR BLOG</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="twocolumns" class="battlement">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            $active_nav = 'blog';
                            include(locate_template('nav-about.php'));
                        ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="visual-img">
                            <?php $header_image = $customizer->get_setting( 'krk_blog_header_image' ); ?>
                            <img src="<?php echo $header_image; ?>" height="299" width="823" alt="image description">

                            <div class="text">
                                <strong>Our<br> Blog</strong>
                            </div>
                        </div>
                        <div class="block-posts">
                            <?php
                                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                                $type = 'krk_blog';
                                //post_per_page controlled by functions/init-functions.php -> krkfranchise_custom_type_archive_display
                                $args=array(
                                    'post_type' => $type,
                                    'post_status' => 'publish',
                                    'order' => 'DESC',
                                    'paged' => $paged);

                                $query = null;
                                $query = new WP_Query($args);
                                if( $query->have_posts() ):
                                    while ($query->have_posts()):
                                        $query->the_post();
                            ?>
                                        <hr class="line grey" id="<?php echo get_the_ID(); ?>"/>
                                        <div class="post-block row">
                                            <div class="col-sm-8">
                                                <div class="text-box">
                                                    <h2>
                                                        <a href="<?php the_permalink()?>" style="text-decoration: none;">
                                                            <?php the_title(); ?>
                                                        </a>
                                                    </h2>
                                                    <?php $date = get_the_date(); ?>
                                                    <time datetime="<?php echo $date; ?>">
                                                        <i><b><?php echo $date; ?> </b></i></time>

                                                    <p><?php the_excerpt(); ?></p>
                                                    <a href="<?php the_permalink() ?>" class="btn btn-primary">Read More <span class="icon icon-arrow-right"></span></a>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="img-box">
                                                    <?php
                                                    if (has_post_thumbnail()) {
                                                        the_post_thumbnail(array(306, 343));
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div
                            <?php
									endwhile;
                                    include(locate_template('pagination-links.php'));
								endif;
                             ?>             
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
    </div>

<?php
    get_footer();
?>