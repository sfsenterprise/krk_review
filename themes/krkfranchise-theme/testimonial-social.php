<?php
switch($source):
    case 'facebook':

        if(!empty(do_shortcode("[url_facebook]"))) : ?>
            <a target="_blank" href="<?php echo do_shortcode(do_shortcode("[url_facebook]")) ?>" class="social" style="background: #3B5998;">
                <span class="icon icon-facebook"></span>
            </a>
        <?php else : ?>
            <div class="social" style="background: #3B5998;">
                <span class="icon icon-facebook"></span>
            </div>
        <?php endif;
        break;

    case 'twitter':

        if(!empty(do_shortcode("[url_twitter]"))) : ?>
            <a target="_blank" href="<?php echo do_shortcode(do_shortcode("[url_twitter]")) ?>" class="social" style="background: #00aced;">
                <span class="icon icon-twitter"></span>
            </a>
        <?php else : ?>
            <div class="social" style="background: #00aced;">
                <span class="icon icon-twitter"></span>
            </div>
        <?php endif;
        break;

    case 'google':

        if(!empty(do_shortcode("[url_google_plus]"))) : ?>
            <a target="_blank" href="<?php echo do_shortcode(do_shortcode("[url_google_plus]")) ?>" class="social">
                <span class="icon icon-google-plus"></span>
            </a>
        <?php else : ?>
            <div class="social">
                <span class="icon icon-google-plus"></span>
            </div>
        <?php endif;
        break;

    default:
        break;

endswitch;
?>