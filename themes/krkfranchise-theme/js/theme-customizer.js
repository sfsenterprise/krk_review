(function( krkCustomizer, $, undefined){
	krkCustomizer.wrapText = function(btn, openTag, closeTag) {
	    var content = $(btn).nextAll('.container_contenteditable');
	    var range = getSelectionRange();
	    var len = content.html().length;
	    var start = range.startOffset;
	    var end = range.endOffset;
	    var selectedText = content.html().substring(start, end);
	    var replacement = openTag + selectedText + closeTag;
	    content.html(content.html().substring(0, start) + replacement + content.html().substring(end, len));
		updateContentEditable(content);
	};

	function updateRichContent(richEditor){
		var textArea = richEditor.textarea.element;
		$('textarea[data-customize-setting-link='+textArea.dataset.customizeSettingLink+']').trigger('change');
	}

	function getSelectionRange(){
		var sel;
    	if (window.getSelection) {
    	    sel = window.getSelection();
    	    if (sel.rangeCount) {
    	        return sel.getRangeAt(0);
    	    }
    	} else if (document.selection) {
    	    return document.selection.createRange();
    	}
    	return null;
	}

	$(document).ready(function(){

		//close button fix. TODO investage why broken.
		$('.customize-controls-close').click(function(){
			window.location = this.href;
		});
		

		$('iframe').ready(function(){	
			//add sortable list items
			$( 'ul.sortable-list' ).each(function(index, list) {
				var items = $(list).find('input[type="hidden"]').val().split(',');
				for(var i=0; i<items.length; i++) {
					$(list).append('<li data-name=' + items[i] +'>' +
										'<label>' +
                    						items[i] +
                						'</label>' +
                						'<i class="dashicons dashicons-menu"></i>' + 
                					'</li>');
				}
				$(list).sortable({
	    			update: function( e, ui ){
	    				updateSortableVal(list);
	    			}
	    		});

			});

			function updateSortableVal(list) {
				/* Get the value, and convert to string. */
			    names = $( list ).find( 'li' ).map( function(){
			        var name = $(this).data('name');
			        return name;
			    }).get().join( ',' );
			   /* Add the value to hidden input. */
			   $( list ).find('input').val( names ).trigger( 'change' );
			}

			$('.krk_customize_rich_content').wysihtml5({
				toolbar: {
				  "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
				  "emphasis": true, //Italics, bold, etc. Default true
				  "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
				  "html": true, //Button which allows you to edit the generated HTML. Default false
				  "link": true, //Button to insert a link. Default true
				  "image": false, //Button to insert an image. Default true,
				  "color": false, //Button to change color of font  
				  "blockquote": false, //Blockquote  
				  "size": 'xs' //default: none, other options are xs, sm, lg
				},
				"events":{
					"blur": function(){
						updateRichContent(this);
					},
					"paste": function(){
						updateRichContent(this);
					},
					"change:composer": function(){
						updateRichContent(this);
					}
				}
			});

			$('.wysihtml5-sandbox').contents().find('body').on("keyup",function() {
        		setTimeout(function(){
        			$(window.parent.document).find('.krk_customize_rich_content').trigger('change');
        		}, 300);
    		});
		});
	});
})(window.krkCustomizer = window.krkCustomizer || {}, jQuery);