/**
 * Created by dustin on 1/1/17.
 */

(function ( formValidator, $, undefined)
{

    /**
     * validate
     *
     * validates a form's input
     * populates error message with error returned from validation
     * @param form {*|HTMLElement}
     * @returns {boolean}
     */
    formValidator.validate = function(form) {
        formValidator.clearErrors(form);

        var inputs = form.find('input');
        var isValid = true;
        for(var i=0; i < inputs.length; i++) {
            var validationMsg = validateInput($(inputs[i]));
            isValid = isValid && validationMsg.valid;

            //if not valid, add to error message
            if(!validationMsg.valid) {
                var errorMsg = formValidator.getErrorMsg(form);
                errorMsg.children('.errors')
                    .append('<li>' +
                                validationMsg.error +
                            '</li>');
            };
        }
        return isValid;
    };

    /**
     * getSuccessMsg
     *
     * returns success message linked to given form
     * success message id must equal {FORM_ID}-success
     * @param form
     * @returns {*|HTMLElement}
     */
    formValidator.getSuccessMsg = function(form) {
        var id = form.attr('id');
        return $('#'+ id + '-success');
    };

    /**
     * getErrorMsg
     *
     * returns error message linked to given form
     * success message id must equal {FORM_ID}-error
     * @param form
     * @returns {*|HTMLElement}
     */
    formValidator.getErrorMsg = function(form) {
        var id = form.attr('id');
        return $('#'+ id + '-error');
    }

    /**
     * showErrorMsg
     *
     * sets error message element as visible
     * hides success message element
     * @param form
     * @returns {*|HTMLElement}
     */
    formValidator.showErrorMsg = function (form) {
        var errorMsg = formValidator.getErrorMsg(form);

        formValidator.getSuccessMsg(form).addClass('hidden');
        errorMsg.removeClass('hidden');
        window.location.hash = errorMsg.attr('id');
    };

    /**
     * clearErrors
     *
     * clears error message element inner html
     * hides error message element
     * hides success message element
     * @param form
     * @returns {*|HTMLElement}
     */
    formValidator.clearErrors = function(form) {
        var errorMsg = formValidator.getErrorMsg(form);

        //clear error focus
        history.pushState('', document.title, window.location.pathname);

        formValidator.getSuccessMsg(form).addClass('hidden');
        errorMsg.addClass('hidden');

        errorMsg.html('<b>Form Error</b><br><ul class="errors"></ul>');
    };


    /**
     * showSuccessMsg
     *
     * sets success message element as visible
     * hides error message element
     * @param form
     * @returns {*|HTMLElement}
     */
    formValidator.showSuccessMsg = function (form) {
        formValidator.getSuccessMsg(form).removeClass('hidden');
        formValidator.getErrorMsg(form).addClass('hidden');
    };

    /**
     * Private functions
     */

    var validateInput = function(input) {
        var validateType = input.data('validation-type');
        switch(validateType){
            case 'phone':
                return validatePhoneInput(input);
                break;

            case 'name':
                return validateNameInput(input);
                break;

            default:
                return validateInputHasValue(input);
                break;
        }
    };

    var validateInputHasValue = function(input) {

        var returnMsg = {valid: true, error: ''};
        if(!input.prop('required')) {
            return returnMsg;
        }

        var value = input.val().replace(/\s/g, '');
        var isValid = value.length > 0;

        if(!isValid) {
            var displayName = input.data('display-name');
            returnMsg.valid = false;
            returnMsg.error = displayName + ': Value Required';
        }

        return returnMsg;
    };

    var validatePhoneInput = function(input) {
        var hasSpecialCharacters = /[~`!#$%\^&@*=\[\]\\';,/{}|\\":<>\?]/g.test(input.val());
        var hasLetters = /[A-Za-z]/g.test(input.val());
        var returnMsg = {valid: true, error: ''};

        if(hasSpecialCharacters) {
            var displayName = input.data('display-name');
            returnMsg.valid = false;
            returnMsg.error = displayName + ': Invalid Special Characters';
        } else if(hasLetters) {
            var displayName = input.data('display-name');
            returnMsg.valid = false;
            returnMsg.error = displayName + ': Invalid Phone Number';
        } else {
            returnMsg = validateInputHasValue(input);
        }
        return returnMsg;
    };

    var validateNameInput = function(input) {
        var hasSpecialCharacters = /[~!#$%@\^&*+=\-\[\]\;/{}|\\":<>\?]/g.test(input.val());
        var returnMsg = {valid: true, error: ''};

        if(hasSpecialCharacters) {
            var displayName = input.data('display-name');
            returnMsg.valid = false;
            returnMsg.error = displayName + ': Invalid Special Characters';
        } else {
            returnMsg = validateInputHasValue(input);
        }
        return returnMsg;
    };

}(window.formValidator = window.formValidator || {}, jQuery));