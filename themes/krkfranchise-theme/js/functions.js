/**
 * Theme functions file.
 *
 * Contains handlers for navigation and widget area.
 */

( function( $ ) {
	$(document).ready(function(){
		//add dropdown on hover
		$( '.dropdown' ).hover( function( e ) {
				//check if mobile resolution
				var isMobile = $(window).width() < 768;
				if(!isMobile) {
					var _this = $(this);
					_this.addClass('open');
				}
			},
			function( e ) {
				var isMobile = $(window).width() < 768;
				if(!isMobile) {
					var _this = $(this);
					_this.removeClass('open');
				}
			}
		);

	});
} )( jQuery );
