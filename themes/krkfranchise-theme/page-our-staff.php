<?php
get_header();
$customizer = new KRK_Staff_Customizer();
?>

<main id="main">
    <div class="main-holder">
        <div class="breadcrumbs-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php multisite_path('/'); ?>">Home</a></li>
                            <li class="active">About</li>
                        </ol>
                        <div class="title-page">
                            <h1>OUR STAFF</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="twocolumns" class="battlement">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            $active_nav = 'staff';
                            include(locate_template('nav-about.php'));
                        ?>
                    </div>
                    <div class="col-sm-9">
                    <div class="visual-img" id="children-first">
                        <?php $header_image = $customizer->get_setting('krk_staff_header_image1');?>
                        <img src="<?php echo $header_image; ?>" height="299" width="823" alt="image description">
                        <?php $header_title = $customizer->get_setting('krk_staff_header_title'); ?>
                        <div class="text">
                            <strong><?php echo $header_title; ?></strong>
                        </div>
                    </div>
                    <div class="wrap-sections">
                        <?php $page_content = $customizer->get_setting('krk_staff_content'); ?>
                        <div class="section-xtx">
                            <?php echo $page_content; ?>
                        </div>
                                    
                </div>
                <hr class="line grey" id="teachers">
                <div class="visual-img">
                    <?php $header_image2 = $customizer->get_setting( 'krk_staff_header_image2', ''); ?>
                    <img src="<?php echo get_stylesheet_directory_uri() . '/images/' . $header_image2; ?>" height="299" width="823" alt="image description">
                    <div class="text">
                        <strong>Our Team</strong>
                    </div>
                        </div>
                        <?php
                        $type = 'krk_staff';
                        $args=array(
                            'post_type' => $type,
                            'post_status' => 'publish',
                            'order' => 'ASC',
                            'orderby' => 'menu_order',
                            'posts_per_page' => -1);
                        
                        $query = null;
                        $query = new WP_Query($args);
                        if( $query->have_posts() ):
                            while ($query->have_posts()):
                                $query->the_post();
                                $name = get_post_meta(get_the_ID(), 'krk_staff_name', true);
                                $title = get_post_meta(get_the_ID(), 'krk_staff_title', true);
                                ?>
                        <div class="item">
                            <div class="visual">
                                 <?php
                                if (has_post_thumbnail()) {
                                    the_post_thumbnail(array(200, 223));
                                }
                                ?>
                            </div>
                            <div class="text">
                                <div class="title">
                                    <h3><?php echo $name; ?></h3>
                                    <h4><?php echo $title; ?></h4>
                                </div>
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <?php
                            endwhile;
                        endif;
                                ?>
                        <?php
                        wp_reset_query();
                        $type = 'krk_career';
                        $args=array(
                            'post_type' => $type,
                            'post_status' => 'publish',
                            'order' => 'DESC',
                            'orderby' => 'parent menu_order',
                            'posts_per_page' => -1);
                        
                        $query = null;
                        $query = new WP_Query($args);
                        if( $query->have_posts() ):  ?>
                            <hr class="line grey" id="careers">
                            <div class="visual-img">
                                <picture>
                                    <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-23.jpg, <?php     bloginfo('stylesheet_directory'); ?>/images/img-23-2x.jpg 2x, <?php bloginfo('  stylesheet_directory'); ?>/images/img-23-3x.jpg 3x">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-23.jpg" height="299"    width="823" alt="image description">
                                </picture>
                                <div class="text">
                                    <strong>Careers</strong>
                                </div>
                            </div>
                            <div class="careers-wrap">
                                <div class="careers">
                                    <h3>Accepting Applications</h3>
                                    <?php while ($query->have_posts()):
                                        $query->the_post(); ?>
                                        <br />
                                        <h4><?php the_title(); ?></h4>
                                        <p><?php the_content(); ?></p>
                                        <div class="button-wrap">
                                            <a href="<?php echo multisite_path('career-application') ?>" class="btn btn-primary">APPLY</a>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
    </div>

<?php

get_footer();
?>