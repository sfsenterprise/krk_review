<?php
/**
 * Created by PhpStorm.
 * User: dustin
 * Date: 1/27/16
 * Time: 11:09 AM
 */

get_header();
the_post(); ?>

    <div class="main-holder">
        <div class="breadcrumbs-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php multisite_path('/'); ?>">Home</a></li>
                            <li>About</></li>
                            <li class="active"><a href="<?php multisite_path('events'); ?>">Our Events</a></li>
                        </ol>
                        <div class="title-page">
                            <h1><?php the_title(); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="twocolumns" class="battlement">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                        $active_nav = 'events';
                        include(locate_template('nav-about.php'));
                        ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="text-box">
                            <?php
                            $start_date = get_post_meta(get_the_ID(), 'krk_event_date', true);
                            $end_date = get_post_meta(get_the_ID(), 'krk_event_end_date', true);

                            $unixtime_start = $start_date;
                            $start_date = gmdate("m/d/Y", $start_date);
                            $expire_date = 0;
                            $unixtime_end = 0;

                            if(!empty($end_date)) {
                                $unixtime_end = $end_date;
                                $end_date = gmdate("m/d/Y", $end_date);
                            }
                            ?>
                            <div class="post-block">
                                <h1 class="title"><?php the_title(); ?></h1>
                                <time datetime="<?php echo date('Y-m-d', $unixtime_start) ?>">
                                    <i><b><?php echo date('F d, Y', $unixtime_start);?> </b></i></time>

                                <?php if($end_date) : ?>
                                    - <time datetime="<?php echo date('Y-m-d', $unixtime_end) ?>">
                                        <i><b><?php echo date('F d, Y', $unixtime_end);?> </b></i></time>
                                <?php endif; ?>

                                <hr class="line grey" />

                                <div class="img-box center">
                                    <?php
                                    if (has_post_thumbnail()) {
                                        the_post_thumbnail(array(420, 400));
                                    }
                                    ?>
                                </div>
                                <div class="text-box">
                                    <p><?php the_content(); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
    </div>

<?php
    get_footer();
?>