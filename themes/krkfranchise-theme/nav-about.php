<?php   
$events = get_active_krk_events();
$active_calendar = get_active_krk_monthly_calendar();
$active_menu = get_active_krk_menu();
?>
<nav class="menu-wrap">
    <ul class="sub-menu">
        <li class="<?php if($active_nav != NULL && $active_nav=='location') echo 'active';?>"><a href="<?php multisite_path('/our-location'); ?>">Our Location</a>
            <ul>
                <li><a href="<?php multisite_path('/our-location/#location'); ?>">Directions</a></li>
                <?php if( !empty(Redwood_KRK_External_Link_Settings::get_virtual_tour_url()) ):?>
                    <?php ?>
                    <li><a href="<?php multisite_path('/our-location/#virtual-tour'); ?>">Virtual Tour</a></li>
                <?php endif; ?>
            </ul>
        </li>
        <li class="<?php if($active_nav != NULL && $active_nav=='staff') echo 'active';?>"><a href="<?php multisite_path('/our-staff'); ?>">Our Staff</a>
            <ul>
                <li><a href="<?php multisite_path('/our-staff/#children-first'); ?>">The Children Come First</a></li>
                <li><a href="<?php multisite_path('/our-staff#teachers'); ?>">Our Team</a></li>

                <?php if( has_published_posts_with_type('krk_career') ):  ?>
                    <li><a href="<?php multisite_path('/our-staff/#careers'); ?>">Careers</a></li>
                <?php endif; ?>
            </ul>
        </li>
        <li class="<?php if($active_nav != NULL && $active_nav=='philosophy') echo 'active';?>"><a href="<?php multisite_path('/our-philosophy'); ?>">Our Philosophy</a></li>
        <li class="<?php if($active_nav != NULL && $active_nav=='curriculum') echo 'active';?>"><a href="<?php multisite_path('/our-curriculum'); ?>">Our Curriculum</a>
            <ul>
                <!--<?php if(! Redwood_KRK_Settings::get_hide_interactive_technology() ):?>
                <li><a href="<?php multisite_path('/our-curriculum/#interactive'); ?>">Interactive Technology</a></li>
                <?php endif; ?>!-->
                <li><a href="<?php multisite_path('/our-curriculum/#brainwaves'); ?>">Brain Waves</a></li>
                <li><a href="<?php multisite_path('/our-curriculum/#steam'); ?>">Steam Ahead</a></li>
                <?php if(! Redwood_KRK_Settings::get_hide_interactive_technology() ):?>
                    <li><a href="<?php multisite_path('/our-curriculum/#interactive'); ?>">Interactive Technology</a></li>
                <?php endif; ?>
            </ul>
        </li>
        <li class="<?php if($active_nav != NULL && $active_nav=='stories') echo 'active';?>" ><a href="<?php multisite_path('/our-stories'); ?>">Our Stories</a></li>
        <li class="<?php if($active_nav != NULL && $active_nav=='school') echo 'active';?>"><a href="<?php multisite_path('/our-school'); ?>">Our School</a>
            <ul>
                <li><a href="<?php multisite_path('/our-school/'); ?>">Our Classrooms</a></li>

                <?php if(count($events) > 0 || $active_calendar != nil) : ?>
                    <li><a href="<?php multisite_path('/our-school/#events'); ?>">Events &amp; Calendar</a></li>
                <?php endif; ?>

                <?php if( has_published_posts_with_type('krk_meal') || $active_menu != nil ): ?>
                    <li><a href="<?php multisite_path('/our-school/#menu'); ?>">Food Menu</a></li>
                <?php endif; ?>

                <?php
                if( has_published_posts_with_type('krk_document') ): ?>
                    <li><a href="<?php multisite_path('/our-school/#documents'); ?>">Forms &amp; Documents</a></li>
                <?php endif; ?>
            </ul>
        </li>

        <li class="<?php if($active_nav != NULL && $active_nav=='news') echo 'active';?>"><a href="<?php multisite_path('/news'); ?>">Our News</a></li>

        <?php if(count($events) > 0 || $active_calendar != nil) : ?>
            <li class="<?php if($active_nav != NULL && $active_nav=='events') echo 'active';?>"><a href="<?php multisite_path('/events'); ?>">Our Events</a></li>
        <?php endif; ?>

        <?php if( has_published_posts_with_type('krk_blog') ): ?>
            <li class="<?php if($active_nav != NULL && $active_nav=='blog') echo 'active';?>"><a href="<?php multisite_path('/blog'); ?>">Our Blog</a></li>
        <?php endif; ?>
        
        <li class="<?php if($active_nav != NULL && $active_nav=='faq') echo 'active';?>"><a href="<?php multisite_path('/faq'); ?>">FAQ</a></li>
        <?php if( has_published_posts_with_type('krk_extracurricular')): ?>
            <li class="<?php if($active_nav != NULL && $active_nav=='extracurricular') echo 'active';?>"><a href="<?php multisite_path('/extracurricular'); ?>">Extra-Curricular Activities</a></li>
        <?php endif; ?>
        
    </ul>
</nav>