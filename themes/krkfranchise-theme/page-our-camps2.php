<?php

get_header();

?>



<main id="main">
	<div class="main-holder">
		<div class="breadcrumbs-wrap">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="title-page title">
							<h1>CAMPS</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="video-container">
			<div class="video-img-wrap">
				<picture>
					<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/summer-games.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/summer-games.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/summer-games.jpg 3x">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/summer-games.jpg" height="509" width="680" alt="image description">
				</picture>
				<a href="#" class="btn-play">
					<span class="icon icon-play"></span>
				</a>
			</div>
			<div class="video-text-wrap">
				<strong class="slogan">The Best Summer Yet!</strong>
				<span class="info"><b>This summer, Kids <span class="krk-ticks">R</span> Kids Learning Academies all across the nation light the torch on a day camp that is undeniably Gold medal-worthy! </span>
				<a href="<?php multisite_path('/schedule-tour'); ?>" class="btn btn-primary">Now Registering<span class="icon icon-arrow-right"></span></a>
			</div>
		</div>
		<div class="two-columns battlement">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="col-first">
							<div class="section-xtx">
								<h2>Camps/School Holidays</h2>
								<p> Quite often, parents find themsevles at a loss when school holidays and breaks don't coincide with their own busy schedules. Unlike some other childcare providers or daycare centers, Kids <span class="krk-ticks">R</span> Kids has an educational program along with flexible and fun options for busy families!</p>
								<p> We're open during all major school breaks and school holidays and provide a secure place for your child to relax and be a kid. Our facilities are built to educate and entertain, making time away from school a joy. We specifically plan field trips, child-directed activities, and special group play to continually enrich and entertain your child </p>
								<p> Fall Break, Winter Break, Spring Break, Summer Break and Special School Holidays - every break is covered! And, as always, all children are welcome in our loving and kid-friendly environment. </p>
								<p> Our Learing Academies are closed for all Federal Holidays. Please check with your Director for specific details. </p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
	</div>

<?php

get_footer();

?>