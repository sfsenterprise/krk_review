<?php
    get_header();

    $customizer = new KRK_Curriculum_Customizer();
?>
    <main id="main">
        <div class="main-holder">
            <div class="breadcrumbs-wrap">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <ol class="breadcrumb">
                                <li><a href="<?php multisite_path('/'); ?>">Home</a></li>
                                <li class="active">About</li>
                            </ol>
                            <div class="title-page">
                                <h1>OUR CURRICULUM</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="twocolumns" class="battlement" data-customizable="true">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <?php
                            $active_nav = 'curriculum';
                            include( locate_template( 'nav-about.php' ));
                            ?>
                        </div>
                        <div class="col-sm-9">
                            <div class="visual-img">
                                <?php
                                $section1_image = $customizer->get_setting( 'krk_curriculum_section1_header_image'); ?>
                                <img src="<?php echo $section1_image; ?>" height="509" width="680" alt="image description">
                                <div class="text">
                                <?php $section1_title = $customizer->get_setting('krk_curriculum_section1_header_title'); ?>
                                    <strong><?php echo $section1_title; ?></strong>
                                </div>
                            </div>
                            <div class="wrap-sections">
                                <?php
                                $section1_content = $customizer->get_setting('krk_curriculum_section1_content');
                                if(strlen($section1_content) <= 0) :
                                    $section1_content = krk_curriculum_customizer_defaults('krk_curriculum_section1_content');
                                endif; ?>
                                <div class="section-xtx">
                                    <?php echo $section1_content; ?>
                                </div>
                            </div>
                            <div class="wrap-sections curriculum row">
                                <div class="col-sm-3">

                                    <picture>
                                        <a href="<?php multisite_path('/our-infants/'); ?>">
                                            <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/BigStep.logo.png, <?php bloginfo('stylesheet_directory'); ?>/   images/BigStep.logo.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/BigStep.logo.png 3x">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/BigStep.logo.png" height="137" width="256" alt="image description">
                                        </a>
                                    </picture>

                                </div>
                                <div class="section-xtx col-sm-9">
                                    <?php $big_steps_snip = $customizer->get_setting('krk_curriculum_big_steps_snip_content'); ?>
                                    <?php echo $big_steps_snip; ?>
                                </div>
                            </div>
                            <div class="wrap-sections curriculum row">
                                <div class="col-sm-3">
                                    <picture>
                                        <a href="<?php multisite_path('/our-preschool/'); ?>">
                                            <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/FastTrackLogo.png, <?php bloginfo('stylesheet_directory'); ?>/   images/FastTrackLogo.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/FastTrackLogo.png 3x">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/FastTrackLogo.png" height="137" width="256" alt="image description">
                                        </a>
                                    </picture>
                                </div>
                                <div class="section-xtx col-sm-9">
                                    <?php $fast_track_snip = $customizer->get_setting( 'krk_curriculum_fast_track_snip_content'); ?>
                                    <?php echo $fast_track_snip; ?>
                                </div>
                            </div>
                            <div class="wrap-sections curriculum row">
                                <div class="col-sm-3">
                                    <a href="<?php multisite_path('/our-schoolage/'); ?>">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/GymLogo.png" height="137" width="256" alt="image description">
                                    </a>
                                </div>
                                <div class="section-xtx col-sm-9">
                                    <?php $gym_snip = $customizer->get_setting( 'krk_curriculum_gym_snip_content'); ?>
                                    <?php echo $gym_snip; ?>
                                </div>
                            </div>
                            <div class="wrap-sections curriculum row">
                                <div class="col-sm-3">
                                    <picture>
                                        <a href="<?php multisite_path('/our-curriculum/#steam'); ?>">
                                            <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/STEAMlogo1.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/STEAMlogo1.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/STEAMlogo1.jpg 3x">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/STEAMlogo1.jpg" height="137" width="256" alt="image description">
                                        </a>
                                    </picture>
                                </div>
                                <div class="section-xtx col-sm-9">
                                    <?php $steam_ahead_snip = $customizer->get_setting('krk_curriculum_steam_ahead_snip_content'); ?>
                                    <?php echo $steam_ahead_snip; ?>
                                </div>
                            </div>
                            <div class="wrap-sections curriculum row">
                                <div class="col-sm-3">
                                    <picture>
                                        <a href="<?php multisite_path('/our-curriculum/#brainwaves'); ?>">
                                            <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/Brain_Waves_corp_Logo.jpg <?php bloginfo('stylesheet_directory'); ?>/images/Brain_Waves_corp_Logo.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/Brain_Waves_corp_Logo.jpg 3x">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/Brain_Waves_corp_Logo.jpg" height="137" width="256" alt="image description">
                                        </a>
                                    </picture>
                                </div>
                                <div class="section-xtx col-sm-9">
                                    <?php $brain_waves_snip = $customizer->get_setting('krk_curriculum_brain_waves_snip_content'); ?>
                                    <?php echo $brain_waves_snip; ?>
                                </div>
                            </div>
                            <div class="wrap-sections curriculum row">
                                <div class="col-sm-3">
                                    <picture>
                                        <a href="<?php multisite_path('/our-camps/'); ?>">
                                            <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/summer-logo.png <?php bloginfo('stylesheet_directory'); ?>/images/summer-logo.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/summer-logo.png 3x">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/summer-logo.png" height="137" width="256" alt="image description">
                                        </a>
                                    </picture>
                                </div>
                                <div class="section-xtx col-sm-9">
                                    <?php $camp_snip = $customizer->get_setting('krk_curriculum_camp_snip_content'); ?>
                                    <?php echo $camp_snip; ?>
                                </div>
                            </div>
                            <hr class="line grey" id="brainwaves">
                            <div class="visual-img-alt center">
                                <img class="image-auto" src="<?php bloginfo('stylesheet_directory'); ?>/images/Brain_Waves_corp_Logo.jpg" height="179" width="278" alt="big-brain">
                            </div>
                            <div class="wrap-sections">
                                <div class="section-xtx">
                                    <strong>Our Exclusive Brain Waves™ Curriculum</strong>
                                    <p>Your child’s learning environment has tremendous impact on their brain development. At Kids <span class="krk-ticks">R</span> Kids Learning Academies, we use strategies from our Brain Waves™ curriculum to support neural pathways for language, social-emotional, cognitive, and physical development during these crucial years.</p>

                                    <div class="wrap-sections curriculum row">
                                        <div class="col-sm-3">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/FrontalLobeLogo.jpg" height="137" width="256" alt="image description">
                                        </div>
                                        <div class="section-xtx col-sm-9">
                                            <p>The main functions of the frontal lobe are cognitive thinking, such as reasoning and problem solving. This portion of the brain is also responsible for motor development, language development, social-emotional behavior, and impulse control.</p>
                                        </div>
                                    </div>
                                    
                                    <div class="wrap-sections curriculum row">
                                        <div class="col-sm-3">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/TemporalLobeLogo.jpg" height="137" width="256" alt="image description">
                                        </div>
                                        <div class="section-xtx col-sm-9">
                                           <p>The two main functions of the temporal lobe are auditory processing and memory. This part of the brain distinguishes different sounds while listening.</p>
                                        </div>
                                    </div>

                                    <div class="wrap-sections curriculum row">
                                        <div class="col-sm-3">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/ParietalLobeLogo.jpg" height="137" width="256" alt="image description">
                                        </div>
                                        <div class="section-xtx col-sm-9">
                                           <p>The parietal lobe processes information relating to touch, temperature, and pain. This lobe also helps with the brain funtion such as perceptual modality, sensory processing, spatial recognition, visual and speech processing, and perception.</p>
                                        </div>
                                    </div>
                                    
                                    <div class="wrap-sections curriculum row">
                                        <div class="col-sm-3">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/OccipitalLobeLogo.jpg" height="137" width="256" alt="image description">
                                        </div>
                                        <div class="section-xtx col-sm-9">
                                           <p>The occipital lobe is the part of the brain responsible for visual processing.</p>
                                        </div>
                                    </div>
                                    
                                    <p>At Kids <span class="krk-ticks">R</span> Kids Learning Academies, our learning stations are filled with math manipulatives, science elements, and stimulating visuals that support brain pathways for cognitive development. In addition, our teachers conduct activities to help children explore cause and effect and employ questioning strategies that promote higher level thinking.</p>
                                    
                                    <p>Kids <span class="krk-ticks">R</span> Kids Learning Academies are filled with stimulating materials that allow teachers to facilitate children’s exploration through sight, sound, smell, taste, and touch &mdash; as these sensory experiences send information directly to the brain and create the platform for healthy brain growth.</p>
                                </div>
                            </div>

                            <hr class="line grey" id="steam">
                            <div class="visual-img center">
                                <?php $steam_ahead_image = $customizer->get_setting( 'krk_curriculum_steam_ahead_header_image'); ?>
                                <img src="<?php echo get_stylesheet_directory_uri() . '/images/' . $steam_ahead_image; ?>" height="509" width="680" alt="image description">
                                
                                <div class="text text-left">
                                <?php $steam_ahead_title = $customizer->get_setting('krk_curriculum_steam_ahead_header_title'); ?>
                                    <strong><?php echo $steam_ahead_title; ?></strong>
                                </div>
                            </div>
                            <div class="two-columns battlement">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="col-first" data-customizable="true">
                                                <img class="image-auto" src="<?php bloginfo('stylesheet_directory'); ?>/images/STEAMlogo-3x.png" height="179" width="325" alt="image description">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="col-second" data-customizable="true">
                                                <img class="image-auto" src="<?php bloginfo('stylesheet_directory'); ?>/images/thinklearnteach2.png" height="250" width="500" alt="image description">
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <?php $steam_ahead_content = $customizer->get_setting('krk_curriculum_steam_ahead_content'); ?>
                                    <?php echo $steam_ahead_content; ?>
                                </div>
                            </div>

                        <?php if(! Redwood_KRK_Settings::get_hide_interactive_technology() ):?>
                            
                                <hr class="line grey" id="interactive">
                                <div class="visual-img">
                                    <?php $tech_img = $customizer->get_setting('krk_curriculum_interactive_tech_image'); ?>
                                    <img src="<?php echo get_stylesheet_directory_uri() . '/images/' . $tech_img; ?>" height="299" width="823" alt="image description">
                                    <div class="text">
                                        <strong>Latest Technology and  <br>Learning Programs</strong>
                                    </div>
                                </div>
                                <div class="wrap-sections">
                                    <?php $section_tech_content = $customizer->get_setting('krk_curriculum_interactive_tech_content'); ?>
                                    <div class="section-xtx">
                                        <?php echo $section_tech_content; ?>
                                    </div>
                                </div>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
        </div>
    </main>
<?php
    get_footer();
?>