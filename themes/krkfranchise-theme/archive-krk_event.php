<?php
/**
 * Created by PhpStorm.
 * User: dustin
 * Date: 1/27/16
 * Time: 11:09 AM
 */

get_header(); 
$customizer = new KRK_Events_Customizer();
?>

    <div class="main-holder">
        <div class="breadcrumbs-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php multisite_path('/'); ?>">Home</a></li>
                            <li class="active">About</li>
                        </ol>
                        <div class="title-page">
                            <h1>OUR EVENTS</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="twocolumns" class="battlement">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            $active_nav = 'events';
                            include(locate_template('nav-about.php'));
                        ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="visual-img">
                            <?php $header_image = $customizer->get_setting( 'krk_events_header_image' ); ?>
                            <img src="<?php echo $header_image ?>" height="299" width="823" alt="image description">

                            <div class="text">
                                <strong>Our<br> Events</strong>
                            </div>
                        </div>
                        <div class="block-posts">
                            <?php
                            wp_reset_query();  // Restore global post data stomped by the_post().
                            $calendar_events = array();
                            $events = get_active_krk_events();
                            $active_calendar = get_active_krk_monthly_calendar();

                            if (count($events) > 0) {

                                foreach ($events as $event) {
                                    array_push($calendar_events, $event);
                                    ?>
                                    <div id="event-<?php echo $event['id'] ?>" style="height: 40px;"></div>
                                    <div class="post-block row">
                                        <div class="col-sm-8">
                                            <div class="text-box">
                                                <h2>
                                                    <a href="<?php echo $event['url']; ?>" style="text-decoration: none;">
                                                        <?php echo $event['title'] ?>
                                                    </a>
                                                </h2>
                                                <time datetime="<?php echo date('Y-m-d', $event['unixtime_start']) ?>">
                                                    <i><b><?php echo date('F d, Y', $event['unixtime_start']);?> </b></i></time>

                                                <?php if($end_date) : ?>
                                                    - <time datetime="<?php echo date('Y-m-d', $event['unixtime_end']); ?>">
                                                        <i><b><?php echo date('F d, Y', $event['unixtime_end']); ?> </b></i></time>
                                                <?php endif; ?>
                                                <p><?php echo $event['excerpt']; ?></p>
                                                <a href="<?php echo $event['url']; ?>" class="btn btn-primary">Read More <span class="icon icon-arrow-right"></span></a>

                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="img-box">
                                                <?php
                                                if ($event['thumbnail'] != false) {
                                                    echo $event['thumbnail'];
                                                }
                                                ?>
                                            </div>
                                        </div>


                                    </div>
                                    <?php
                                }
                            }
                            wp_reset_query();  // Restore global post data stomped by the_post().
                            ?>
                            <p>Stay informed about our upcoming events.</p>

                            <?php if($active_calendar != nil) : ?>
                                <a href="<?php echo $active_calendar['attachment'] ?>"><?php echo $active_calendar['link-text']; ?></a>
                            <?php endif; ?>

                            <!-- Event Calendar -->
                            <?php if(count($events) > 0) : ?>
                                <?php include(locate_template('events-calendar.php')); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
    </div>

<?php
    get_footer();
?>