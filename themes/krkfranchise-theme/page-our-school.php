<?php
/**
 * Created by PhpStorm.
 * User: dustin
 * Date: 1/27/16
 * Time: 11:04 AM
 */
$customizer = new KRK_School_Customizer();
get_header();
?>
    <main id="main">
    <div class="main-holder">
        <div class="breadcrumbs-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php multisite_path('/'); ?>">Home</a></li>
                            <li class="active">About</li>
                        </ol>
                        <div class="title-page">
                            <h1>Our School</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="twocolumns" class="battlement">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                        $active_nav = 'school';
                        include(locate_template('nav-about.php'));
                        ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="visual-img">
                            <?php $header_image = $customizer->get_setting('krk_school_header_image'); ?>
                            <img src="<?php echo $header_image; ?>" height="299" width="823" alt="image description">
                            <?php $header_title = $customizer->get_setting('krk_school_header_title'); ?>
                            <div class="text">
                                <strong><?php echo $header_title; ?></strong>
                            </div>
                        </div>
                        <div class="text-container">
                            <?php
                            $section_our_classroom_content = $customizer->get_setting('krk_school_classroom_content');
                            echo $section_our_classroom_content; ?>

                        </div>
                        <?php
                        wp_reset_query();  // Restore global post data stomped by the_post(). 
                        $calendar_events = array();
                        $events = get_active_krk_events();
                        $active_calendar = get_active_krk_monthly_calendar();
                        
                        if(count($events) > 0 || $active_calendar != nil) : ?>
                            <hr class="line grey" id="events">
                            <div class="visual-img">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/events.jpg" height="299"
                                     width="823" alt="Events" />
                                <div class="text">
                                    <strong>Events &amp; Calendar</strong>
                                </div>
                            </div>
                            <?php foreach ($events as $event) :
                                array_push($calendar_events, $event);
                                ?>
                                <div id="event-<?php echo $event['id'] ?>" style="height: 40px;"></div>
                                <div class="post-block row">
                                    <div class="col-sm-8"><div class="text-box">
                                            <h2>
                                                <a href="<?php echo $event['url']; ?>" style="text-decoration: none;">
                                                    <?php echo $event['title']; ?>
                                                </a>
                                            </h2>
                                            <time datetime="<?php echo date('Y-m-d', $event['unixtime_start']) ?>">
                                                <i><b><?php echo date('F d, Y', $event['unixtime_start']);?> </b></i></time>

                                            <?php if($event['enddate']) : ?>
                                                - <time datetime="<?php echo date('Y-m-d', $event['unixtime_end']) ?>">
                                                    <i><b><?php echo date('F d, Y', $event['unixtime_end']);?> </b></i></time>
                                            <?php endif; ?>
                                            <p><?php echo $event['excerpt'] ?></p>
                                            <a href="<?php echo $event['url']?>" class="btn btn-primary">Read More <span class="icon icon-arrow-right"></span></a>

                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="img-box">
                                            <?php
                                            if ($event['thumbnail'] != false) {
                                                echo $event['thumbnail'];
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            endforeach;
                            wp_reset_query(); ?>
                            <p>Stay informed about our upcoming events.</p>
                            <?php if($active_calendar != nil) : ?>
                                    <a href="<?php echo $active_calendar['attachment'] ?>"><?php echo $active_calendar['link-text']; ?></a>
                            <?php endif; ?>

                            <!-- Event Calendar -->
                            <?php if(count($events) > 0) : ?>
                                <?php include(locate_template('events-calendar.php')); ?>
                            <?php else : ?>
                                <br><br>
                                <hr class="line grey">
                            <?php endif; ?>
                        <?php endif; ?>

                        <div class="visual-img">
                            <picture id="menu">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/menu.jpg" height="299"
                                     width="823" alt="Food Menu" />
                            <div class="text">
                                <strong>Food <br> Menu</strong>
                            </div>
                        </div>
                        <div class="text-container">
                            <?php
                            $meal_content = $customizer->get_setting('krk_school_meal_content');
                            echo $meal_content; ?>
                            <hr class="line grey">
                            <?php $active_menu = get_active_krk_menu();?>
                            <?php
                            wp_reset_query();
                            $type = 'krk_meal';
                            $args = array(
                                'post_type' => $type,
                                'post_status' => 'publish',
                                'orderby' => 'menu_order',
                                'order' => 'ASC',
                                'posts_per_page' => -1);

                            $query_meal = null;
                            $query_meal = new WP_Query($args);
                            if ($query_meal->have_posts()):
                                while ($query_meal->have_posts()):
                                    $query_meal->the_post();
                                    $title = get_the_title();
                                    $has_link = get_post_meta(get_the_ID(), 'krk_meal_has_menu_link', true);
                                    $content_meal = get_the_content();
                                    // using the customizer instead of this post
                                    if($title != 'Complete, balanced meals') :
                                        if($has_link && $active_menu != nil) {
                                            $content_meal .= ' Find our menu <a href="#pdf-menu">HERE</a> to see today\'s ' .
                                                strtolower(get_the_title()) . '.';
                                        }
                                        ?>
                                        <h2> <?php echo $title ?> </h2>
                                        <p><?php echo $content_meal ?></p>
                                    <?php endif; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>

                            <?php if($active_menu != nil) : ?>
                                    <?php if(wp_is_mobile()) :?>
                                        <a id="pdf-menu" href="<?php echo $active_menu;?>">
                                            Download our full menu!
                                        </a>
                                    <?php else : ?>
                                        <br/><br/>
                                        <div class="pdf-wrapper" id="pdf-menu">
                                            <iframe src="<?php echo $active_menu ?>" width="100%" height="800px"></iframe>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                        </div>
                        <?php
                        wp_reset_query();
                        $type = 'krk_document';
                        $args = array(
                            'post_type' => $type,
                            'post_status' => 'publish',
                            'order' => 'DESC',
                            'posts_per_page' => -1);

                        $query = null;
                        $query = new WP_Query($args);
                        if ($query->have_posts()): ?>
                            <hr class="line grey" id="documents">
                            <div class="visual-img">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/FormsAndDocs.jpg" height="299" width="823" alt="image description">
                                <div class="text">
                                    <strong>Forms & <br> Documents</strong>
                                </div>
                            </div>
                            <div class="text-container">
                                <ul>
                                    <?php while ($query->have_posts()):
                                        $query->the_post();
                                        $description = get_post_meta(get_the_ID(), 'krk_document_description', true);
                                        $attachment = get_post_meta(get_the_ID(), 'krk_document_attachment', true);
                                        $link_text = get_post_meta(get_the_ID(), 'krk_document_link_text', true);
                                        ?>
                                        <li>
                                            <a href="<?php echo $attachment ?>"><?php if (empty($link_text)) the_title(); else echo $link_text; ?></a>
                                            <p><?php echo $description; ?></p>
                                        </li>
                                    <?php
                                endwhile;
                                endif;
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
    </div>

<?php
get_footer();
?>