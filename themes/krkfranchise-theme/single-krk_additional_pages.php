<?php
/**
 * Created by PhpStorm.
 * User: dustin
 * Date: 1/27/16
 * Time: 11:09 AM
 */

get_header(); ?>

    <div class="main-holder">
        <div class="breadcrumbs-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="title-page title">
                            <h1><?php the_title(); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="twocolumns" class="battlement">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <?php if(has_post_thumbnail()) : 
                            $post_thumbnail_id = get_post_thumbnail_id(get_the_ID());
                            $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );?>
                            <div class="visual-img">
                                <img src="<?php echo $post_thumbnail_url; ?>" height="299" width="823" alt="image description">
                                <div class="text">
                                    <strong><?php the_title(); ?></strong>
                                </div>
                            </div>
                        <?php else :?>
                            <div class="page-title">
                                    <strong><?php the_title(); ?></strong>
                            </div>
                        <?php endif; ?>
                        <div class="block-posts">
                            <hr class="line grey">
                            <div class="post-box" style="border-bottom: none;">
                                <p><?php the_content(); ?></p>
                                <div class='clearfix'></div>
                            </div>        
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
        </div>
        <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
    </div>

<?php
    get_footer();
?>