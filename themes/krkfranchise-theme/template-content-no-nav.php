<?php
/**
 * Template Name: KRK Content Page No Nav
 */


get_header();
    while(have_posts()): the_post();
        $content = get_the_content();
?>

<main id="main">
			<div class="main-holder">
				<div class="breadcrumbs-wrap">
					<div class="container-fluid">
						<div class="row">
							<div class="col-sm-12">
								<ol class="breadcrumb">
                                    <br/>
								</ol>
								<div class="title-page">
									<h1><?php the_title(); ?></h1>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="battlement">
					<div class="container">
						<div class="row">
							<div class="wrapper">
							<h3><?php the_title(); ?></h3>
								<?php
                                    the_content();
                                ?>
							</div>
						</div>
					</div>
				</div>
				<a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
			</div>
			<div class="box-tour visible-xs">
				<strong class="title">Come and See How We Hold the Future™!</strong>
				<p>Children of all ages are welcome!</p>
				<a href="#" class="button-tour">SCHEDULE A TOUR <span class="ico"></span></a>
			</div>
		</main>


<?php
    endwhile;
get_footer();

?>