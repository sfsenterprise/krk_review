<?php
    get_header();

    $customizer = new KRK_Schedule_Tour_Customizer();
?>
    <main id="main">
        <div class="main-holder">
            <div class="breadcrumbs-wrap">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="title-page title">
                                <h1>SCHEDULE A TOUR</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="twocolumns" class="battlement">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <nav class="menu-wrap">
                                <ul class="sub-menu">
                                    <li class="active"><a href="#">Schedule Form</a>
                                        <ul>
                                            <li><a href="#parents-information">Parent's Information</a></li>
                                            <li><a href="#childs-information">Child's Information</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-sm-9">
                            <div class="info-block">
                                <div class="visual-img">
                                    <?php $header_img = $customizer->get_setting('krk_schedule_tour_header_image'); ?>
                                    <img src="<?php echo $header_img ?>"  height="299" width="823" alt="image description">
                                </div>
                                
                                <h2>We can’t wait to meet your family!</h2>
                                <p>From our family to your family, we extend a sincere thank you for considering Kids ‘R’ Kids, the premier preschool for your child. Each and every Kids 'R' Kids Learning Academy is independently owned and operated. An owner is on site and takes a very active role in the management of each school. Our founders, Pat and Janice Vinson, have the philosophy that children should be hugged first and then taught. It was this very philosophy that inspired us to open our very own Kids ‘R’ Kids Learning Academy. If you would like more information about our learning academy or wish to schedule a tour, please complete the form below or contact us by phone at 
                                <?php echo do_shortcode("[phone_number]"); ?>
                                </p>
                            </div>
                            <div class="alert alert-success hidden" id="schedule-tour-form-success">Thank you for your interest in our school! We will be in contact with you shortly to arrange the details of your visit!</div>
                            <div class="alert alert-danger hidden" id="schedule-tour-form-error">Uh oh! Something went wrong trying to send your message. Please try again later.</div>
                            <form id='schedule-tour-form'
                                  class="info-form"
                                  onsubmit="return true"
                                  method="post"
                                  data-type="ajax"
                                  data-fade="true">
                                
                                <?php
                                    $crm_key = Redwood_KRK_Settings::get_childcare_crm_key();
                                    $location = get_option('redwood_location_settings', array());
                                    $state = $location['state'];
                                    $location_id = $location['location_id'];
                                    if($crm_key && $state && $location_id) : ?>
                                        <input type="hidden" name="hash" value="<?php echo $crm_key ?>" />
                                        <input type="hidden" name="location" value="<?php echo 'KRK' . $state . '#' . $location_id; ?>" />
                                <?php endif; ?>
                                
                                <input type="hidden" value="send_schedule_tour_form" name="action">
                                <fieldset>
                                <h3><span class="required-marker">*</span> Denotes required field</h3>
                                <h2 id="parents-information">Parent's Information</h2>
                                    <div class="form-box">
                                        <h3>PARENT’S NAME</h3>
                                        <div class="form-group">
                                            <span class="required-marker">*</span>
                                            <input type="text"
                                                   name="FirstName"
                                                   class="form-control"
                                                   placeholder="First Name"
                                                   data-validation-type="name"
                                                   data-display-name="Parent's First Name"
                                                   required>
                                        </div>
                                        <div class="form-group">
                                            <span class="required-marker">*</span>
                                            <input type="text"
                                                   name="LastName"
                                                   class="form-control"
                                                   placeholder="Last Name"
                                                   data-validation-type="name"
                                                   data-display-name="Parent's Last Name"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-box">
                                        <h3>YOUR ADDRESS</h3>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="Address" placeholder="Street Address">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="City" placeholder="City">
                                        </div>
                                        <div class="form-group">
                                            <select name="State">
                                                <option class="hidden">State</option>
                                                <option>AL</option>
                                                <option>AK</option>
                                                <option>AZ</option>
                                                <option>AR</option>
                                                <option>CA</option>
                                                <option>CO</option>
                                                <option>CT</option>
                                                <option>DE</option>
                                                <option>FL</option>
                                                <option>GA</option>
                                                <option>HI</option>
                                                <option>ID</option>
                                                <option>IL</option>
                                                <option>IN</option>
                                                <option>IA</option>
                                                <option>KS</option>
                                                <option>KY</option>
                                                <option>LA</option>
                                                <option>ME</option>
                                                <option>MD</option>
                                                <option>MA</option>
                                                <option>MI</option>
                                                <option>MN</option>
                                                <option>MS</option>
                                                <option>MO</option>
                                                <option>MT</option>
                                                <option>NE</option>
                                                <option>NV</option>
                                                <option>NH</option>
                                                <option>NJ</option>
                                                <option>NM</option>
                                                <option>NY</option>
                                                <option>NC</option>
                                                <option>ND</option>
                                                <option>OH</option>
                                                <option>OK</option>
                                                <option>OR</option>
                                                <option>PA</option>
                                                <option>RI</option>
                                                <option>SC</option>
                                                <option>SD</option>
                                                <option>TN</option>
                                                <option>TX</option>
                                                <option>UT</option>
                                                <option>VT</option>
                                                <option>VA</option>
                                                <option>WA</option>
                                                <option>WV</option>
                                                <option>WI</option>
                                                <option>WY</option>
                                                <option>DC</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" name="Zip" class="form-control" placeholder="Zip Code">
                                        </div>
                                    </div>
                                    <div class="form-box">
                                        <h3>CONTACT INFORMATION</h3>
                                        <div class="form-group">
                                            <span class="required-marker">*</span>
                                            <input type="tel"
                                                   name="PhoneNumber"
                                                   class="form-control"
                                                   placeholder="Phone Number (XXX) XXX-XXXX"
                                                   data-validation-type="phone"
                                                   data-display-name="Phone Number"
                                                   required>
                                            <input type="hidden" name="PhoneType" value="Home" />
                                            <span class="required-marker">*</span>
                                            <input type="email" name="Email" class="form-control" placeholder="Email Address" required>
                                        </div>
                                    </div>

                                    <div class="form-box">
                                        <h3>How did you hear about us?</h3>
                                        <div class="form-group">
                                            <span class="required-marker">*</span>
                                            <select name="ParentSource" class="form-control" required>
                                                <option value="0"></option>
                                                <option value="Center Event">Center Event</option>
                                                <option value="Community Event">Community Event</option>
                                                <option value="Direct Mail">Direct Mail</option>
                                                <option value="Drive By Location">Drive By Location</option>
                                                <option value="Email Campaign">Email Campaign</option>
                                                <option value="Flyer">Flyer</option>
                                                <option value="Referral-Agency">Referral-Agency</option>
                                                <option value="Referral-Family">Referral-Family</option>
                                                <option value="Referral-Staff">Referral-Staff</option>
                                                <option value="Web Site">Web Site</option>
                                                <option value="Yellow Pages Online">Yellow Pages Online</option>
                                                <option value="Other">Other</option>
                                            </select> 
                                        </div>
                                    </div>

                                    <hr class="line grey" id="childs-information">
                                    <h2>Child's Information</h2>
                                    <div class="form-box">
                                        <h3>CHILD’S NAME</h3>
                                        <div class="form-group">
                                            <span class="required-marker">*</span>
                                            <input type="text"
                                                   class="form-control"
                                                   name="ChildFirstName1"
                                                   placeholder="First Name"
                                                   data-validation-type="name"
                                                   data-display-name="Child First Name"
                                                   required>
                                        </div>
                                        <div class="form-group">
                                            <span class="required-marker">*</span>
                                            <input type="text"
                                                   class="form-control"
                                                   name="Child1LastName1"
                                                   placeholder="Last Name"
                                                   data-validation-type="name"
                                                   data-display-name="Child Last Name"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-box">
                                        <h3>CHILD'S DOB</h3>
                                        <div class="form-group small">
                                            <span class="required-marker">*</span>
                                            <input type="date"
                                                   name="ChildDOB1"
                                                   class="form-control"
                                                   placeholder="Please enter date in mm/dd/yyyy format."
                                                   title="mm/dd/yyyy"
                                                   pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d"
                                                   min="1990-01-01"
                                                   max="2100-01-01"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-box">
                                        <h3>CHILD'S EXPECTED START DATE</h3>
                                        <div class="form-group small">
                                            <span class="required-marker">*</span>
                                            <input type="date" name="ChildStartDate1" class="form-control" placeholder="Please enter date in mm/dd/yyyy format." title="mm/dd/yyyy" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" required>
                                        </div>
                                    </div>
                                    <hr>

                                    <div id="additional-child-wrapper"></div>
                                    
                                    <button type="button" class="btn btn-primary add-child"> + ADD ADDITIONAL CHILD</button>
                                    <br/><br/>
                                    
                                    <div class="form-box">
                                        <h3>ADDITIONAL COMMENTS</h3>
                                        <div class="form-group">
                                            <textarea name="Comments" class="form-control"></textarea>
                                        </div>
                                        <!--<a href="#" class="btn-add"><span class="icon">+</span>ADD ADDITIONAL CHILD</a>-->
                                    </div>                    
                                    <button id="submit-button" type="submit" class="btn btn-primary">SUBMIT<span class="icon-arrow-1"></span></button>
                                </fieldset>
                            </form>
                            <br />
                        <br />
                    </div>
                    </div>
                </div>
            </div>
            <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
        </div>

        <script type="text/template" id="child-template">
            <div class="form-box">
                <h3>CHILD’S NAME</h3>
                <div class="form-group">
                    <input type="text"
                           name="ChildFirstName"
                           class="form-control"
                           placeholder="First Name"
                           data-validation-type="name"
                           data-display-name="Child First Name">
                </div>
                <div class="form-group">
                    <input type="text"
                           name="ChildLastName"
                           class="form-control"
                           placeholder="Last Name"
                           data-validation-type="name"
                           data-display-name="Child Last Name">
                </div>
            </div>
            <div class="form-box">
                <h3>CHILD'S DOB</h3>
                <div class="form-group small">
                    <input type="date" name="ChildDOB" class="form-control" placeholder="Please enter date in mm/dd/yyyy format." title="mm/dd/yyyy" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" />
                </div>
            </div>    
            <div class="form-box">
                <h3>CHILD'S EXPECTED START DATE</h3>
                <div class="form-group small">
                    <input type="date" name="ChildStartDate" class="form-control" placeholder="Please enter date in mm/dd/yyyy format." title="mm/dd/yyyy" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" />
                </div>
            </div>
            <hr>
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                var childTemplate = $('#child-template');
                var childCount =  childCount || 1;
                $('.btn.add-child').click(function(){

                    childCount++;
                    var template = $(childTemplate.clone().html());

                    template.find('input, select, textarea').each(function(i, input){
                        var name = $(input).attr('name');
                        $(input).attr('name', name+childCount);
                    });

                    $('#additional-child-wrapper').append(template);
                    jcf.replaceAll();
                });
            });
        </script>

<?php
    get_footer();
?>
