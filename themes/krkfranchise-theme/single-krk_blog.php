<?php
/**
 * Created by PhpStorm.
 * User: dustin
 * Date: 1/2/17
 * Time: 12:56 PM
 */

get_header();
the_post(); ?>

    <div class="main-holder">
        <div class="breadcrumbs-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php multisite_path('/'); ?>">Home</a></li>
                            <li>About</></li>
                            <li class="active"><a href="<?php multisite_path('blog'); ?>">Our Blog</a></li>
                        </ol>
                        <div class="title-page">
                            <h1><?php the_title(); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="twocolumns" class="battlement">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                        $active_nav = 'blog';
                        include(locate_template('nav-about.php'));
                        ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="text-box">
                            <div class="post-block">
                                <h1 class="title"><?php the_title(); ?></h1>
                                <time datetime="<?php echo get_the_date() ?>">
                                    <i><b><?php echo the_date();?> </b></i></time>

                                <hr class="line grey" />

                                <div class="img-box center">
                                    <?php
                                    if (has_post_thumbnail()) {
                                        the_post_thumbnail(array(420, 400));
                                    }
                                    ?>
                                </div>
                                <div class="text-box">
                                    <p><?php the_content(); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
    </div>

<?php
get_footer();
?>