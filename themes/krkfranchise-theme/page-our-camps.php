<?php
get_header();
$customizer = new KRK_Camp_Customizer();
?>

<main id="main">
	<div class="main-holder">
		<div class="breadcrumbs-wrap">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="title-page title">
							<h1>CAMPS</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="video-container">
			<div class="video-img-wrap">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/camps/2017/video_thumbnail.png" height="509" width="680" alt="image description">
				<a href="#"
				   data-featherlight='
				   			<iframe width="560" height="315" src="https://www.youtube.com/embed/XcRzCsGsneo?autoplay=1" frameborder="0" allowfullscreen></iframe>
						'
				   class="btn-play">
					<span class="icon icon-play"></span>
				</a>
			</div>
			<div class="video-text-wrap">
				<?php $title = $customizer->get_setting('krk_camp_header_title');
					$header_content = $customizer->get_setting('krk_camp_header_content'); ?>
				<strong class="slogan"><?php echo $title; ?></strong>
				<span class="info" style="font-size: 18px; line-height: 25px;"><?php echo $header_content; ?></span>
				<a href="<?php multisite_path('/schedule-tour'); ?>" class="btn btn-primary">Now Registering For Ages 5-12<span class="icon icon-arrow-right"></span></a>
			</div>
		</div>
		<div class="two-columns battlement">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-7">
						<div class="col-first">
							<div class="section-xtx">
								<h2>We're Connected!</h2>
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/camps/2017/banner.jpg" height="509" width="680" alt="image description"  style="padding-bottom:25px;">
								<?php $left_content = $customizer->get_setting('krk_camp_left_content'); ?>
								<?php echo $left_content; ?>
								<a href="<?php multisite_path('/schedule-tour'); ?>" class="btn btn-primary">Register Your Child Today<span class="icon icon-arrow-right"></span></a>
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<?php $section1_content = $customizer->get_setting('krk_camp_right_content_section1');
						$section1_content = $customizer->get_setting('krk_camp_right_content_section1');
						$section2_content = $customizer->get_setting('krk_camp_right_content_section2');
						$section3_content = $customizer->get_setting('krk_camp_right_content_section3');
						$section4_content = $customizer->get_setting('krk_camp_right_content_section4');
						$section5_content = $customizer->get_setting('krk_camp_right_content_section5');
						$section6_content = $customizer->get_setting('krk_camp_right_content_section6');
						$section7_content = $customizer->get_setting('krk_camp_right_content_section7');
						$section8_content = $customizer->get_setting('krk_camp_right_content_section8');
						$section9_content = $customizer->get_setting('krk_camp_right_content_section9');
						$section10_content = $customizer->get_setting('krk_camp_right_content_section10'); ?>

						<div class="col-second camp-details">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/camps/2017/engineer_it.png"/>
							<?php echo $section1_content; ?>

							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/camps/2017/beyond_it.png"/>
							<?php echo $section2_content; ?>

							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/camps/2017/produce_it.png"/>
							<?php echo $section3_content; ?>

							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/camps/2017/move_it.png"/>
							<?php echo $section4_content; ?>

							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/camps/2017/live_it.png"/>
							<?php echo $section5_content; ?>

							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/camps/2017/love_it.png"/>
							<?php echo $section6_content; ?>

							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/camps/2017/imagine_it.png"/>
							<?php echo $section7_content; ?>

							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/camps/2017/picasso_it.png"/>
							<?php echo $section8_content; ?>

							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/camps/2017/cook_it.png"/>
							<?php echo $section9_content; ?>

							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/camps/2017/give_it.png"/>
							<?php echo $section10_content; ?>
					</div>
				</div>
			</div>
		</div>
		<a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
	</div>

<?php
get_footer();
?>
