<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<script type=""application/ld+json"">
{
    "@context":    "http://schema.org",
    "@type":       "LocalBusiness",
    "name":        "Kids 'R' Kids of <?php echo do_shortcode("[location_name bare='true']"); ?>",
    "logo":        "http://www.kidsrkids.com/wp-content/themes/krkcorporate-theme/images/logo.png",
    "description": "Kids 'R' Kids of <?php echo do_shortcode("[location_name bare='true']"); ?> is a <?php echo do_shortcode("[location_city bare='true']"); ?>, <?php echo do_shortcode("[location_state bare='true']"); ?> kids learning center that provides the highest educational standards for children 6 weeks to 12 years. Contact us today!",
    "URL":         "<?php echo get_bloginfo('url') ?>",
    "image": "<?php echo get_bloginfo('url') ?>",
    "priceRange": "$-$$$$",
    "address" : {
        "@type" :           "PostalAddress",
        "streetAddress" :   "<?php echo do_shortcode("[location_street_address bare='true']"); ?>",
        "addressLocality" : "<?php echo do_shortcode("[location_city bare='true']"); ?>",
        "addressRegion" :   "<?php echo do_shortcode("[location_state bare='true']"); ?>",
        "postalCode" :      "<?php echo do_shortcode("[location_zip bare='true']"); ?>"
    },
     "openingHours": "<?php echo do_shortcode("[redwood_hours id='hours_operation' should_display_title=0 combine_matching_days=1 bare='true']"); ?>",
    "telephone":   "<?php echo do_shortcode("[phone_number bare='true']"); ?>"
}
</script>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
    
</head>

<body <?php body_class(); ?>>
<div id="wrapper">
    <?php 
        $announcements = query_posts('post_type=krk_announcement');
        foreach ($announcements as $announcement) {
            if ($announcement->krk_announcement_active === 'on') {
    ?>
                <div class="alert alert-info" role="alert" style="background: <?php echo $announcement->krk_announcement_background_color; ?>">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <p>
                        <?php if(!empty($announcement->krk_announcement_url)) : ?>
                        <a href="<?php echo $announcement->krk_announcement_url ?>">
                        <?php endif ?>

                            <?php echo $announcement->post_title ?> <span class="ico icon-arrow-1"></span>
                        
                        <?php if(!empty($announcement->krk_announcement_url)) : ?>
                        </a>
                        <?php endif ?>

                        </p>
                </div>
    <?php   
                break;
            }
        }
		wp_reset_query();
    ?>

    <header id="header">
        <a href="<?php multisite_path('/schedule-tour'); ?>" class="label-home hidden-xs">
            <?php $schedule_tour_name = do_shortcode('[schedule_tour_name]'); ?>
            <span class="text"
                <?php if(strlen($schedule_tour_name) > 14) : ?>
                    style="font-size:19px;"
                <?php endif; ?>>
                <?php echo $schedule_tour_name; ?>
            </span>
            <span class="ico"></span>
        </a>
        <nav class="navbar navbar-default">
            <div class="header-panel">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php multisite_path('/'); ?>">
                        <img class="hidden-xs" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" height="100" width="342" alt="Kids R Kids learning Academy">
                        <img class="visible-xs" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-mobile.png" height="84" width="260" alt="Kids R Kids learning Academy">
                    </a>
                </div>
                <span class="slogan hidden-xs">
                    <?php 
                        $MAX_PIXELS_PER_LINE = 560;
                        $MAX_FONT_SIZE = 38;

                        $header_display_name_full = do_shortcode('[header_display_name bare="true"]');
                        $header_location_name = $header_display_name_full; 
                        //extract header name if 'of' is present
                        $of_pos = stripos($header_display_name_full, "of ");
                        
                        if($of_pos !== false) {
                            $header_location_name = substr($header_location_name, $of_pos+3);
                        }
                    ?>
                    <?php if(strlen($header_location_name) > 0) : 
                            $font_size = $MAX_PIXELS_PER_LINE / strlen($header_location_name);
                            if($font_size > $MAX_FONT_SIZE)
                                $font_size = $MAX_FONT_SIZE;
                        ?>
                        <?php if($of_pos !== false) : ?>
                            <span class="header_display_name small"
                                style="font-size: 25px;">
                                of
                            </span>
                        <?php endif;?>
                        <span class="header_display_name
                            <?php echo strlen($header_location_name) > 25 ? 'small' : '' ?>"
                            style="font-size: <?php echo $font_size ?>px;">
                        
                            <?php echo $header_location_name ?>    
                        </span>
                    <?php endif; ?>
                </span>
                <div class="box hidden-xs">
                    <?php
                        $camera_login = do_shortcode('[redwood_camera_login_url]');
                        if( !empty($camera_login) ) :
                    ?>
                        <span><a href="<?php echo $camera_login ?>" target="_blank">Camera Login</a> &bull; </span>
                    <?php endif; ?>
                    <?php
                        $portal_login = do_shortcode('[redwood_parent_portal_url]');
                        if( !empty($portal_login) ) :
                    ?>
                        <span><a href="<?php echo $portal_login; ?>" target="_blank">Portal Login</a> &bull; </span>
                    <?php endif; ?>
                    <?php echo do_shortcode("[phone_number]"); ?>
                    <?php echo do_shortcode("[location_full_address]"); ?>
                    <ul class="social-networks header">
                    	<?php if(!empty(do_shortcode("[url_instagram]"))){ ?>
                        	<li><a href="<?php echo do_shortcode("[url_instagram]"); ?>" target="_blank"><span class="ico icon-instagram"></span></a></li>
                        <?php } ?>
                    	<?php if(!empty(do_shortcode("[url_google_plus]"))){ ?>
                        	<li><a href="<?php echo do_shortcode("[url_google_plus]"); ?>" target="_blank"><span class="ico icon-google"></span></a></li>
                        <?php } ?>
                    	<?php if(!empty(do_shortcode("[url_pinterest]"))){ ?>
                        	<li><a href="<?php echo do_shortcode("[url_pinterest]"); ?>" target="_blank"><span class="ico icon-pinterest"></span></a></li>
                        <?php } ?>
                    	 <?php if(!empty(do_shortcode("[url_twitter]"))){ ?>
                        	<li><a href="<?php echo do_shortcode("[url_twitter]"); ?>" target="_blank"><span class="ico icon-twitter"></span></a></li>
                        <?php } ?>
                        <?php if(!empty(do_shortcode("[url_facebook]"))){ ?>
                        	<li><a href="<?php echo do_shortcode("[url_facebook]"); ?>" target="_blank"><span class="ico icon-facebook"></span></a></li>
                        <?php } ?> 
                        <?php if(!empty(do_shortcode("[url_youtube]"))){ ?>
                            <li><a href="<?php echo do_shortcode("[url_youtube]"); ?>" target="_blank"><span class="ico icon-youtube"></span></a></li>
                        <?php } ?>
                        <?php if(!empty(do_shortcode("[url_yelp]"))){ ?>
                            <li><a href="<?php echo do_shortcode("[url_yelp]"); ?>" target="_blank"><span class="ico icon-yelp"></span></a></li>
                        <?php } ?>
                        <?php if(!empty(do_shortcode("[url_angies_list]"))){ ?>
                            <li><a href="<?php echo do_shortcode("[url_angies_list]"); ?>" target="_blank"><span class="ico icon-angies-list"></span></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>

            <?php 
                //dynamic header styling logic
                $dynamic_nav_style = get_nav_menu_dynamic_style();
            ?>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php
                $params = array(
                    'theme_location' => 'primary',
                    'container' => false,
                    'menu_class' => 'nav navbar-nav',
                    'walker' => new Custom_Nav_Walker
                );
                 wp_nav_menu($params) ?>
            </div>
        </nav>
        <!-- Dynamic styling for the headers -->
        <style>
            .navbar-nav > li {
                padding-left: <?php echo $dynamic_nav_style['horizontal-padding']; ?>px !important;
                padding-right: <?php echo$dynamic_nav_style['horizontal-padding']; ?>px !important;
            }

            .navbar-default .navbar-nav > li > a {
                font-size: <?php echo $dynamic_nav_style['font-size']; ?>px !important;
            }

            @media (max-width: 767px) {
                .navbar-nav > li {
                    padding-left: 0px !important;
                    padding-right: 0px !important;
                }

                .navbar-default .navbar-nav > li > a {
                    font-size: initial !important;
                }
            }

        </style>
    </header>

	<div id="content" class="site-content">
