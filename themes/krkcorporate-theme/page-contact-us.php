<?php
/**
 * Created by PhpStorm.
 * User: dustin
 * Date: 1/27/16
 * Time: 11:09 AM
 */

get_header(); ?>

    <div class="main-holder">
        <div class="breadcrumbs-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php multisite_path('') ?>">Home</a></li>
                            <li class="active">About</li>
                        </ol>
                        <div class="title-page">
                            <h1>Contact Us</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="twocolumns" class="battlement">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            $active_nav = 'contact-us';
                            include(locate_template('nav-about.php'));
                        ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="visual-img">
                            <picture>
                                    <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-31.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-31-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-31-3x.jpg 3x">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-31.jpg" height="299" width="823" alt="image description">
                            </picture>
                            <div class="text">
                                <strong>Contact<br> Us</strong>
                            </div>
                        </div>
                        <div class="section-xtx">
							<p>Kids 'R' Kids International<br>
							1625 Executive South Drive<br>
							Duluth, GA 30096</p>
											
							<p>
                                <b>Corporate Phone:</b>
                                <?php echo do_shortcode('[phone_number phone-number="7702797777"]')?> /
                                <?php echo do_shortcode('[phone_number phone-number="18002790033"]')?>
                            </p>
							<div class="disclaimer-wrapper">
								<p>
                                    <b>Corporate Fax:</b>
                                    <?php echo do_shortcode('[phone_number phone-number="7702799699"]')?>
                                </p>
								<p class="disclaimer">* This fax # is not for children's records or employment verifications. Please check with the individual school for details.</p>
							</div>
							<p> To contact a Learning Academy in your area please search under "Locations".</p>
							<p> To email our Franchise Support Center only, please complete the following:</p>

                            <div class="alert alert-success hidden" id="contact-us-form-success" style="color: #000000;">Thank you for your interest in our school! We will be in contact with you shortly!</div>
                            <div class="alert alert-danger hidden" id="contact-us-form-error">Uh oh! Something went wrong trying to send your message. Please try again later.</div>
                            
							<form id="contact-us-form"
                                  method="post"
                                  data-type="ajax"
                                  data-fade="true">

                                <input type="hidden" value="send_contact_us_form" name="action">

								<div class="form-box">
                                    <div class="form-group">
                                        <span class="required-marker">*</span>
                                        <input name="name"
                                               type="text"
                                               class="form-control"
                                               placeholder="Contact Name"
                                               data-validation-type="name"
                                               data-display-name="Contact Name"
                                               required>
                                    </div>
                                </div>
								<div class="form-box">
                                    <div class="form-group">
                                    	<input name="phone"
                                               type="text"
                                               class="form-control"
                                               placeholder="Phone Number"
                                               data-validation-type="phone"
                                               data-display-name="Phone Number">
                                    </div>
                                    <div class="form-group">
                                        <span class="required-marker">*</span><input type="email" name="email" class="form-control" placeholder="Email" required>
                                    </div>
                                </div>
								<div class="form-box">
									<h3>Comments</h3>
                                    <div class="form-group">
                                            <textarea name="message" class="form-control"></textarea>
                                    </div>
                                </div>
								<button type="submit" class="btn btn-primary">SUBMIT<span class="icon-arrow-1"></span></button>
							</form>
							<br><br>
							<p> If your comment is about a specific Kids 'R' Kids Learning Academy, please specify the owner's name. </p>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
    </div>

<?php
    get_footer();
?>