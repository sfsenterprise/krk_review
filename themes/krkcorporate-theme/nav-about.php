<nav class="menu-wrap">
    <ul class="sub-menu">
        <li class="<?php if($active_nav != NULL && $active_nav=='philosophy') echo 'active';?>"><a href="<?php multisite_path('philosophy') ?> ">Our Philosophy</a>
        </li>
        <li class="<?php if($active_nav != NULL && $active_nav=='curriculum') echo 'active';?>"><a href="<?php multisite_path('curriculum') ?> ">Our Curriculum</a>
            <ul>
                <li><a href="<?php multisite_path('curriculum/#developmentalprogram') ?> ">Developmental Program</a></li>
                <li><a href="<?php multisite_path('curriculum/#interactive') ?> ">Interactive Technology</a></li>
                <li><a href="<?php multisite_path('curriculum/#steam') ?> ">STEAM Ahead</a></li>
            </ul>
        </li>
        <li class="<?php if($active_nav != NULL && $active_nav=='stories') echo 'active';?>"><a href="<?php multisite_path('our-stories') ?> ">Our Stories</a>
        </li>
        <li class="<?php if($active_nav != NULL && $active_nav=='tour') echo 'active';?>"><a href="<?php multisite_path('tour') ?> ">Our Facilities</a>
        </li>
            <li class="<?php if($active_nav != NULL && $active_nav=='staff') echo 'active';?>"><a href="<?php multisite_path('staff') ?> ">Our Staff</a>
            <ul>
                <li><a href="<?php multisite_path('staff') ?> ">The Children Come First</a></li>
                <li><a href="<?php multisite_path('staff/#teachers') ?> ">Our Team</a></li>
                <li><a href="<?php multisite_path('staff/#careers') ?> ">Careers</a></li>
            </ul>
        </li>
        <li class="<?php if($active_nav != NULL && $active_nav=='news') echo 'active';?>"><a href="<?php multisite_path('news') ?> ">Our News</a></li>
        <li class="<?php if($active_nav != NULL && $active_nav=='blog') echo 'active';?>"><a href="<?php multisite_path('blog') ?> ">Our Blog</a></li>
        <li class="<?php if($active_nav != NULL && $active_nav=='contact-us') echo 'active';?>"><a href="<?php multisite_path('contact-us') ?> ">Contact Us</a></li>
    </ul>
</nav>