<?php
get_header();
$customizer = new KRK_Curriculum_Customizer();
?>

<main id="main">
	<div class="main-holder">
		<div class="breadcrumbs-wrap">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<ol class="breadcrumb">
							<li><a href="<?php multisite_path('') ?>">Home</a></li>
							<li class="active">About</li>
						</ol>
						<div class="title-page">
							<h1>OUR CURRICULUM</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="twocolumns" class="battlement" data-customizable="true">
			<div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                        $active_nav = 'curriculum';
							include( locate_template( 'nav-about.php' ));
							?>
						</div>
						<div class="col-sm-9">

							<div class="visual-img">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/DevolopeProgram.jpg" height="299" width="823" alt="image description">
								<div class="text">
									<strong>Developmental <br>Program</strong>
								</div>
							</div>
							<div class="wrap-sections">
								<div class="section-xtx">
									<h4>Learning Through Play</h4>
									<p>At Kids 'R' Kids Learning Academies age-appropriate classrooms are arranged in to a variety of play stations that help to enhance learning. Our exclusive curriculum focuses on the philosophy that children learn best through play. We find it's important for children to have opportunities to construct their own knowledge through exploration, interaction with materials and imitation of positive role models. </p>
								</div>
							</div>
							<hr class="line grey" id="interactive">
							<div class="visual-img">
								<?php $section2_img = $customizer->get_setting('krk_curriculum_section2_header_image'); ?>
								<img src="<?php echo get_template_directory_uri() . '/images/' . $section2_img ?>" height="299" width="823" alt="image description">
								<div class="text">
									<strong>Interactive <br>Technology</strong>
								</div>
							</div>
							<div class="wrap-sections">
								<div class="section-xtx">
									<h4>The 21st Century Learner</h4>
									<p>Our newest generation of learners are growing up in the digital age where everything is touch activated. Our schools have Smart Board technology with Internet capabilities to log on and visit the world from the classroom. Our curriculum also correlates with the award-winning ABCmouse.com for online activities designed to reinforce lessons from the classroom to home. We encourage family involvement in every child's education at home to prepare for kindergarten and beyond. </p>
								</div>
							</div>
							<hr class="line grey" id="steam">
							<div class="visual-img">
								<?php $section3_img = $customizer->get_setting('krk_curriculum_section3_header_image'); ?>
								<img src="<?php echo get_template_directory_uri() . '/images/' . $section3_img ?>" height="299" width="823" alt="image description">
								<div class="text">
									<strong>STEAM <br>AHEAD <br>Curriculum</strong>
								</div>
							</div>
							<div class="logo-columns">
								<div class="col">
									<picture>
										<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/STEAMlogo1.png, <?php bloginfo('stylesheet_directory'); ?>/images/STEAMlogo-2x.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/STEAMlogo-3x.png 3x">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/STEAMlogo1.png" height="169" width="339" alt="image description">
									</picture>
								</div>
								<div class="col">
									<picture>
										<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/logo-02.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/logo-02-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/logo-02-3x.jpg 3x">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-02.jpg" height="83" width="420" alt="image description">
									</picture>
								</div>
							</div>
							<div class="wrap-sections">
								<div class="section-xtx">
									<p>The classroom explore STEAM AHEAD&reg; projects together. In an effort to develop a child's natural curiosity, teachers encourage them to ask, "why?" Children work and play in an environment where they have opportunities to observe, question, investigate, predict, experiment, build and share what they learn. Projects are hands-on and bring together many facets of what children find interesting in a way that packs an array of educational "AH HA" moments.</p>
								</div>
								<div class="section-xtx">
									<h4>What is STEAM AHEAD?</h4>
									<p>Kids 'R' Kids STEAM AHEAD&reg; is an integrated project-based curriculum incorporating more science, technology, engineering, art and math into a child's everyday learning though exploration and play.</p>
								</div>
								<div class="section-xtx">
									<h4>Why STEAM AHEAD?</h4>
									<p>Unlike academic learning of memorization with no logical or practical application, Kids 'R' Kids STEAM AHEAD&reg; projects encourage early learners to be natural scientists using reasoning, hypothesising, predicting and theorising during the natural trial and error of play. </p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
		</div>




<?php

get_footer();

?>