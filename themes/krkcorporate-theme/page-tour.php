<?php
get_header();
$customizer = new KRK_Tour_Customizer();
?>

<main id="main">
	<div class="main-holder">
		<div class="breadcrumbs-wrap">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<ol class="breadcrumb">
							<li><a href="<?php multisite_path('') ?>">Home</a></li>
							<li class="active">About</li>
						</ol>
						<div class="title-page">
							<h1>OUR FACILITIES</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="twocolumns" class="battlement">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3">
						<?php
						$active_nav = 'tour';
						include( locate_template( 'nav-about.php' ));
						?>
					</div>
					<div class="col-sm-9">
						<?php if ( !empty( do_shortcode('[redwood_virtual_tour_iframe]') ) ): ?>
							<div class="visual-img">
								<picture>
									<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-32.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-32-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-32-3x.jpg 3x">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-32.jpg" height="299" width="823" alt="image description">
								</picture>
								<div class="text">
									<strong>Virtual <br>Tour</strong>
								</div>
							</div>
							<div class="box-view">
								<div class="visual" id="tour-visual">
									<a class="cursor-pointer">
										<?php echo do_shortcode('[redwood_virtual_tour_iframe]'); ?>
										<span class="text">
											Take a Virtual Tour!
											<span class="ico icon-arrow-click"></span>
										</span>
									</a>
								</div>
								<div class="body-view">
									<h2>How to navigate:</h2>
									<p><i>Click to</i> “Walk”</p>
									<p><i>Scroll to</i> “Zoom”</p>
									<p><i>Click &amp; Hold to</i> “Look Around”</p>
								</div>
							</div>
						<?php endif ?>
						<?php
						//Render the image gallery from tour
						while ( have_posts() ) : the_post();
							get_template_part('gallery', 'tour');
						endwhile; ?>
						<div class="txt">
							<?php $content = $customizer->get_setting('krk_tour_content'); ?>
							<?php echo $content; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
	</div>

	<script type="text/javascript">
		jQuery(document).ready(function () {
			jQuery('#tour-visual').click(function () {
				var elm = jQuery(this);
				if (!elm.data('tour-active')) {
					elm.data('tour-active', true);
					elm.removeClass('visual');

					jQuery('.text', elm).hide();
				}
			});
		});
	</script>
	
<?php

get_footer();

?>