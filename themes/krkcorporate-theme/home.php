<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header();
$customizer = new KRK_Home_Customizer();
$main_img = $customizer->get_setting('krk_home_main_image');
?>

<?php if(strlen($main_img) > 0) : ?>
    <style>
        .section-first {
            background: url(<?php echo get_template_directory_uri() . '/images/' . $main_img; ?>);
            background-size: cover;
        }
    </style>
<?php endif; ?>

    <div class="main-holder">
        <div class="section-first hidden-xs">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <strong class="slogan">Smart Choice. Smarter Child.</strong>
                        <div class="btn-wrap">
                            <a href="<?php multisite_path('locations') ?>" class="btn btn-primary">Find a School<span class="icon icon-arrow-right"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="first-mobile visible-xs">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text">
                            <span class="title">Welcome!</span>
                            <p>We believe that <b>happy, loved and connected children</b> are destined for success in every facet of their lives.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="section-video">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="text-block">
                            <h2>Kids ‘R’ Amazing!</h2>
                            <span class="desk">Watch to see why our kids are so exceptional!</span>
                            <div class="btn-wrap">
                                <a href="#"
                                   data-featherlight=
                                   '
											<video width="100%" height="auto" controls autoplay>
												<source src="<?php bloginfo('stylesheet_directory'); ?>/videos/KidsRCampaign.mp4" type="video/mp4">
											</video>
									 	' class="btn-play">
                                    <span class="icon icon-play"></span>
                                </a>
                            </div>
                            <a href="#"
                               data-featherlight=
                               '
										<video width="100%" height="auto" controls autoplay>
											<source src="<?php bloginfo('stylesheet_directory'); ?>/videos/KidsRCampaign.mp4" type="video/mp4">
										</video>
									 ' class="btn btn-primary hidden-xs">Watch Now</a>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-curriculum">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <div class="text-block">
                            <h2>Our Philosophy &amp; Curriculum</h2>
                            <span class="desk">Our exclusive curriculum propels Kids ‘R’ Kids Learning Academies beyond daycare centers and childcare providers. Take a peek into our programs.</span>
                            <ul class="kids-curriculum">
                                <li><a href="<?php multisite_path('philosophy') ?>"><picture><source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-04.png, <?php bloginfo('stylesheet_directory'); ?>/images/img-04-2x.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-04-3x.png 3x"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-04.png" height="179" width="278" alt="image description"></picture></a></li><li><a href="<?php multisite_path('curriculum') ?>"><picture><source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-05.png, <?php bloginfo('stylesheet_directory'); ?>/images/img-05-2x.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-05-3x.png 3x"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-05.png" height="179" width="278" alt="image description"></picture></a></li><li><a href="<?php multisite_path('curriculum/#steam') ?>"">
                                    <picture>
                                        <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-07.png, <?php bloginfo('stylesheet_directory'); ?>/images/img-07-2x.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-07-3x.png 3x">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-07.png" height="179" width="278" alt="image description">
                                    </picture></a></li><li>
                                    <a href="<?php multisite_path('curriculum/#brainwaves') ?>"">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/brain-waves.jpg" height="179" width="278" alt="image description">
                                    </a>
                                </li>
                            </ul>
                            <a href="<?php multisite_path('curriculum') ?>" class="btn btn-primary hidden-xs">Learn More <span class="icon icon-arrow-right"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        $type = 'krk_testimonial';
        $args=array(
            'post_type' => $type,
            'post_status' => 'publish',
            'order' => 'DESC',
            'posts_per_page' => 3);
        $query = null;
        $query = new WP_Query($args);
        if ($query->have_posts()) : ?>
        <section class="section-gallery">
            <h2>Our Stories</h2>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php
                    $i = 0;
                    while ($query->have_posts()):
                        $i++;
                        $query->the_post();
                        $name = get_post_meta(get_the_ID(), "krk_testimonial_name", true);
                        $title = get_post_meta(get_the_ID(), "krk_testimonial_title", true);
                        $source = get_post_meta(get_the_ID(), "krk_testimonial_source", true); ?>
                        <div class="item <?php if ($i==1) echo "active"; ?>">
                            <div class="text-block">
                                    <span class="desk"><?php $content = get_the_content();
                                        if (strlen($content) > 150) {
                                            echo strip_tags(substr($content, 0, strpos($content, " ", 150)))."...";
                                        } else {
                                            the_content();
                                        }
                                        ?>
                                    </span>
                                <div class="link-to">
                                    <?php if ($source=="facebook") { ?>
                                        <a href="https://www.facebook.com/kidsrkidscorporate" class="social" style="background:#3b5998;">
                                            <span class="icon icon-facebook"></span>
                                        </a>
                                    <?php }
                                    elseif ($source=="google") { ?>
                                        <a href="https://plus.google.com/110315008907308268607" class="social" style="background:#dd4b39;">
                                            <span class="icon icon-google-plus"></span>
                                        </a>

                                    <?php }
                                    elseif ($source=="twitter") { ?>
                                        <a href="https://twitter.com/kidsrkidscorp" class="social" style="background:#00aced;">
                                            <span class="icon icon-twitter"></span>
                                        </a>

                                    <?php } ?>
                                    <span class="name"> <?php echo $name; ?> <br> <?php echo $title; ?> </span>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php if($query->post_count > 1) { ?>
                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    <?php } ?>
                    <?php endif; ?>



        </section>
        <!--<section class="section-learning">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
									<div class="text-block">
										<h2>Latest Technology & Learning Programs</h2>
                                        <a href="<?php multisite_path('curriculum/#interactive') ?>">
                                     <picture>
                                         <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-06.png, <?php bloginfo('stylesheet_directory'); ?>/images/img-06-2x.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-06-3x.png 3x">
                                         <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-06.png" height="179" width="278" alt="image description">
                                     </picture>
                                         </a>
										<span class="desk">Our newest generation of learners are growing up in the digital age where everything is touch activated.</span>
										<a href="#" class="btn btn-primary">Learn More <span class="icon icon-arrow-right"></span></a>
									</div>
								</div>
							</div>
						</div>
					</section>!-->
        <?php
        $type = 'krk_news';
        $args=array(
            'post_type' => $type,
            'post_status' => 'publish',
            'order' => 'DESC',
            'posts_per_page' => 2);

        $query = null;
        $query = new WP_Query($args);
        if( $query->have_posts() ): ?>
            <section class="section-news">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8">
                            <div class="text-block">
                                <h2>Latest News</h2>
                                <?php
                                while ($query->have_posts()):
                                    $query->the_post();?>
                                    <div class="post-news container">
                                        <h3><a href="<?php the_permalink() ?>"> <?php the_title(); ?> </a></h3>
                                        <div class="row">
                                            <?php
                                            if (has_post_thumbnail()):
                                                ?>
                                                <div class="col-md-2">
                                                    <?php the_post_thumbnail('thumbnail', array( 'class' => 'img-thumbnail' ));  ?>
                                                </div>
                                                <?php
                                            endif
                                            ?>
                                            <div class="<?php echo has_post_thumbnail() ? 'col-md-6' : 'col-md-8' ?>">
                                                <?php $content = get_the_content();
                                                if (strlen($content) > 250) {
                                                    echo strip_tags(substr($content, 0, strpos($content, " ", 250)))."...";
                                                } else {
                                                    the_content();
                                                }
                                                ?>
                                                <br />
                                                <a href="<?php the_permalink() ?>" class="read-more">Read More <span class="icon icon-arrow-right"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                endwhile;?>
                                <div class="btn-wrap">
                                    <a href="news" class="load-more">See All News</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php
        endif;
        wp_reset_query();  // Restore global post data stomped by the_post().
        ?>
    </div>
    </div>
    </div>

<?php get_footer(); ?>