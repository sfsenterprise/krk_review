<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<div class="tour-holder">
    <div class="box-tour-mobile visible-xs">
        <div class="box-txt">
            <strong class="title">Come and see how WE HOLD THE FUTURE!&trade;</strong>
            <p>For children 6 weeks - 12 years</p>
        </div>
        <div class="box-btn">
            <a href="<?php multisite_path('locations') ?>" class="button-tour">FIND A SCHOOL <span class="ico"></span></a>
        </div>
    </div>
     <div class="box-tour hidden-xs">
        <div class="box-txt">
            <strong class="title">Come and see how WE HOLD THE FUTURE!&trade;</strong>
            <p>For children 6 weeks - 12 years</p>
        </div>
        <div class="box-btn">
            <a href="<?php multisite_path('locations') ?>" class="button-tour">FIND A SCHOOL <span class="ico"></span></a>
        </div>
    </div>
</div>
</main><!-- .site-main -->
</div><!-- .content-area -->
	</div><!-- .site-content -->
<footer id="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <form id="newsletter-form"
                      data-type="ajax"
                      data-fade="true"
                      action="#"
                      class="email-form">
                    <input type="hidden" name="action" value="send_newsletter_signup" />
                    <fieldset>
                        <span class="title">Sign Up for Our Newsletter</span>
                        <p><i>Receive helpful info for parents!</i></p>
                        <div class="input-wrap">
                            <input type="email"
                                   name="email"
                                   class="form-control"
                                   placeholder="Enter your email address"
                                   data-display-name="Email"
                                   required>
                            <button type="submit" class="btn btn-default"><span class="icon-arrow-right"></span></button>
                        </div>
                    </fieldset>
                </form>
                <div id="newsletter-form-success" class="hidden small">Thank you for signing up!</div>
                <div id="newsletter-form-error" class="hidden small">Uh oh! Something went wrong signing you up. Please try again later.</div>
            </div>
            <div class="col-sm-2">
                <h2>Learn About Us</h2>
                <nav class="menu">
                    <ul>
                        <li><a href="<?php multisite_path('curriculum') ?>">Our Curriculum</a></li>
                        <li><a href="<?php multisite_path('staff') ?>">Our Staff</a></li>
                        <li><a href="<?php multisite_path('our-stories') ?>">Our Stories</a></li>
                        <li><a href="<?php multisite_path('our-locations') ?> ">Our Schools</a></li>
                        <li><a href="<?php multisite_path('contact-us') ?>">Contact Us</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-sm-2">
                <h2>LEARN MORE</h2>
                <nav class="menu">
                    <ul>
                        <li><a href="<?php multisite_path('news') ?>">News</a></li>
                        <li><a href="<?php multisite_path('blog') ?>">Blog</a></li>

                        <?php if( !empty(do_shortcode('[redwood_camera_login_url]')) ) :?>
                            <li><a href="<?php echo do_shortcode('[redwood_camera_login_url]') ?>">Current Parents</a></li>
                        <?php endif; ?>

                        <?php if( !empty(do_shortcode('[redwood_parent_portal_url]')) ) :?>
                            <li><a href="<?php echo do_shortcode('[redwood_parent_portal_url]') ?>">Portal Login</a></li>
                        <?php endif; ?>

                        <?php if( !empty(do_shortcode('[redwood_payment_url]')) ): ?>
                            <li><a href="<?php echo do_shortcode('[redwood_payment_url]') ?>">Pay Online</a></li>
                        <?php endif ?>
                    </ul>
                </nav>
            </div>
            <div class="col-sm-4">
                <ul class="social-networks">
                   <?php if(!empty(do_shortcode("[url_instagram]"))){ ?>
                        <li><a href="(<?php echo do_shortcode("[url_instagram]"); ?>"><span class="ico icon-instagram"></span></a></li>
                    <?php } ?>
                    <?php if(!empty(do_shortcode("[url_google_plus]"))){ ?>
                         <li><a href="<?php echo do_shortcode("[url_google_plus]"); ?>"><span class="ico icon-google"></span></a></li>
                    <?php } ?>
                    <?php if(!empty(do_shortcode("[url_pinterest]"))){ ?>
                        <li><a href="<?php echo do_shortcode("[url_pinterest]"); ?>"><span class="ico icon-pinterest"></span></a></li>
                    <?php } ?>
                    <?php if(!empty(do_shortcode("[url_twitter]"))){ ?>
                        <li><a href="<?php echo do_shortcode("[url_twitter]"); ?>"><span class="ico icon-twitter"></span></a></li>
                    <?php } ?>
                    <?php if(!empty(do_shortcode("[url_facebook]"))){ ?>
                        <li><a href="<?php echo do_shortcode("[url_facebook]"); ?>"><span class="ico icon-facebook"></span></a></li>
                    <?php } ?> 
                    <?php if(!empty(do_shortcode("[url_youtube]"))){ ?>
                        <li><a href="<?php echo do_shortcode("[url_youtube]"); ?>"><span class="ico icon-youtube"></span></a></li>
                    <?php } ?>
                    <?php if(!empty(do_shortcode("[url_yelp]"))){ ?>
                        <li><a href="<?php echo do_shortcode("[url_yelp]"); ?>"><span class="ico icon-yelp"></span></a></li>
                    <?php } ?>
                    <?php if(!empty(do_shortcode("[url_angies_list]"))){ ?>
                        <li><a href="<?php echo do_shortcode("[url_angies_list]"); ?>"><span class="ico icon-angies-list"></span></a></li>
                    <?php } ?> 
                </ul>
                <div class="btn-hold hidden-xs"><a href="http://kidsrkidsfranchise.com/" target="_blank" class="button-become">Become a Franchisee!</a></div>
            </div>
        </div>
    </div>
</footer>

</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>
