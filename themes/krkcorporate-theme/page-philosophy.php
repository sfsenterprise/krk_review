<?php
get_header();
$customizer = new KRK_Philosophy_Customizer();
?>
<main id="main">
	<div class="main-holder">
		<div class="breadcrumbs-wrap">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<ol class="breadcrumb">
							<li><a href="<?php multisite_path('') ?>">Home</a></li>
							<li class="active">About</li>
						</ol>
						<div class="title-page">
							<h1>OUR PHILOSOPHY</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="twocolumns" class="battlement">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3">
						<?php
						$active_nav = 'philosophy';
						include( locate_template( 'nav-about.php' ));
						?>
					</div>
					<div class="col-sm-9" data-customizable="true">
						<div class="visual-img">
							<?php $background = $customizer->get_setting('krk_philosophy_header_image'); ?>
							<img src="<?php echo $background; ?>" height="299" width="823" alt="image description">
							<div class="text">
								<?php $title = $customizer->get_setting('krk_philosophy_header_title'); ?>
								<strong><?php echo $title; ?></strong>
							</div>
						</div>
						<div class="wrap-sections">
							<?php $content = $customizer->get_setting("krk_philosophy_content"); ?>
							<?php echo $content; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
	</div>

<?php
get_footer();
?>