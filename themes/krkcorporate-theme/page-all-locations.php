<?php

get_header();

?>
<?php $state_map = Redwood_Location_utilities::get_all_locations_by_state();
	//error_log(print_r($locations, true)); ?>
<main id="main">
	<div class="main-holder">
		<div class="breadcrumbs-wrap">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<ol class="breadcrumb">
							<li><a href="<?php multisite_path('')?> ">Home</a></li>
							<li><a href="#">About</a></li>
							<li class="active">Our Locations</li>
						</ol>
						<div class="title-page">
							<h1>ALL LOCATIONS</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="twocolumns" class="battlement">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3">
						<nav class="menu-wrap">
							<ul class="sub-menu">
								<li><a href="<?php multisite_path('all-locations')?> ">All Locations</a></li>
							</ul>
						</nav>
					</div>
					<div class="col-sm-9">
						<div class="visual-img">
							<picture>
								<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-43.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-43-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-43-3x.jpg 3x">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-43.jpg" height="299" width="823" alt="image description">
							</picture>
							<div class="text">
								<strong>All <br> Locations</strong>
							</div>
						</div>
						<?php
						foreach($state_map as $state => $locations){ ?>
						<?php if(empty($locations)){
							continue;
						} ?>
						<div class="block-inform">
							<div class="block">
								<h2><?php echo $state; ?></h2>
								<?php foreach($locations as $location){ ?>
									<div class="hold">
										<h3> <?php echo $location['city']; ?></h3>
										<div class="row">
											<div class="col-sm-5">
												<address>
													<span> <?php echo $location['street_address']; ?> </span>
													<span> <?php echo $location['city']; ?>, <?php echo $location['state']; ?> <?php echo $location['zip']; ?> </span>
													<span>
														<a href="tel:<?php echo $location['phone']; ?>">
															<?php echo do_shortcode("[phone_number phone-number='". $location['phone'] ."']")?>
														</a>
													</span>
													<ul>
														<li><a href="<?php echo $location['site_url'] ?>">Website</a></li>
														<li><a href="http://www.maps.google.com/maps?daddr=<?php echo $location["encoded_address"]; ?> ">Directions</a></li>
													</ul>
												</address>
											</div>
											<div class="col-sm-7">
												<strong>Regular Hours</strong>
												<span><?php echo do_shortcode("[redwood_hours id='hours_operation' should_display_title=0 combine_matching_days=1]"); ?>
												</span>
											</div>
										</div>
									</div><?php
								}
								} ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
		</div>
	</div>

<?php

get_footer();

?>