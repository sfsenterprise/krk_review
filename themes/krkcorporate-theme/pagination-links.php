<?php

    $pages = paginate_links(array(
        'total' => $query->max_num_pages,
        'prev_text' => __('<span class="icon icon-arrow-left"></span> Previous'),
        'next_text' => __('Next <span class="icon icon-arrow-right"></span>'),
        'type' => 'array'
    ));

    if( is_array( $pages ) ) {
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<div class="pagination-wrap"><ul class="pagination">';
        foreach ( $pages as $page ) {
            echo "<li>$page</li>";
        }
        echo '</ul></div>';
    }

?>