<?php
get_header();
$customizer = new KRK_Staff_Customizer();
?>

<main id="main">
    <div class="main-holder">
        <div class="breadcrumbs-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php multisite_path('') ?>">Home</a></li>
                            <li class="active">About</li>
                        </ol>
                        <div class="title-page">
                            <h1>OUR STAFF</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="twocolumns" class="battlement">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <?php $active_nav = 'staff';
                        include(locate_template('nav-about.php')); ?>
                    </div>
                    <div class="col-sm-9">
                    <div class="visual-img">
                        <picture>
                            <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-05.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-05-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-05-3x.jpg 3x">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-05.jpg" height="299" width="823" alt="image description">
                        </picture>
                        <div class="text">
                            <strong>The Children Come First</strong>
                        </div>
                    </div>
                    <div class="wrap-sections">
                        <div class="section-xtx">
                            <?php $content = $customizer->get_setting('krk_staff_content'); ?>
                            <?php echo $content; ?>
                        </div>
                    </div>
                    <hr class="line grey" id="teachers">
                    <div class="visual-img">
                        <picture>
                            <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-17.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-17-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-17-3x.jpg 3x">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-17.jpg" height="299" width="823" alt="image description">
                        </picture>
                    <div class="text">
                        <strong>Our Team</strong>
                    </div>
                        </div>
                        <?php
                        $type = 'krk_staff';
                        $args=array(
                            'post_type' => $type,
                            'post_status' => 'publish',
                            'order' => 'DESC',
                            'posts_per_page' => -1);

                        $query = null;
                        $query = new WP_Query($args);
                        if( $query->have_posts() ):
                            while ($query->have_posts()):
                                $query->the_post();
                                $name = get_post_meta(get_the_ID(), 'krk_staff_name', true);
                                $title = get_post_meta(get_the_ID(), 'krk_staff_title', true);
                                ?>
                        <div class="item">
                            <div class="visual">
                                 <?php
                                if (has_post_thumbnail()) {
                                    the_post_thumbnail(array(200, 223));
                                }
                                ?>
                            </div>
                            <div class="text">
                                <div class="title">
                                    <h3><?php echo $name; ?></h3>
                                    <h4><?php echo $title; ?></h4>
                                </div>
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <?php
                            endwhile;
                        endif;
                                ?>
                        <hr class="line grey" id="careers">
                        <div class="visual-img">
                            <picture>
                                <source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-23.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-23-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-23-3x.jpg 3x">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-23.jpg" height="299" width="823" alt="image description">
                            </picture>
                            <div class="text">
                                <strong>Careers</strong>
                            </div>

                        </div>
                        <iframe width="100%" height="299" src="https://www.youtube.com/embed/oH5MkSEohQw" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
    </div>

<?php

get_footer(); 
?>