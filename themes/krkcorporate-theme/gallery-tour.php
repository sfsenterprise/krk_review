<ul class="list-img">
    <?php
    add_filter( 'shortcode_atts_gallery', 'get_gallery_full_size' ); //returns the full images from the gallery
    $gallery = get_post_gallery_images(get_the_ID());
    foreach($gallery as $image_url):
        ?>
            <li>
                <a href="<?php echo $image_url ?>" data-featherlight="image">
                    <img src="<?php echo $image_url; ?>" height="245" width="245" alt="image description">
                </a>
            </li>
    <?php
    endforeach;
    ?>
</ul>