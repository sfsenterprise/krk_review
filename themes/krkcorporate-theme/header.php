<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed site">
	<header id="header">
		<nav class="navbar navbar-default">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php multisite_path('')?> ">
					<img class="hidden-xs" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" height="100" width="342" alt="Kids R Kids learning Academy">
					<img class="visible-xs" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-mobile.png" height="84" width="260" alt="Kids R Kids learning Academy">
				</a>
			</div>
			<div class="open-close-search">
				<div class="slide">
					<form class="navbar-form" action="<?php multisite_path('locations') ?>" method="get">
						<div class="form-group">
							<input id="schoolQuery" name="zipcode" type="text" class="form-control" placeholder="Find a School">
						</div>
						<button type="submit" class="btn btn-default opener">
							<span class="icon-search"></span>
						</button>
					</form>
				</div>
			</div>
			<a href="<?php multisite_path('contact-us') ?>" class="label-home hidden-xs"><span>CONTACT <br> US <span class="ico"></span></span></a>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li <?php if(is_page('locations')) echo 'class="active"'; ?> >
						<a href="<?php multisite_path('locations') ?>">LOCATIONS</a>
					</li>
					<li class="dropdown right-drop
						<?php if(is_page('philosophy') ||
								 is_page('curriculum') ||
								 is_page('tour') ||
								 is_page('staff') ||
								 is_post_type_archive('krk_news'))
									echo "active"; ?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ABOUT US</a>
						<div class="dropdown-menu">
							<div class="columns">
								<div class="col">
									<div class="box">
										<ul>
											<li><a href="<?php multisite_path('philosophy') ?>">Our Philosophy</a></li>
											<li><a href="<?php multisite_path('curriculum') ?>"> Our Curriculum</a></li>
											<li><a href="<?php multisite_path('tour') ?>" >Our Facilities</a></li>
											<li><a href="<?php multisite_path('staff') ?>">Our Staff</a></li>
										</ul>
									</div>
								</div>
								<div class="col">
									<div class="visual">
										<a href="<?php multisite_path('locations') ?>">
											<picture>
												<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-01.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-01-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-01-3x.jpg 3x">
												<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-01.jpg" height="157" width="162" alt="image description">
											</picture>
											<span class="text">Find A School <span class="ico icon-arrow"></span></span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="dropdown right-drop
						<?php if(is_page('infants') ||
							is_page('toddlers') ||
							is_page('pre-kindergarten') ||
							is_page('preschool') ||
							is_page('school-age'))
								echo "active"; ?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PROGRAMS</a>
						<div class="dropdown-menu">
							<div class="columns">
								<div class="col">
									<div class="box">
										<ul>
											<li><a href="<?php multisite_path('infants') ?>">Infants</a></li>
											<li><a href="<?php multisite_path('toddlers') ?>">Toddlers</a></li>
											<li><a href="<?php multisite_path('pre-kindergarten') ?>">Pre-Kindergarten</a></li>
											<li><a href="<?php multisite_path('preschool') ?>">Pre-School</a></li>
											<li><a href="<?php multisite_path('school-age') ?>">School Age</a></li>
										</ul>
									</div>
								</div>
								<div class="col">
									<div class="visual">
										<a href="<?php multisite_path('locations') ?>">
											<picture>
												<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-01.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-01-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-01-3x.jpg 3x">
												<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-01.jpg" height="157" width="162" alt="image description">
											</picture>
											<span class="text">Find A School <span class="ico icon-arrow"></span></span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li <?php if(is_post_type_archive('krk_blog')) echo 'class="active"' ?> >
						<a href="<?php multisite_path('blog') ?>">OUR BLOG</a>
					</li>
					<!--<li class="dropdown right-drop">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">FOR PARENTS</a>
						<div class="dropdown-menu">
							<div class="columns">
								<div class="col">
									<div class="box">
										<ul>
											<li><a href="<?php multisite_path('blog') ?>">Blogs</a></li>
										</ul>
									</div>
								</div>
								<div class="col">
									<div class="visual">
										<a href="<?php multisite_path('locations') ?>">
											<picture>
												<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-01.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-01-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-01-3x.jpg 3x">
												<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-01.jpg" height="157" width="162" alt="image description">
											</picture>
											<span class="text">Find A School <span class="ico icon-arrow"></span></span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</li>!-->
					<li>
						<a href="http://kidsrkidsfranchise.com/">FRANCHISING</a>
					</li>
					<li <?php if(is_page('contact-us')) echo 'class="active"' ?>>
						<a href="<?php multisite_path('contact-us') ?>">CONTACT US</a>
					</li>
				</ul>
				<div class="box">
					<nav>
						<ul class="sub-nav">
							<li><a href="<?php multisite_path('philosophy') ?>">Our Philosophy</a></li>
							<li><a href="<?php multisite_path('tour') ?>">Our Facilities</a></li>
							<li><a href="<?php multisite_path('our-stories') ?>">Our Stories</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</nav>
	</header>
