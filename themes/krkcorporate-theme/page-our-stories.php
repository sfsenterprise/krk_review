<?php
/**
 * Created by PhpStorm.
 * User: dustin
 * Date: 1/27/16
 * Time: 11:09 AM
 */

get_header(); ?>

    <div class="main-holder">
        <div class="breadcrumbs-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php multisite_path('') ?>">Home</a></li>
                            <li class="active">About</li>
                        </ol>
                        <div class="title-page">
                            <h1>OUR STORIES</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="twocolumns" class="battlement">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            $active_nav = 'stories';
                            include(locate_template('nav-about.php'));
                        ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="visual-img">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/OurStories_2.jpg" height="299" width="823" alt="image description">
                            <div class="text">
                                <strong>Our<br> Stories</strong>
                            </div>
                        </div>
                        <div class="block-posts">
                            <?php
                                $type = 'krk_testimonial';
                                $args=array(
                                    'post_type' => $type,
                                    'post_status' => 'publish',
                                    'order' => 'DESC',
                                    'posts_per_page' => -1);

                                $query = null;
                                $query = new WP_Query($args);
                                if( $query->have_posts() ):
                                    while ($query->have_posts()):
                                        $query->the_post();
                                        $name = get_post_meta(get_the_ID(), 'krk_testimonial_name', true);
                                        $title = get_post_meta(get_the_ID(), 'krk_testimonial_title', true);
                                        $source= get_post_meta(get_the_ID(), 'krk_testimonial_source', true);
                            ?>
                                        <div class="post-box">
                                            <p><?php the_content(); ?></p>
                                            <div class="link-to">
                                                <?php
                                                switch($source):
                                                    case 'facebook':
                                                        echo '<a href="'. do_shortcode('[url_facebook]') .'" class="social" style="background: #3B5998;">
                                                                <span class="icon icon-facebook"></span>
                                                              </a>';
                                                        break;
                                                    case 'twitter':
                                                        echo '<a href="'. do_shortcode('[url_twitter]') .'" class="social" style="background: #00aced;">
                                                                <span class="icon icon-twitter"></span>
                                                              </a>';
                                                        break;
                                                    case 'google':
                                                        echo '<a href="'. do_shortcode('[url_google_plus]') .'" class="social">
                                                                <span class="icon icon-google-plus"></span>
                                                            </a>';
                                                        break;
                                                    default:
                                                        break;

                                                endswitch;
                                                ?>
                                                <span class="name"><?php echo $name . '<br/>' . $title?></span>
                                            </div>
                                        </div>
                              <?php
                                    endwhile;
                                 endif;
                               ?>
                        </div>
                        <div class="info-social">
                        	 <?php
                        		$socialLinks = [];
								if(!empty(do_shortcode("[url_yelp]")))
									$socialLinks["Yelp"] = do_shortcode("[url_yelp]"); 
								if(!empty(do_shortcode("[url_google_plus]")))
									$socialLinks["Google+"] = do_shortcode("[url_google_plus]"); 
								if(!empty(do_shortcode("[url_facebook]")))
									$socialLinks["Facebook"] = do_shortcode("[url_facebook]"); 

                            	if(count($socialLinks) > 0){
                            		echo '<p>See more stories on ';
	                            	foreach($socialLinks as $key => $value){
	                            		$link = '<a href="'.$value.'""> '.$key.'</a>';
										
		                            	//determine if append 'comma' or prepend 'and'
		                            	if(count($socialLinks) > 1){
		                            		if(end(array_keys($socialLinks)) == $key ){
		                            			echo 'and ' . $link;
		                            		}
											else{
												echo $link . ', ';
											}
										}
										else{
											echo $link;
										}
									}
									echo '!</p>';
								}
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
    </div>

<?php
    get_footer();
?>