<?php

get_header();

?>

<main id="main">
	<div class="main-holder">
		<div class="breadcrumbs-wrap">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<ol class="breadcrumb">
							<li><a href="<?php multisite_path('') ?>">Home</a></li>
							<li class="active">About</li>
						</ol>
						<div class="title-page">
							<h1>CONTENT PAGE NAME</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="twocolumns" class="battlement">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3">
						<nav class="menu-wrap">
							<ul class="sub-menu">
								<li><a href="#">Content Page Section</a></li>
							</ul>
						</nav>
					</div>
					<div class="col-sm-9">
						<div class="visual-img">
							<picture>
								<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-03.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-03-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-03-3x.jpg 3x">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-03.jpg" height="299" width="823" alt="image description">
							</picture>
							<div class="text">
								<strong>Content Page <br>Section Name</strong>
							</div>
						</div>
						<div class="wrap-sections">
							<div class="section-xtx">
								<h4>Subtitle Goes Here</h4>
								<p>Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow's nest nipperkin grog yardarm hempen halter furl. Swab barque interloper chantey doubloon starboard grog black jack gangway rutters. Deadlights jack lad schooner scallywag dance the hempen jig carouser broadside. cable strike colors. Bring a spring upon her cable holystone blow the man down spanker Shiver me timbers to go on account lookout wherry doubloon chase. Belay yo-ho-ho keelhaul squiffy black spot yardarm spyglass sheet transom heave to..</p>
							</div>
							<div class="section-xtx">
								<h4>Subtitle Goes Here</h4>
								<p>Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow's nest nipperkin grog yardarm hempen halter furl. Swab barque interloper chantey doubloon starboard grog black jack gangway rutters. Deadlights jack lad schooner scallywag dance the hempen jig carouser broadside. cable strike colors.</p>
							</div>
						</div>
						<div class="visual-img">
							<picture>
								<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/img-04.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/img-04-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/img-04-3x.jpg 3x">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img-04.jpg" height="299" width="823" alt="image description">
							</picture>
						</div>
						<div class="wrap-sections">
							<div class="section-xtx">
								<h4>Subtitle Goes Here</h4>
								<p>Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow's nest nipperkin g rog yardarm hempen halter furl. Swab barque interloper chantey doubloon starboard grog black jack gangway rutters. Deadlights jack lad schooner scallywag dance the hempen jig carouser broadside. cable strike colors.</p>
							</div>
							<div class="section-xtx">
								<h4>Subtitle Goes Here</h4>
								<p>Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow's nest nipperkin g rog yardarm hempen halter furl. Swab barque interloper chantey doubloon starboard grog black jack gangway rutters. Deadlights jack lad schooner scallywag dance the hempen jig carouser broadside. cable strike colors.</p>
							</div>
							<div class="section-xtx">
								<h4>Subtitle Goes Here</h4>
								<p>Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace cable strike colors. Bring a spring upon her cable holystone blow the man down spanker Shiver me timbers to go on account lookout wherry doubloon chase. Belay yo-ho-ho keelhaul squiffy black spot yardarm spyglass sheet transom heave to.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
	</div>

<?php

get_footer();

?>