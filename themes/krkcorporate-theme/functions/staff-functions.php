<?php

$krk_staff_meta_fields = array(
   array(
       'label' => 'Name',
       'desc'  => 'The name of the staff member',
       'id'    => 'krk_staff_name',
       'type'  => 'text'
   ),
   array(
       'label' => 'Title',
       'desc'  => 'The title of the staff member',
       'id'    => 'krk_staff_title',
       'type'  => 'text'
   )
);

function register_krk_staff_meta_box(){
   add_meta_box(
       'krk_staff_options', // $id
       'Staff Options', // $title
       'show_krk_staff_meta_box', // $callback
       'krk_staff', // $page
       'normal', // $context
       'high'); // $priority
}

function show_krk_staff_meta_box(){
   global $krk_staff_meta_fields, $post;
   // Use nonce for verification
   echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';

   echo '<table class="form-table">';
   foreach ($krk_staff_meta_fields as $field) {
       $meta = get_post_meta($post->ID, $field['id'], true);
       echo '<tr>
               <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
               <td>';
       switch($field['type']) {
           case 'text':
               echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
                   <br /><span class="description">'.$field['desc'].'</span>';
               break;
       }
       echo '</td></tr>';
   }
   echo '</table>';
}

function save_krk_staff_meta($post_id) {
   global $krk_staff_meta_fields;
   // verify nonce
   if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
       return $post_id;
   // check autosave
   if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
       return $post_id;
   // check permissions
   if ('page' == $_POST['post_type']) {
       if (!current_user_can('edit_page', $post_id))
           return $post_id;
   } elseif (!current_user_can('edit_post', $post_id)) {
       return $post_id;
   }
   foreach ($krk_staff_meta_fields as $field) {
       $old = get_post_meta($post_id, $field['id'], true);
       $new = $_POST[$field['id']];

       if ($new && $new != $old) {
           update_post_meta($post_id, $field['id'], $new);
       } elseif ('' == $new && $old) {
           delete_post_meta($post_id, $field['id'], $old);
       }
   }
}
add_action('add_meta_boxes', 'register_krk_staff_meta_box');
add_action('save_post', 'save_krk_staff_meta');

?>