<?php

require_once(get_template_directory() . '/functions/customizers/defaults/infants-defaults.php');

add_action( 'customize_register', 'krk_infants_customizer' ); 
function krk_infants_customizer( $wp_customize ) {

	/**
	* Sections
	*/
	$wp_customize->add_section(
        'krk_infants_page_content',
        array(
            'title' => 'Infants Page Content',
            'priority' => 35,
            'active_callback' => function(){ return is_page("Infants"); }
        )
    );
   
	/**
	* Settings
	*/
    $wp_customize->add_setting( 'krk_infants_video_upload' );
    $wp_customize->add_setting( 'krk_infants_video_thumbnail', array(
        'default' => krk_infants_customizer_defaults('krk_infants_video_thumbnail')
      ));
    $wp_customize->add_setting( 'krk_infants_header_image');
    $wp_customize->add_setting( 'krk_infants_header_title', array(
        'default' => krk_infants_customizer_defaults('krk_infants_header_title')
      ));
    $wp_customize->add_setting( 'krk_infants_header_content', array(
        'default' => krk_infants_customizer_defaults('krk_infants_header_content')
      ));
    $wp_customize->add_setting( 'krk_infants_left_content', array(
        'default' => krk_infants_customizer_defaults('krk_infants_left_content')
      ));
    $wp_customize->add_setting( 'krk_infants_right_content', array(
        'default' => krk_infants_customizer_defaults('krk_infants_right_content')
      ));
	/**
	* Controls
	*/
    $wp_customize->add_control( 
    	new WP_Customize_Media_Control( $wp_customize, 'krk_infants_video_upload', 
    		array(
    			'label' => __( 'Infants Page Video' ),
    			'section' => 'krk_infants_page_content',
    			'settings' => 'krk_infants_video_upload',
    			'mime_type' => 'video',
			) 
		  ) 
	  );
	  $wp_customize->add_control( 
    	new WP_Customize_Control( $wp_customize, 'krk_infants_video_thumbnail',
    		array(
    			'label' => __( 'Video Thumbnail' ),
    			'section' => 'krk_infants_page_content',
    			'settings' => 'krk_infants_video_thumbnail',
				'type'           => 'select',
				'choices'        => array(
					'infant1.jpg' => __('Image 1'),
					'infant2.jpg' => __('Image 2'),
					'infant3.jpg' => __('Image 3'),
					'infant4.jpg' => __('Image 4'),
				)
			) 
	  	) 
	  );
	  $wp_customize->add_control( 
    	new WP_Customize_Image_Control( $wp_customize, 'krk_infants_header_image', 
    		array(
    			'label' => __( 'Header Image' ),
    			'section' => 'krk_infants_page_content',
    			'settings' => 'krk_infants_header_image',
			) 
	  	) 
	  );
	  $wp_customize->add_control( 
      'krk_infants_header_title', 
		  array(
  			'label' => __( 'Header Title' ),
  			'type' => 'text',
  			'section' => 'krk_infants_page_content',
  			'settings' => 'krk_infants_header_title',
		  ) 
	  );
	  $wp_customize->add_control( 
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_infants_header_content', 
			array(
  				'label' => __( 'Header Content' ),
  				'section' => 'krk_infants_page_content',
  				'settings' => 'krk_infants_header_content',
			)
		) 
	  );
	  $wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_infants_left_content', 
			array(
  				'label' => __( 'Left Content' ),
  				'section' => 'krk_infants_page_content',
  				'settings' => 'krk_infants_left_content',
			) 
		)
	  );
	  $wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_infants_right_content',
			array(
  				'label' => __( 'Right Content' ),
  				'section' => 'krk_infants_page_content',
  				'settings' => 'krk_infants_right_content',
			) 
		)
	  );
}
?>