<?php
class KRK_Preschool_Customizer extends KRK_Customizer
{

	public $page_name = 'preschool';

	public function __construct()
	{
		add_action('customize_register', array($this, 'register_customizer'));
	}

	function register_customizer($wp_customize)
	{
		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_preschool_page_content',
			array(
				'title' => 'Pre-School Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_page($this->page_name); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_preschool_video_upload' );
		$wp_customize->add_setting( 'krk_preschool_video_thumbnail', array(
			'default' => $this->defaults('krk_preschool_video_thumbnail')
		));
		$wp_customize->add_setting( 'krk_preschool_header_image', array(
			'default' => $this->defaults('krk_preschool_header_image')
		));
		$wp_customize->add_setting( 'krk_preschool_header_title', array(
			'default' => $this->defaults('krk_preschool_header_title')
		));
		$wp_customize->add_setting( 'krk_preschool_header_content', array(
			'default' => $this->defaults('krk_preschool_header_content')
		));
		$wp_customize->add_setting( 'krk_preschool_left_content', array(
			'default' => $this->defaults('krk_preschool_left_content')
		));
		$wp_customize->add_setting( 'krk_preschool_right_content', array(
			'default' => $this->defaults('krk_preschool_right_content')
		));
		/**
		 * Controls
		 */
		$wp_customize->add_control(
			new WP_Customize_Media_Control( $wp_customize, 'krk_preschool_video_upload',
				array(
					'label' => __( 'Pre-School Page Video' ),
					'section' => 'krk_preschool_page_content',
					'settings' => 'krk_preschool_video_upload',
					'mime_type' => 'video',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control( $wp_customize, 'krk_preschool_video_thumbnail',
				array(
					'label' => __( 'Video Thumbnail' ),
					'section' => 'krk_preschool_page_content',
					'settings' => 'krk_preschool_video_thumbnail',
					'type'           => 'select',
					'choices'        => array(
						'preschool1.jpg' => __('Image 1'),
						'preschool2.jpg' => __('Image 2'),
						'preschool3.jpg' => __('Image 3'),
						'preschool4.jpg' => __('Image 4'),
					)
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Image_Control( $wp_customize, 'krk_preschool_header_image',
				array(
					'label' => __( 'Header Image' ),
					'section' => 'krk_preschool_page_content',
					'settings' => 'krk_preschool_header_image',
				)
			)
		);
		$wp_customize->add_control( 'krk_preschool_header_title',
			array(
				'label' => __( 'Header Title' ),
				'type' => 'text',
				'section' => 'krk_preschool_page_content',
				'settings' => 'krk_preschool_header_title',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_preschool_header_content',
				array(
					'label' => __( 'Header Content' ),
					'section' => 'krk_preschool_page_content',
					'settings' => 'krk_preschool_header_content',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_preschool_left_content',
				array(
					'label' => __( 'Left Content' ),
					'section' => 'krk_preschool_page_content',
					'settings' => 'krk_preschool_left_content',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_preschool_right_content',
				array(
					'label' => __( 'Right Content' ),
					'section' => 'krk_preschool_page_content',
					'settings' => 'krk_preschool_right_content',
				)
			)
		);
	}

	protected function defaults($setting)
	{
		if($setting == 'krk_preschool_header_title') {
			return 'Our Eager Learners!';
		}
		elseif($setting == 'krk_preschool_video_thumbnail'){
			return 'preschool1.jpg';
		}
		elseif ($setting == 'krk_preschool_header_content') {
			return '<b>Our Advanced Accredited Program</b> ensures that every child receives an accelerated foundation for elementary school.';
		}
		elseif($setting == 'krk_preschool_left_content') {
			$activity_book_src = get_stylesheet_directory_uri() . '/images/activity_book3.pdf';
			return <<<EOT
    <div class="section-xtx">
      <h2>Our Preschool Program</h2>
      <p>Our activities are standards-driven and address the whole child by promoting:  </p>
      <ul>
        <li> Developmentally-appropriate practices </li>
        <li> Hands-on involvement </li>
        <li> Decision-making and problem solving skills </li>
        <li>Character development </li>
        <li> Vocabulary </li>
        <li> Social-emotional activities </li>
        <li> Literacy skills </li>
        <li> Small group instruction </li>
      </ul>
      <p>Life is an adventure when you are a preschooler. Our Learning Academy provides plenty of choice for your preschooler to explore and grow. The sounds of children laughing and playing are the sounds of children learning at Kids 'R' Kids Learning Academies</p>
    </div>
    <div class="section-xtx">
      <h2>Our Brain Waves Activity Book</h2>
      <p>It's crucial to connect a child's neural pathways as quickly as possible during the first 5 years. This activity book will enlighten you and your child of how the brain works and how Brain Waves&trade; can prepare your child for future educational success!</p>
      <p> Click <a href="{$activity_book_src}"> HERE </a> to download our Brain Waves Activity Book!</p>
    </div>          
EOT;
		}
		elseif($setting == 'krk_preschool_right_content') {
			return <<<EOT
    <div class="section-xtx">
      <h2>Our Exclusive Kids 'R' Kids Preschool Fast Track Curriculum&#8482</h2>
      
      <p></p>
      <p>Our preschoolers, ages 3 through 5, are on an accelerated track to acquire literacy and cognitive skills through a standards-driven, play-based curriculum. They are immersed in an abundance of hands-on, multi-sensory activities designed to advance their learning. Preschoolers thrive in well-organized classrooms with schedules that allow for a balance of active and quiet activities. All learning domains are covered in the <b>Fast Track Curriculum&#8482</b> through large and small group instruction. Students further practice their skills through independent play. Get ready for the abundant adventures that await your preschooler! </p>
    </div>
EOT;
		}
		else{
			return "";
		}
	}
}

new KRK_Preschool_Customizer();
?>