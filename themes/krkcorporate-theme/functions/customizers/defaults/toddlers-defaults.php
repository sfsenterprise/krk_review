<?php

function krk_toddlers_customizer_defaults($setting) {
  if($setting == 'krk_toddlers_header_title') {
    return 'Our Busy Learners!';
  }
  elseif($setting == 'krk_toddlers_video_thumbnail'){
    return 'toddler1.jpg';
  }
  elseif ($setting == 'krk_toddlers_header_content') {
    return '<b>Experimental learning</b> is fundamental during the toddler years and is the foundation for future development.';
  }
  elseif($setting == 'krk_toddlers_left_content') {
    $activity_book_src = get_stylesheet_directory_uri() . '/images/Infant_Toddler_Activity_Book.pdf';
    return <<<EOT
              <h2>Our Toddler Program</h2>
              <ul>
                <li> Sign language program </li>
                <li> Pre-literacy skills </li>
                <li> Toddler curriculum activities</li>
                <li> Small group activities </li>
                <li> Developmentally-appropriate practices </li>
                <li> Indoor and outdoor physical activities </li>
                <li> Sensory play </li>
              </ul>
              <p> Each classroom centers its program on three important components: Loving Relationships, Stimulating Environments, and Developmentally-Appropriate Practices.</p>
              <p> We know that hands-on learning is fundamental during the toddler years and is the foundation for future development. </p>
              <p> Click <a href="{$activity_book_src}"> HERE </a> to download our Brain Waves Activity Book!</p>
EOT;
  }
  elseif($setting == 'krk_toddlers_right_content') {
    return <<<EOT
            <h2>Big Steps Toddler Curriculum</h2>
            <p>The Big Steps Toddler Curriculum&trade; continues to build on the big steps taken during infancy. Toddlers experience an environment that invites interaction with play materials and organized play spaces. </p>
            <p>The children practice literacy through sign language, books, puppet play, music, singing, games, and much conversation with peers and adults. Their cognitive skills blossom with a multitude of activities designed to unleash their already inquisitive and creative nature. </p>
EOT;
  }
}
?>