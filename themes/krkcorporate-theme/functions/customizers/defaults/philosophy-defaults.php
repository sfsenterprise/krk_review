<?php

function krk_philosophy_customizer_defaults($setting) {
  if($setting == 'krk_philosophy_header_title') {
    return 'Our Philosophy';
  }
  elseif($setting == 'krk_philosophy_content') {    
    return <<<EOT
      <div class="section-xtx"> 
        <p>We believe that happy, loved, and connected children are destined for success in every facet of their lives.</p>
      </div>
      <div class="section-xtx">
        <h2>“Hug First, Then Teach”</h2>
        <p>Our most cherished principle, “Hug First, Then Teach”, defines every aspect of who we are at Kids ‘R’ Kids Learning Academies. Our whole child approach works to strengthen and encourage every child’s emotional, intellectual, social, and physical need through the expert care of our child care providers and unique partnership with parents.</p>
      </div>
      <div class="section-xtx">
        <h2>Involve Families</h2>
        <p>When it comes to teaching, Kids ‘R’ Kids Learning Academies understand the importance of involving families with the developmental milestones and accomplishments of their children. New skills are reinforced through daily communication with parents, keeping a close connection between home and school.</p>
      </div>
      <div class="section-xtx">
        <h2>Dedicated Teachers</h2>
        <p>Our teachers know that children are naturally curious, so why not encourage this curiosity in a secure and educational environment? Our highly trained teachers use an individualized approach in each classroom, creating the foundation needed to develop higher-order thinking skills.</p>
        <p> Kids ‘R’ Kids Learning Academies is committed to providing a safe, fun, and educational environment for your child where dedicated experts love and teach to the highest standards in childcare services, growth development and education. </p>

      </div>
      <div class="section-xtx">
        <h2>Mission/Vision Statement</h2>
        <p>Kids ‘R’ Kids Learning Academies provide a secure, nurturing, and educational environment for children. Our school is a place for children to bloom into responsible, considerate, and contributing members of society.</p>
        <p>Kids ‘R’ Kids Learning Academies wants all children to have the opportunity to grow physically, emotionally, socially, and intellectually by playing, exploring, and learning with others in a fun, safe, and healthy environment.</p>
        <p>As a family-owned and operated organization, Kids ‘R’ Kids Learning Academies welcomes positive family involvement and encourages a parent-teacher approach where the needs of every child come first.</p>
        <br></br>
        <h2 align="center"> WE HOLD THE FUTURE&#8482</h2>
      </div>
EOT;
  }
}
?>