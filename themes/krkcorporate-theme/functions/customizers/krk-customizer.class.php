<?php
    abstract class KRK_Customizer {

        abstract protected function register_customizer($wp_customize);
        abstract protected function defaults($settings);

        public function get_setting($setting) {
            $val = get_theme_mod($setting, '');
            if(strlen(strip_tags($val)) <= 0) {
                $val = $this->defaults($setting);
            }
            return $val;
        }
    }
?>