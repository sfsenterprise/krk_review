<?php

if( class_exists( 'WP_Customize_Control' ) ):
	class WP_Customize_Sortable_Control extends WP_Customize_Control {
		public $type = 'sortable';
 
		public function render_content() {
        ?>

        <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>

        <ul class="sortable-list">
            <input type="hidden" <?php $this->link(); ?> value="<?php echo $this->value() ?>">
        </ul>
    	<?php
    	}
	}
endif;

?>