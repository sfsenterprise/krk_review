<?php
class KRK_Toddlers_Customizer extends KRK_Customizer
{

	public $page_name = 'toddlers';

	public function __construct()
	{
		add_action('customize_register', array($this, 'register_customizer'));
	}

	function register_customizer($wp_customize)
	{
		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_toddlers_page_content',
			array(
				'title' => 'Toddlers Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_page($this->page_name); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_toddlers_video_upload' );
		$wp_customize->add_setting( 'krk_toddlers_video_thumbnail', array(
			'default' => $this->defaults('krk_toddlers_video_thumbnail')
		));
		$wp_customize->add_setting( 'krk_toddlers_header_image', array(
			'default' => $this->defaults('krk_toddlers_header_image')
		));
		$wp_customize->add_setting( 'krk_toddlers_header_title', array(
			'default' => $this->defaults('krk_toddlers_header_title')
		));
		$wp_customize->add_setting( 'krk_toddlers_header_content', array(
			'default' => $this->defaults('krk_toddlers_header_content')
		));
		$wp_customize->add_setting( 'krk_toddlers_left_content', array(
			'default' => $this->defaults('krk_toddlers_left_content')
		));
		$wp_customize->add_setting( 'krk_toddlers_right_content', array(
			'default' => $this->defaults('krk_toddlers_right_content')
		));
		/**
		 * Controls
		 */
		$wp_customize->add_control(
			new WP_Customize_Media_Control( $wp_customize, 'krk_toddlers_video_upload',
				array(
					'label' => __( 'Toddlers Page Video' ),
					'section' => 'krk_toddlers_page_content',
					'settings' => 'krk_toddlers_video_upload',
					'mime_type' => 'video',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control( $wp_customize, 'krk_toddlers_video_thumbnail',
				array(
					'label' => __( 'Video Thumbnail' ),
					'section' => 'krk_toddlers_page_content',
					'settings' => 'krk_toddlers_video_thumbnail',
					'type'           => 'select',
					'choices'        => array(
						'toddler1.jpg' => __('Image 1'),
						'toddler2.jpg' => __('Image 2'),
						'toddler3.jpg' => __('Image 3'),
						'toddler4.jpg' => __('Image 4'),
						'toddler5.jpg' => __('Image 5'),
						'toddler6.jpg' => __('Image 6'),
						'toddler7.jpg' => __('Image 7'),
					)
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Image_Control( $wp_customize, 'krk_toddlers_header_image',
				array(
					'label' => __( 'Header Image' ),
					'section' => 'krk_toddlers_page_content',
					'settings' => 'krk_toddlers_header_image',
				)
			)
		);
		$wp_customize->add_control( 'krk_toddlers_header_title',
			array(
				'label' => __( 'Header Title' ),
				'type' => 'text',
				'section' => 'krk_toddlers_page_content',
				'settings' => 'krk_toddlers_header_title',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_toddlers_header_content',
				array(
					'label' => __( 'Header Content' ),
					'section' => 'krk_toddlers_page_content',
					'settings' => 'krk_toddlers_header_content',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_toddlers_left_content',
				array(
					'label' => __( 'Left Content' ),
					'section' => 'krk_toddlers_page_content',
					'settings' => 'krk_toddlers_left_content',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_toddlers_right_content',
				array(
					'label' => __( 'Right Content' ),
					'section' => 'krk_toddlers_page_content',
					'settings' => 'krk_toddlers_right_content',
				)
			)
		);
	}

	protected function defaults($setting)
	{
		if($setting == 'krk_toddlers_header_title') {
			return 'Our Busy Learners!';
		}
		elseif($setting == 'krk_toddlers_video_thumbnail'){
			return 'toddler1.jpg';
		}
		elseif ($setting == 'krk_toddlers_header_content') {
			return '<b>Experimental learning</b> is fundamental during the toddler years and is the foundation for future development.';
		}
		elseif($setting == 'krk_toddlers_left_content') {
			$activity_book_src = get_stylesheet_directory_uri() . '/images/Infant_Toddler_Activity_Book.pdf';
			return <<<EOT
              <h2>Our Toddler Program</h2>
              <ul>
                <li> Sign language program </li>
                <li> Pre-literacy skills </li>
                <li> Toddler curriculum activities</li>
                <li> Small group activities </li>
                <li> Developmentally-appropriate practices </li>
                <li> Indoor and outdoor physical activities </li>
                <li> Sensory play </li>
              </ul>
              <p> Each classroom centers its program on three important components: Loving Relationships, Stimulating Environments, and Developmentally-Appropriate Practices.</p>
              <p> We know that hands-on learning is fundamental during the toddler years and is the foundation for future development. </p>
              <p> Click <a href="{$activity_book_src}"> HERE </a> to download our Brain Waves Activity Book!</p>
EOT;
		}
		elseif($setting == 'krk_toddlers_right_content') {
			return <<<EOT
            <h2>Big Steps Toddler Curriculum</h2>
            <p>The Big Steps Toddler Curriculum&trade; continues to build on the big steps taken during infancy. Toddlers experience an environment that invites interaction with play materials and organized play spaces. </p>
            <p>The children practice literacy through sign language, books, puppet play, music, singing, games, and much conversation with peers and adults. Their cognitive skills blossom with a multitude of activities designed to unleash their already inquisitive and creative nature. </p>
EOT;
		}
		else{
			return "";
		}
	}
}

new KRK_Toddlers_Customizer();
?>