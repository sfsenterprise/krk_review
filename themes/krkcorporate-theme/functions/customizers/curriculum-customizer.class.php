<?php
class KRK_Curriculum_Customizer extends KRK_Customizer {

	public $page_name = 'curriculum';

	public function __construct() {
		add_action( 'customize_register', array($this, 'register_customizer'));
	}

	function register_customizer( $wp_customize ) {
		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_curriculum_page_content',
			array(
				'title' => 'Curriculum Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_page($this->page_name); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_curriculum_section2_header_image');
		$wp_customize->add_setting( 'krk_curriculum_section3_header_image');

		/**
		 * Controls
		 */
		$wp_customize->add_control(
			new WP_Customize_Control( $wp_customize, 'krk_curriculum_section2_header_image',
				array(
					'label' => __( 'Interactive Technology Header Image' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_section2_header_image',
					'type'           => 'select',
					'choices'        => array(
						'Technology1.jpg' => __('Image 1'),
						'Technology2.jpg' => __('Image 2'),
					)
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control( $wp_customize, 'krk_curriculum_section3_header_image',
				array(
					'label' => __( 'STEAM AHEAD Header Image' ),
					'section' => 'krk_curriculum_page_content',
					'settings' => 'krk_curriculum_section3_header_image',
					'type'           => 'select',
					'choices'        => array(
						'STEAM1.jpg' => __('Image 1'),
						'STEAM2.jpg' => __('Image 2'),
					)
				)
			)
		);
	}

	protected function defaults($setting)
	{
		if($setting == 'krk_curriculum_section2_header_image') {
			return 'Technology1.jpg';
		}
		else if($setting == 'krk_curriculum_section3_header_image') {
			return 'STEAM1.jpg';
		}
	}
}

new KRK_Curriculum_Customizer();
?>