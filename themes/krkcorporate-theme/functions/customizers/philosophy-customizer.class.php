<?php
class KRK_Philosophy_Customizer extends KRK_Customizer
{

	public $page_name = 'philosophy';

	public function __construct()
	{
		add_action('customize_register', array($this, 'register_customizer'));
	}

	function register_customizer($wp_customize)
	{
		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_philosophy_page_content',
			array(
				'title' => 'Philosophy Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_page($this->page_name); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_philosophy_header_image');
		$wp_customize->add_setting( 'krk_philosophy_header_title', array(
			'default' => $this->defaults('krk_philosophy_header_title')
		));
		$wp_customize->add_setting( 'krk_philosophy_content', array(
			'default' => $this->defaults('krk_philosophy_content')
		));

		/**
		 * Controls
		 */
		$wp_customize->add_control(
			new WP_Customize_Image_Control( $wp_customize, 'krk_philosophy_header_image',
				array(
					'label' => __( 'Header Image' ),
					'section' => 'krk_philosophy_page_content',
					'settings' => 'krk_philosophy_header_image',
				)
			)
		);
		$wp_customize->add_control( 'krk_philosophy_header_title',
			array(
				'label' => __( 'Header Title' ),
				'type' => 'text',
				'section' => 'krk_philosophy_page_content',
				'settings' => 'krk_philosophy_header_title',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_philosophy_content',
				array(
					'label' => __( 'Page Content' ),
					'section' => 'krk_philosophy_page_content',
					'settings' => 'krk_philosophy_content',
				)
			)
		);
	}

	protected function defaults($setting)
	{
		if($setting == 'krk_philosophy_header_title') {
			return 'Our Philosophy';
		}
		elseif($setting == 'krk_philosophy_header_image') {
			return get_stylesheet_directory_uri() . '/images/OurPhilosophy_1.jpg';
		}
		elseif($setting == 'krk_philosophy_content') {
			return <<<EOT
		<div class="section-xtx">
			<p>We believe that happy, loved, and connected children are destined for success in every facet of their lives.</p>
		</div>
		<div class="section-xtx">
			<h2>“Hug First, Then Teach”</h2>
			<p>Our most cherished principle, “Hug First, Then Teach”, defines every aspect of who we are at Kids ‘R’ Kids Learning Academies. Our whole child approach works to strengthen and encourage every child’s emotional, intellectual, social, and physical need through the expert care of our child care providers and unique partnership with parents.</p>
		</div>
		<div class="section-xtx">
			<h2>Involve Families</h2>
			<p>When it comes to teaching, Kids ‘R’ Kids Learning Academies understand the importance of involving families with the developmental milestones and accomplishments of their children. New skills are reinforced through daily communication with parents, keeping a close connection between home and school.</p>
		</div>
		<div class="section-xtx">
			<h2>Dedicated Teachers</h2>
			<p>Our teachers know that children are naturally curious, so why not encourage this curiosity in a secure and educational environment? Our highly trained teachers use an individualized approach in each classroom, creating the foundation needed to develop higher-order thinking skills.</p>
			<p> Kids ‘R’ Kids Learning Academies is committed to providing a safe, fun, and educational environment for your child where dedicated experts love and teach to the highest standards in childcare services, growth development and education. </p>
		</div>
		<div class="section-xtx">
			<h2>Mission/Vision Statement</h2>
			<p>Kids ‘R’ Kids Learning Academies provide a secure, nurturing, and educational environment for children. Our school is a place for children to bloom into responsible, considerate, and contributing members of society.</p>
			<p>Kids ‘R’ Kids Learning Academies wants all children to have the opportunity to grow physically, emotionally, socially, and intellectually by playing, exploring, and learning with others in a fun, safe, and healthy environment.</p>
			<p>As a family-owned and operated organization, Kids ‘R’ Kids Learning Academies welcomes positive family involvement and encourages a parent-teacher approach where the needs of every child come first.</p>
			<h1 align="center"> WE HOLD THE FUTURE&#8482</h1>
			<br>
			<h2> History of Kids 'R' Kids Learning Academies </h2>
			<p> Pat and Janice Vinson opened the first Kids ‘R’ Kids Learning Academy in 1985. They have an incredible history in preschool development and management, which began with their first preschool in 1961. Their unique vision-breaking the boundaries of traditional childcare-has propelled the Kids ‘R’ Kids name to its current community status. This is a true testament to their experience and commitment.</p>
			<p> The Vinson’s are dedicated to remaining on the leading edge of early childhood development. They will continue to provide the absolute best in childcare and early childhood education. </p>
			<h2> The Corporation Today </h2>
			<p> The Kids ‘R’ Kids Franchise Support Center provides support to Learning Academies located in Colorado, Florida, Georgia, Illinois, Kansas, Kentucky, Mississippi, Missouri, Nevada, New Jersey, North Carolina, Ohio, South Carolina, Tennessee, Texas, Virginia and now globally. </p>
			<p> The Kids ‘R’ Kids Franchise Support Center is located in Duluth, Georgia. A full staff of dedicated professionals continually provides support to all Kids ‘R’ Kids Learning Academies. </p>
		</div>
EOT;
		}
		else{
			return "";
		}
	}
}

new KRK_Philosophy_Customizer();
?>