<?php

require_once(get_template_directory() . '/functions/customizers/defaults/philosophy-defaults.php');

add_action( 'customize_register', 'krk_philosophy_customizer' );
function krk_philosophy_customizer( $wp_customize ) {

	/**
	 * Sections
	 */
	$wp_customize->add_section(
		'krk_philosophy_page_content',
		array(
			'title' => 'Philosophy Page Content',
			'priority' => 35,
			'active_callback' => function(){ return is_page("our-philosophy"); }
		)
	);

	/**
	 * Settings
	 */
	$wp_customize->add_setting( 'krk_philosophy_header_image');
	$wp_customize->add_setting( 'krk_philosophy_header_title', array(
        'default' => krk_philosophy_customizer_defaults('krk_philosophy_header_title')
      ));
	$wp_customize->add_setting( 'krk_philosophy_content', array(
        'default' => krk_philosophy_customizer_defaults('krk_philosophy_content')
      ));

	/**
	 * Controls
	 */
	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'krk_philosophy_header_image',
			array(
				'label' => __( 'Header Image' ),
				'section' => 'krk_philosophy_page_content',
				'settings' => 'krk_philosophy_header_image',
			)
		)
	);
	$wp_customize->add_control( 'krk_philosophy_header_title',
		array(
			'label' => __( 'Header Title' ),
			'type' => 'text',
			'section' => 'krk_philosophy_page_content',
			'settings' => 'krk_philosophy_header_title',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_philosophy_content',
			array(
				'label' => __( 'Page Content' ),
				'section' => 'krk_philosophy_page_content',
				'settings' => 'krk_philosophy_content',
			)
		)
	);
}
?>