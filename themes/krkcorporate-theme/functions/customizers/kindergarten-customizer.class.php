<?php
class KRK_Kindergarten_Customizer extends KRK_Customizer {

	public $page_name = 'kindergarten';

	public function __construct() {
		add_action( 'customize_register', array($this, 'register_customizer'));
	}

	function register_customizer($wp_customize) {
		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_kindergarten_page_content',
			array(
				'title' => 'Kindergarten Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_page($this->page_name); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_kindergarten_video_upload' );
		$wp_customize->add_setting( 'krk_kindergarten_video_thumbnail', array(
			'default' => $this->defaults('krk_kindergarten_video_thumbnail')
		));
		$wp_customize->add_setting( 'krk_kindergarten_header_image', array(
			'default' => $this->defaults('krk_kindergarten_header_image')
		));
		$wp_customize->add_setting( 'krk_kindergarten_header_title', array(
			'default' => $this->defaults('krk_kindergarten_header_title')
		));
		$wp_customize->add_setting( 'krk_kindergarten_header_content', array(
			'default' => $this->defaults('krk_kindergarten_header_content')
		));
		$wp_customize->add_setting( 'krk_kindergarten_left_content', array(
			'default' => $this->defaults('krk_kindergarten_left_content')
		));
		$wp_customize->add_setting( 'krk_kindergarten_right_content', array(
			'default' => $this->defaults('krk_kindergarten_right_content')
		));
		/**
		 * Controls
		 */
		$wp_customize->add_control(
			new WP_Customize_Media_Control( $wp_customize, 'krk_kindergarten_video_upload',
				array(
					'label' => __( 'Kindergarten Page Video' ),
					'section' => 'krk_kindergarten_page_content',
					'settings' => 'krk_kindergarten_video_upload',
					'mime_type' => 'video',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Image_Control( $wp_customize, 'krk_kindergarten_video_thumbnail',
				array(
					'label' => __( 'Video Thumbnail' ),
					'section' => 'krk_kindergarten_page_content',
					'settings' => 'krk_kindergarten_video_thumbnail',
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Image_Control( $wp_customize, 'krk_kindergarten_header_image',
				array(
					'label' => __( 'Header Image' ),
					'section' => 'krk_kindergarten_page_content',
					'settings' => 'krk_kindergarten_header_image',
				)
			)
		);
		$wp_customize->add_control( 'krk_kindergarten_header_title',
			array(
				'label' => __( 'Header Title' ),
				'type' => 'text',
				'section' => 'krk_kindergarten_page_content',
				'settings' => 'krk_kindergarten_header_title',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_kindergarten_header_content',
				array(
					'label' => __( 'Header Content' ),
					'section' => 'krk_kindergarten_page_content',
					'settings' => 'krk_kindergarten_header_content',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_kindergarten_left_content',
				array(
					'label' => __( 'Left Content' ),
					'section' => 'krk_kindergarten_page_content',
					'settings' => 'krk_kindergarten_left_content',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_kindergarten_right_content',
				array(
					'label' => __( 'Right Content' ),
					'section' => 'krk_kindergarten_page_content',
					'settings' => 'krk_kindergarten_right_content',
				)
			)
		);
	}

	protected function defaults($setting)
	{
		if($setting == 'krk_kindergarten_header_title') {
			return 'Our Active Learners!';
		}
		elseif($setting == 'krk_kindergarten_video_thumbnail'){
			return get_stylesheet_directory_uri() . '/images/preschool1.jpg';
		}
		elseif ($setting == 'krk_kindergarten_header_content') {
			return '<b>Preparing for Elementary School</b>';
		}
		elseif($setting == 'krk_kindergarten_left_content') {
			$location_state = do_shortcode("[location_state]");
			$kindergarten_program_name = do_shortcode("[kindergarten_program_name]");
			return <<<EOT
              <div class="section-xtx">
                <h2>Our {$kindergarten_program_name} Program</h2>
                <p>The {$kindergarten_program_name} programs are both state-approved and privately-funded programs that promote:</p>
                <ul>
                  <li> Decision-making and problem solving skills </li>
                  <li> Character development</li>
                  <li> Increased vocabulary </li>
                  <li> Social-emotional development </li>
                  <li> Literacy Skills </li>
                  <li> Small group instruction </li>
                </ul>
                    
                <p>The Kids ‘R’ Kids <b>First Class Curriculum&#8482</b> proudly offers activities that are academically challenging while still fun! The students learn through theme-based modules which are designed to be entertaining as well as educational. Each child receives an unparalleled level of individual attention to ensure academic success. The program is tailored to increase knowledge along with a child’s self-confidence. Kids ‘R’ Kids Learning Academies offer the best foundation in preparing your child for their elementary school years.</p>
              </div>
              <div class="section-xtx">
                <h2>{$kindergarten_program_name} <b> Fast Track Curriculum&#8482</b></h2>
                <p>In preparation for Elementary school, our {$kindergarten_program_name} children work with original themed modules, designed to be appealing and educational. Our standards-driven curriculum as been approved by the state of {$location_state} so transitioning to Kindergarten is a breeze. A great percentage of our Pre-K graduates test high above their peers within the first year of Kindergarten.</p>
              </div>
EOT;
		}
		elseif($setting == 'krk_kindergarten_right_content') {
			return <<<EOT
      <div class="section-xtx">
        <h2> The Teacher's Role as Facilitator </h2>
        <p>Teachers create high quality learning environments that provide students with opportunities to observe, question, investigate, predict, experiment, build, and share what they learn.</p>
        <p>Students and teachers explore STEAM AHEAD&reg; projects together. To tap into each student’s natural interests while facilitating learning, project materials are placed in learning stations for independent and small group exploration.</p>
        <p> Teachers serve as guides while students participate in STEAM AHEAD&reg; projects, making observations of each student’s growth, development, and interests along the way. </p>
      </div>
EOT;
		}
		else{
			return "";
		}
	}
}

new KRK_Kindergarten_Customizer();
?>