<?php
class KRK_Tour_Customizer extends KRK_Customizer {

	public $page_name = 'tour';

	public function __construct() {
		add_action( 'customize_register', array($this, 'register_customizer'));
	}

	function register_customizer( $wp_customize ) {

		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_tour_page_content',
			array(
				'title' => 'Tour Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_page($this->page_name); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_tour_content', array(
			'default' => $this->defaults('krk_tour_content')
		));
		/**
		 * Controls
		 */
		$wp_customize->add_control(
			new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_tour_content',
				array(
					'label' => __( 'Section Content' ),
					'section' => 'krk_tour_page_content',
					'settings' => 'krk_tour_content',
				)
			)
		);
	}

	protected function defaults($setting) {
		if($setting == 'krk_tour_content') {
			return <<<EOT
			<div class="row-text">
				<h4>We welcome you!</h4>
				<p>Walk through our facility by taking a virtual tour or clicking images in the gallery below. Or <a href="<?php echo multisite_path('locations') ?>">plan your visit</a> to a center near you!</p>
			</div>
			<h4>Facilities Overview</h4>
			<ul class="list">
				<li>Our Academies are equipped with multiple cameras in every classroom, cafeteria and outdoor play areas. Our interior is designed with safety glass walls and doors to ensure each child is supervised by staff at all times. </li>
				<li>With over 17,000 square feet, we design a one-of-a-kind building that features age appropriate classrooms equipped with the right tools to properly educate your child along with large fenced-in playgrounds matching each developmental age range.</li>
				<li>School staff members and volunteers greet each child under our Kiss 'N Go Lane stationed right at the front door.</li>
				<li>Our cafeteria is a unique room designed for each classroom to join one another for lunch and snacks. We believe it's important to teach manners and help create conversations meant for dining. </li>
				<li>The equipment used in our buildings is specifically made for children. It's durable and safe and has been researched among early childhood educators as what works best. Each room is arranged to meet the needs of child led and teacher led lessons. </li>
				<li>Our gym area, mainly used for our before and after school age children is also used for indoor play with our preschoolers during inclement weather. </li>
				<li>Outdoor time is always a treat and our play yards are packed full of fun. We have shade units with UV protection built above our play structures and most of our Academies are now installing Splash Pads for those hot summer days</li>
			</ul>
EOT;
		}
		else{
			return "";
		}
	}
}

new KRK_Tour_Customizer();
?>