<?php
class KRK_Home_Customizer extends KRK_Customizer {

	public function __construct() {
		add_action( 'customize_register', array($this, 'register_customizer'));
	}

	function register_customizer( $wp_customize ) {
		/**
		 * Sections
		 */
		$wp_customize->add_section(
			'krk_home_page_content',
			array(
				'title' => 'Home Page Content',
				'priority' => 35,
				'active_callback' => function(){ return is_home(); }
			)
		);

		/**
		 * Settings
		 */
		$wp_customize->add_setting( 'krk_home_main_image',
			array(
				'default' => $this->defaults('krk_home_main_image'),
			));

		/**
		 * Controls
		 */
		$wp_customize->add_control(
			new WP_Customize_Control( $wp_customize, 'krk_home_main_image',
				array(
					'label' => __( 'Jumbo Image' ),
					'section' => 'krk_home_page_content',
					'settings' => 'krk_home_main_image',
					'type'           => 'select',
					'choices'        => array(
						'Franchise_Main1.jpg' => __('Image 1'),
						'Franchise_Main2.jpg' => __('Image 2'),
						'Franchise_Main3.jpg'=> __('Image 3'),
					)
				)
			)
		);
	}

	protected function defaults($setting)
	{
		if ($setting = 'krk_home_main_image') {
			return 'Franchise_Main1.jpg';
		}
		else{
			return "";
		}
	}
}

new KRK_Home_Customizer();
?>