<?php

require_once(get_template_directory() . '/functions/customizers/defaults/location-defaults.php');

add_action( 'customize_register', 'krk_location_customizer' );
function krk_location_customizer( $wp_customize ) {

	/**
	 * Sections
	 */
	$wp_customize->add_section(
		'krk_location_page_content',
		array(
			'title' => 'Location Page Content',
			'priority' => 35,
			'active_callback' => function(){ return is_page("our-location"); }
		)
	);

	/**
	 * Settings
	 */
	$wp_customize->add_setting( 'krk_location_directions_header_image');
	$wp_customize->add_setting( 'krk_location_directions_header_title', array(
			'default' => krk_location_customizer_defaults('krk_location_directions_header_title')
		));
	$wp_customize->add_setting( 'krk_location_directions_content', array(
			'default' => krk_location_customizer_defaults('krk_location_directions_content')
		));
	$wp_customize->add_setting( 'krk_location_section2_content', array(
			'default' => krk_location_customizer_defaults('krk_location_section2_content')
		));

	/**
	 * Controls
	 */
	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'krk_location_directions_header_image',
			array(
				'label' => __( 'Directions Header Image' ),
				'section' => 'krk_location_page_content',
				'settings' => 'krk_location_directions_header_image',
			)
		)
	);
	$wp_customize->add_control( 'krk_location_directions_header_title',
		array(
			'label' => __( 'Directions Header Title' ),
			'type' => 'text',
			'section' => 'krk_location_page_content',
			'settings' => 'krk_location_directions_header_title',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_location_directions_content',
			array(
				'label' => __( 'Directions Content' ),
				'section' => 'krk_location_page_content',
				'settings' => 'krk_location_directions_content',
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_location_section2_content',
			array(
				'label' => __( 'Section 2 Content' ),
				'section' => 'krk_location_page_content',
				'settings' => 'krk_location_section2_content',
			)
		)
	);
}
?>