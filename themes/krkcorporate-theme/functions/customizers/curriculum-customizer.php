<?php

add_action( 'customize_register', 'krk_curriculum_customizer' ); 
function krk_curriculum_customizer( $wp_customize ) {

	/**
	* Sections
	*/
	$wp_customize->add_section(
        'krk_curriculum_page_content',
        array(
            'title' => 'Curriculum Page Content',
            'priority' => 35,
            'active_callback' => function(){ return is_page("curriculum"); }
        )
    );
   
	/**
	* Settings
	*/
	$wp_customize->add_setting( 'krk_curriculum_section2_header_image');
	$wp_customize->add_setting( 'krk_curriculum_section3_header_image');

	/**
	* Controls
	*/
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_curriculum_section2_header_image',
			array(
				'label' => __( 'Section 2 Header Image' ),
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_section2_header_image',
				'type'           => 'select',
				'choices'        => array(
					'Technology1.jpg' => __('Image 1'),
					'Technology2.jpg' => __('Image 2'),
				)
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'krk_curriculum_section3_header_image',
			array(
				'label' => __( 'Section 3 Header Image' ),
				'section' => 'krk_curriculum_page_content',
				'settings' => 'krk_curriculum_section3_header_image',
				'type'           => 'select',
				'choices'        => array(
					'STEAM1.jpg' => __('Image 1'),
					'STEAM2.jpg' => __('Image 2'),
				)
			)
		)
	);
}
?>