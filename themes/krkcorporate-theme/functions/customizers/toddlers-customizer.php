<?php

require_once(get_template_directory() . '/functions/customizers/defaults/toddlers-defaults.php');

add_action( 'customize_register', 'krk_toddlers_customizer' ); 
function krk_toddlers_customizer( $wp_customize ) {

	/**
	* Sections
	*/
	$wp_customize->add_section(
        'krk_toddlers_page_content',
        array(
            'title' => 'Toddlers Page Content',
            'priority' => 35,
            'active_callback' => function(){ return is_page("Toddlers"); }
        )
    );
   
	/**
	* Settings
	*/
    $wp_customize->add_setting( 'krk_toddlers_video_upload' );
    $wp_customize->add_setting( 'krk_toddlers_video_thumbnail', array(
        'default' => krk_toddlers_customizer_defaults('krk_toddlers_video_thumbnail')
      ));
    $wp_customize->add_setting( 'krk_toddlers_header_image', array(
        'default' => krk_toddlers_customizer_defaults('krk_toddlers_header_image')
      ));
    $wp_customize->add_setting( 'krk_toddlers_header_title', array(
        'default' => krk_toddlers_customizer_defaults('krk_toddlers_header_title')
      ));
    $wp_customize->add_setting( 'krk_toddlers_header_content', array(
        'default' => krk_toddlers_customizer_defaults('krk_toddlers_header_content')
      ));
    $wp_customize->add_setting( 'krk_toddlers_left_content', array(
        'default' => krk_toddlers_customizer_defaults('krk_toddlers_left_content')
      ));
    $wp_customize->add_setting( 'krk_toddlers_right_content', array(
        'default' => krk_toddlers_customizer_defaults('krk_toddlers_right_content')
      ));
	/**
	* Controls
	*/
    $wp_customize->add_control( 
    	new WP_Customize_Media_Control( $wp_customize, 'krk_toddlers_video_upload', 
    		array(
    			'label' => __( 'Toddlers Page Video' ),
    			'section' => 'krk_toddlers_page_content',
    			'settings' => 'krk_toddlers_video_upload',
    			'mime_type' => 'video',
			) 
		) 
	);
	$wp_customize->add_control( 
    	new WP_Customize_Control( $wp_customize, 'krk_toddlers_video_thumbnail',
    		array(
    			'label' => __( 'Video Thumbnail' ),
    			'section' => 'krk_toddlers_page_content',
    			'settings' => 'krk_toddlers_video_thumbnail',
				'type'           => 'select',
				'choices'        => array(
					'toddler1.jpg' => __('Image 1'),
					'toddler2.jpg' => __('Image 2'),
					'toddler3.jpg' => __('Image 3'),
					'toddler4.jpg' => __('Image 4'),
					'toddler5.jpg' => __('Image 5'),
					'toddler6.jpg' => __('Image 6'),
					'toddler7.jpg' => __('Image 7'),
				)
			) 
		) 
	);
	$wp_customize->add_control( 
    	new WP_Customize_Image_Control( $wp_customize, 'krk_toddlers_header_image', 
    		array(
    			'label' => __( 'Header Image' ),
    			'section' => 'krk_toddlers_page_content',
    			'settings' => 'krk_toddlers_header_image',
			) 
		) 
	);
	$wp_customize->add_control( 'krk_toddlers_header_title', 
		array(
  			'label' => __( 'Header Title' ),
  			'type' => 'text',
  			'section' => 'krk_toddlers_page_content',
  			'settings' => 'krk_toddlers_header_title',
		) 
	);
	$wp_customize->add_control( 
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_toddlers_header_content', 
			array(
  				'label' => __( 'Header Content' ),
  				'section' => 'krk_toddlers_page_content',
  				'settings' => 'krk_toddlers_header_content',
			)
		) 
	);

	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_toddlers_left_content', 
			array(
  				'label' => __( 'Left Content' ),
  				'section' => 'krk_toddlers_page_content',
  				'settings' => 'krk_toddlers_left_content',
			) 
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Rich_Textarea_Control($wp_customize, 'krk_toddlers_right_content',
			array(
  				'label' => __( 'Right Content' ),
  				'section' => 'krk_toddlers_page_content',
  				'settings' => 'krk_toddlers_right_content',
			) 
		)
	);
}
?>