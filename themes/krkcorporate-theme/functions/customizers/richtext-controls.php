<?php

if( class_exists( 'WP_Customize_Control' ) ):
	class WP_Customize_Rich_Textarea_Control extends WP_Customize_Control {
		public $type = 'textarea';
 
		public function render_content() {
		?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>

				<textarea class="krk_customize_rich_content" rows="5" style="width:100%;" <?php $this->link(); ?>>
					<?php echo esc_textarea( $this->value() );?>
				</textarea>
		<?php
		}
	}
endif;

?>