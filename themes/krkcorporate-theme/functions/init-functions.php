<?php

function krkcorporate_add_default_pages()
{    
	$default_pages = ['All-Locations', 'Our-Stories', 'Contact-Us','Content', 'Curriculum', 'Infants', 'Locations',
						 'Philosophy', 'Pre-Kindergarten', 'Preschool', 'School-Age', 'Staff', 'Thank-You', 'Toddlers', 'Tour'];
   foreach ($default_pages as $page) {
       if (get_page_by_title($page) == NULL) {
           $post = array(
               'comment_status' => 'closed',
               'ping_status' => 'closed',
               'post_date' => date('Y-m-d H:i:s'),
               'post_name' => $page,
               'post_status' => 'publish',
               'post_title' => $page,
               'post_type' => 'page',
           );
           //insert page and save the id
           $newvalue = wp_insert_post($post, false);
       }
   }
}
add_action('init', 'krkcorporate_add_default_pages');

function krkcorporate_hide_post_page(){
    remove_menu_page( 'edit.php' );
}
add_action( 'admin_menu', 'krkcorporate_hide_post_page' ); 

/**
 * Set the size attribute to 'full' in the next gallery shortcode.
 */
function get_gallery_full_size( $out )
{
    remove_filter( current_filter(), __FUNCTION__ );
    $out['size'] = 'full';
    return $out;
}

function krkcorporate_register_post_types()
{
	register_post_type( 'krk_event',
        array(
            'labels' => array(
                'name' => 'Events',
                'singular_name' => 'Event',
                'add_new_item' => 'Add Event',
                'edit_item' => 'Edit Event',
                'new_item' => 'New Event',
                'view_item' => 'View Event'
            ),
            'menu_icon' => 'dashicons-calendar-alt',
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'events'),
            'supports' => array( 'title', 'editor', 'thumbnail')
        )
    );

	register_post_type( 'krk_news',
        array(
            'labels' => array(
                'name' => 'News',
                'singular_name' => 'Add New News',
                'add_new_item' => 'Add News',
                'edit_item' => 'Edit News',
                'new_item' => 'New News',
                'view_item' => 'View News'
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'news'),
            'supports' => array( 'title', 'editor', 'thumbnail')
        )
    );

	register_post_type( 'krk_staff',
        array(
            'labels' => array(
                'name' => 'Staff',
                'singular_name' => 'Staff',
                'add_new_item' => 'Add New Staff',
                'edit_item' => 'Edit Staff',
                'new_item' => 'New Staff',
                'view_item' => 'View Staff'
            ),
            'menu_icon' => 'dashicons-businessman',
            'public' => false,
            'show_ui' => true,
            'has_archive' => false,
            'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
            'hierarchical' => true
        )
    );
    register_post_type( 'krk_testimonial',
        array(
            'labels' => array(
                'name' => 'Testimonials',
                'singular_name' => 'Testimonial',
                'add_new_item' => 'Add New Testimonial',
                'edit_item' => 'Edit Testimonial',
                'new_item' => 'New Testimonial',
                'view_item' => 'View Testimonial'

            ),
            'menu_icon' => 'dashicons-megaphone',
            'public' => false,
            'show_ui' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'testimonials'),
            'supports' => array('title','editor')
        )
    );
     register_post_type( 'krk_blog',
        array(
            'labels' => array(
                'name' => 'Blog',
                'singular_name' => 'Add New Blog',
                'add_new_item' => 'Add Blog',
                'edit_item' => 'Edit Blog',
                'new_item' => 'New Blog',
                'view_item' => 'View Blog'
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'blog'),
            'supports' => array( 'title', 'editor', 'thumbnail')
        )
    );
}
	add_action('init', 'krkcorporate_register_post_types');

/**
 * Set max number of posts to paginated custom post types
 */

function krkcorporate_custom_type_archive_display($query) {
    $max_num = 4;
    if (!is_admin() && (is_post_type_archive('krk_news') ||
                        is_post_type_archive('krk_blog')
        )) {

        $query->set('posts_per_page', $max_num);
        return;
    }
}

add_action('pre_get_posts', 'krkcorporate_custom_type_archive_display');


?>