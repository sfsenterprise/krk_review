<?php

require_once('functions/init-functions.php');
require_once('functions/event-functions.php');
require_once('functions/staff-functions.php');
require_once('functions/testimonial-functions.php');
require_once('functions/customizer-functions.php');


if ( ! function_exists( 'multisite_path' ) ) {
    /**
     * Returns the path within the current multisite that will provide access
     * to the given non-multisite link.
     *
     * Example: /infants would return /currentmultisite/infants, where
     * currentmultisite is the path of the currently active multisite.
     */
    function multisite_path($page) {
        if ($page[0] == '/') {
            $page = substr($page, 1);
        }

        if(is_main_site()) {
            echo site_url() . '/' . $page;
        }

        else {
            global $current_blog;
            echo $current_blog->path . $page;
        }
    }
}

if ( ! function_exists( 'get_multisite_path' ) ) {
	/**
	 * Returns the path within the current multisite that will provide access
	 * to the given non-multisite link.
	 *
	 * Example: /infants would return /currentmultisite/infants, where
	 * currentmultisite is the path of the currently active multisite.
	 */
	function get_multisite_path($page) {
		if ($page[0] == '/') {
			$page = substr($page, 1);
		}

		if(is_main_site()) {
			return site_url() . '/' . $page;
		}

		else {
			global $current_blog;
			return $current_blog->path . $page;
		}
	}
}

if ( ! function_exists( 'krkcorporate_setup' ) ) :

function krkcorporate_setup() {

	
	load_theme_textdomain( 'krkcorporate', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	
	add_theme_support( 'title-tag' );

	
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );


	
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	
	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', krkcorporate_fonts_url() ) );
}
endif; // krkcorporate_setup
add_action( 'after_setup_theme', 'krkcorporate_setup' );

function media_to_editor($html, $send_id, $attachment ){
	if(wp_attachment_is_image($send_id)){
		return $html;
	}
	$attachment_url = wp_get_attachment_url($send_id);
	return $attachment_url;
}

add_filter('media_send_to_editor', 'media_to_editor', 10, 10);

if ( ! function_exists( 'krkcorporate_fonts_url' ) ) :

 
function krkcorporate_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Sans, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'krkcorporate' ) ) {
		$fonts[] = 'Noto Sans:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Serif, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'krkcorporate' ) ) {
		$fonts[] = 'Noto Serif:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Inconsolata, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'krkcorporate' ) ) {
		$fonts[] = 'Inconsolata:400,700';
	}

	/*
	 * Translators: To add an additional character subset specific to your language,
	 * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
	 */
	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'krkcorporate' );

	if ( 'cyrillic' == $subset ) {
		$subsets .= ',cyrillic,cyrillic-ext';
	} elseif ( 'greek' == $subset ) {
		$subsets .= ',greek,greek-ext';
	} elseif ( 'devanagari' == $subset ) {
		$subsets .= ',devanagari';
	} elseif ( 'vietnamese' == $subset ) {
		$subsets .= ',vietnamese';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Fifteen 1.1
 */
function krkcorporate_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'krkcorporate_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Fifteen 1.0
 */
function krkcorporate_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'krkcorporate-fonts', krkcorporate_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our main stylesheet.
	wp_enqueue_style( 'bootstrap-extended-style', get_template_directory_uri() . '/css/bootstrap-extended.css', array());
	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.css', array());
    wp_enqueue_style( 'featherlight-style', get_template_directory_uri() . '/css/featherlight.css', array());
    wp_enqueue_style( 'krkcorporate-style', get_stylesheet_uri() );


	wp_enqueue_script( 'krkcorporate-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '20141010', true );
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-1.11.2.min.js', array(), '20141010', true );
	wp_enqueue_script( 'jquerydotdotdot', get_template_directory_uri() . '/js/jquery.dotdotdot.js', array(), '20141010', true );
	wp_enqueue_script( 'featherlight-js', get_template_directory_uri() . '/js/featherlight.js', array());
	wp_enqueue_script( 'markerwithlabel', get_template_directory_uri() . '/js/markerwithlabel.js', array());
    wp_enqueue_script( 'locationSearch', get_template_directory_uri() . '/js/locationSearch.js', array());
	wp_enqueue_script( 'form-validator', get_template_directory_uri() . '/js/form-validator.js', array());
	wp_enqueue_script( 'form-submitter', get_template_directory_uri() . '/js/form-submitter.js', array());
	wp_enqueue_script( 'functions', get_template_directory_uri() . '/js/functions.js', array());

}
add_action( 'wp_enqueue_scripts', 'krkcorporate_scripts' );

function krkcorporate_admin_scripts() {
   wp_enqueue_script( 'jquery-ui-datepicker' );
   wp_enqueue_style( 'datepicker-style', get_template_directory_uri() . '/css/datepicker.css', array());
   wp_enqueue_script( 'myDatepicker', get_template_directory_uri() . '/js/myDatepicker.js', array());
}
add_action('admin_init', 'krkcorporate_admin_scripts');


function krkcorporate_customizer_scripts() {
	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our stylesheets.
	wp_enqueue_style( 'bootstrap-extended-style', get_template_directory_uri() . '/css/bootstrap-extended.css', array());
	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.css', array());
	wp_enqueue_style( 'bootstrap-wyishtml5-style', get_template_directory_uri() . '/css/bootstrap3-wysihtml5.min.css', array());
	wp_enqueue_style( 'theme-customizer-style', get_template_directory_uri() . '/css/theme-customizer.css', array());

	//Load our js
	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.css', array());
	wp_enqueue_script( 'jquery-main', get_template_directory_uri() . '/js/jquery.main.js', array());
	wp_enqueue_script( 'jquerydotdotdot', get_template_directory_uri() . '/js/jquery.dotdotdot.js', array());
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array());
	wp_enqueue_script( 'bootstrap-wysihtml5', get_template_directory_uri() . '/js/bootstrap3-wysihtml5.all.min.js', array('jquery'));
	wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js', array('jquery'));
	wp_enqueue_script( 'theme-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array('jquery', 'bootstrap-wysihtml5' ,'customize-preview', 'jquery-ui'));

}
add_action( 'customize_controls_enqueue_scripts', 'krkcorporate_customizer_scripts' );

?>