<?php
get_header();
?>

<script>

</script>


<main id="main">
	<div class="main-holder">
		<div class="breadcrumbs-wrap">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="title-page title">
							<h1>LOCATIONS</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<form class="form-info" id="locationSearch">
			<fieldset>
				<strong class="title">Find a school nearest to you:</strong>
				<div class="hold">
					<div class="form-group">
						<input type="text" class="form-control" id="input-zipcode" placeholder="Zip Code">
					</div>
					<button type="submit" class="btn btn-default">go</button>
				</div>
				<span class="txt"><a href="<?php multisite_path('all-locations') ?>">See All Locations</a></span>
			</fieldset>
		</form>
		<div id="twocolumns" class="battlement">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-6">
						<div class="box-map" id="map">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="block-contact">
							<div class="contact-info jcf-scrollable">
								<div class="box-hold" id="location-results">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php
get_footer();
?>