<?php
get_header();
?>


<main id="main">
			<div id="content">
				<div class="main-holder">
					<section class="section-404">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="block-information">
										<strong class="big-title">Oops!</strong>
										<span class="info">We can't seem to find the page you're looking for.</span>
										<span class="error">Error Code: 404</span>
										<span class="txt">Here are some helpful links:</span>
										<ul>
											<li><a href="/">Home</a></li>
											<li><a href="#">Our School</a></li>
											<li><a href="/curriculum">Our Curriculum</a></li>
											<li><a href="#">Our Stories</a></li>
											<li><a href="#">Schedule a Tour</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</section>	
				</div>
			</div>
		</main>



<?php
get_footer();
?>