<?php
get_header();
$customizer = new KRK_Prekindergarten_Customizer();
$pre_k_program_name = do_shortcode("[pre_k_program_name]");
?>

<main id="main">
	<div class="main-holder">
		<div class="breadcrumbs-wrap">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="title-page title">
							<h1><?php echo $pre_k_program_name; ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="video-container">
			<div class="video-img-wrap">
				<?php $thumbnail = $customizer->get_setting('krk_prekindergarten_video_thumbnail'); ?>
				<img src="<?php echo $thumbnail; ?>" height="509" width="680" alt="image description">
				<a href="#" class="btn-play">
					<span class="icon icon-play"></span>
				</a>
			</div>
			<div class="video-text-wrap">
				<?php $title = $customizer->get_setting('krk_prekindergarten_header_title'); ?>
				<strong class="slogan"><?php echo $title; ?></strong>
				<?php $info = $customizer->get_setting('krk_prekindergarten_header_content'); ?>
				<span class="info"><b><?php echo $info; ?></b></span>
				<a href="<?php multisite_path('locations') ?>" class="btn btn-primary">See Us in Action <span class="icon icon-arrow-right"></span></a>
			</div>
		</div>
		<div class="two-columns battlement">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-7">
						<div class="col-first">
							<?php $left_content = $customizer->get_setting('krk_prekindergarten_left_content'); ?>
							<?php echo $left_content; ?>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="col-second">
							<?php $right_content = $customizer->get_setting('krk_prekindergarten_right_content'); ?>
							<?php echo $right_content; ?>
							<div class="logo-hold">
								<picture class="program">
									<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/fast-track.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/fast-track-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/fast-track-3x.jpg 3x">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/fast-track.jpg" height="137" width="256" alt="image description">
								</picture>
							</div
						</div>
					</div>
				</div>
			</div>
		</div>
		<a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
	</div>

<?php

get_footer();

?>