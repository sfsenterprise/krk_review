<?php
get_header();
$customizer = new KRK_Infants_Customizer(); ?>
	<main id="main">
	<div class="main-holder">
		<div class="breadcrumbs-wrap">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="title-page title">
							<h1>INFANTS</h1>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="video-container">
			<div class="video-img-wrap">
				<?php $video_thumbnail = $customizer->get_setting( 'krk_infants_video_thumbnail'); ?>
				<img src="<?php echo bloginfo('stylesheet_directory') . '/images/' . $video_thumbnail ?>" height="509" width="680" alt="image description">
				<a href="#"
				   data-featherlight=
				   '
						<?php
				   $custom_video = $customizer->get_setting('krk_infants_video_upload');
				   if(strlen($custom_video) > 0) :
					   ?>
							<video width="100%" height="auto" controls autoplay>
								<source src="<?php echo wp_get_attachment_url($custom_video); ?>" type="<?php echo get_post_mime_type($custom_video); ?>">
								</video>
						<?php else : ?>
							<iframe width="800" height="450" src="https://www.youtube.com/embed/7RkjVSYbWyY?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>
						<?php endif; ?>
						'
				   class="btn-play">
					<span class="icon icon-play"></span>
				</a>
			</div>
			<?php $header_image = $customizer->get_setting('krk_infants_header_image');?>
			<div class="video-text-wrap" <?php echo strlen($header_image) > 0 ? 'style="padding: 0px;"' : ''; ?>>
				<?php if(strlen($header_image) > 0) : ?>
					<img src="<?php echo $header_image ?>" height="509" width="680" alt="image description">
				<?php else : ?>
					<?php $header_title = $customizer->get_setting('krk_infants_header_title'); ?>
					<strong class="slogan"><?php echo $header_title; ?></strong>

					<?php $header_content = $customizer->get_setting('krk_infants_header_content'); ?>
					<span class="info"><?php echo $header_content; ?></span>

					<a href="<?php multisite_path('/locations'); ?>" class="btn btn-primary">See Us in Action <span class="icon icon-arrow-right"></span></a>

				<?php endif; ?>
			</div>
		</div>

		<div class="two-columns battlement">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-7">
						<div class="col-first" data-customizable="true">
							<?php $section_content = $customizer->get_setting('krk_infants_left_content'); ?>
							<div class="section-xtx"><?php echo $section_content ?></div>
						</div>
					</div>
					<div class="col-sm-5" data-customizable="true">
						<?php $section_content = $customizer->get_setting('krk_infants_right_content'); ?>
						<div class="section-xtx"><?php echo $section_content ?></div>

						<div class="logo-hold">
							<img class="program" src="<?php echo bloginfo('stylesheet_directory') . '/images/BigStepLogoinfant.png' ?>" height="137" width="256" alt="image description">
						</div>
					</div>
				</div>
			</div>
		</div>

		<a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
	</div>
<?php get_footer(); ?>