<?php
get_header();
?>


<main id="main">
	<div id="content">
		<div class="main-holder">
			<section class="section-thankyou">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="block-information">
								<strong class="big-title">Thank you!</strong>
								<span class="info">Thank you for considering Kids 'R' Kids Learning Academies for your child's first preschool experience.</span>
								<span class="txt">We understand choosing a preschool for your family can be a difficult choice. We appreciate the research it takes to find a quality preschool which offers nurturing care along with excellent early education. Our teachers are educated in the latest techniques for helping children and their families transition to the preschool environment. Should you have any further questions we can answer to help you make this very important decision, do not hesitate to contact us.</span>
							</div>
						</div>
					</div>
				</div>
				<picture>
					<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/bg-08.jpg, <?php bloginfo('stylesheet_directory'); ?>/images/bg-08-2x.jpg 2x, <?php bloginfo('stylesheet_directory'); ?>/images/bg-08-3x.jpg 3x">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/bg-08.jpg" height="1064" width="1200" alt="image description">
				</picture>
			</section>
		</div>
	</div>

<?php
get_footer();
?>