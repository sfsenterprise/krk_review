jQuery(document).ready(function(){

    urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results != null ? results[1] : null;
    };

    if(jQuery('#locationSearch').length) {
        var markers = [];
        var DEFAULT_ZIPCODE = 30018;
        var DEFAULT_LAT = 33.713301;
        var DEFAULT_LONG = -83.798551;
        var map = '';

        geoLocate = function() {
            navigator.geolocation.getCurrentPosition(
                function(position){
                    map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 12,
                        center: {lat: position.coords.latitude, lng: position.coords.longitude}
                    });
                    var zipcode = getZipcode(position, function(zipcode){
                        DEFAULT_ZIPCODE = zipcode;
                        getLocations(zipcode);
                        map.setZoom(map.getZoom() +1);
                    });
                },
                function (error) {
                    map = map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 12,
                        center: {lat: 33.7550, lng: -84.3900}
                    });
                    getLocations(DEFAULT_ZIPCODE);
                    map.setZoom(map.getZoom() +1);
                });
        };

        pinSymbol = function(color) {
          return {
            path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#000',
            strokeWeight: 2,
            scale: 2
          };
        }

        clearMapMarkers = function () {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            markers = [];
        };

        addMapMarker = function (location, label) {
            var myLatLng = {lat: parseFloat(location['lat']), lng: parseFloat(location['lng'])};
            var marker = new MarkerWithLabel({
                position: myLatLng,
                map: map,
                draggable: false,
                raiseOnDrag: false,
                labelContent: label + "",
                labelAnchor: new google.maps.Point(15, 75),
                labelClass: "labels", // the CSS class for the label
                labelInBackground: false,
                icon: pinSymbol('#06467a')
              });
            var iw = new google.maps.InfoWindow({
                content: '<strong class="title" style="color: #fb8945; font-size: 16px;">' + location['location_name'] + '</strong>' +
                                '<address>' +
                                '<div>' + location['street_address'] + '</div>' +
                                '<div>' + location['city'] + ', ' + location['state'] + ' ' + location['zip'] + '</div>' +
                                '<div><a href="tel:' + location['phone'] + '">' + location['phone'] + '</a></div>' +
                                '</address>' +
                                //'<div>M-F 6:30AM - 6:30PM</div>' +
                                '<a href="' + location['site_url'] + '">Visit Site</a> | ' +
                                '<a href="mailto:' + location['email'] + '">Contact</a>'
            });
            google.maps.event.addListener(marker, "click", function(e) {
                iw.open(map, this);
            });
            
            markers.push(marker);
        };

        centerMap = function () {
            var latlngbounds = new google.maps.LatLngBounds();
            markers.forEach(function (marker) {
                latlngbounds.extend(marker.getPosition());
            });
            map.setCenter(latlngbounds.getCenter());
            map.fitBounds(latlngbounds);
        };

        getZipcode = function(position, callback){
            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            geocoder.geocode({'latLng': latlng}, function(results, status)
            {
                console.log(results);
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        for (var i = 0; i < results[0].address_components.length; i++) {
                            var types = results[0].address_components[i].types;

                            for (var typeIdx = 0; typeIdx < types.length; typeIdx++) {
                                if (types[typeIdx] == 'postal_code') {
                                    callback(results[0].address_components[i].short_name);
                                }
                            }
                        }
                    }
                }
                else{
                    callback(DEFAULT_ZIPCODE);
                }
            });
        };

        getLocations = function(zipcode){
            if(!zipcode){
                zipcode = DEFAULT_ZIPCODE;
            }
            jQuery.post(
                ajaxurl, {
                    'action': 'location_search_json',
                    'search-location': zipcode,
                    'search-distance': 20,
                    'action_type': 'find'
                },
                function (response) {
                    clearMapMarkers();
                    //alert(JSON.stringify(response));

                    if (response['status'] == 'success') {
                        var locationResults = jQuery('#location-results');
                        locationResults.empty();

                        if (response['message'].length == 0) {
                            var div = document.createElement('div');
                            div.className = 'box';
                            div.innerHTML =
                                '<div class="text">' +
                                '<strong class="title">No Locations found near ' + zipcode + '</strong>' +
                                '</div>';
                            locationResults.append(div);
                        }
                        else {
                            var i = 0;
                            response['message'].forEach(function (location) {
                                i++;
                                var div = document.createElement('div');
                                div.className = 'box';
                                div.innerHTML = '<span class="number">' + i + '.</span>' +
                                '<div class="text">' +
                                '<strong class="title">' + location['location_name'] + '</strong>' +
                                '<address>' +
                                '<span>' + location['street_address'] + '</span>' +
                                '<span>' + location['city'] + ', ' + location['state'] + ' ' + location['zip'] + '</span>' +
                                '<span><a href="tel:' + location['phone'] + '">' + location['phone'] + '</a></span>' +
                                '</address>' +
                                '<ul>' +
                                '<li><a href="' + location['site_url'] + '">Visit Site</a></li>' +
                                '<li><a href="mailto:' + location['email'] + '">Contact</a></li>' +
                                '</ul>' +
                                '</div>';
                                locationResults.append(div);
                                addMapMarker(location, i);
                            });
                            centerMap();
                        }
                    }
                }
            );
        };

        jQuery('#locationSearch').submit(function () {
            var zipcode = jQuery('#input-zipcode').val();
            getLocations(zipcode);
            return false;
        });

         init = function() {
            var query = urlParam("zipcode");
            if(query==null) {
                geoLocate();
            }
            else {
                map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 12,
                        center: {lat: DEFAULT_LAT, lng: DEFAULT_LONG}
                });
                jQuery("#input-zipcode").val(query);
                getLocations(query);
            }
            
        };
        init();
    }

});