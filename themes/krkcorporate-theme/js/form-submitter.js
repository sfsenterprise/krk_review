/**
 * Created by dustin on 1/1/17.
 */

(function (formSubmitter, formValidator, $, undefined) {

    var isSubmitting = false;

    var crmRequst = function (form) {
        if(!isSubmitting) {
            isSubmitting = true;

            jQuery.ajax({
                url: 'https://live.childcarecrm.com/import/webImportReceiver.php',
                method: 'POST',
                data: form.serialize(),
                success: function (data) {
                    isSubmitting =  false;
                    if (data && data['status'] === 'success') {
                        formValidator.showSuccessMsg();
                        var fadeForm = form.data('fade');
                        if (fadeForm) {
                            form.fadeOut();
                        }
                    } else {
                        formValidator.showErrorMsg();
                    }
                },
                error: function () {
                    isSubmitting = false;
                    formValidator.showErrorMsg();
                }
            });
        }
    };

    var ajaxStandardRequest = function (form) {
        if(!isSubmitting) {
            isSubmitting = true;

            jQuery.ajax({
                url: ajaxurl,
                method: 'POST',
                data: form.serialize(),
                success: function (data) {
                    isSubmitting = false;
                    if (data && data['status'] === 'success') {
                        formValidator.showSuccessMsg(form);

                        var fadeForm = form.data('fade');
                        if (fadeForm) {
                            form.fadeOut();
                        }
                    } else {
                        formValidator.getErrorMsg(form).html('Uh oh! Something went wrong trying to send your message. Please try again later.');
                        formValidator.showErrorMsg(form);
                    }
                },
                error: function () {
                    isSubmitting = false;
                    formValidator.getErrorMsg(form).html('Uh oh! Something went wrong trying to send your message. Please try again later.');
                    formValidator.showErrorMsg(form);
                }
            });
        }
    };

    var isCrmRequest = function (form) {
        var crmKeyInput = form.find('input[name="hash"]');
        var formId = form.attr('id');

        return formId == 'schedule-tour-form' && crmKeyInput.length !== 0;
    };

    $(function () {
        $('form[data-type="ajax"]').on('submit', function (e) {
            e.preventDefault();

            var form = $(this);
            var isValid = formValidator.validate(form);

            if (isValid) {
                if(isCrmRequest(form)) {
                    crmRequst(form);
                } else{
                  ajaxStandardRequest(form);
                }
            } else {
                formValidator.showErrorMsg(form);
                return;
            }
        });
    });
})(window.formSubmitter = window.formSubmitter || {}, window.formValidator, jQuery);