<?php

get_header();

require_once(get_template_directory() . '/functions/customizers/defaults/schoolage-defaults.php');
?>

<main id="main">
	<div class="main-holder">
		<div class="breadcrumbs-wrap">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="title-page title">
							<h1>SCHOOL AGE</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="video-container">
			<div class="video-img-wrap">
				<?php $video_thumbnail = get_theme_mod( 'krk_schoolage_video_thumbnail', '' );
				
				if(strlen($video_thumbnail) <= 0) :
					$video_thumbnail = krk_schoolage_customizer_defaults('krk_schoolage_video_thumbnail');
				endif; ?>
				
				<img src="<?php echo get_template_directory_uri() . '/images/' .$video_thumbnail ?>" height="509" width="680" alt="image description">

				<?php $custom_video = get_theme_mod( 'krk_preschool_video_upload', '' ); ?>
				<a href="#"
						data-featherlight='
							<?php if(strlen($custom_video) > 0) : ?>
								<video width="100%" height="auto" controls autoplay>
									<source src="<?php echo wp_get_attachment_url($custom_video); ?>" type="<?php echo get_post_mime_type($custom_video); ?>">
								</video>
							<?php else: ?>
								<iframe width="560" height="315" src="https://www.youtube.com/embed/dL2sDRfWgaA?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>
							<?php endif; ?>
						'
					   	class="btn-play">
						<span class="icon icon-play"></span></a>
			</div>
			<?php $header_image = get_theme_mod( 'krk_schoolage_header_image' , ''); ?>
			
			<div class="video-text-wrap" <?php echo strlen($header_image) > 0 ? 'style="padding: 0px;"' : ''; ?>>
			
				<?php if(strlen($header_image) > 0) : ?>
						
					<img src="<?php echo $header_image ?>" height="509" width="680" alt="image description">
				
				<?php else : ?>
					<?php $header_title = get_theme_mod( 'krk_schoolage_header_title', '' );
				
					if(strlen($header_title) <= 0) :
						$header_title = krk_schoolage_customizer_defaults('krk_schoolage_header_title');
					endif; ?>
				
					<strong class="slogan"><?php echo $header_title; ?></strong>
				
					<?php $header_content = get_theme_mod( 'krk_schoolage_header_content', '' );
				
					if(strlen($header_content) <= 0) : 
						$header_content = krk_schoolage_customizer_defaults('krk_schoolage_header_content');
					endif; ?>
				
					<span class="info"><?php echo $header_content; ?></span>
							
					<a href="<?php multisite_path('/schedule-tour'); ?>" class="btn btn-primary">See Us in Action <span class="icon icon-arrow-right"></span></a>
				<?php endif; ?>
			</div>
		</div>

		<div class="two-columns battlement">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-7" data-customizable="true">
						<div class="col-first">
							<?php $section_content = get_theme_mod( 'krk_schoolage_left_content', '');
							
							if(strlen($section_content) <= 0 ) : 
								$section_content = krk_schoolage_customizer_defaults('krk_schoolage_left_content');
							endif; ?>
							
							<div class="section-xtx"><?php echo $section_content ?></div>
						</div>
					</div>
					<div class="col-sm-5" data-customizable="true">
						<div class="col-second">
							<?php $section_content = get_theme_mod( 'krk_schoolage_right_content', '');
							
							if(strlen($section_content) <= 0 ) :
								$section_content = krk_schoolage_customizer_defaults('krk_schoolage_right_content');
							endif;?>
							
							<div class="section-xtx"><?php echo $section_content ?></div>
							
							<div class="logo-hold">
								<a href="<?php multisite_path('#'); ?>">
									<picture>
										<source srcset="<?php bloginfo('stylesheet_directory'); ?>/images/GymLogo.png, <?php bloginfo('stylesheet_directory'); ?>/images/GymLogo.png 2x, <?php bloginfo('stylesheet_directory'); ?>/images/GymLogo.png 3x">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/GymLogo.png" height="137" width="256" alt="image description">
									</picture>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a href="#wrapper" class="back-to-top"><span class="ico icon-hand"></span> Top</a>
	</div>
	
<?php

get_footer();

?>